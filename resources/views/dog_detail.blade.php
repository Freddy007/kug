@extends('layouts.index3_layout')

{{--@section('scripts')--}}

{{--    <script>--}}
{{--       $('.edit-column').hide();--}}
{{--    </script>--}}

{{--@endsection--}}

@section('content')

    <!-- Page Heading -->
    <section class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1>Dog detail</h1>
                </div>
                <div class="col-md-6">
                    <ul class="breadcrumb">
                        <li><a href="/">Home</a></li>
                        <li class="active">Dog detail </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- Page Heading / End -->

    <!-- Page Content -->
    <section class="page-content">
        <div class="container">

            <div class="row">
                <div class="col-md-5">
                    <figure class="alignnone">
                        <img src="{{$dog->image_name == null ? url('https://placehold.it/500x500/cccccc/000000?text=no+image+uploaded'): url('https://placehold.it/300x200/cccccc/000000?text=no+image+uploaded')}}" alt="">
                    </figure>
                </div>
                <div class="col-md-7">
                    <div class="spacer visible-sm visible-xs"></div>
                    <div class="team-single-head">
                        <h2>{{$dog->name}} <small>/ {{$dog->breeder_name}}</small></h2>
                    </div>

                    <h5>Details</h5>

                    <dl class="dl-horizontal">
                        <dt>Registration Number</dt>
                        <dd><a href="#">{{$dog->registration_number}}</a></dd>
                        <dt>Microchip Number</dt>
                        <dd><a href="#">{{$dog->microchip_number}}</a></dd>
                        <dt>Tattoo Number</dt>
                        <dd><a href="#">{{$dog->tattoo_number}}</a></dd>
                        <dt>DNA ID</dt>
                        <dd><a href="#">{{$dog->dna_id}}</a></dd>
                        <dt>Sex</dt>
                        <dd><a href="#">{{$dog->sex == "male" ? "Sire" : "Dam"}}</a></dd>
                        <dt>Colour</dt>
                        <dd><a href="#">{{$dog->colour}}</a></dd>
                        <dt>Height</dt>
                        <dd><a href="#">{{$dog->height}}</a></dd>

                        <dt>Coat</dt>
                        <dd><a href="#">{{$dog->coat}}</a></dd>

                        <dt>Breed</dt>
                        <dd><a href="#">{{$dog->breeder_name}}</a></dd>
                        <dt>Kennel Name / Owner</dt>
                        <dd><a href="#">{{$dog->kennel_name ?: $dog->member}}</a></dd>
                    </dl>
                </div>
            </div>

        </div>

    </section>

    <section>

        <h1 class="text-center">
            <a class="btn btn-success btn-lg" role="button" href="/main-table?id={{$dog->id}}" target="_blank">
                Pedigree Info
            </a>

        </h1>

{{--        @include('partials.table_minimal')--}}

    </section>

    <!-- Footer -->
    <footer class="footer" id="footer">

        <div class="footer-copyright">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        Copyright &copy; 2020 <a href="/">Kennel Union of Ghana</a> &nbsp;| &nbsp;All Rights Reserved
                    </div>
                    <div class="col-sm-6 col-md-8">
                        <div class="social-links-wrapper">
                            <span class="social-links-txt">Keep in Touch</span>
                            <ul class="social-links social-links__light">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fa fa-rss"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer / End -->

@endsection