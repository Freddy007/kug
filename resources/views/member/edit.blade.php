@extends('layouts.admin_master')

@section('scripts')
    <script src="{{asset('../../bower_components/jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('../../bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/js/select2.min.js"></script>
    <script>
        
    </script>

@stop

@section('content')

    <form action="{{url('/member/update',$dog->id)}}" class="form-horizontal" method="post">
        {!! csrf_field() !!}
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">Registration Number</label>
            <div class="col-sm-5">
                <input type="text" class="form-control"  id="registration_number" placeholder="Name" value="{{$dog->registration_number}}" readonly>
            </div>
        </div>

        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">Registration Date</label>
            <div class="col-sm-5">
                <input type="text" class="form-control" id="registration_date" value="{{$dog->created_at}}" readonly>
            </div>
        </div>


        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">Name</label>
            <div class="col-sm-5">
                <input type="text" class="form-control" name="name" id="inputEmail3" placeholder="Name" value="{{$dog->name}}">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Father's Registration Number</label>
            <div class="col-sm-5">
                <input type="text" name="father" class="form-control" value="{{$dog->father}}">
                {{--<select class="dog_parent form-control"  id="father" name="father"  >--}}
                {{--<option value="{{$dog->father}}">{{$dog->father}}</option>--}}
                {{--</select>--}}
            </div>
            {{--<input  maxlength="100" type="text" class="form-control father" id="father" name="father" placeholder="Enter Dog's Father"  />--}}
        </div>

        <div class="form-group">
            <label class="control-label col-sm-2"> Mother's Registration Number</label>
            <div class="col-sm-5">
                <input type="text" name="mother" class="form-control" value="{{$dog->mother}}">
                {{--<select class="dog_parent form-control" name="mother" id="mother" data-gender="female">--}}
                {{--<option value=""></option>--}}
                {{--</select>--}}
            </div>
            {{--<input  maxlength="100" type="text" class="form-control" id="mother" name="mother" placeholder="Enter Dog's Mother"  />--}}
        </div>

        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">Date of Birth</label>
            <div class="col-sm-5">
                <input type="text" class="form-control" name="dob" id="dob" placeholder="Name" value="{{$dog->dob}}">
            </div>
        </div>

        <div class="form-group">
            <label for="breeder" class="col-sm-2 control-label">Breeder</label>
            <div class="col-sm-5">
                <select class="form-control" name="breeder_id">
                    <option value="{{$dog->breeder_id}}">{{$dog->breeder_name}}</option>
                    @foreach($breeders as $breeder)
                        <option value="{{$breeder->id}}">{{$breeder->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="sex" class="col-sm-2 control-label">Sex</label>
            <div class="col-sm-5">
                <select class="form-control" name="sex">
                    @if($dog->sex == 'female')
                        <option>{{$dog->sex}}</option>
                        <option value="male">Male</option>
                    @else
                        <option>{{$dog->sex}}</option>
                        <option value="female">Female</option>
                    @endif
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">Tattoo Number</label>
            <div class="col-sm-5">
                <input type="text" class="form-control" name="tattoo_number" id="tattoo_number" placeholder="Tattoo Number" value="{{$dog->tattoo_number}}">
            </div>
        </div>

        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">Microchip Number</label>
            <div class="col-sm-5">
                <input type="text" class="form-control" name="microchip_number" id="microchip_number" placeholder="Microchip Number" value="{{$dog->microchip_number}}">
            </div>
        </div>

        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">Colour</label>
            <div class="col-sm-5">
                <input type="text" class="form-control" name="colour" id="colour" placeholder="Colour" value="{{$dog->colour}}">
            </div>
        </div>

        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">Height</label>
            <div class="col-sm-5">
                <input type="text" class="form-control" name="height" id="height" placeholder="Height" value="{{$dog->height}}">
            </div>
        </div>

        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">DNA ID</label>
            <div class="col-sm-5">
                <input type="text" class="form-control" name="DNA" id="dna" placeholder="Name" value="{{$dog->DNA}}">
            </div>
        </div>

        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">Titles</label>
            <div class="col-sm-5">
                <input type="text" class="form-control" name="titles" id="titles" placeholder="Titles" value="{{$dog->titles}}">
            </div>
        </div>


        <div class="form-group">
            <label for="dead" class="col-sm-2 control-label">Status</label>
            <div class="col-sm-5">
                <select class="form-control" name="dead">
                    @if($dog->dead == false)
                        <option>{{'Alive'}}</option>
                        <option value="true">Dead</option>
                    @else
                        <option>{{'Dead'}}</option>
                        <option value="false">Alive</option>
                    @endif
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-success">Update Dog Info</button>
            </div>
        </div>
    </form>
@stop