@extends('layouts.admin_master')

@section('scripts')
    <script src="{{asset('../../bower_components/jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('../../bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

    <script>
        jQuery(document).ready(function(){
            @foreach($dogs as $dog)
           $('#delete-<?php echo $dog->id?>').off('click').on('click',function(){
                        $('#delete-modal-<?php echo $dog->id ?>').modal();
                    })
            $('#restore-<?php echo $dog->id?>').off('click').on('click',function(){
                $('#restore-modal-<?php echo $dog->id ?>').modal();
            })
            @endforeach

//               $('#add-new-dog').off('click').on('click',function(){
//                        $('#register-dog-modal').modal();
//                    });

            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        });

    </script>

@stop

@section('content')

    <div class="content">

        <div class="container">
            {!! Breadcrumbs::render('all-dogs') !!}

            @include('flash::message')
            <h2 class="">{{\App\Dog::onlyTrashed()->count()}} Dog(s) in Trash</h2>

            {{--<h2 class="pull-right">Hello </h2>--}}

            <br>

            <div class="row" style="margin-bottom: 45px;">
                <form  action="{{url('/admin/trash')}}" method="get">
                    {{csrf_field()}}
                    <div class="col-md-7">
                        <input type="text" name="term" class="form-control input-lg  " placeholder="Dog by  Name , Color , Tattoo Number , microchip Number " value="{{Request::get('term')}}">

                    </div>

                    <div class="col-md-2">
                        <input type="submit" class="form-control input-lg btn btn-success" value="Search">
                    </div>

                </form>

                <div class="col-md-2" >
                    {{--<input type="submit" class="form-control input-lg btn btn-success btn-sm"  value="ADD NEW DOG">--}}
                </div>
            </div>

            <div class="portlet portlet-default">

                <div class="portlet-header">
                    {{--<h4 class="portlet-title">--}}
                    {{--<u>Pagination</u>--}}
                    {{--</h4>--}}
                </div> <!-- /.portlet-header -->

                @if($dogs->count())
                    <div class="portlet-body">
                        <div class="table-top-menu">
                            <ul>
                                <li class="table-top-pagination"> {!! $dogs->render() !!}</li>
                            </ul>
                        </div>

                        <table
                                class="table table-striped table-bordered table-hover ui-datatable"
                                data-ajax="../../global/js/demos/json/table_data.json"
                                data-global-search="false"
                                data-length-change="false"
                                data-info="true"
                                data-paging="true"
                                data-page-length="10"
                                >
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Name</th>
                                <th>Breeder</th>
                                <th>Sex</th>
                                <th>Registration Number</th>
                                <th>Owner</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>

                            </thead>
                            <tbody>
                            <?php $i = $dogs->firstItem(); ?>
                            @foreach($dogs as $dog)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$dog->name}}</td>
                                    <td>{{$dog->breeder_name}}</td>
                                    <td>{{$dog->sex}}</td>
                                    <td>{{$dog->registration_number}}</td>
                                    <td>{{$dog->owner}}</td>
                                    <td>{{$dog->created_at}}</td>
                                    <td>
                                        </a>

                                        <button class="btn btn-success btn-sm" id="restore-{{$dog->id}}"
                                           data-toggle="tooltip" data-placement="left" title="restore {{$dog->name}}"><i class="fa fa-paw"></i>
                                        </button>

                                        <button id="delete-{{$dog->id}}" class="btn btn-danger btn-sm"
                                           data-toggle="tooltip" data-placement="left" title="permanently Delete {{$dog->name}}"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                                <div class="modal fade" id="restore-modal-{{$dog->id}}" tabindex="-1" role="dialog" aria-labelledby="restore-ModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel"><strong>Restore {{$dog->name}}</strong></h4>
                                            </div>
                                            <div class="modal-body">
                                                <form action="{{url('admin/restore',$dog->id)}}" method="post">
                                                    {!! csrf_field() !!}
                                                    <div class="row">
                                                        <div class="col-sm-3 col-sm-offset-2">
                                                            <label>Are you sure you want to restore {{$dog->name}}?</label>
                                                        </div>
                                                        <div class="col-sm-4">
                                                        </div>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary">Restore {{$dog->name}}</button>
                                                    </div>
                                                </form>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="modal fade" id="delete-modal-{{$dog->id}}" tabindex="-1" role="dialog" aria-labelledby="delete-ModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel"><strong>Permanently Delete {{$dog->name}}</strong></h4>
                                            </div>
                                            <div class="modal-body">
                                                <form action="{{url('admin/permanently-delete',$dog->id)}}" method="post">
                                                    {!! csrf_field() !!}
                                                    <div class="row">
                                                        <div class="col-sm-3 col-sm-offset-2">
                                                            <label>Are you sure to you want to permanently delete ?</label>
                                                        </div>
                                                        <div class="col-sm-4">

                                                        </div>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary">Permanently Delete</button>
                                                    </div>
                                                </form>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                            @endforeach

                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $dogs->render() !!}
                        </div>
                    </div> <!-- /.portlet-body -->
                @else
                    {{'No dog trashed yet !'}}
                @endif

            </div> <!-- /.portlet -->

        </div> <!-- /.container -->

    </div> <!-- .content -->

    <div class="modal fade" id="register-dog-modal" tabindex="-1" role="dialog" aria-labelledby="confirm-ModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><strong>ADD NEW DOG</strong></h4>
                </div>
                <div class="modal-body">
                    <form action="{{url('admin/register-dog')}}" method="post" id="dog-form">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-sm-12">

                                <div class="portlet portlet-boxed portlet-success">

                                    <div class="portlet-header">
                                        <h4 class="portlet-title">
                                            <u><i class="fa fa-check portlet-icon"></i>New Dog</u>
                                        </h4>
                                    </div> <!-- /.portlet-header -->

                                    <div class="portlet-body">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="name">Name</label>
                                                    <input type="text" id="name" name="name" class="form-control" data-parsley-required="true" required minlength="3">
                                                </div> <!-- /.form-group -->

                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="dob">Date of Birth</label>
                                                    <input type="date" id="dob" name="dob" class="form-control" data-parsley-required="true" required >
                                                </div> <!-- /.form-group -->

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="breed">Breed</label>
                                                    <select class="form-control breeders" name="breed">
                                                        <option></option>
                                                        @foreach($breeders as $breeder)
                                                            <option value="{{$breeder->id}}">{{$breeder->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    {{--<input type="text" id="breed" name="breed" class="form-control" data-parsley-required="true" required>--}}
                                                </div> <!-- /.form-group -->

                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="sex">Sex</label>
                                                    <select class="form-control"  id="sex" name="sex" data-parsley-required="true">
                                                        <option value=""></option>
                                                        <option value="male">Male</option>
                                                        <option value="female">Female</option>
                                                    </select>
                                                </div> <!-- /.form-group -->

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="father_reg_no">Father's Registration Number</label>
                                                    <input type="text" id="father_reg_no" name="father" class="form-control" data-parsley-required="true">
                                                    <div id="message_two">

                                                    </div>

                                                </div> <!-- /.form-group -->


                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="mother_reg_no">Mother's Registration Number</label>
                                                    <input type="text" id="mother_reg_no" name="mother" class="form-control" data-parsley-required="true">
                                                </div> <!-- /.form-group -->

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="father_reg_no">Owner</label>
                                                    <select class="form-control" name="user_id">
                                                        <option></option>
                                                        @foreach($users as $user)
                                                            <option value="{{$user->id}}">{{$user->first_name .' '. $user->last_name}}</option>
                                                        @endforeach
                                                    </select>
                                                    <div id="message_two">

                                                    </div>
                                                </div>

                                                {{--<div class="form-group">--}}
                                                {{--<button type="button" id="add-dog" class="btn btn-default">ADD NEW DOG</button>--}}
                                                {{--</div> <!-- /.form-group -->--}}

                                                <div id="message">

                                                </div>

                                            </div> <!-- /.portlet-body -->

                                        </div> <!-- /.portlet -->

                                    </div> <!-- /.col -->

                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" id="add-dog" class="btn btn-success">ADD NEW DOG</button>
                                </div>
                    </form>



                </div>

            </div>
        </div>
    </div>

@stop

