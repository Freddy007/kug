
@extends('layouts.admin_master')

@section('scripts')
    <script src="{{asset('../../bower_components/jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('../../bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

    <script>

    </script>

@stop

@section('content')
<div class="content">

    <div class="container">

        <div class="layout layout-main-right layout-stack-sm">

            <div class="col-md-3 col-sm-4 layout-sidebar">

                <div class="nav-layout-sidebar-skip">
                    <strong>Tab Navigation</strong> / <a href="#settings-content">Skip to Content</a>
                </div>

                <ul id="myTab" class="nav nav-layout-sidebar nav-stacked">
                    <li class="active">
                        <a href="#profile-tab" data-toggle="tab">
                            <i class="fa fa-user"></i>
                            &nbsp;&nbsp;Profile Settings
                        </a>
                    </li>

                    <li>
                        <a href="#password-tab" data-toggle="tab">
                            <i class="fa fa-lock"></i>
                            &nbsp;&nbsp;Change Password
                        </a>
                    </li>

                    @can('administer')

                    <li>
                        <a href="#messaging" data-toggle="tab">
                            <i class="fa fa-bullhorn"></i>
                            &nbsp;&nbsp;Notifications
                        </a>
                    </li>

                    @endcan

                </ul>

            </div> <!-- /.col -->



            <div class="col-md-9 col-sm-8 layout-main">

                <div id="settings-content" class="tab-content stacked-content">

                    <div class="tab-pane fade in active" id="profile-tab">

                        <div class="heading-block">
                            <h3>
                                Edit Profile
                            </h3>
                        </div> <!-- /.heading-block -->

                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>

                        <br><br>

                        <form action="{{url('/settings')}}" method="post" class="form-horizontal">

                            <div class="form-group">

                                <label class="col-md-3 control-label">Avatar</label>

                                <div class="col-md-7">

                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="min-width: 125px; min-height: 125px;">
                                            <img src="../../global/img/avatars/avatar-3-md.jpg" alt="Avatar">
                                        </div>
                                        <div>
                                            <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="..."></span>
                                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div> <!-- /.fileupload -->

                                </div> <!-- /.col -->

                            </div> <!-- /.form-group -->



                            <div class="form-group">

                                <label class="col-md-3 control-label">Username</label>

                                <div class="col-md-7">
                                    <input type="text" name="user-name" value="jumpstartui" class="form-control" disabled />
                                </div> <!-- /.col -->

                            </div> <!-- /.form-group -->



                            <div class="form-group">

                                <label class="col-md-3 control-label">First Name</label>

                                <div class="col-md-7">
                                    <input type="text" name="first-name" value="Rod" class="form-control" />
                                </div> <!-- /.col -->

                            </div> <!-- /.form-group -->



                            <div class="form-group">

                                <label class="col-md-3 control-label">Last Name</label>

                                <div class="col-md-7">
                                    <input type="text" name="last-name" value="Howard" class="form-control" />
                                </div> <!-- /.col -->

                            </div> <!-- /.form-group -->



                            <div class="form-group">

                                <label class="col-md-3 control-label">Email Address</label>

                                <div class="col-md-7">
                                    <input type="text" name="email-address" value="rod@example.com" class="form-control" />
                                </div> <!-- /.col -->

                            </div> <!-- /.form-group -->



                            <div class="form-group">

                                <label class="col-md-3 control-label">Website</label>

                                <div class="col-md-7">
                                    <input type="text" name="website" value="http://jumpstartthemes.com" class="form-control" />
                                </div> <!-- /.col -->

                            </div> <!-- /.form-group -->



                            <div class="form-group">

                                <label class="col-md-3 control-label">About You</label>

                                <div class="col-md-7">
                                    <textarea id="about-textarea" name="about-you" rows="6" class="form-control">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes.</textarea>
                                </div> <!-- /.col -->

                            </div> <!-- /.form-group -->



                            <div class="form-group">
                                <div class="col-md-7 col-md-push-3">
                                    <button type="submit" class="btn btn-primary">Save Changes</button>
                                    &nbsp;
                                    <button type="reset" class="btn btn-default">Cancel</button>
                                </div> <!-- /.col -->
                            </div> <!-- /.form-group -->



                    </div> <!-- /.tab-pane -->



                    <div class="tab-pane fade" id="password-tab">

                        <div class="heading-block">
                            <h3>
                                Change Password
                            </h3>
                        </div> <!-- /.heading-block -->

                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes.</p>

                        <br><br>

                            <div class="form-group">

                                <label class="col-md-3 control-label">Old Password</label>

                                <div class="col-md-7">
                                    <input type="password" name="old-password" class="form-control" />
                                </div> <!-- /.col -->

                            </div> <!-- /.form-group -->


                            <hr>


                            <div class="form-group">

                                <label class="col-md-3 control-label">New Password</label>

                                <div class="col-md-7">
                                    <input type="password" name="new-password-1" class="form-control" />
                                </div> <!-- /.col -->

                            </div> <!-- /.form-group -->


                            <div class="form-group">

                                <label class="col-md-3 control-label">New Password Confirm</label>

                                <div class="col-md-7">
                                    <input type="password" name="new-password-2" class="form-control" />
                                </div> <!-- /.col -->

                            </div> <!-- /.form-group -->


                            <div class="form-group">

                                <div class="col-md-7 col-md-push-3">
                                    <button type="submit" class="btn btn-primary">Save Changes</button>
                                    &nbsp;
                                    <button type="reset" class="btn btn-default">Cancel</button>
                                </div> <!-- /.col -->

                            </div> <!-- /.form-group -->


                    </div> <!-- /.tab-pane -->



                    <div class="tab-pane fade" id="messaging">

                        <div class="heading-block">
                            <h3>
                                Notification Settings
                            </h3>
                        </div> <!-- /.heading-block -->

                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes.</p>

                        <br><br>

                            <div class="form-group">

                                <label class="col-md-3 control-label">Toggle Notifications</label>

                                <div class="col-md-7">

                                    <div class="checkbox">
                                        <label>
                                            <input value="" type="checkbox" checked>
                                            Send email notification
                                        </label>
                                    </div> <!-- /.checkbox -->

                                    <div class="checkbox">
                                        <label>
                                            <input value="" type="checkbox" checked>
                                            Send Sms notification
                                        </label>
                                    </div> <!-- /.checkbox -->

                                </div> <!-- /.col -->

                            </div> <!-- /.form-group -->


                            <div class="form-group">

                                <label class="col-md-3 control-label">Email for Notifications</label>

                                <div class="col-md-7">
                                    <select name="email" class="form-control">

                                       <option>{{Cache::get('notification_email','No Email Yet')}}</option>
                                        @foreach($users as $user)
                                            <option value={{$user->email}}>{{$user->email}}</option>
                                            @endforeach
                                    </select>
                                </div> <!-- /.col -->

                            </div> <!-- /.form-group -->

                            <div class="form-group">

                                <label class="col-md-3 control-label">Sms for Notifications</label>

                                <div class="col-md-7">
                                    <select name="phone" class="form-control">

                                        <option>{{Cache::get('notification_phone_number','No Phone Number Yet')}}</option>
                                        @foreach($users as $user)
                                            <option value={{$user->phone}}>{{$user->first_name }}  {{$user->last_name}}</option>
                                        @endforeach
                                    </select>
                                </div> <!-- /.col -->

                            </div> <!-- /.form-group -->


                            <div class="form-group">

                                <div class="col-md-7 col-md-push-3">
                                    <button type="submit" class="btn btn-primary">Save Changes</button>
                                    &nbsp;
                                    <button type="reset" class="btn btn-default">Cancel</button>
                                </div> <!-- /.col -->

                            </div> <!-- /.form-group -->

                        </form>

                    </div> <!-- /.tab-pane -->

                </div> <!-- /.tab-content -->

            </div> <!-- /.col -->

        </div> <!-- /.row -->


    </div> <!-- /.container -->

</div> <!-- .content -->

    @stop