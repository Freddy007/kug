@extends('layouts.admin_master')

@section('scripts')
    <script src="{{asset('../../bower_components/jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('../../bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery.validate.js')}}"></script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        jQuery(document).ready(function() {
            $('#add-new-dog').off('click').on('click', function () {
                $('#register-dog-modal').modal();
            });


            $('#print').off('click').on('click', function() {

                $.post("{{url('admin/issue-certificate',$dog->id)}}",function(data){
                    alert(data);
                });
            });

            $('#show-parent').on('click',function(){
                $('#parentage-table').show()
            });

        });

    </script>

@section('content')

    <div class="content">

        <div class="container-fluid">

            <div class="portlet portlet-default">

                @include('partials.pdfcert')
            </div>

        </div> <!-- /.table-responsive -->

    </div> <!-- /.portlet-body -->

    </div> <!-- /.portlet -->
    <br>

    </div> <!-- /.container -->

    </div> <!-- .content -->

@stop