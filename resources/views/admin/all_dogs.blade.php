
@extends('layouts.admin_master')

@section('scripts')
    <script src="{{asset('../../bower_components/jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('../../bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

    <!-- Required datatable js -->
    <script src="{{asset('datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('datatables/dataTables.bootstrap4.min.js')}}"></script>
    {{--<!-- Buttons examples -->--}}
    <script src="{{asset('datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('datatables/jszip.min.js')}}"></script>

    <script>
        jQuery(document).ready(function(){

            var query = "{{$query}}";

            var table = $('#datatable-buttons').DataTable({
                processing: true,
                buttons : ['pdf'],
                serverSide: true,
                ordering : false,
                ajax: '/admin/all-dogs-data'+query,
                columns: [

                    { data: "name",name:'name'},
                    { data: "dob",name:'dob' },
                    { data: "breed", name:'breed' },
                    { data: "sex",name:'sex' },
                    { data: "registration_number", name:'registration_number'},
                    {data : 'breeder', name: 'breeder'},
                    {data : "status", name:'status'},
                    {data : "certificate", name:'certificate'},
                    {data : 'action', name : 'action'}
                ],
//                "initComplete": function( settings, json ) {
//                    addButtons();
//                },
            });



            function registerDeleteEvent(){
                $.get('/admin/all-dogs-data',function(xhr){
                    $.each(xhr.data, function(index, element) {
                        $('#delete-dog-'+element.id).off('click').on('click',function(){
//                            console.log(element.id);
//                            alert(element.id);
                            $('#delete-modal').modal();
                            $('.delete-name').text(element.name);
                            $('#delete-dog-form').attr('action','/admin/delete/'+element.id);
                        })
                        $('#btnConfirm-'+element.id).off('click').on('click',function(){
                            $('#confirm-modal').modal();
                            var confirmStatus = element.confirmed ? 'Revoke' : 'Confirm';
                            $('.confirm-name').text(confirmStatus+ ' '+element.name);
                            $('#confirm-dog-form').attr('action','/admin/confirm-dog/'+element.id);

                        })
                    });

                })

            }

            table.on( 'draw.dt', function () {
                console.log( 'Redraw occurred at: '+new Date().getTime() );
                registerDeleteEvent();
                $('.owner').hide();


                $('.add-owner-detail').on('click',function(e){
                    e.preventDefault();
                    var dog_id = $(this).data('id');
//                    alert ('hello'+' '+$(this).data('id'));
                    $('.add-owner-detail-modal').modal();

                    $('#new_owner').on('change',function(){
                        if($(this).is(":checked")){
                            $('.owner').show()
                        }else{
                            $('.owner').hide()
                        }
                    });

                    $('.submit-certificate').off('click').on('click',function(){
                        var base_url = '{{url('admin/pedigree-certificate/')}}';
                        var name = $('#name').val() !="" ?"name="+$('#name').val()+'&':"";
                        var phone = $('#phone').val() !="" ?"phone="+$('#phone').val()+'&':"";
                        var address = $('#address').val() !="" ?"address="+$('#address').val():"";
//                        alert(base_url+'/'+dog_id+'?'+name+phone+address);
                        location.href=base_url+'/'+dog_id+'?'+name+phone+address;
                    })
                })
            } );




//            var table = $('#datatable-buttons').DataTable({
//                buttons: ['copy', 'excel', 'pdf'],
//                "ajax" : "/admin/all-dogs-data",
//                'ordering': false,
//                "columns": [
//                    { "data": "name" },
//                    { "data": "dob" },
//                    { "data": "breed" },
//                    { "data": "sex" },
//                    { "data": "registration_number"},
//                    {"data" : "breeder"},
//                    {"data" : "status"},
//                    {"data" : "certificate"},
//                    {"data" : "action"},
//                ],
//                "initComplete": function( settings, json ) {
//                    addButtons();
//                },
//                lengthChange: false,
//            });
//
            function addButtons(){
                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
            }
        });

    </script>

@stop

@section('content')

    <div class="content">

        <div class="container">
            {!! Breadcrumbs::render('all-dogs') !!}

            @include('flash::message')
            {{--<h2 class="">{{\App\Dog::count()}} Currently Submitted Dogs</h2>--}}

            {{--<h2 class="pull-right">Hello </h2>--}}

            <br>

            <div class="portlet portlet-default">

                <div class="portlet-header">
                    {{--<h4 class="portlet-title">--}}
                    {{--<u>Pagination</u>--}}
                    {{--</h4>--}}
                </div> <!-- /.portlet-header -->

                <div class="row">

                    <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Date of Birth</th>
                            <th>Breed </th>
                            <th>Sex </th>
                            <th>Registration number </th>
                            <th>Breeder</th>
                            <th>Status</th>
                            <th>Certificate</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                    </table>

                </div>

            </div> <!-- /.portlet -->

        </div> <!-- /.container -->

    </div> <!-- .content -->

    <div class="modal fade" id="report-modal" tabindex="-1" role="dialog" aria-labelledby="report-ModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><strong>REPORT DUPLICATED REGISTRATION NUMBER</strong></h4>
                </div>
                <div class="modal-body">
                    <form id="report-form">
                        <p>We experienced some challenges with dogs registration numbers.
                            Report any of such incident here with the registration number.</p>
                        <label>Registration Number</label>
                        <input type="number" class="form-control" required id="report-input" name="report_registration_number" placeholder="Suspected dog registration number">

                        <div class="report-message h3"></div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-default" id="report">Report</button>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="delete-ModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><strong>Delete  <span class="delete-name"></span> </strong></h4>
                </div>
                <div class="modal-body">
                    <form method="post" id="delete-dog-form" >
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-2">
                                <label>This Action will delete <em><span class="delete-name"></span></em> <br><br>
                                    Are you sure you want to continue with this action ?
                                </label>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Delete <span class="delete-name"></span></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="confirm-modal" tabindex="-1" role="dialog" aria-labelledby="confirm-ModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><strong><span class="confirm-name"></span> </strong></h4>
                </div>
                <div class="modal-body">
                    <form id="confirm-dog-form" method="post">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-sm-3 col-sm-offset-2">

                                <label>Confirm Action ?</label>
                            </div>
                            <div class="col-sm-4">
                                <select class="form-control" name="status">
                                    <option value="{{'0'}}">Revoke</option>
                                    <option value="{{'1'}}">Confirm</option>

                                </select>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Effect Change</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade add-owner-detail-modal" id="add-owner-detail-modal" tabindex="-1" role="dialog" aria-labelledby="add-owner-detail-ModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><strong><span class="confirm-name"></span> </strong></h4>
                </div>
                <div class="modal-body">
                    {{--<form id="confirm-dog-form">--}}
                        {{--{!! csrf_field() !!}--}}
                        <div class="row">
                            <div class="col-sm-3 col-sm-offset-2">

                                <label>Breeder is different from owner </label>
                            </div>
                            <div class="col-sm-4">
                               <input type="checkbox" name="new_owner" id="new_owner">
                            </div>
                        </div>

                    <div class="owner">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-7"><input type="text" class="form-control" id="name" name="name" placeholder="new owner's name" required></div><br><br>
                            <div class="col-md-7 col-md-offset-3"><input type="text" class="form-control" id="phone" name="phone" placeholder="new owner's phone" required></div><br><br>
                            <div class="col-md-7 col-md-offset-3"><input type="text" class="form-control" id="address" name="address" placeholder="new owner's address" required></div><br><br><br>
                        </div>

                    </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-success submit-certificate">Effect Change</button>
                        </div>
                    {{--</form>--}}
                </div>
            </div>
        </div>
    </div>



@stop



