@extends('layouts.admin_master')

@section('scripts')
    <script src="{{asset('../../bower_components/jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('../../bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery.validate.js')}}"></script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        jQuery(document).ready(function(){
            $('#add-new-dog').off('click').on('click',function(){
                $('#register-breeder-modal').modal();
            });

            @foreach($breeders as $breeder)
            $('#edit-breeder-<?php echo $breeder->id ?>').off('click').on('click',function(){
                        $("edit-name-<?php $breeder->id ?>").val('loading...').attr('disabled','true'); //show when submitting
                $('#edit-breeder-modal-<?php echo  $breeder->id ?>').modal();
                        $.post("{{url('admin/breeder',[$breeder->id])}}", function (data) {
                            $('#edit-name-<?php echo $breeder->id ?>').val(data);
                            $("edit-name-<?php $breeder->id ?>").attr('disabled','false'); //show when submitting
                        });
            });

            @endforeach


            {{--$('#add-dog').off('click').on('click',function(){--}}

                {{--$.post("{{url('/member/register-dog')}}",$('#register-dog-modal form').serialize(), function (data) {--}}
                    {{--$('#message').text(data);--}}
                    {{--var hide = function(){$('#register-dog-modal').modal('hide')};--}}
                    {{--setTimeout(hide,5000);--}}
                {{--});--}}

            {{--})--}}

        });

    </script>

@section('content')

    <div class="content">
        <div class="container">
            {!! Breadcrumbs::render('all-breeders') !!}
            @include('flash::message')
            <div class="portlet-header">

                <h3 class="portlet-title"><u>Search </u></h3>

            </div> <!-- /.portlet-header -->
            <div class="row" style="margin-bottom: 45px;">
                <form  action="{{url('admin/all-breeders')}}" method="get">
                    {{csrf_field()}}
                    <div class="col-md-7">
                        <input type="text" name="term" class="form-control input-lg  " value="{{Request::get('term')}}">

                    </div>

                    <div class="col-md-2">
                        <input type="submit" class="form-control input-lg btn btn-success">
                    </div>

                </form>

                <div class="col-md-2" style="margin-top: -15px;">
                    <input type="button" class="form-control input-lg btn btn-success" id="add-new-dog" value="ADD NEW BREED">
                </div>
            </div>

            <div class="portlet portlet-default">

                {{--<div class="portlet-header">--}}

                <h3 class="portlet-title"><u>{{$breeders->count()}} Registered Breeds </u></h3>



                {{--</div> <!-- /.portlet-header -->--}}

                @if($breeders->count())
                <div class="portlet-body">

                    <div class="table-responsive">

                        <table class="table table-striped table-bordered thumbnail-table">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Breed</th>
                                <th class="text-center">Actions</th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php $i = $breeders->firstItem(); ?>
                            @foreach($breeders as $breeder)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td class="valign-middle">
                                        {{$breeder->name}}
                                    </td>


                                    <td class="text-center valign-middle">
                                        <a href="{{url('/admin/breeder-dogs',$breeder->id)}}" class="btn btn-success btn-sm">Dogs</a>
                                        &nbsp;
                                        {{--<a href="{{url('/admin/breeder-edit',$breeder->id)}}" class="btn btn-success btn-sm">Edit</a>--}}
                                        <button  class="btn btn-success btn-sm" id="edit-breeder-{{$breeder->id}}">Edit</button>
                                        &nbsp;
                                        {{--<a href="{{url('/admin/breeder-delete',$breeder->id)}}" class="btn btn-danger btn-sm">Delete</a>--}}
                                    </td>
                                </tr>

                                <div class="modal fade" id="edit-breeder-modal-{{$breeder->id}}" tabindex="-1" role="dialog" aria-labelledby="confirm-ModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel"><strong>EDIT BREED</strong></h4>
                                            </div>
                                            <div class="modal-body">
                                                <form action="{{url('admin/edit-breeder',$breeder->id)}}" method="post" id="breeder-form">
                                                    {!! csrf_field() !!}
                                                    <div class="row">
                                                        <div class="col-sm-12">

                                                            <div class="portlet portlet-boxed portlet-success">

                                                                <div class="portlet-header">
                                                                    <h4 class="portlet-title">
                                                                        <u><i class="fa fa-check portlet-icon"></i>Edit {{$breeder->name}} Breed</u>
                                                                    </h4>
                                                                </div> <!-- /.portlet-header -->

                                                                <div class="portlet-body">
                                                                    <div class="row">
                                                                        <div class="col-sm-6 col-sm-offset-3">
                                                                            <div class="form-group">
                                                                                <label class="control-label" for="name">Name</label>

                                                                                <input type="text" id="edit-name-{{$breeder->id}}" name="edited_name" class="form-control" required placeholder="Edit Breed">
                                                                            </div> <!-- /.form-group -->

                                                                        </div>
                                                                    </div>

                                                                </div> <!-- /.portlet-body -->

                                                            </div> <!-- /.portlet -->

                                                        </div> <!-- /.col -->

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        <input type="submit" id="add-breeder" class="btn btn-success" value="Edit {{$breeder->name}} breed">
                                                    </div>

                                                </form>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                            @endforeach
                            </tbody>
                        </table>
                        {{--{!! $breeders->render() !!}--}}

                        <?php
                        if(isset($paginator)){ ?>
                        {{--{!! $paginator->render() !!}--}}
                        {!! $paginator->appends(['term' => Request::get('term')])->render() !!}

                        <?php   }else {?>
                        {!! $breeders->render() !!}
                        <?php } ?>


                    </div> <!-- /.table-responsive -->

                </div> <!-- /.portlet-body -->
                    @else
                        <div style="height: 400px;">
                            <div class="text-center" style="height: 100px; border: 5px inset #cccccc; border-radius: 5px; padding-top : 10px;
                                    background-color: #cccccc; color: white;">
                                <div class="h2">No Breeds Entered Yet !</div>
                                <div class="h4"><a href="javascript:history.back()"> Back </a></div>
                            </div>
                        </div>

                    @endif

            </div> <!-- /.portlet -->
            <div class="modal fade" id="register-breeder-modal" tabindex="-1" role="dialog" aria-labelledby="confirm-ModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel"><strong>ADD NEW BREED</strong></h4>
                        </div>
                        <div class="modal-body">
                            <form action="{{url('admin/register-breeder')}}" method="post" id="dog-form">
                                {!! csrf_field() !!}
                                <div class="row">
                                    <div class="col-sm-12">

                                        <div class="portlet portlet-boxed portlet-success">

                                            <div class="portlet-header">
                                                <h4 class="portlet-title">
                                                    <u><i class="fa fa-check portlet-icon"></i>New Breed</u>
                                                </h4>
                                            </div> <!-- /.portlet-header -->

                                            <div class="portlet-body">
                                                    <div class="row">
                                                        <div class="col-sm-6 col-sm-offset-3">
                                                            <div class="form-group">
                                                                <label class="control-label" for="name">Name</label>
                                                                <input type="text" id="name" name="name" class="form-control" required placeholder="Add New Breed">
                                                            </div> <!-- /.form-group -->

                                                        </div>
                                                    </div>

                                                    <div id="message">

                                                    </div>

                                            </div> <!-- /.portlet-body -->

                                        </div> <!-- /.portlet -->

                                    </div> <!-- /.col -->

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <input type="submit" id="add-breeder" class="btn btn-success" value="ADD NEW">
                                </div>

                            </form>

                        </div>

                    </div>
                </div>
            </div>

            <div class="modal fade" id="edit-breeder-modal" tabindex="-1" role="dialog" aria-labelledby="confirm-ModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel"><strong>EDIT BREED</strong></h4>
                        </div>
                        <div class="modal-body">
                            <form action="{{url('admin/edit-breeder')}}" method="post" id="breeder-form">
                                {!! csrf_field() !!}
                                <div class="row">
                                    <div class="col-sm-12">

                                        <div class="portlet portlet-boxed portlet-success">

                                            <div class="portlet-header">
                                                <h4 class="portlet-title">
                                                    <u><i class="fa fa-check portlet-icon"></i>New Breed</u>
                                                </h4>
                                            </div> <!-- /.portlet-header -->

                                            <div class="portlet-body">
                                                <div class="row">
                                                    <div class="col-sm-6 col-sm-offset-3">
                                                        <div class="form-group">
                                                            <label class="control-label" for="name">Name</label>
                                                            <input type="text" id="name" name="name" class="form-control" required placeholder="Add New Breeder">
                                                        </div> <!-- /.form-group -->

                                                    </div>
                                                </div>

                                                <div id="message">

                                                </div>

                                            </div> <!-- /.portlet-body -->

                                        </div> <!-- /.portlet -->

                                    </div> <!-- /.col -->

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <input type="submit" id="add-breeder" class="btn btn-success" value="ADD NEW">
                                </div>

                            </form>

                        </div>

                    </div>
                </div>
            </div>

            <br>

        </div> <!-- /.container -->

    </div> <!-- .content -->

@stop