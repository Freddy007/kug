@extends('layouts.admin_master')

@section('scripts')
    <script src="{{asset('../../bower_components/jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('../../bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

    <script>
        jQuery(document).ready(function(){
            @foreach($requests as $request)
           $('#btnConfirm-<?php echo $request->id?>').off('click').on('click',function(){
                        $('#confirm-modal-<?php echo $request->id ?>').modal();
                    })

            @endforeach

//               $('#add-new-dog').off('click').on('click',function(){
//                        $('#register-dog-modal').modal();
//                    });

            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        });

    </script>

@stop

@section('content')

    <div class="content">

        <div class="container">
            {!! Breadcrumbs::render('certificate-requests') !!}

            @include('flash::message')
            <h2 class="">{{\App\RequestedCertificate::count()}} Currently Requested Dog Certificates</h2>

            {{--<h2 class="pull-right">Hello </h2>--}}

            <br>

            {{--<div class="row" style="margin-bottom: 45px;">--}}
                {{--<form  action="{{url('/admin/all-dogs')}}" method="get">--}}
                    {{--{{csrf_field()}}--}}
                    {{--<div class="col-md-7">--}}
                        {{--<input type="text" name="term" class="form-control input-lg  " placeholder="Dog by  Name , Color , Tattoo Number , microchip Number " value="{{Request::get('term')}}">--}}

                    {{--</div>--}}

                    {{--<div class="col-md-2">--}}
                        {{--<input type="submit" class="form-control input-lg btn btn-success" value="Search">--}}
                    {{--</div>--}}

                {{--</form>--}}

                {{--<div class="col-md-2" >--}}
                    {{--<input type="submit" class="form-control input-lg btn btn-success btn-sm"  value="ADD NEW DOG">--}}
                {{--</div>--}}
            {{--</div>--}}

            <div class="portlet portlet-default">

                <div class="portlet-header">
                    {{--<h4 class="portlet-title">--}}
                    {{--<u>Pagination</u>--}}
                    {{--</h4>--}}
                </div> <!-- /.portlet-header -->

                @if($requests->count())
                    <div class="portlet-body">
                        <div class="table-top-menu">
                            <ul>
                                <li class="table-top-pagination"> {!! $requests->render() !!}</li>
                                {{--<li class="trash">--}}
                                    {{--<a href="{{url('/admin/trash')}}">Trash</a>--}}
                                {{--</li>--}}

                            </ul>
                        </div>

                        <table
                                class="table table-striped table-bordered table-hover ui-datatable"
                                data-ajax="../../global/js/demos/json/table_data.json"
                                data-global-search="false"
                                data-length-change="false"
                                data-info="true"
                                data-paging="true"
                                data-page-length="10"
                                >
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Requester</th>
                                <th>Phone Number</th>
                                <th>Kennel</th>
                                <th>Dog</th>
                                <th>Registration Number</th>
                                <th>Date Requested</th>
                                <th>Honoured</th>
                            </tr>

                            </thead>
                            <tbody>
                           <?php $i = $requests->firstItem(); ?>
                                @foreach($requests as $request)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$request->member}}</td>
                                        <td>{{$request->phone}}</td>
                                        <td>{{$request->kennel_name}}</td>
                                        <td>{{$request->name}}</td>
                                    <td>{{$request->registration_number}}</td>
                                    <td>{{$request->created_at}}</td>
                                    <td>
                                        @if($request->honoured == false)
                                            <a href="{{url('admin/honour-certificate',$request->id)}}" class="btn btn-success btn-sm">Honour Request</a>
                                            @else
                                        <strong>Honoured</strong>
                                            @endif
                                    </td>
                                </tr>

                            @endforeach

                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $requests->render() !!}
                        </div>
                    </div> <!-- /.portlet-body -->
                @else
                    <div style="height: 400px;">
                        <div class="text-center" style="height: 100px; border: 5px inset #cccccc; border-radius: 5px; padding-top : 10px;
                                    background-color: #cccccc; color: white;">
                            <div class="h2">There are no current dogs submitted. </div>
                            <div class="h4"><a href="javascript:history.back()"> Back </a></div>
                        </div>
                    </div>

                @endif

            </div> <!-- /.portlet -->

        </div> <!-- /.container -->

    </div> <!-- .content -->

@stop

