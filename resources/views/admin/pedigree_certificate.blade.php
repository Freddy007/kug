@extends('layouts.admin_master')

@section('scripts')
    <script src="{{asset('../../bower_components/jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('../../bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery.validate.js')}}"></script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        jQuery(document).ready(function(){
            $('#add-new-dog').off('click').on('click',function(){
                $('#register-dog-modal').modal();
            });

            $('#add-dog').off('click').on('click',function(){

                $.post("{{url('/member/register-dog')}}",$('#register-dog-modal form').serialize(), function (data) {
                    $('#message').text(data);
                    // $('#register-dog-modal').modal('hide');
                });

            })

            //$('#parentage-table').hide();
            $('#show-parent').on('click',function(){
                $('#parentage-table').show()
            });

        });

    </script>

@section('content')

    <div class="content">

        <div class="container-fluid">

            <div class="portlet portlet-default">

                @include('partials.main_certificate')
            </div>

        </div> <!-- /.table-responsive -->

    </div> <!-- /.portlet-body -->

    </div> <!-- /.portlet -->
    <br>

    </div> <!-- /.container -->

    </div> <!-- .content -->

@stop