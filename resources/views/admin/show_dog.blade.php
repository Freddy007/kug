@extends('layouts.admin_master')

@section('scripts')
    <script src="{{asset('../../bower_components/jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('../../bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery.validate.js')}}"></script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        jQuery(document).ready(function(){
            $('#add-new-dog').off('click').on('click',function(){
                $('#register-dog-modal').modal();
            });

            $('#add-dog').off('click').on('click',function(){

                $.post("{{url('/member/register-dog')}}",$('#register-dog-modal form').serialize(), function (data) {
                    $('#message').text(data);
                    // $('#register-dog-modal').modal('hide');
                });

            })

            //$('#parentage-table').hide();
            $('#show-parent').on('click',function(){
                $('#parentage-table').show()
            });

        });

    </script>

    <style>
        .water-mark{
            z-index: 20000;
            position: absolute;
            top: 600px;
            left: 200px;
            font-weight: bold;
            font-size: 60px;
            opacity: 0.3;
            -ms-transform: rotate(45deg); /* IE 9 */
            -webkit-transform: rotate(45deg); /* Chrome, Safari, Opera */
            transform: rotate(45deg);
        }

        hr {
            padding: 0;
            border: none;
            border-top: medium double #333;
            color: #333;
            text-align: center;
            width: 95%;
            margin-left: 0px;
        }
        hr:after {
            /*content: "�";*/
            display: inline-block;
            position: relative;
            top: -0.7em;
            font-size: 1.5em;
            padding: 0 0.25em;
            background: white;
        }
        .male {
            background-color: rgb(191,133,10);
            color: #000000;
            font-family: Sans Serif;
            font-size: 8pt;
            text-align: center;
            vertical-align: middle;
            border-collapse: collapse;
            border: 1px solid #000000;
        }

        .female {
            background-color: #FFFFFF;
            color: #000000;
            font-family: Sans Serif;
            font-size: 8pt;
            text-align: center;
            vertical-align: middle;
            border-collapse: collapse;
            border: 1px solid #000000;
        }

        @media print {
            .male{
                background-color: rgb(191,133,10)!important;
                -webkit-print-color-adjust: exact;
            }
            header {
                display: none;
            }
            .mainnav{
                display: none;
            }
            footer,title,.print{
                display:none;
            }

            @page { size: auto;  margin: 0mm; }

            table{
                height: 95%;
                /*background-color: yellowgreen !important;*/
                -webkit-print-color-adjust: exact;
            }


        }
    </style>

@section('content')

    <div class="content">

        <div class="container">

            <div class="portlet portlet-default">

                {{--<div class="portlet-header">--}}

                {{--<h3 class="portlet-title"><u>Table Gallery</u></h3>--}}

                {{--</div> <!-- /.portlet-header -->--}}
                <div class="portlet-body">

                    <div class="row">
                        <table class="table-responsive">

                            <table class="table table-striped table-bordered thumbnail-table">
                                <tr>
                                    <td>Name</td>
                                    <td>
                                        {{$dog->name }}
                                    </td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>image</td>
                                    <td> <figure><img src="/images/catalog/{{$dog->image_name}}" style="width: 200px; height: 150px;"></figure>

                                    </td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>Breed</td>
                                    <td>{{$dog->breeder_name}}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Sex</td>
                                    <td>{{$dog->sex}}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Registration Number</td>
                                    <td>{{$dog->registration_number}}</td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>Microchip Number</td>
                                    <td>{{$dog->microchip_number}}</td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>Colour</td>
                                    <td>{{$dog->colour}}</td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>Height</td>

                                    <td>  @if($dog->height == 0 )
                                            <em> height not set</em>
                                        @else
                                            {{$dog->height}}
                                        @endif
                                    </td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>Tattoo Number</td>
                                    <td>{{$dog->tattoo_number}}</td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>DNA ID</td>
                                    <td>{{$dog->DNA}}</td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>Elbow ED Results</td>
                                    <td>{{$dog->elbow_ed_results}}</td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>Appraisal Score</td>
                                    <td>
                                        @if($dog->appraisal_score == 0)
                                            <em>Appraisal score not set</em>
                                        @else
                                            {{$dog->appraisal_score}}
                                        @endif
                                    </td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>Other Health Checks</td>
                                    <td>{{$dog->other_health_checks}}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Titles</td>
                                    <td>{{$dog->titles}}</td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>Performance Titles</td>
                                    <td>{{$dog->performance_titles}}</td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>Registered Date</td>
                                    <td>{{$dog->created_at}}</td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>Status</td>
                                    @if($dog->dead == true)
                                        <strong>  <td>{{'Dead'}}</td></strong>
                                        <td></td>
                                    @else
                                        <strong> <td>{{'Alive'}}</td></strong>
                                        <td></td>
                                    @endif

                                </tr>


                                <tr>
                                    <td>Other Certifications</td>
                                    <td>{{$dog->other_certifications}}</td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td></td>
                                    <td class="text-center"><h3></h3></td>
                                    <td>

                                    </td>
                                </tr>
                    </div>

                    </table>
                    {{--<h3 class="text-center"> <button class="btn btn-success" id="show-parent">Show Parentage</button></h3>--}}
                    @if($dog->father || $dog->mother)
                        <?php
                        $father_id = \App\Dog::getDogId($dog->father);
                        $mother_id = \App\Dog::getDogId($dog->mother);
                            ?>

                           <h1 class="text-center">
                               <a class="btn btn-success btn-lg" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                    Parentage
                               </a>

                           </h1>
                            <div class="collapse" id="collapseExample">
                                    <div>
                                        @include('partials.table_minimal')
                                        {{--@include('demo-cert')--}}
                                    @else
                                            <div class="text-center"> <h3>No Parents Data Yet !</h3> </div>
                                        @endif
                                </div>
                            </div>
                </div>
            </div>

        </div> <!-- /.table-responsive -->

    </div> <!-- /.portlet-body -->

    </div> <!-- /.portlet -->
    <br>

    </div> <!-- /.container -->

    </div> <!-- .content -->

@stop
