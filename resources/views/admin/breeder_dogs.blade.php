@extends('layouts.admin_master')

@section('scripts')
    <script src="{{asset('../../bower_components/jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('../../bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

    <script>
        jQuery(document).ready(function(){
            @foreach($dogs as $dog)
           $('#btnConfirm-<?php echo $dog->id?>').off('click').on('click',function(){
                        $('#confirm-modal-<?php echo $dog->id ?>').modal();
                    })
            @endforeach

               $('#add-new-dog').off('click').on('click',function(){
                        $('#register-dog-modal').modal();
                    });
        });

    </script>

@stop

@section('content')

    <div class="content">

        <div class="container">

            @include('flash::message')
            <h2 class="">{{$breeder->name.' Breed'}}</h2>

            {{--<h2 class="pull-right">Hello </h2>--}}

            <br>

            <div class="row" style="margin-bottom: 45px;">
                <form  action="{{url('/')}}" method="get">
                    {{csrf_field()}}
                    <div class="col-md-7">
                        <input type="text" name="term" class="form-control input-lg  " value="{{Request::get('term')}}">

                    </div>

                    <div class="col-md-2">
                        <input type="submit" class="form-control input-lg btn btn-success" value="Search">
                    </div>

                </form>

                {{--<div class="col-md-2" >--}}
                    {{--<input type="button" class="form-control input-lg btn btn-success btn-sm" id="add-new-dog" value="ADD NEW DOG">--}}
                {{--</div>--}}
            </div>

            <div class="portlet portlet-default">

                <div class="portlet-header">
                    {{--<h4 class="portlet-title">--}}
                    {{--<u>Pagination</u>--}}
                    {{--</h4>--}}
                </div> <!-- /.portlet-header -->

                <div class="portlet-body">

                    <table
                            class="table table-striped table-bordered table-hover ui-datatable"
                            data-ajax="../../global/js/demos/json/table_data.json"
                            data-global-search="false"
                            data-length-change="false"
                            data-info="true"
                            data-paging="true"
                            data-page-length="10"
                            >
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Date of Birth</th>
                            <th>Sex</th>
                            <th>Registration Number</th>
                            <th>Date</th>
                        </tr>

                        </thead>
                        <tbody>
                        @foreach($dogs as $dog)
                            <tr>
                                <td>{{$dog->name}}</td>
                                <td>{{$dog->dob}}</td>
                                <td>{{$dog->sex}}</td>
                                <td>{{$dog->registration_number}}</td>
                                <td>{{$dog->created_at}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div> <!-- /.portlet-body -->
            </div> <!-- /.portlet -->

        </div> <!-- /.container -->

    </div> <!-- .content -->
@stop

