@extends('layouts.admin_master')

@section('scripts')
    <script src="{{asset('../../bower_components/jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('../../bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

    <script>
        jQuery(document).ready(function(){
            @foreach($dogs as $dog)
           $('#btnConfirm-<?php echo $dog->id?>').off('click').on('click',function(){
                        $('#confirm-modal-<?php echo $dog->id ?>').modal();
                    })
            $('#delete-<?php echo $dog->id ?>').off('click').on('click',function(){
                $('#delete-modal-<?php echo $dog->id ?>').modal();
            })
            @endforeach
        });

    </script>


@stop

@section('content')

    <div class="content">

        <div class="container">

            <h2 class="">{{$first_name .' '.  $last_name."'s Dogs" }}</h2>

            {{--<h2 class="pull-right">Hello {{$user}} </h2>--}}

            <br>

            <div class="portlet portlet-default">

                <div class="portlet-header">
                    {{--<h4 class="portlet-title">--}}
                    {{--<u>Pagination</u>--}}
                    {{--</h4>--}}
                </div> <!-- /.portlet-header -->
                @if ($dogs->count())

                <div class="portlet-body">

                    <table
                            class="table table-striped table-bordered table-hover ui-datatable"
                            >
                        <thead>
                        <tr>
                            <th> Name</th>
                            <th>Breed</th>
                            <th>Sex</th>
                            <th>Registration No.</th>
                            <th>Registered Date</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>

                        </thead>
                        <tbody>
                        @foreach($dogs as $dog)
                            <tr>
                                <td>{{$dog->name}}</td>
                                <td>{{$dog->breeder_name}}</td>
                                <td>{{$dog->sex}}</td>
                                <td>{{$dog->registration_number}}</td>
                                <td>{{$dog->created_at}}</td>
                                @if($dog->confirmed == true)
                                    <td>{{'Confirmed'}}</td>
                                @else
                                    <td><button id="btnConfirm-{{$dog->id}}" class="btn btn-success btn-sm">Confirm</button></td>
                                @endif
                                <td>
                                    <a class="btn btn-success btn-sm" href="{{url('admin/dog-show',$dog->id)}}"> View</a>
                                    <a class="btn btn-success btn-sm" href="{{url('admin/dog-edit',$dog->id)}}"> Edit</a>
                                    <button id="delete-{{$dog->id}}" class="btn btn-danger btn-sm">Delete</button>
                                </td>
                            </tr>
                            <div class="modal fade" id="confirm-modal-{{$dog->id}}" tabindex="-1" role="dialog" aria-labelledby="confirm-ModalLabel">
<!--                              --><?php //$confirm = \App\Dog::where('id',$dog->id)->first()->confirmed == true ? 'Revoke' : 'Confirm'; ?>

                                {{--<div class="modal-dialog" role="document">--}}
                                    {{--<div class="modal-content">--}}
                                        {{--<div class="modal-header">--}}
                                            {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>--}}
                                            {{--<h4 class="modal-title" id="myModalLabel"><strong>CONFIRM NEW DOG</strong></h4>--}}
                                        {{--</div>--}}
                                        {{--<div class="modal-body">--}}
                                            {{--<form action="{{url('admin/confirm-dog',$dog->id)}}" method="post">--}}
                                                {{--{!! csrf_field() !!}--}}
                                                {{--<div class="row">--}}
                                                    {{--<div class="col-sm-3 col-sm-offset-2">--}}
                                                        {{--<label>Are you sure you want to {{$confirm}}?</label>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-sm-4">--}}
                                                        {{--<select class="form-control" name="status">--}}
                                                            {{--@if(\App\Dog::where('id',$dog->id)->first()->confirmed)--}}
                                                            {{--<option value="{{'0'}}">Revoke</option>--}}
                                                            {{--@else--}}
                                                            {{--<option value="{{'1'}}">confirm</option>--}}
                                                                {{--@endif--}}
                                                        {{--</select>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}

                                                {{--<div class="modal-footer">--}}
                                                    {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                                                    {{--<button type="submit" class="btn btn-success">Confirm</button>--}}
                                                {{--</div>--}}
                                            {{--</form>--}}

                                        {{--</div>--}}

                                    {{--</div>--}}
                                {{--</div>--}}
                            </div>

                            <div class="modal fade" id="delete-modal-{{$dog->id}}" tabindex="-1" role="dialog" aria-labelledby="confirm-ModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel"><strong>CONFIRM DELETE DOG</strong></h4>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{url('admin/delete-dog',$dog->id)}}" method="post">
                                                {!! csrf_field() !!}
                                                <div class="row">
                                                    <div class="col-sm-5 col-sm-offset-2">
                                                        <label>sure you want to Delete dog <strong> {{$dog->name}} </strong>?</label>
                                                    </div>
                                                </div>

                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-danger">Delete</button>
                                                </div>
                                            </form>

                                        </div>

                                    </div>
                                </div>
                            </div>

                        @endforeach

                        </tbody>
                    </table>

                </div> <!-- /.portlet-body -->

            </div> <!-- /.portlet -->
            @else
                    <div style="height: 400px;">
                        <div class="text-center" style="height: 100px; border: 5px inset #cccccc; border-radius: 5px; padding-top : 10px;
                                    background-color: #cccccc; color: white;">
                            <div class="h2">This Member has not yet submitted any dog !</div>
                            <div class="h4"><a href="javascript:history.back()"> Back </a></div>
                        </div>
                    </div>
                @endif

        </div> <!-- /.container -->

    </div> <!-- .content -->
@stop

