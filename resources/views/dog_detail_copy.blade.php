@extends('layouts.index3_layout')

@section('scripts')

    <script>
       $('.edit-column').hide();
    </script>

@endsection

@section('content')

    <!-- breadcrumbs -->
    <section class="breadcrumbs"
             {{--style="background-image: url(/kug_version2/frontend/assets/images/breadcrumbs/best-room.jpg)"--}}
             style="background-color: #000000"
    >
{{--        <div class="container">--}}
{{--            <div class="row">--}}
{{--                <div class="col-md-6">--}}
{{--                    <h1 class="h1"> Dog details</h1>--}}
{{--                </div>--}}
{{--                <div class="col-md-6">--}}
{{--                    <ol class="breadcrumb">--}}
{{--                        <li><a href="{{url('/')}}">Home</a><i class="fa fa-angle-right"></i></li>--}}
{{--                        --}}{{--<li><a href="#">Rooms</a><i class="fa fa-angle-right"></i></li>--}}
{{--                        <li class="active">Dog etails</li>--}}
{{--                    </ol>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}

<!-- Page Heading -->
    <section class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1>Search Results</h1>
                </div>
                <div class="col-md-6">
                    <ul class="breadcrumb">
                        <li><a href="/">Home</a></li>
                        <li class="active">Search Results</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- Page Heading / End -->


    <section class="page-content">
        <div class="container">

            <!-- Light Section -->
            <section class="section-light section-nomargin">
                <div class="row">
                    <div class="col-md-12">

                        <div class="col-lg-12 marg50"><h2 class="h2">{{$dog->name}} <i>info</i></h2></div>

                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="owl-carousel owl_gallery">
                                <div class="item">
                                    {{--<img class="img-responsive" src="{{asset('kug_version2/frontend/assets/images/gallery/3.jpg')}}">--}}
                                    <img class="img-responsive" src="http://placehold.it/260x160/cccccc/ffffff">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="room-detail_overview">
                                <table class="simple">

                                    <tr>
                                        <td><strong>Kennel Name / Owner:</strong></td>
                                        <td>{{$dog->kennel_name ?: $dog->member}}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Breed:</strong></td>
                                        <td>{{$dog->breeder_name}}<span></span></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Sex:</strong></td>
                                        <td>{{$dog->sex}}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Registration Number:</strong></td>
                                        <td>{{$dog->registration_number}}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Microchip Number:</strong></td>
                                        <td>{{$dog->microchip_number}}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Colour:</strong></td>
                                        <td>{{$dog->colour}}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Height:</strong></td>
                                        <td>
                                            @if($dog->height == 0 )
                                                <em> height not set</em>
                                            @else
                                                {{$dog->height}}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Coat:</strong></td>
                                        <td>{{$dog->coat}}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Tattoo Number:</strong></td>
                                        <td>{{$dog->tattoo_number}}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>DNA ID:</strong></td>
                                        <td>{{$dog->dna_id}}</td>
                                    </tr>

                                </table>
                            </div>
                        </div>

                    </div>

                </div>

            </section>
        </div>

    </section>


    <section class="page-content">
{{--        <div class="container">--}}
{{--    <section class="room-detail">--}}
        <div class="container">
            <div class="row">

                <div class="col-lg-12 marg50"><h2 class="h2">{{$dog->name}} <i>info</i></h2></div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="owl-carousel owl_gallery">
                        <div class="item">
                            {{--<img class="img-responsive" src="{{asset('kug_version2/frontend/assets/images/gallery/3.jpg')}}">--}}
                            <img class="img-responsive" src="http://placehold.it/260x160/cccccc/ffffff">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="room-detail_overview">
                        <table class="simple">

                            <tr>
                                <td><strong>Kennel Name / Owner:</strong></td>
                                <td>{{$dog->kennel_name ?: $dog->member}}</td>
                            </tr>
                            <tr>
                                <td><strong>Breed:</strong></td>
                                <td>{{$dog->breeder_name}}<span></span></td>
                            </tr>
                            <tr>
                                <td><strong>Sex:</strong></td>
                                <td>{{$dog->sex}}</td>
                            </tr>
                            <tr>
                                <td><strong>Registration Number:</strong></td>
                                <td>{{$dog->registration_number}}</td>
                            </tr>
                            <tr>
                                <td><strong>Microchip Number:</strong></td>
                                <td>{{$dog->microchip_number}}</td>
                            </tr>
                            <tr>
                                <td><strong>Colour:</strong></td>
                                <td>{{$dog->colour}}</td>
                            </tr>
                            <tr>
                                <td><strong>Height:</strong></td>
                                <td>
                                    @if($dog->height == 0 )
                                        <em> height not set</em>
                                    @else
                                        {{$dog->height}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Coat:</strong></td>
                                <td>{{$dog->coat}}</td>
                            </tr>
                            <tr>
                                <td><strong>Tattoo Number:</strong></td>
                                <td>{{$dog->tattoo_number}}</td>
                            </tr>
                            <tr>
                                <td><strong>DNA ID:</strong></td>
                                <td>{{$dog->dna_id}}</td>
                            </tr>

                        </table>
                    </div>
            </div>
        </div>

            @if($dog->father || $dog->mother)
                <?php
                $father_id = \App\Dog::getDogId($dog->father);
                $mother_id = \App\Dog::getDogId($dog->mother);
                ?>

                <h1 class="text-center">
                    <a class="btn btn-success btn-lg" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                        Pedigree Info
                    </a>

                </h1>
                <div class="collapse" id="collapseExample">
                    <div>
                        @include('partials.table_minimal')
                        @else
                            <div class="text-center"> <h3>No Pedigree Data Yet!</h3> </div>
                        @endif
                    </div>
                </div>
            </div>

        {{--</div>--}}
    </section>

@endsection