<div class="portlet-body form">
    {{--<form action="{{url('version2/register-litter')}}" class="form-horizontal" id="submit_form" method="POST">--}}
        {{--<form action="{{url('admin/register-dog')}}" class="form-horizontal" id="submit_form" method="POST">--}}
        <div class="form-wizard">
            <div class="form-body">
                <ul class="nav nav-pills nav-justified steps">
                    <li>
                        <a href="#tab1" data-toggle="tab" class="step">
                            <span class="number"> 1 </span>
                            <span class="desc">
                                                                        <i class="fa fa-check"></i> Basic Details </span>
                        </a>
                    </li>
                    <li>
                        <a href="#tab2" data-toggle="tab" class="step">
                            <span class="number"> 2 </span>
                            <span class="desc">
                                                                        <i class="fa fa-check"></i> Secondary Details </span>
                        </a>
                    </li>
                    <li>
                        <a href="#tab3" data-toggle="tab" class="step">
                            <span class="number">3 </span>
                            <span class="desc">
                                                                        <i class="fa fa-check"></i> Confirm </span>
                        </a>
                    </li>
                </ul>

                <div id="bar" class="progress progress-striped" role="progressbar">
                    <div class="progress-bar progress-bar-success"> </div>
                </div>
                <div class="tab-content">
                    <div class="alert alert-danger display-none">
                        <button class="close" data-dismiss="alert"></button> You have some form errors. Please check below. </div>
                    <div class="alert alert-success display-none">
                        <button class="close" data-dismiss="alert"></button> Your form validation is successful! </div>
                    <div class="tab-pane active" id="tab1">
                        <h3 class="block">Basic details</h3>

                        <form action="{{url('version2/register-litter')}}" method="post" enctype="multipart/form-data"
                              {{--v-on:submit.prevent="submitLitter" --}}
                              id="litter-form"
                              class="form-horizontal">
                            {{csrf_field()}}

                            <div class="columns" style="border: 1px solid #cccccc; border-radius: 7px; width: 50%; margin: auto; margin-bottom: 10px; padding: 10px 5px 0 5px;">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Name
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control name" name="name[]" id="name" required />
                                        <span class="help-block">  </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Gender
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <select class="form-control" name="sex[]">
                                            <option></option>
                                            <option>Male</option>
                                            <option>Female</option>
                                        </select>
                                        <span class="help-block">  </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Colour
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control colour" name="colour[]" id="colour" value="" required />
                                        <span class="help-block">  </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Height
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control height" name="height[]" id="height" required />
                                        <span class="help-block">  </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Coat
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control coat" name="coat[]" id="coat" required />
                                        <span class="help-block">  </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Microchip number
                                        <span class="required"> </span>
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control microchip_number" name="microchip_number[]" id="coat" required />
                                        <span class="help-block">  </span>
                                    </div>
                                </div>
                            </div>


                            <div class="additional-columns"></div>

                            <div class="form-group text-center">
                                <button type="button" class="btn btn-outline green add-new-column" id="add-new-column"> Add puppy
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>


                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-outline green btn btn-success" id="add-new-column"> Save litter
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>

                        </form>

                    </div>

                    <div class="tab-pane" id="tab2">
                        <h3 class="block">Secondary Details</h3>

                        <div class="form-group">
                            <label class="control-label col-md-3">Date of Birth
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-4">
                                <input type="date" class="form-control" name="dob" id="dob" required />
                                <span class="help-block">  </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Father(Sire)
                                {{--<span class="required"> * </span>--}}
                            </label>
                            <div class="col-md-4">
                                <select class="dog_parent form-control"  id="father" name="father"  data-gender="male" style="width: 320px;" >
                                    <option value=""></option>
                                </select>
                                <span class="help-block">  </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Mother(Dam)
                                {{--<span class="required"> * </span>--}}
                            </label>
                            <div class="col-md-4">
                                <select class="dog_parent form-control" name="mother" id="mother" data-gender="female" style="width: 320px;">
                                    <option value=""></option>
                                </select>
                                <span class="help-block">  </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Breed
                                <span class="required"> * </span>
                                {{--<span> * </span>--}}

                            </label>
                            <div class="col-md-4">
                                {{--<select ng-model="selectedName" ng-options="item as item.name for item in breeds track by item.id" class="form-control" id="breed" name="breed">--}}
                                <select  class="form-control" id="breed" name="breeder_id" required>
                                    <option></option>
                                    @foreach($breeds as $breed)
                                        <option value="{{$breed->id}}">{{$breed->name}}</option>
                                    @endforeach
                                </select>
                                <span class="help-block">  </span>
                            </div>
                        </div>

                    </div>

                    <div class="tab-pane" id="tab3">
                        <h3 class="block">Confirm Details</h3>
                        {{--<h4 class="form-section"> Details</h4>--}}
                        <h4 class="count text-center"></h4>

                    </div>

                </div>
            </div>

            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                        <a href="javascript:;" class="btn default button-previous">
                            <i class="fa fa-angle-left"></i> Back </a>
                        <a href="javascript:;" class="btn btn-outline green button-next"> Continue
                            <i class="fa fa-angle-right"></i>
                        </a>

                        <button type="submit" class="btn green button-submit">Submit
                            <i class="fa fa-check"></i>
                        </button>

                    </div>
                </div>
            </div>
        </div>
    {{--</form>--}}
</div>
