@extends('version2.layouts.admin_layout')

@section('styles')

    <link href="{{asset('kug_version2/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />

    @endsection

    @section('scripts')
            <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{asset('kug_version2/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{asset('kug_version2/assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('kug_version2/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('kug_version2/assets/pages/scripts/table-datatables-managed.min.js')}}" type="text/javascript"></script>


    <script>
        jQuery(document).ready(function(){
            @foreach($dogs as $dog)
           $('#btnConfirm-<?php echo $dog->id?>').off('click').on('click',function(){
                $('#confirm-modal-<?php echo $dog->id ?>').modal();
            })
            $('#delete-<?php echo $dog->id ?>').off('click').on('click',function(){
                $('#delete-modal-<?php echo $dog->id ?>').modal();
            })
            @endforeach
        });

    </script>

    @endsection


    @section('content')
            <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <div class="container">
                    <!-- BEGIN PAGE TITLE -->
                    <div class="page-title">
                        <h1>All Dogs
                            {{--<small>managed datatable samples</small>--}}
                        </h1>
                    </div>
                    <!-- END PAGE TITLE -->

                </div>
            </div>
            <!-- END PAGE HEAD-->

            <!-- BEGIN PAGE CONTENT BODY -->
            <div class="page-content">
                <div class="container">
                    <!-- BEGIN PAGE BREADCRUMBS -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="{{url('version2')}}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{url('/version2/all-members')}}">Members</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        {{--<li>--}}
                            {{--<a href="#">Tables</a>--}}
                            {{--<i class="fa fa-circle"></i>--}}
                        {{--</li>--}}
                        <li>
                            <span>member dogs</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMBS -->
                    <!-- BEGIN PAGE CONTENT INNER -->
                    <div class="page-content-inner">
                        {{--<div class="m-heading-1 border-green m-bordered">--}}
                        {{--<h3>DataTables jQuery Plugin</h3>--}}
                        {{--<p> DataTables is a plug-in for the jQuery Javascript library. It is a highly flexible tool, based upon the foundations of progressive enhancement, and will add advanced interaction controls to any HTML table. </p>--}}
                        {{--<p> For more info please check out--}}
                        {{--<a class="btn red btn-outline" href="http://datatables.net/" target="_blank">the official documentation</a>--}}
                        {{--</p>--}}
                        {{--</div>--}}

                        <div class="row">
                            <div class="col-md-12">
                                @include('flash::message')

                                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light ">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            {{--<i class="icon-settings font-dark"></i>--}}
                                            <span class="caption-subject bold uppercase">
                                                        {{$first_name .' '.  $last_name."'s Dogs" }}

                                            </span>
                                        </div>
                                        {{--<div class="actions">--}}
                                        {{--<div class="btn-group btn-group-devided" data-toggle="buttons">--}}
                                        {{--<label class="btn btn-transparent dark btn-outline btn-circle btn-sm active">--}}
                                        {{--<input type="radio" name="options" class="toggle" id="option1">Actions</label>--}}
                                        {{--<label class="btn btn-transparent dark btn-outline btn-circle btn-sm">--}}
                                        {{--<input type="radio" name="options" class="toggle" id="option2">Settings</label>--}}
                                        {{--</div>--}}
                                        {{--</div>--}}
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="btn-group">
                                                        {{--<button id="sample_editable_1_new" class="btn sbold green"> Add New--}}
                                                            {{--<i class="fa fa-plus"></i>--}}
                                                        {{--</button>--}}

{{--                                                        {{$first_name .' '.  $last_name."'s Dogs" }}--}}

                                                        {{$dogs->count() >1 ? $dogs->count() ." dogs":$dogs->count(). " dog" }} registered

                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="btn-group pull-right">
                                                        <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="fa fa-print"></i> Print </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover order-column" id="datatable-buttons">
                                            <thead>
                                            <tr>
                                                <th> Name</th>
                                                <th>Breed</th>
                                                <th>Sex</th>
                                                <th>Registration No.</th>
                                                <th>Registered Date</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            @foreach($dogs as $dog)
                                                <tr>
                                                    <td>{{$dog->name}}</td>
                                                    <td>{{$dog->breeder_name}}</td>
                                                    <td>{{$dog->sex}}</td>
                                                    <td>{{$dog->registration_number}}</td>
                                                    <td>{{$dog->created_at}}</td>
                                                    @if($dog->confirmed == true)
                                                        <td>{{'Confirmed'}}</td>
                                                    @else
                                                        <td><button id="btnConfirm-{{$dog->id}}" class="btn btn-success btn-sm">Confirm</button></td>
                                                    @endif
                                                    <td>
                                                        <a class="btn btn-success btn-xs" href="{{url('admin/dog-show',$dog->id)}}"> View</a>
                                                        <a class="btn btn-success btn-xs" href="{{url('admin/dog-edit',$dog->id)}}"> <i class="fa fa-edit"></i></a>
                                                        <button id="delete-{{$dog->id}}" class="btn btn-danger btn-xs"><i class="fa fa-remove"></i></button>
                                                    </td>
                                                </tr>
                                                <div class="modal fade" id="confirm-modal-{{$dog->id}}" tabindex="-1" role="dialog" aria-labelledby="confirm-ModalLabel">
                                                    <!--                              --><?php //$confirm = \App\Dog::where('id',$dog->id)->first()->confirmed == true ? 'Revoke' : 'Confirm'; ?>

                                                </div>

                                                <div class="modal fade" id="delete-modal-{{$dog->id}}" tabindex="-1" role="dialog" aria-labelledby="confirm-ModalLabel">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                <h4 class="modal-title" id="myModalLabel"><strong>CONFIRM DELETE DOG</strong></h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form action="{{url('admin/delete-dog',$dog->id)}}" method="post">
                                                                    {!! csrf_field() !!}
                                                                    <div class="row">
                                                                        <div class="col-sm-5 col-sm-offset-2">
                                                                            <label>sure you want to Delete dog <strong> {{$dog->name}} </strong>?</label>
                                                                        </div>
                                                                    </div>

                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                        <button type="submit" class="btn btn-danger">Delete</button>
                                                                    </div>
                                                                </form>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            @endforeach

                                            </tbody>
                                        </table>
                                        <?php
                                        if(isset($paginator)){ ?>
                                        {{--{!! $paginator->render() !!}--}}
                                        {!! $paginator->appends(['term' => Request::get('term')])->render() !!}

                                        <?php   }else {?>
                                        {!! $dogs->render() !!}
                                        <?php } ?>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>

                    </div>
                    <!-- END PAGE CONTENT INNER -->
                </div>
            </div>
            <!-- END PAGE CONTENT BODY -->
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
    <!-- END CONTAINER -->

@endsection