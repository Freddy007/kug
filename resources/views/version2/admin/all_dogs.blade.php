@extends('version2.layouts.admin_layout')

@section('styles')
    <link href="{{asset('kug_version2/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('scripts')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{asset('kug_version2/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{asset('kug_version2/assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('kug_version2/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('kug_version2/assets/pages/scripts/table-datatables-managed.min.js')}}" type="text/javascript"></script>


        <script>
        jQuery(document).ready(function(){

            var query = "{{$query}}";

            var table = $('#datatable-buttons').DataTable({
                processing: true,
                buttons : ['pdf'],
                serverSide: true,
                ordering : false,
                ajax: '/admin/all-dogs-data'+query,
                columns: [
                    { data: "name",name:'name'},
                    { data: "dob",name:'dob' },
                    { data: "breed", name:'breed' },
                    { data: "sex",name:'sex' },
                    { data: "registration_number", name:'registration_number'},
                    {data : 'breeder', name: 'breeder'},
                    {data : "status", name:'status'},
                    {data : "certificate", name:'certificate'},
                    {data : 'action', name : 'action'}
                ],
//                "initComplete": function( settings, json ) {
//                    addButtons();
//                },
            });


            function registerDeleteEvent(){
                $.get('/admin/all-dogs-data',function(xhr){
                    $.each(xhr.data, function(index, element) {
                        $('#delete-dog-'+element.id).off('click').on('click',function(){
//                            console.log(element.id);
//                            alert(element.id);
                            $('#delete-modal').modal();
                            $('.delete-name').text(element.name);
                            $('#delete-dog-form').attr('action','/admin/delete/'+element.id);
                        })
                        $('#btnConfirm-'+element.id).off('click').on('click',function(){
                            $('#confirm-modal').modal();
                            var confirmStatus = element.confirmed ? 'Revoke' : 'Confirm';
                            $('.confirm-name').text(confirmStatus+ ' '+element.name);

                            $('#confirm-dog-form').attr('action','/admin/confirm-dog/'+element.id);

                        })
                    });

                })

            }

            table.on( 'draw.dt', function () {
                console.log( 'Redraw occurred at: '+new Date().getTime() );
                registerDeleteEvent();
                $('.owner').hide();


                $('.add-owner-detail').on('click',function(e){
                    // e.preventDefault();
                    var dog_id = $(this).data('id');
                    var dog_name = $(this).data('name');
//                    alert ('hello'+' '+$(this).data('id'));
//                     $('.add-owner-detail-modal').modal();

                    // $.ajax({
                    //     url: "http://3.123.126.62/certification/public/"+dog_id,
                    // }).done(function( data ) {
                    //         // if ( console && console.log ) {
                    //             // console.log( "Sample of data:", data.slice( 0, 100 ) );
                    //         // }
                    //     alert("Hello odne");
                    //     });

                    $('#new_owner').on('change',function(){
                        if($(this).is(":checked")){
                            $('.owner').show()
                        }else{
                            $('.owner').hide()
                        }
                    });

                    $('.submit-certificate').off('click').on('click',function(){
                        var base_url = "{{url('version2/pedigree-certificate/')}}";
                        // var base_url = 'http://3.123.126.62/certification/public';
                        var name = $('#name').val() !=="" || $('#name').val()  === undefined  ? "name="+$('#name').val()+'&':"";
                        var phone = $('#phone').val() !=="" || $('#phone').val() === undefined ? "phone="+$('#phone').val()+'&':"";
                        var address = $('#address').val() !=="" || $('#address').val() === undefined ?"address="+$('#address').val():"";
//                        alert(base_url+'/'+dog_id+'?'+name+phone+address);
                        location.href=base_url+'/'+dog_id+'?'+name+phone+address;
                        $('.add-owner-detail-modal').modal("hide");
                        // location.href=base_url+'?name='+dog_name+'&id='+dog_id;
                    })
                })
            });

            function addButtons(){
                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
            }
        });

    </script>

    @endsection


    @section('content')
            <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <div class="container">
                    <!-- BEGIN PAGE TITLE -->
                    <div class="page-title">
                        <h1>All Dogs
                            {{--<small>managed datatable samples</small>--}}
                        </h1>
                    </div>
                    <!-- END PAGE TITLE -->

                </div>
            </div>
            <!-- END PAGE HEAD-->

            <!-- BEGIN PAGE CONTENT BODY -->
            <div class="page-content">
                <div class="container">
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="{{url('/version2')}}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        {{--<li>--}}
                        {{--<a href="#">More</a>--}}
                        {{--<i class="fa fa-circle"></i>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                        {{--<a href="#">Tables</a>--}}
                        {{--<i class="fa fa-circle"></i>--}}
                        {{--</li>--}}
                        <li>
                            <span>All Dogs</span>
                        </li>
                    </ul>

                    <!-- END PAGE BREADCRUMBS -->
                    <!-- BEGIN PAGE CONTENT INNER -->
                    <div class="page-content-inner">
                        {{--<div class="m-heading-1 border-green m-bordered">--}}
                            {{--<h3>DataTables jQuery Plugin</h3>--}}
                            {{--<p> DataTables is a plug-in for the jQuery Javascript library. It is a highly flexible tool, based upon the foundations of progressive enhancement, and will add advanced interaction controls to any HTML table. </p>--}}
                            {{--<p> For more info please check out--}}
                                {{--<a class="btn red btn-outline" href="http://datatables.net/" target="_blank">the official documentation</a>--}}
                            {{--</p>--}}
                        {{--</div>--}}

                        <div class="row">
                            <div class="col-md-12">
                                @include('flash::message')

                                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light ">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            {{--<i class="icon-settings font-dark"></i>--}}
                                            <span class="caption-subject bold uppercase"> {{\App\Dog::count()}} dogs registered</span>
                                        </div>
                                        {{--<div class="actions">--}}
                                            {{--<div class="btn-group btn-group-devided" data-toggle="buttons">--}}
                                                {{--<label class="btn btn-transparent dark btn-outline btn-circle btn-sm active">--}}
                                                    {{--<input type="radio" name="options" class="toggle" id="option1">Actions</label>--}}
                                                {{--<label class="btn btn-transparent dark btn-outline btn-circle btn-sm">--}}
                                                    {{--<input type="radio" name="options" class="toggle" id="option2">Settings</label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="btn-group">
                                                        <a href="{{url('version2/register-new-dog')}}" id="sample_editable_1_new" class="btn sbold green"> Add Dog
                                                            <i class="fa fa-plus"></i>
                                                        </a>


                                                        <a href="{{url('version2/register-litter')}}" id="sample_editable_1_new" class="btn sbold yellow"> Add Litter
                                                            <i class="fa fa-plus"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="btn-group pull-right">
                                                        <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Query by
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a href="{{url('/version2/all-dogs?dogs=confirmed')}}">
                                                                    {{--<i class="fa fa-print"></i> --}}
                                                                    Confirmed only </a>
                                                            </li>

                                                            <li>
                                                                <a href="{{url('/version2/all-dogs?dogs=unconfirmed')}}">

                                                                    {{--<i class="fa fa-print"></i> --}}
                                                                    Unconfirmed only </a>
                                                            </li>

                                                            <li>
                                                                <a href="{{url('/version2/all-dogs?dogs=male')}}">

                                                                    {{--<i class="fa fa-print"></i> --}}
                                                                    Males only </a>
                                                            </li>

                                                            <li>
                                                                <a href="{{url('/version2/all-dogs?dogs=female')}}">

                                                                    {{--<i class="fa fa-print"></i> --}}
                                                                    Females only </a>
                                                            </li>


                                                            {{--<li>--}}
                                                            {{--<a href="javascript:;">--}}
                                                            {{--<i class="fa fa-file-excel-o"></i>  </a>--}}
                                                            {{--</li>--}}
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover order-column" id="datatable-buttons">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Date of Birth</th>
                                                <th>Breed </th>
                                                <th>Sex </th>
                                                <th>Registration number </th>
                                                <th>Breeder</th>
                                                <th>Status</th>
                                                <th>Certificate</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>

                    </div>
                    <!-- END PAGE CONTENT INNER -->
                </div>
            </div>
            <!-- END PAGE CONTENT BODY -->
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
    <!-- END CONTAINER -->

    <div class="modal fade" id="report-modal" tabindex="-1" role="dialog" aria-labelledby="report-ModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><strong>REPORT DUPLICATED REGISTRATION NUMBER</strong></h4>
                </div>
                <div class="modal-body">
                    <form id="report-form">
                        <p>We experienced some challenges with dogs registration numbers.
                            Report any of such incident here with the registration number.</p>
                        <label>Registration Number</label>
                        <input type="number" class="form-control" required id="report-input" name="report_registration_number" placeholder="Suspected dog registration number">

                        <div class="report-message h3"></div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-default" id="report">Report</button>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="delete-ModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><strong>Delete  <span class="delete-name"></span> </strong></h4>
                </div>
                <div class="modal-body">
                    <form method="post" id="delete-dog-form" >
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-2">
                                <label>This Action will delete <em><span class="delete-name"></span></em> <br><br>
                                    {{--Are you sure you want to continue with this action ?--}}
                                </label>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Delete <span class="delete-name"></span></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="confirm-modal" tabindex="-1" role="dialog" aria-labelledby="confirm-ModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><strong><span class="confirm-name"></span> </strong></h4>
                </div>
                <div class="modal-body">
                    <form id="confirm-dog-form" method="post">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-sm-3 col-sm-offset-2">

                                <label>Confirm Action ?</label>
                            </div>
                            <div class="col-sm-4">
                                <select class="form-control" name="status">
                                    {{--<option value="{{'0'}}">Revoke</option>--}}
                                    <option value="{{'1'}}">Confirm</option>

                                </select>
                            </div>
                            <br>
                            <br>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Confirm action</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade add-owner-detail-modal" id="add-owner-detail-modal" tabindex="-1" role="dialog" aria-labelledby="add-owner-detail-ModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><strong><span class="confirm-name"></span> </strong></h4>
                </div>
                <div class="modal-body">
                    {{--<form id="confirm-dog-form">--}}
                    {{--{!! csrf_field() !!}--}}
                    <div class="row">
                        <div class="col-sm-12">

{{--                            <label>Breeder is different from owner </label>--}}
                            <label>Download certificate? </label>
                        </div>
{{--                        <div class="col-sm-4">--}}
{{--                            <input type="checkbox" name="new_owner" id="new_owner">--}}
{{--                        </div>--}}
                    </div>

{{--                    <div class="owner">--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-md-offset-3 col-md-7"><input type="text" class="form-control" id="name" name="name" placeholder="new owner's name" required></div><br><br>--}}
{{--                            <div class="col-md-7 col-md-offset-3"><input type="text" class="form-control" id="phone" name="phone" placeholder="new owner's phone" required></div><br><br>--}}
{{--                            <div class="col-md-7 col-md-offset-3"><input type="text" class="form-control" id="address" name="address" placeholder="new owner's address" required></div><br><br><br>--}}
{{--                        </div>--}}

{{--                    </div>--}}

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success submit-certificate">Yes</button>
                    </div>
                    {{--</form>--}}
                </div>
            </div>
        </div>
    </div>

@endsection