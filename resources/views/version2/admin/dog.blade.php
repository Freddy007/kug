@extends('version2.layouts.admin_layout')


@section('scripts')

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.min.js"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

    <script>
        $("#pedigree_table").on("click",function (e) {
            e.preventDefault();
            location.href = "/main-table/?id=" + "{{$dog->id}}";
        });
    </script>

@endsection

@section('content')
        <!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Edit dog
                        {{--<small>managed datatable samples</small>--}}
                    </h1>
                </div>
                <!-- END PAGE TITLE -->

            </div>
        </div>
        <!-- END PAGE HEAD-->

        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{url('/version2')}}">Dashboard</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{url('/version2/all-dogs')}}">all dogs</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>dog</span>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">

                    <div class="row">
                        <div class="col-md-12">
                            @include('flash::message')
                            <div class="portlet-body">

                                <div class="row">
                                    <table class="table-responsive">

                                        <table class="table table-striped table-bordered thumbnail-table">
                                            <tr>
                                                <td>Name</td>
                                                <td>
                                                    {{$dog->name }}
                                                </td>
                                                <td></td>
                                            </tr>

                                            <tr>
                                                <td>image</td>
                                                <td>
                                                    {{--<figure><img src="/images/catalog/{{$dog->image_name}}" style="width: 200px; height: 150px;"></figure>--}}
                                                    <?php $image = $dog->image_name == null  ? "http://placehold.it/100x100" : "/images/catalog/$dog->image_name"  ?>
                                                    <img src="{{$image}}" style="width: 120px; height: 70px;" alt="Gallery Image">
                                                </td>
                                                <td></td>
                                            </tr>

                                            <tr>
                                                <td>Breed</td>
                                                <td>{{$dog->breeder_name}}</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>Sex</td>
                                                <td>{{$dog->sex}}</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>Registration Number</td>
                                                <td>{{$dog->registration_number}}</td>
                                                <td></td>
                                            </tr>

                                            <tr>
                                                <td>Microchip Number</td>
                                                <td>{{$dog->microchip_number}}</td>
                                                <td></td>
                                            </tr>

                                            <tr>
                                                <td>Colour</td>
                                                <td>{{$dog->colour}}</td>
                                                <td></td>
                                            </tr>

                                            <tr>
                                                <td>Height(inches)</td>
                                                <td>
                                                    @if($dog->height == 0 )
                                                        <em> height not set</em>
                                                    @else
                                                        {{$dog->height}}
                                                    @endif

                                                </td>
                                                <td></td>
                                            </tr>

                                            <tr>
                                                <td>Coat</td>
                                                <td>{{$dog->coat}}</td>
                                                <td></td>
                                            </tr>

                                            <tr>
                                                <td>Tattoo Number</td>
                                                <td>{{$dog->tattoo_number}}</td>
                                                <td></td>
                                            </tr>

                                            <tr>
                                                <td>DNA ID</td>
                                                <td>{{$dog->DNA}}</td>
                                                <td></td>
                                            </tr>

                                            <tr>
                                                <td>Elbow ED Results</td>
                                                <td>{{$dog->elbow_ed_results}}</td>
                                                <td></td>
                                            </tr>

                                            <tr>
                                                <td>Appraisal Score</td>
                                                <td>
                                                    @if($dog->appraisal_score == 0)
                                                        <em>Appraisal score not set</em>
                                                    @else
                                                        {{$dog->appraisal_score}}
                                                    @endif
                                                </td>
                                                <td></td>
                                            </tr>

                                            <tr>
                                                <td>Other Health Checks</td>
                                                <td>{{$dog->other_health_checks}}</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>Titles</td>
                                                <td>{{$dog->titles}}</td>
                                                <td></td>
                                            </tr>

                                            <tr>
                                                <td>Performance Titles</td>
                                                <td>{{$dog->performance_titles}}</td>
                                                <td></td>
                                            </tr>

                                            <tr>
                                                <td>Registered Date</td>
                                                <td>{{$dog->created_at}}</td>
                                                <td></td>
                                            </tr>

                                            <tr>
                                                <td>Status</td>
                                                @if($dog->dead == true)
                                                    <strong>  <td>{{'Dead'}}</td></strong>
                                                    <td></td>
                                                @else
                                                    <strong> <td>{{'Alive'}}</td></strong>
                                                    <td></td>
                                                @endif

                                            </tr>

                                            <tr>
                                                <td >Member Details</td>
                                                <td>
                                                    <p>{{$dog->member}}</p>
                                                    <p>{{$dog->kennel_name}}</p>
                                                    <p>{{$dog->phone}}</p>
                                                    <p>{{$dog->email}}</p>
                                                </td>
                                                <td></td>
                                            </tr>
                                </div>

                                </table>

                                    <h1 class="text-center">
                                        <a id="pedigree_table" class="btn btn-success btn-lg" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                            Pedigree Information
                                        </a>

                                    </h1>
                                    <div class="collapse" id="collapseExample">
                                        <div id="pedigree-table">
                                            @include('partials.table_minimal')
                                        </div>
                                    </div>
                            </div>

                        </div>
                    </div>

                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->

@endsection