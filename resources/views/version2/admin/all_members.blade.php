@extends('version2.layouts.admin_layout')

@section('styles')

    <link href="{{asset('kug_version2/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />

@endsection

@section('scripts')
    {{--<script src="{{asset('datatables/jquery.dataTables.min.js')}}"></script>--}}

    <script src="{{asset('kug_version2/assets/layouts/global/scripts/quick-sidebar.min.js')}}" type="text/javascript"></script>


    <!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{asset('kug_version2/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
<script src="{{asset('kug_version2/assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('kug_version2/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('kug_version2/assets/pages/scripts/table-datatables-managed.min.js')}}" type="text/javascript"></script>


    <script>
        jQuery(document).ready(function(){

            var query = "{{$query}}";

            var table = $('#datatable-buttons').DataTable({
//                buttons: ['copy', 'excel', 'pdf'],

                processing: true,
                serverSide: true,
                ajax: '/version2/members-data'+query,
                columns: [

                    { data: "name",name:'name'},
                    { data: "kennel_name",name:'kennel_name' },
//                    { data: "email", name:'email' },
                    { data: "phone",name:'phone' },
                    { data: "created_at", name:'created_at'},
                    {data : 'dogs', name: 'dogs'},
                    {data : "role", name:'role'},
                    {data : "status", name:'status'},
                    {data : 'action', name : 'action'}
                ],
                "initComplete": function( settings, json ) {

                    registerDeleteEvent();
                },
            });

            registerDeleteEvent();


//            function addButtons(){
//                table.buttons().container()
//                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
//            }


            function registerDeleteEvent(){
                $.get('/version2/members-data',function(xhr){
                    $.each(xhr.data, function(index, element) {
                        $('#delete-member-'+element.id).off('click').on('click',function(){
//                            console.log(element.id);
//                            alert(element.id);
                            $('#delete-modal').modal();
                            $('.delete-name').text(element.name);
                            $('#delete-member-form').attr('action','/admin/delete-member/'+element.id);
                        })
                        $('#confirm-member-'+element.id).off('click').on('click',function(){
                            $('#confirm-modal').modal();
                            $('.confirm-name').text(element.name);
                            $('#confirm-member-form').attr('action','/admin/confirm/'+element.id);
                        })
                    });
                })
            }

            table.on( 'draw.dt', function () {
                console.log( 'Redraw occurred at: '+new Date().getTime() );
                registerDeleteEvent();

            });
        });

    </script>

    @endsection


@section('content')
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>All Members
                        {{--<small>managed datatable samples</small>--}}
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
                <!-- BEGIN PAGE TOOLBAR -->
                <div class="page-toolbar">

                </div>
                <!-- END PAGE TOOLBAR -->
            </div>
        </div>
        <!-- END PAGE HEAD-->

                <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{url('/version2')}}">Dashboard</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    {{--<li>--}}
                    {{--<a href="#">More</a>--}}
                    {{--<i class="fa fa-circle"></i>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                    {{--<a href="#">Tables</a>--}}
                    {{--<i class="fa fa-circle"></i>--}}
                    {{--</li>--}}
                    <li>
                        <span>All Members</span>
                    </li>
                </ul>

                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    {{--<div class="m-heading-1 border-green m-bordered">--}}
                        {{--<h3>DataTables jQuery Plugin</h3>--}}
                        {{--<p> DataTables is a plug-in for the jQuery Javascript library. It is a highly flexible tool, based upon the foundations of progressive enhancement, and will add advanced interaction controls to any HTML table. </p>--}}
                        {{--<p> For more info please check out--}}
                            {{--<a class="btn red btn-outline" href="http://datatables.net/" target="_blank">the official documentation</a>--}}
                        {{--</p>--}}
                    {{--</div>--}}
                    <div class="row">
                        <div class="col-md-12">
                            @include('flash::message')

                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        {{--<i class="icon-settings font-dark"></i>--}}
                                        <span class="caption-subject bold uppercase"> Members</span>
                                    </div>
                                    <div class="actions">
                                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                                            <label class="btn btn-transparent dark btn-outline btn-circle btn-sm active">
                                                <input type="radio" name="options" class="toggle" id="option1">Actions</label>
                                            <label class="btn btn-transparent dark btn-outline btn-circle btn-sm">
                                                <input type="radio" name="options" class="toggle" id="option2">Settings</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="btn-group">
                                                    <button id="sample_editable_1_new" class="btn sbold green"> Add New
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="btn-group pull-right">
                                                        <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Query by
                                                        <i class="fa fa-angle-down"></i>
                                                    </button>
                                                    <ul class="dropdown-menu pull-right">
                                                        <li>
                                                            <a href="{{url('/version2/all-members?members=confirmed')}}">
                                                                {{--<i class="fa fa-print"></i> --}}
                                                                Confirmed only </a>
                                                        </li>

                                                        <li>
                                                            <a href="{{url('/version2/all-members?members=unconfirmed')}}">

                                                            {{--<i class="fa fa-print"></i> --}}
                                                                Unconfirmed only </a>
                                                        </li>
                                                        <li>
                                                            <a href="{{url('/version2/all-members?members=administrator')}}">

                                                                {{--<i class="fa fa-file-pdf-o"></i>--}}
                                                                Administrators only  </a>
                                                        </li>

                                                        <li>
                                                            <a href="{{url('/version2/all-members?members=member')}}">
                                                                {{--<i class="fa fa-file-pdf-o"></i>--}}
                                                                Members only </a>
                                                        </li>
                                                        {{--<li>--}}
                                                            {{--<a href="javascript:;">--}}
                                                                {{--<i class="fa fa-file-excel-o"></i>  </a>--}}
                                                        {{--</li>--}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <table class="table table-striped table-bordered table-hover order-column" id="datatable-buttons">
                                        <thead>
                                        <tr>
                                            {{--<th>No.</th>--}}
                                            <th> Name</th>
                                            {{--<th>Last Name</th>--}}
                                            <th>Kennel Name</th>
                                            {{--<th>Email</th>--}}
                                            <th>Phone</th>
                                            <th>Registered Date</th>
                                            <th>Dog(s)</th>
                                            <th>Role</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>

                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->

    <div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="delete-ModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><strong>Delete  <span class="delete-name"></span> </strong></h4>
                </div>
                <div class="modal-body">
                    <form method="post" id="delete-member-form" >
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-2">
                                <label>This Action will delete <em><span class="delete-name"></span></em> and dogs submitted .
                                    {{--<br><br>--}}
                                    {{--Are you sure you want to continue with this action ?--}}
                                </label>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Delete <span class="delete-name"></span></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="confirm-modal" tabindex="-1" role="dialog" aria-labelledby="confirm-ModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><strong><span class="confirm-name"></span> </strong></h4>
                </div>
                <div class="modal-body">
                    <form id="confirm-member-form" method="post">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-sm-3 col-sm-offset-2">

                                <label>Confirm Action ?</label>
                            </div>
                            <div class="col-sm-4">
                                <select class="form-control" name="status">
                                    <option value="{{'0'}}">Revoke</option>
                                    <option value="{{'1'}}">Confirm</option>

                                </select>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Effect Change</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection