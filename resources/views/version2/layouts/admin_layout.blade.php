<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="utf-8" />
    <title>Pedigree Database</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet" type="text/css" />
    <link href="{{asset('kug_version2/assets/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('kug_version2/assets/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('kug_version2/assets/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('kug_version2/assets/global/plugins/uniform/css/uniform.default.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('kug_version2/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->


    <link href="{{asset('kug_version2/assets/pages/css/error.min.css')}}" rel="stylesheet" type="text/css" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" rel="stylesheet" type="text/css" />

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{asset('kug_version2/assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('kug_version2/assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{asset('kug_version2/assets/global/css/components-rounded.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />
    <link href="{{asset('kug_version2/assets/global/css/plugins.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="{{asset('kug_version2/assets/layouts/layout3/css/layout.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('kug_version2/assets/layouts/layout3/css/themes/default.min.css')}}" rel="stylesheet" type="text/css" id="style_color" />
    <link href="{{asset('kug_version2/assets/layouts/layout3/css/custom.min.css')}}" rel="stylesheet" type="text/css" />

    <style>
        .page-header .page-header-menu {
            background: #444d58;
            margin-top: 20px !important;
        }

        .page-header .page-header-top .page-logo {
            margin-top: 6px !important;
        }
    </style>

<!-- END HEAD -->

@yield('styles')

<body class="page-container-bg-solid page-boxed">
<!-- BEGIN HEADER -->
<div class="page-header">
    <!-- BEGIN HEADER TOP -->
    <div class="page-header-top">
        <div class="container">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <a href="{{url('/')}}" style="margin-bottom: 15px">
                    <img src="{{asset('img/logo_changed.png')}}" alt="logo" style="margin-bottom: 15px; padding-bottom: 20px">
                </a>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler"></a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    @if(Auth::check() && Auth::user()->administrator == 1)
                    <!-- BEGIN NOTIFICATION DROPDOWN -->
                    <li class="dropdown dropdown-extended dropdown-notification dropdown-dark" id="header_notification_bar">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <i class="icon-bell"></i>
                            <span class="badge badge-default">
                                {{$total_notif = \App\Dog::checkUnconfirmedDogs() + \App\User::checkUnconfirmedMembers() + \App\RequestedCertificate::getRequestedCertificates()}}

                            </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="external">
                                <h3>You have
                                    <strong>{{$total_notif}} pending</strong> tasks</h3>
                                <a href="">view all</a>
                            </li>
                            <li>
                                <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">

                                    @if(\App\User::checkUnconfirmedMembers() == 0)

                                    @else

                                        <li>
                                            <a href="{{url('/version2/all-members?members=unconfirmed')}}">
                                                <span class="time">
{{--                                                    {{ getMemberRegisteredDay(\App\User::getLastMemberTime()) }}--}}
                                                </span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-success">
                                                            <i class="fa fa-user"></i>
                                                        </span>
                                                        {{\App\User::checkUnconfirmedMembers()}} new member(s).
                                                    </span>
                                            </a>
                                        </li>

                                    @endif

                                        @if(\App\Dog::checkUnconfirmedDogs() == 0)

                                        @else

                                            <li>
                                                <a href="{{url('/version2/all-dogs?dogs=unconfirmed')}}">
                                                    <span class="time">
{{--                                                    {{ getDogRegisteredDay(\App\User::getLastMemberTime()) }}--}}
                                                    </span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-danger">
                                                            <i class="fa fa-paw"></i>
                                                        </span> {{\App\Dog::checkUnconfirmedDogs()}} New Dog(s)</span>
                                                </a>
                                            </li>
                                        @endif


                                    <li>
                                        <a href="{{url('/version2/certificate-requests')}}">
                                            {{--<span class="time">3 mins</span>--}}
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-default">
                                                            <i class="fa fa-certificate"></i>
                                                        </span>
                                                        {{\App\RequestedCertificate::getRequestedCertificates()}} New Certificates Requests
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            {{--</li>--}}
                        </ul>
                    </li>
                    <!-- END NOTIFICATION DROPDOWN -->

                    <li class="droddown dropdown-separator">
                        <span class="separator"></span>
                    </li>
                    @endif
                    <!-- BEGIN INBOX DROPDOWN -->
                    <li class="dropdown dropdown-extended dropdown-inbox dropdown-dark" id="header_inbox_bar">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <span class="circle">3</span>
                            <span class="corner"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="external">
                                <h3>
                                    {{--You have--}}
                                    {{--<strong>7 New</strong> Messages</h3>--}}
                                    {{--<strong>New</strong> --}}
                                    Notitifcations feature</h3>
                                <a href="#">view all</a>
                            </li>
                            <li>
                                <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                                    <li>
                                        <a href="#">
                                                    <span class="photo">
{{--                                                        <img src="{{asset('kug_version2/assets/layouts/layout3/img/avatar2.jpg" class="img-circle" alt=""')}}"> --}}
</span>
                                                    <span class="subject">
                                                        <span class="from"> Notification </span>
                                                        <span class="time">Just Now </span>
                                                    </span>
                                            <span class="message"> Feature under construction </span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <!-- END INBOX DROPDOWN -->
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
{{--                            <img alt="" class="img-circle" src="{{asset('kug_version2/assets/layouts/layout3/img/avatar9.jpg"')}}">--}}
                            <span class="username username-hide-mobile">{{Auth::user()->first_name}}</span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">

                            @if(Auth::check() && Auth::user()->administrator == 1)
                            <li>
                                <a href="{{url('version2/profile')}}">
                                    <i class="icon-user"></i> My Profile </a>
                            </li>

                                <li class="divider"> </li>


                            @endif
                            <li>
                                <a href="{{url('version2/user-dogs',Auth::id())}}">
                                    <i class="fa fa-paw"></i> My Dogs </a>
                            </li>

                            <li class="divider"> </li>
                            <li>
                                <a href="{{url('auth/logout')}}">
                                    <i class="icon-key"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
    </div>
    <!-- END HEADER TOP -->
    <!-- BEGIN HEADER MENU -->
    <div class="page-header-menu">
        <div class="container">
            <!-- BEGIN HEADER SEARCH BOX -->
            {{--<form class="search-form" action="" method="GET">--}}
                {{--<div class="input-group">--}}
                    {{--<input type="text" class="form-control" placeholder="Search" name="query">--}}
                            {{--<span class="input-group-btn">--}}
                                {{--<a href="javascript:;" class="btn submit">--}}
                                    {{--<i class="icon-magnifier"></i>--}}
                                {{--</a>--}}
                            {{--</span>--}}
                {{--</div>--}}
            {{--</form>--}}
            <!-- END HEADER SEARCH BOX -->
            <!-- BEGIN MEGA MENU -->
            <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
            <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
            <div class="hor-menu  ">

                @if(Auth::check() && Auth::user()->administrator == true)
                    {!! $AdminNav->asUl(array('class' => 'nav navbar-nav')) !!}

                @else

                    {!! $MemberNav->asUl(array('class' => 'nav navbar-nav')) !!}


                @endif


            </div>
            <!-- END MEGA MENU -->
        </div>
    </div>
    <!-- END HEADER MENU -->
</div>
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->


        @yield('content')

        <!-- END PAGE CONTENT BODY -->

</div>

<div class="page-footer">
    <div class="container"> {{date('Y')}} &copy; Kennel Union of Ghana.</div>
</div>
<div class="scroll-to-top">
    <i class="icon-arrow-up"></i>
</div>
<!-- END INNER FOOTER -->
<!-- END FOOTER -->
<!--[if lt IE 9]>

<script src="{{asset('kug_version2/assets/global/plugins/respond.min.js')}}"></script>
<script src="{{asset('kug_version2/assets/global/plugins/excanvas.min.js')}}"></script>
<![endif]-->

<script src="{{asset('kug_version2/assets/global/plugins/jquery.min.js')}}" type="text/javascript"></script>

<!-- BEGIN CORE PLUGINS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js"></script>

<script src="{{asset('kug_version2/assets/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('kug_version2/assets/global/plugins/js.cookie.min.js')}}" type="text/javascript"></script>
<script src="{{asset('kug_version2/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')}}" type="text/javascript"></script>
<script src="{{asset('kug_version2/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
<script src="{{asset('kug_version2/assets/global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('kug_version2/assets/global/plugins/uniform/jquery.uniform.min.js')}}" type="text/javascript"></script>
<script src="{{asset('kug_version2/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="{{asset('kug_version2/assets/global/scripts/app.min.js')}}" type="text/javascript"></script>
{{--<script src="{{asset('js/vue.js')}}" type="text/javascript"></script>--}}
<!-- END THEME GLOBAL SCRIPTS -->

<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="{{asset('kug_version2/assets/global/scripts/app.min.js')}}" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="{{asset('kug_version2/assets/layouts/layout3/scripts/layout.min.js')}}" type="text/javascript"></script>
<script src="{{asset('kug_version2/assets/layouts/layout3/scripts/demo.min.js')}}" type="text/javascript"></script>
<script src="{{asset('kug_version2/assets/layouts/global/scripts/quick-sidebar.min.js')}}" type="text/javascript"></script>
<script>
    window.Laravel = <?php echo json_encode([
		'csrfToken' => csrf_token(),
	]); ?>
</script>



<!-- END THEME LAYOUT SCRIPTS -->

@yield('scripts')

<script>

</script>

</body>

</html>