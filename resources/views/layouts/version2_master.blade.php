<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Kennel Union Ghana</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    {{--<link rel="shortcut icon" href="assets/images/favicon/favicon.png">--}}
    <link rel="stylesheet" href="{{asset('kug_version2/frontend/assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('kug_version2/frontend/assets/css/responsive.css')}}">
    @yield('styles')


</head>
<body>
<!-- main wrapper -->
<div class="wrapper">

    @include('partials.version2_header')

    @yield('content')
    <!-- /enjoy our services -->
    <!-- testiomonials -->
    {{--<section class="testimonials">--}}
    {{--<div class="container">--}}
    {{--<div class="title-main"><h2 class="h2">Some Testimonails<span class="title-secondary">People Says About Us</span></h2></div>--}}
    {{--<div class="owl-carousel">--}}
    {{--<div class="item">--}}
    {{--<div class="testimonials-block_i">--}}
    {{--<div class="testimonials-block_t">Great <span>Service</span></div>--}}
    {{--<p>Old unsatiable our now but considered travelling impression. In excuse hardly summer in basket misery. By rent an part need. At wrong of of water those linen. Needed oppose seemed how all</p>--}}
    {{--</div>--}}
    {{--<div class="testimonials-block_user">--}}
    {{--<div class="user_img"><img src="kug_version2/frontend/assets/images/testimonials/mike.jpg" alt=""/></div>--}}
    {{--<div class="user_n">Garry Carlson</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="item">--}}
    {{--<div class="testimonials-block_i">--}}
    {{--<div class="testimonials-block_t">Thank You Very Much <span>I Am Happy!</span></div>--}}
    {{--<p>Certainty listening no no behaviour existence assurance situation is. Because add why not esteems amiable him. Interested the unaffected mrs law friendship add principles.</p>--}}
    {{--</div>--}}
    {{--<div class="testimonials-block_user">--}}
    {{--<div class="user_img"><img src="kug_version2/frontend/assets/images/testimonials/mila.png" alt=""/></div>--}}
    {{--<div class="user_n">Mila Markovna</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</section>--}}
    <!-- /testiomonials -->
    <!-- map -->
    {{--<section class="map">--}}
    {{--<div id="map"></div>--}}
    {{--</section>--}}
    <!-- /map -->
    <!-- /main wrapper -->
    <!-- footer -->
    <footer class="footer">
        {{--<div class="footer-top">--}}
        {{--<div class="container">--}}
        {{--<div class="row">--}}
        {{--<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">--}}
        {{--<div class="footer-top_logo"><a href="index-2.html">--}}
        {{--<img src="{{asset('kug_version2/frontend/assets/images/logo/logo.png')}}" alt="Footer logo"/>--}}
        {{--</a></div>--}}
        {{--<div class="footer-top_txt">--}}
        {{--<p>Continual delighted as elsewhere am convinced unfeeling. Introduced stimulated attachment no by projection. To lady whom my mile sold four need introduced.</p>--}}
        {{--</div>--}}
        {{--<div class="footer-top_address">--}}
        {{--<div><i class="fa fa-phone"></i> Phone: <span>8 (043) 567 - 89 - 30</span></div>--}}
        {{--<div><i class="fa fa-envelope-o"></i> E-mail: <span><a href="mailto:support@email.com">support@email.com</a></span></div>--}}
        {{--<div><i class="fa fa-home"></i> Location: <span>455 Martinson, Los Angeles</span></div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">--}}
        {{--<div class="footer-top_rooms">--}}
        {{--<ul>--}}
        {{--<li>--}}
        {{--<div class="rooms_img">--}}
        {{--<a href="blog-detail.html"><img src="{{asset('kug_version2/frontend/assets/images/footer/1.jpg')}}" alt=""/></a>--}}
        {{--</div>--}}
        {{--<div class="rooms_info">--}}
        {{--<div class="rooms_t"><a href="blog-detail.html">Grang Super Luxury</a></div>--}}
        {{--<div class="rooms_props">3 Bed  /  Wi-Fi  /  2 - Bathroom<span>$119</span></div>--}}
        {{--</div>--}}
        {{--</li>--}}
        {{--<li>--}}
        {{--<div class="rooms_img">--}}
        {{--<a href="blog-detail.html"><img src="assets/images/footer/2.jpg" alt=""/></a>--}}
        {{--</div>--}}
        {{--<div class="rooms_info">--}}
        {{--<div class="rooms_t"><a href="blog-detail.html">President Room</a></div>--}}
        {{--<div class="rooms_props">4 Bed  /  Wi-Fi  /  3 - Bathroom<span>$329</span></div>--}}
        {{--</div>--}}
        {{--</li>--}}
        {{--<li>--}}
        {{--<div class="rooms_img">--}}
        {{--<a href="blog-detail.html"><img src="assets/images/footer/3.jpg" alt=""/></a>--}}
        {{--</div>--}}
        {{--<div class="rooms_info">--}}
        {{--<div class="rooms_t"><a href="blog-detail.html">Spa Double Room</a></div>--}}
        {{--<div class="rooms_props">2 Bed  /  Wi-Fi  /  2 - Bathroom<span>$749</span></div>--}}
        {{--</div>--}}
        {{--</li>--}}
        {{--</ul>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-lg-4 col-md-4 col-sm-12 marg-sm-50 col-xs-12">--}}
        {{--<div class="footer-top_contact_form">--}}
        {{--<div class="contact_form_t">Contact Form</div>--}}
        {{--<form action="http://html.dankov-theme.com/deluxhotel/mail.php" method="POST" class="row form-horizontal form-wizzard">--}}
        {{--<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">--}}
        {{--<input type="text" name="name" class="form-control" placeholder="Name ..."/>--}}
        {{--</div>--}}
        {{--<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">--}}
        {{--<input type="email" name="email" class="form-control" placeholder="Email ..."/>--}}
        {{--</div>--}}
        {{--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">--}}
        {{--<textarea rows="6" name="message" class="form-control" placeholder="Message ..."></textarea>--}}
        {{--</div>--}}
        {{--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">--}}
        {{--<input type="submit" value="Send message" class="btn btn-default"/>--}}
        {{--</div>--}}
        {{--</form>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="footer-bottom_copy">Copyright © {{date('Y')}} Kennel Union of Ghana.
                            {{--Design by <a href="https://themeforest.net/user/Dankov" target="_blank">Dankov</a>--}}
                        </div>
                    </div>
                    {{--<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">--}}
                    {{--<div class="footer-bottom_links">--}}
                    {{--<a class="active" href="index-2.html">Home</a>--}}
                    {{--<a href="gallery.html">Gallery</a>--}}
                    {{--<a href="blog.html">Blog</a>--}}
                    {{--<a href="wizzard-step1.html">Reservation</a>--}}
                    {{--<a href="#">Purchase</a>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </footer>
</div>
<!-- /footer -->
<!-- Scripts -->
<script type="text/javascript" src="{{asset('kug_version2/frontend/assets/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('kug_version2/frontend/assets/js/tether.min.js')}}"></script>
<script type="text/javascript" src="{{asset('kug_version2/frontend/assets/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('kug_version2/frontend/assets/js/jquery-ui.min.js')}}"></script>
<script type="text/javascript" src="{{asset('kug_version2/frontend/assets/js/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('kug_version2/frontend/assets/js/jquery.smartmenus.js')}}"></script>
<script type="text/javascript" src="{{asset('kug_version2/frontend/assets/js/jquery.parallax.min.js')}}"></script>
<script type="text/javascript" src="{{asset('kug_version2/frontend/assets/js/jquery.shuffle.min.js')}}"></script>
<script type="text/javascript" src="{{asset('kug_version2/frontend/assets/js/owl.carousel.min.js')}}"></script>
{{--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>--}}
<script type="text/javascript" src="{{asset('kug_version2/frontend/assets/js/map.js')}}"></script>
<script type="text/javascript" src="{{asset('kug_version2/frontend/assets/js/main.js')}}"></script>
<!-- /Scripts -->

@yield('scripts')
</body>

</html>