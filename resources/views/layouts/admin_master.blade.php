<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->

<head>
    <title>KUG PEDIGREE DATABASE</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    {{--<meta name="description" content="">--}}
    {{--<meta name="author" content="">--}}
    {!! \Artesaos\SEOTools\Facades\SEOMeta::generate() !!}
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Google Font: Open Sans -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,800,800italic">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:400,300,700">

    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="../../bower_components/fontawesome/css/font-awesome.min.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">

    <link rel="stylesheet" href="{{asset('../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css')}}">

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="{{asset('../../bower_components/select2/dist/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('../../bower_components/select2-bootstrap-theme/dist/select2-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('../../bower_components/jquery-icheck/skins/minimal/_all.css')}}">
    <link rel="stylesheet" href="{{asset('../../bower_components/bootstrap-3-timepicker/css/bootstrap-timepicker.css')}}">
    <link rel="stylesheet" href="{{asset('../../bower_components/bootstrap-datepicker/css/datepicker3.css')}}">
    <link rel="stylesheet" href="{{asset('../../bower_components/bootstrap-jasny/dist/css/jasny-bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('/css/form-wizard.css')}}">
    <link rel="stylesheet" href="{{asset('/css/custom.css')}}">

    <!-- DataTables -->
    <link href="{{asset('datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="{{asset('datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />



    <!-- App CSS -->
    <link rel="stylesheet" href="{{asset('css/mvpready-admin.css')}}">
    <!-- <link rel="stylesheet" href="./css/custom.css"> -->

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class=" ">

@include('partials.header')

<div id="wrapper">
    <div class="mainnav ">

        <div class="container">

            <a class="mainnav-toggle" data-toggle="collapse" data-target=".mainnav-collapse">
                <span class="sr-only">Toggle navigation</span>
                <i class="fa fa-bars"></i>
            </a>

            <nav class="collapse mainnav-collapse" role="navigation">

                {{--<form class="mainnav-form" role="search">--}}
                    {{--<input type="text" class="form-control input-md mainnav-search-query" placeholder="Search">--}}
                    {{--<button class="btn btn-sm mainnav-form-btn"><i class="fa fa-search"></i></button>--}}
                {{--</form>--}}

                <ul class="mainnav-menu">

                    <li class="dropdown">
                        <a href="{{url('/')}}">
                            Main Website
                            {{--<i class="mainnav-caret"></i>--}}
                        </a>
                    </li>

                    <?php $index_active = isset($index) ? 'active' : ''; ?>
                    <li class="dropdown ">
                        <a href="{{url('/version2')}}">
                            Pedigree Database
                            {{--<i class="mainnav-caret"></i>--}}
                        </a>
                    </li>

                    @can('administer')
                    <?php $dashboard_active = isset($dashboard) ? 'active' : ''; ?>
                    <li class="dropdown {{$dashboard_active}}">
                        <a href="{{url('/version2')}}">
                            Dashboard
                            {{--<i class="mainnav-caret"></i>--}}
                        </a>

                    </li>


                     <?php $members_active = isset($members) ? 'active' : ''; ?>
                    <li class="dropdown {{$members_active}}">

                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
                          Members
                            <i class="mainnav-caret"></i>
                        </a>

                        <ul class="dropdown-menu" role="menu">

                            <li>
                                <a href="{{url('/version2/all-members')}}">
                                    <i class="fa fa-bars dropdown-icon "></i>
                                  All Members
                                </a>
                            </li>

                            {{--<li>--}}
                                {{--<a href="{{url('auth/register')}}">--}}
                                    {{--<i class="fa fa-plus dropdown-icon "></i>--}}
                                   {{--Add New Member--}}
                                {{--</a>--}}
                            {{--</li>--}}

                            {{--<li>--}}
                                {{--<a href="{{url('auth/register')}}">--}}
                                    {{--<i class="fa fa-plus dropdown-icon "></i>--}}
                                    {{--Recorder--}}
                                {{--</a>--}}
                            {{--</li>--}}


                        </ul>
                    </li>

                    <?php $breeders_active = isset($breeders_menu) ? 'active' : ''; ?>
                    <li class="dropdown {{$breeders_active}}">

                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">

                            Breeds
                            <i class="mainnav-caret"></i>
                        </a>

                        <ul class="dropdown-menu" role="menu">

                            <li>
                                <a href="{{url('/version2/all-breeds')}}">
                                    <i class="fa fa-bars dropdown-icon "></i>
                                   All Breeds
                                </a>
                            </li>

                            {{--<li>--}}
                                {{--<a href="components-popups.html">--}}
                                    {{--<i class="fa fa-calendar-o dropdown-icon "></i>--}}
                                    {{--Popups &amp; Alerts--}}
                                {{--</a>--}}
                            {{--</li>--}}
                        </ul>
                    </li>

                    <?php $transfers_active = isset($transfers) ? 'active' : ''; ?>
                    {{--<li class="dropdown {{$transfers_active}}">--}}

                        {{--<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">--}}

                            {{--Transfers--}}
                            {{--<i class="mainnav-caret"></i>--}}
                        {{--</a>--}}

                        {{--<ul class="dropdown-menu" role="menu">--}}

                            {{--<li>--}}
                                {{--<a href="{{url('/admin/dog-transfers')}}">--}}
                                    {{--<i class="fa fa-bars dropdown-icon "></i>--}}
                                    {{--Transferred Dogs--}}
                                {{--</a>--}}
                            {{--</li>--}}

                            {{--<li>--}}
                                {{--<a href="{{url('admin/transfer-dog')}}">--}}
                                    {{--<i class="fa fa-bars dropdown-icon "></i>--}}
                                    {{--Transfer Dog--}}
                                {{--</a>--}}
                            {{--</li>--}}

                            {{--<li>--}}
                            {{--<a href="components-popups.html">--}}
                            {{--<i class="fa fa-calendar-o dropdown-icon "></i>--}}
                            {{--Popups &amp; Alerts--}}
                            {{--</a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}

                    <?php $dogsactive = isset($dogs_active) ? 'active': ''; ?>
                    <li class="dropdown {{$dogsactive}}">

                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">

                            Dogs
                            <i class="mainnav-caret"></i>
                        </a>

                        <ul class="dropdown-menu" role="menu">

                            <li>
                                <a href="{{url('version2/all-dogs')}}">
                                    <i class="fa fa-bars dropdown-icon "></i>
                                    All Dogs
                                </a>
                            </li>

                            <li>
                                <a href="{{url('version2/register-new-dog')}}">
                                    <i class="fa fa-plus dropdown-icon "></i>
                                    Add New Dog
                                </a>
                            </li>

                            <li>
                                <a href="{{url('version2/register-litter')}}">
                                    <i class="fa fa-plus dropdown-icon "></i>
                                    Add Litter
                                </a>
                            </li>

                        </ul>


                    </li>

                    <?php $certificate_active = isset($certificate) ? 'active' : ''; ?>

                    <li class="dropdown {{$certificate_active}}">

                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">

                            Certificates
                            <i class="mainnav-caret"></i>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            {{--<li>--}}
                                {{--<a href="{{url('admin/issued-certificates')}}">--}}
                                    {{--<i class="fa fa-bars dropdown-icon "></i>--}}
                                    {{--Certificates Issued--}}
                                {{--</a>--}}
                            {{--</li>--}}

                            <li>
                                <a href="{{url('version2/certificate-requests')}}">
                                    <i class="fa fa-bars dropdown-icon "></i>
                                   Certificate Requests
                                </a>
                            </li>


                        </ul>


                    </li>
                    @elseif(Auth::check() == true)

                        <?php $manage_dogs_active = isset($manage_dogs) ? 'active' : ''; ?>

                        <li class="dropdown {{$manage_dogs_active}}">

                            <a href="{{url('/member')}}" data-hover="dropdown">
                                Manage Dogs
                            </a>

                        </li>

                        <?php $transfers_active = isset($transfers) ? 'active' : ''; ?>
                        <li class="dropdown {{$transfers_active}}">

                            {{--<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">--}}

                                {{--Transfers--}}
                                {{--<i class="mainnav-caret"></i>--}}
                            {{--</a>--}}

                            <ul class="dropdown-menu" role="menu">

                                {{--<li>--}}
                                    {{--<a href="{{url('/admin/dog-transfers')}}">--}}
                                        {{--<i class="fa fa-bars dropdown-icon "></i>--}}
                                        {{--Transferred Dogs--}}
                                    {{--</a>--}}
                                {{--</li>--}}

                                {{--<li>--}}
                                    {{--<a href="{{url('member/transfer-dog')}}">--}}
                                        {{--<i class="fa fa-bars dropdown-icon "></i>--}}
                                        {{--Transfer Dog--}}
                                    {{--</a>--}}
                                {{--</li>--}}

                                {{--<li>--}}
                                {{--<a href="components-popups.html">--}}
                                {{--<i class="fa fa-calendar-o dropdown-icon "></i>--}}
                                {{--Popups &amp; Alerts--}}
                                {{--</a>--}}
                                {{--</li>--}}
                            </ul>
                        </li>


                        @endcan

                </ul>


            </nav>

        </div> <!-- /.container -->

    </div> <!-- /.mainnav -->

    @yield('content')

</div> <!-- /#wrapper -->
@include('partials.footer')
@yield('scripts')

</body>

</html>