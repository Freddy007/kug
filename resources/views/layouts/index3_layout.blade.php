
<!DOCTYPE html>
<!--[if IE 7]>                  <html class="ie7 no-js" lang="en">     <![endif]-->
<!--[if lte IE 8]>              <html class="ie8 no-js" lang="en">     <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html class="not-ie no-js" lang="en">  <!--<![endif]-->
<head>

    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <title> @yield("title") | Kennel Union of Ghana</title>
    <meta name="description" content="Kennel Union of Ghana">
    <meta name="author" content="">


    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0">

    <!-- Google Web Fonts
    ================================================== -->
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic|Bitter:700' rel='stylesheet' type='text/css'>

{{--    <meta http-equiv="Content-Security-Policy" content="default-src http://3.122.218.55/; child-src 'none'; object-src 'none'">--}}

<!-- CSS
    ================================================== -->
    <!-- Base + Vendors CSS -->
    <link rel="stylesheet" href={{asset("css/bootstrap.min.css")}}>
    <link rel="stylesheet" href={{asset("css/font-awesome.css")}}>
    <link rel="stylesheet" href={{asset("css/entypo.css")}}>
    <link rel="stylesheet" href={{asset("vendor/owl.carousel.css")}} media="screen">
    <link rel="stylesheet" href={{asset("vendor/owl.theme.css")}} media="screen">
    <link rel="stylesheet" href={{asset("vendor/magnific-popup.css")}} media="screen">
    <link rel="stylesheet" href={{asset("vendor/flexslider.css")}} media="screen">

    <!-- Theme CSS-->
    <link rel="stylesheet" href={{asset("css/theme.css")}}>
    <link rel="stylesheet" href={{asset("css/theme-elements.css")}}>
    <link rel="stylesheet" href={{asset("css/animate.min.css")}}>


    <!-- Head Libs -->
    <script src={{asset("vendor/modernizr.js")}}></script>

    <!--[if lt IE 9]>
    <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src={{asset("vendor/respond.min.js")}}></script>
    <![endif]-->

    <!--[if IE]>
    <link rel="stylesheet" href="css/ie.css">
    <![endif]-->

    <!-- Favicons
    ================================================== -->
    {{--    <link rel="shortcut icon" href="images/favicon.ico">--}}
    {{--    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">--}}
    {{--    <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">--}}
    {{--    <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">--}}
    {{--    <link rel="apple-touch-icon" sizes="144x144" href="images/apple-touch-icon-144x144.png">--}}

</head>
<body>

<div class="site-wrapper">

    <!-- Header -->
    <header class="header header-menu-fullw">

        <div class="header-top">
            <div class="container">
                <div class="header-top-left">
                    <ul class="header-top-nav">
{{--                        <li><a href="features-pricing-tables.html">Pricing</a></li>--}}
{{--                        <li><a href="#">Sitemap</a></li>--}}
{{--                        <li><a href="pages-faqs.html">FAQs</a></li>--}}
                    </ul>
                </div>
                <div class="header-top-right">
						<span class="register">
							Have an account? <a href="/auth/login" class="btn btn-sm btn-warning">Sign In</a>
						</span>
                </div>
            </div>
        </div>
        <div class="header-main">
            <div class="container">
                <!-- Logo -->
                <div class="logo">
{{--                    <a href="/"><img src="images/logo.png" alt="PetSitter"></a>--}}
                    <a href="/"><img src="/img/logo_changed.png" alt=""></a>
                    <!-- <h1><a href="index.html"><span>Pet</span>Sitter</a></h1>
                    <p class="tagline">Find an Awesome PetSitter</p> -->
                </div>
                <!-- Logo / End -->

                <button type="button" class="navbar-toggle">
                    <i class="fa fa-bars"></i>
                </button>

                <!-- Banner -->
                <div class="head-info">
                    <ul class="head-info-list">
                        <li><span>Call us on:</span> +233 (0) 244361966</li>
                        <li><span>Email:</span> <a href="mailto:info@kennelunionofgh.com">info@kennelunionofgh.com</a></li>
                    </ul>
                    <ul class="social-links">
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-rss"></i></a></li>
                    </ul>
                </div>
                <!-- Banner / End -->
            </div>
        </div>

        <!-- Navigation -->
        <nav class="nav-main">
            <div class="container">
{{--                <ul data-breakpoint="992" class="flexnav">--}}
{{--                    <li class="active">--}}
{{--                        <a href="/">Home</a></li>--}}
{{--                    <li><a href="/contact-us">Contact Us </a></li>--}}
{{--                    <li><a href="/auth/login">Login </a></li>--}}
{{--                    <li><a href="/auth/register">Register </a></li>--}}
{{--                </ul>--}}

                @if(Auth::check() && Auth::user()->administrator)
                {!! $MainPageNav->asUl(array('data-breakpoint'=>"992",'class' => 'flexnav')) !!}
                @elseif(Auth::check() && !Auth::user()->administrator)
                    {!! $MemberNav->asUl(array('data-breakpoint'=>"992",'class' => 'flexnav')) !!}
                @else
                {!! $GuestMainPageNav->asUl(array('data-breakpoint'=>"992",'class' => 'flexnav')) !!}
                @endif

            </div>
        </nav>
        <!-- Navigation / End -->

    </header>
    <!-- Header / End -->

    @yield("content")

</div>


<!-- Javascript Files
================================================== -->
<script src="vendor/jquery-1.11.0.min.js"></script>

<script>

    $("#themenoticeframe").remove();

</script>

<script src="vendor/jquery-migrate-1.2.1.min.js"></script>
<script src="vendor/bootstrap.js"></script>
<script src="vendor/jquery.flexnav.min.js"></script>
<script src="vendor/jquery.hoverIntent.minified.js"></script>
<script src="vendor/jquery.flickrfeed.js"></script>
<script src="vendor/jquery.isotope.min.js"></script>
<script src="vendor/jquery.isotope.sloppy-masonry.min.js"></script>
<script src="vendor/jquery.imagesloaded.min.js"></script>
<script src="vendor/jquery.magnific-popup.js"></script>
<script src="vendor/owl.carousel.min.js"></script>
<script src="vendor/jquery.fitvids.js"></script>
<script src="vendor/jquery.appear.js"></script>
<script src="vendor/jquery.stellar.min.js"></script>
<script src="vendor/jquery.flexslider-min.js"></script>

<script src="js/custom.js"></script>

<!-- Newsletter Form -->
<script src="vendor/jquery.validate.js"></script>
<script src="js/newsletter.js"></script>


{{--<script language="JavaScript" type="text/javascript">--}}
{{--    //<![CDATA[--}}
{{--    window.onbeforeunload = function(){--}}
{{--        return 'Are you sure you want to leave?';--}}
{{--    };--}}
{{--    //]]>--}}
{{--</script>--}}

@yield("scripts")

<script>

    jQuery(function($){
        $('body').addClass('loading');
    });

    $(window).load(function(){
        $('.flexslider').flexslider({
            animation: "fade",
            controlNav: true,
            directionNav: false,
            prevText: "",
            nextText: "",
            start: function(slider){
                $('body').removeClass('loading');
            }
        });
    });
</script>



<script>

    // (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    //     (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    //     m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    // })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    //
    // ga('create', 'UA-49851325-1', 'dan-fisher.com');
    // ga('send', 'pageview');

</script>

</body>
</html>
