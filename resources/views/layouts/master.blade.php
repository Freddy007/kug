<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->

<!-- Mirrored from jumpstartthemes.com/demo/v/2.1.0/templates/admin/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2016 20:33:47 GMT -->
<head>
    <title>Pedigree Database</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    {{--<meta name="description" content="">--}}
    {{--<meta name="author" content="">--}}
    {!! \Artesaos\SEOTools\Facades\SEOMeta::generate() !!}

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Google Font: Open Sans -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,800,800italic">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:400,300,700">

    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="../../bower_components/fontawesome/css/font-awesome.min.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">

    <link rel="stylesheet" href="{{asset('../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css')}}">

    <link rel="stylesheet" href="{{asset('/css/custom.css')}}">

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="{{asset('../../bower_components/select2/dist/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('../../bower_components/select2-bootstrap-theme/dist/select2-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('../../bower_components/jquery-icheck/skins/minimal/_all.css')}}">
    <link rel="stylesheet" href="{{asset('../../bower_components/bootstrap-3-timepicker/css/bootstrap-timepicker.css')}}">
    <link rel="stylesheet" href="{{asset('../../bower_components/bootstrap-datepicker/css/datepicker3.css')}}">
    <link rel="stylesheet" href="{{asset('../../bower_components/bootstrap-jasny/dist/css/jasny-bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('/css/form-wizard.css')}}">
    <link rel="stylesheet" href="{{asset('../../bower_components/select2/dist/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('../../bower_components/select2-bootstrap-theme/dist/select2-bootstrap.min.css')}}">
    {{--<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/css/select2.min.css" rel="stylesheet" />--}}



    <!-- App CSS -->
    <link rel="stylesheet" href="{{asset('css/mvpready-admin.css')}}">
    <!-- <link rel="stylesheet" href="./css/custom.css"> -->

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class=" ">

@include('partials.header')

<div id="wrapper">
    <div class="mainnav ">

        <div class="container">

            <a class="mainnav-toggle" data-toggle="collapse" data-target=".mainnav-collapse">
                <span class="sr-only">Toggle navigation</span>
                <i class="fa fa-bars"></i>
            </a>

            <nav class="collapse mainnav-collapse" role="navigation">

                {{--<form class="mainnav-form" role="search">--}}
                    {{--<input type="text" class="form-control input-md mainnav-search-query" placeholder="Search">--}}
                    {{--<button class="btn btn-sm mainnav-form-btn"><i class="fa fa-search"></i></button>--}}
                {{--</form>--}}

                <ul class="mainnav-menu">

                    <li class="dropdown active">
                        <a href="{{url('/')}}" >

                            Home
                            {{--<i class="mainnav-caret"></i>--}}
                        </a>

                        {{--<ul class="dropdown-menu" role="menu">--}}
                            {{--<li>--}}
                                {{--<a href="index-2.html">--}}
                                    {{--<i class="fa fa-dashboard dropdown-icon "></i>--}}
                                    {{--Analytics Dashboard--}}
                                {{--</a>--}}
                            {{--</li>--}}

                            {{--<li>--}}
                                {{--<a href="dashboard-2.html">--}}
                                    {{--<i class="fa fa-dashboard dropdown-icon "></i>--}}
                                    {{--Sidebar Dashboard--}}
                                {{--</a>--}}
                            {{--</li>--}}

                            {{--<li>--}}
                                {{--<a href="dashboard-3.html">--}}
                                    {{--<i class="fa fa-dashboard dropdown-icon "></i>--}}
                                    {{--Reports Dashboard--}}
                                {{--</a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    </li>

                    {{--<li class="dropdown ">--}}

                        {{--<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">--}}

                            {{--Components--}}
                            {{--<i class="mainnav-caret"></i>--}}
                        {{--</a>--}}

                        {{--<ul class="dropdown-menu" role="menu">--}}

                            {{--<li>--}}
                                {{--<a href="components-tabs.html">--}}
                                    {{--<i class="fa fa-bars dropdown-icon "></i>--}}
                                    {{--Tabs &amp; Accordions--}}
                                {{--</a>--}}
                            {{--</li>--}}

                            {{--<li>--}}
                                {{--<a href="components-popups.html">--}}
                                    {{--<i class="fa fa-calendar-o dropdown-icon "></i>--}}
                                    {{--Popups &amp; Alerts--}}
                                {{--</a>--}}
                            {{--</li>--}}

                            {{--<li>--}}
                                {{--<a href="form-plugins.html">--}}
                                    {{--<i class="fa fa-plug dropdown-icon "></i>--}}
                                    {{--Forms Plugins--}}
                                {{--</a>--}}
                            {{--</li>--}}

                            {{--<li>--}}
                                {{--<a href="components-datatables.html">--}}
                                    {{--<i class="fa fa-table dropdown-icon "></i>--}}
                                    {{--Data Tables--}}
                                {{--</a>--}}
                            {{--</li>--}}

                            {{--<li>--}}
                                {{--<a href="components-charts.html">--}}
                                    {{--<i class="fa fa-bar-chart-o dropdown-icon "></i>--}}
                                    {{--Charts--}}
                                {{--</a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}

                </ul>

            </nav>

        </div> <!-- /.container -->

    </div> <!-- /.mainnav -->

  @yield('content')

</div> <!-- /#wrapper -->
@include('partials.footer')
@yield('scripts')

</body>

</html>