<table class="table table-striped" border="1" style="border: 1px outset #283846; height:60%; border-radius: 10px;">
    <tr style="color: white; background-color: #283846;">
        <th>1st </th>
        <th>2nd </th>
        <th>3rd </th>
        <th>4th </th>
        <th>5th </th>


    </tr>

    <tr>
        <td rowspan="16">

            @if ($dog->father)
                <div class="caption box">
                    Name :<strong>{{\App\Dog::getRelationship($dog->father)}}</strong>(Sire)<br/>
                    No.{{$dog->father}} <br>
                </div>

                </div>
            @endif
        </td>
        <td rowspan="8">
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($dog->father,'father')}}</strong>(Sire)<br/>
                No :{{$secondgen = \App\Dog::getParentId($dog->father,'father','registration_number')}}
            </div>


        </td>
        <td rowspan="4"><i class="identifier">right 2 third generation</i> <br>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($secondgen,'father')}}</strong>(Sire)<br/>
                No: {{$third_generation = \App\Dog::getParentId($secondgen,'father','registration_number')}}
            </div>

        </td>
        <td rowspan="2"><i class="identifier">right 3 </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($third_generation,'father')}}</strong>(Sire)<br/>
                No: {{$fifth_generation = \App\Dog::getParentId($third_generation,'father','registration_number')}}
            </div>

        </td>
        <td><i class="identifier">right 4 </i>
            <div class="caption box">
                Name :  {{\App\Dog::getParent($fifth_generation ,'father')}}<br/>
                No : {{$fifth_generation = \App\Dog::getParentId($fifth_generation,'father','registration_number')}}
                <div>

        </td>


    </tr>
    <tr>
        <td><i class="identifier">bottom right a </i>
            <div class="caption box">
                Name : {{\App\Dog::getParent($fifth_generation ,'mother')}}<br/>
                No : {{$fifth_generation = \App\Dog::getParentId($fifth_generation,'mother','registration_number')}}
            </div>
        </td>

    </tr>

    <tr>
        <td rowspan="2"><i class="identifier">new 1 </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($third_generation,'mother')}}</strong>(Dam)<br/>
                No: {{$generation_five = \App\Dog::getParentId($third_generation,'mother','registration_number')}}
            </div>
        </td>
        <td><i class="identifier">new 1 a </i>

            <div class="caption box">
                Name : {{\App\Dog::getParent($generation_five ,'father')}}<br/>

                No : {{$generation_five = \App\Dog::getParentId($generation_five,'father','registration_number')}}
            </div>


        </td>

    </tr>

    <tr>
        <td><i class="identifier">new 2 </i>
            <div class="caption box">
                Name : {{\App\Dog::getParent($generation_five ,'mother')}}

                No :  {{$generation_five = \App\Dog::getParentId($generation_five,'mother','registration_number')}}
            </div>

        </td>
    </tr>

    <tr>
        <td rowspan="4"><i class="identifier">new 9 </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($secondgen,'mother')}}</strong>(Dam)<br/>
                No: {{$fourth_generation = \App\Dog::getParentId($secondgen,'mother','registration_number')}}
            </div>

        </td>
        <td rowspan="2"><i class="identifier">9a </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($fourth_generation,'father')}}</strong>(Sire)<br/>
                No: {{$generation_five_b = \App\Dog::getParentId($fourth_generation,'father','registration_number')}}
            </div>
        </td>
        <td><i class="identifier">9b </i>
            <div class="caption box">
                Name :  {{\App\Dog::getParent($generation_five_b,'father')}}<br/>
                No : {{ \App\Dog::getParentId($generation_five_b,'father','registration_number')}}
            </div>
        </td>

    </tr>

    <tr>
        <td><i class="identifier">new 10 </i>
            <div class="caption box">
                Name : {{\App\Dog::getParent($generation_five_b,'mother')}}<br/>
                No : {{ \App\Dog::getParentId($generation_five_b,'mother','registration_number')}}
            </div>
        </td>
    </tr>

    <tr>
        <td rowspan="2"><i class="identifier">new 11 </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($fourth_generation,'mother')}}</strong>(Dam)<br>
                No: {{$generation_five_c = \App\Dog::getParentId($fourth_generation,'mother','registration_number')}}
            </div>

        </td>
        <td><i class="identifier">11a </i>
            <div class="caption box">
                Name : {{\App\Dog::getParent($generation_five_c,'father')}}
                No : {{ \App\Dog::getParentId($generation_five_c,'father','registration_number')}}
            </div>
        </td>

    </tr>

    <tr>
        <td><i class="identifier">new 13 </i>
            <div class="caption box">
                Name : {{\App\Dog::getParent($generation_five_c,'mother')}} <br/>
                No : {{ \App\Dog::getParentId($generation_five_c,'mother','registration_number')}}
            </div>
        </td>
    </tr>

    <tr>
        <td rowspan="8"><i class="identifier">new 3 second generation</i> <br>
            <div class="caption box">
                Name: <strong>{{\App\Dog::getParent($dog->father,'mother')}}</strong>
                {{$thirdgen_mother = \App\Dog::getParentId($dog->father,'mother','registration_number')}}
            </div>


        </td>
        <td rowspan="4"><i class="identifier">new 3 a third generation </i><br>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($thirdgen_mother,'father')}}</strong>(Sire)<br>
                No: {{$fourth_gen = \App\Dog::getParentId($thirdgen_mother,'father','registration_number')}}
            </div>


        </td>
        <td rowspan="2"><i class="identifier">new 3 b </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($fourth_gen,'father')}}</strong>(Sire)<br/>
                No: {{$generation_five_d = \App\Dog::getParentId($fourth_gen,'father','registration_number')}}
            </div>

        </td>
        <td><i class="identifier">new 3 c </i>
            <div class="caption box">
                Name : {{\App\Dog::getParent($generation_five_d,'father')}}<br/>
                No : {{ \App\Dog::getParentId($generation_five_d,'father','registration_number')}}
            </div>

        </td>


    </tr>

    <tr>
        <td><i class="identifier">new 4 </i>
            <div class="caption box">
                Name : {{\App\Dog::getParent($generation_five_d,'mother')}}
                No : {{ \App\Dog::getParentId($generation_five_d,'mother','registration_number')}}
            </div>
        </td>
    </tr>

    <tr>
        <td rowspan="2"><i class="identifier">new 5 </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($fourth_gen,'mother')}}</strong>(Dam)<br/>
                No: {{$generation_five_e = \App\Dog::getParentId($fourth_gen,'mother','registration_number')}}
            </div>
        </td>
        <td><i class="identifier">5a </i>
            <div class="caption box">
                Name : {{\App\Dog::getParent($generation_five_e,'father')}}<br/>
                No : {{$generation_five_e =  \App\Dog::getParentId($generation_five_e,'father','registration_number')}}
            </div>
        </td>
    </tr>
    <tr>
        <td><i class="identifier">new 6 </i>
            <div class="caption box">
                Name :  {{\App\Dog::getParent($generation_five_e,'mother')}}<br/>
                No :  {{$generation_five_e =  \App\Dog::getParentId($generation_five_e,'mother','registration_number')}}
            </div>
        </td>
    </tr>

    <tr>
        <td rowspan="4"><i class="identifier">new 7 </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($thirdgen_mother,'mother')}}</strong>(Dam)<br/>
                No: {{$generation_four = \App\Dog::getParentId($thirdgen_mother,'mother','registration_number')}}
            </div>

        </td>
        <td rowspan="2"><i class="identifier">7a </i>
            Name : <strong>{{\App\Dog::getParent($generation_four,'father')}}</strong>(Sire)<br/>
            No: {{$generation_five_f = \App\Dog::getParentId($generation_four,'father','registration_number')}}

        </td>
        <td><i class="identifier">7b </i>
            <div class="caption box">
                Name : {{\App\Dog::getParent($generation_five_f,'father')}}<br/>
                No : {{$generation_five_f = \App\Dog::getParentId($generation_five_f,'father','registration_number')}}
            </div>
        </td>

    </tr>


    <tr>
        <td><i class="identifier">8a </i>
            <div class="caption box">
                Name : {{\App\Dog::getParent($generation_five_f,'mother')}}<br/>
                No : {{$generation_five_f = \App\Dog::getParentId($generation_five_f,'mother','registration_number')}}
            </div>
        </td>
    </tr>

    <tr>
        <td rowspan="2"><i class="identifier">12a </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($generation_four,'mother')}}</strong>(Dam)<br/>
                No: {{$generation_five_g = \App\Dog::getParentId($generation_four,'mother','registration_number')}}
            </div>

        </td>
        <td><i class="identifier">12b </i>
            <div class="caption box">
                Name : {{\App\Dog::getParent($generation_five_g,'father')}}
                No : {{ \App\Dog::getParentId($generation_five_g,'father','registration_number')}}
            </div>
        </td>
    </tr>

    <tr>
        <td><i class="identifier">14a </i>
            <div class="caption box">
                Name : {{\App\Dog::getParent($generation_five_g,'mother')}}<br/>
                No : {{ \App\Dog::getParentId($generation_five_g,'mother','registration_number')}}
            </div>
        </td>


    </tr>

    <tr>
        <td rowspan="16">
            <div class="caption box">
                @if ($dog->mother)
                    Name: <strong>{{\App\Dog::getRelationship($dog->mother)}}</strong>(Dam)<br/>
                    <div class="caption">
                        No.: {{$dog->mother}}
                        @endif
                    </div>
            </div>
        </td>
        <td rowspan="8"><i class="identifier">top right 2 </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($dog->mother,'father')}}</strong>(Sire)<br/>
                No: {{$generation_two_a = \App\Dog::getParentId($dog->mother,'father','registration_number')}}
            </div>
        </td>
        <td rowspan="4"><i class="identifier">right 2 </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($generation_two_a,'father')}}</strong>(Sire)<br/>
                No: {{ $generation_two_c = \App\Dog::getParentId($generation_two_a,'father','registration_number')}}
            </div>
        </td>
        <td rowspan="2"><i class="identifier">right 3 huu </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($generation_two_c,'father')}}</strong>(Sire)<br/>
                No: {{$generation_4_d =  \App\Dog::getParentId($generation_two_c,'father','registration_number')}}
            </div>
        </td>
        <td><i class="identifier">right 4 </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($generation_4_d,'father')}}</strong>(Sire)<br/>
                No: {{  \App\Dog::getParentId($generation_4_d,'father','registration_number')}}
            </div>
        </td>


    </tr>
    <tr>
        <td><i class="identifier">bottom right a </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($generation_4_d,'mother')}}</strong>(Dam)<br/>
                No: {{  \App\Dog::getParentId($generation_4_d,'mother','registration_number')}}
            </div>
        </td>

    </tr>

    <tr>
        <td rowspan="2"><i class="identifier">new 1 jk </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($generation_two_c,'mother')}}</strong>(Dam)<br/>
                No: {{$generation_5_a =  \App\Dog::getParentId($generation_two_c,'mother','registration_number')}}
            </div>
        </td>
        <td><i class="identifier">new 1 a </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($generation_5_a,'father')}}</strong>(Sire)<br/>
                No: {{ \App\Dog::getParentId($generation_5_a,'father','registration_number')}}
            </div>
        </td>

    </tr>

    <tr>
        <td><i class="identifier">new 2 </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($generation_5_a,'mother')}}</strong>(Dam)<br/>
                No: {{ \App\Dog::getParentId($generation_5_a,'mother','registration_number')}}
            </div>
        </td>
    </tr>

    <tr>
        <td rowspan="4"><i class="identifier">new 9 h </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($generation_two_a,'mother')}}</strong>(Dam)<br/>
                No: {{$generation_3_a =  \App\Dog::getParentId($generation_two_a,'mother','registration_number')}}
            </div>

        </td>
        <td rowspan="2"><i class="identifier">9a n </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($generation_3_a,'father')}}</strong>(Sire)<br/>
                No: {{$generation_5_e = \App\Dog::getParentId($generation_3_a,'father','registration_number')}}
            </div>
        </td>
        <td><i class="identifier">9b </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($generation_5_e,'father')}}</strong>(Sire)<br/>
                No: {{ \App\Dog::getParentId($generation_5_e,'father','registration_number')}}
            </div>
        </td>

    </tr>

    <tr>
        <td><i class="identifier">new 10 </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($generation_5_e,'mother')}}</strong>(Dam)<br/>
                No: {{ \App\Dog::getParentId($generation_5_e,'mother','registration_number')}}
            </div>
        </td>
    </tr>

    <tr>
        <td rowspan="2"><i class="identifier">new 11 e </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($generation_3_a,'mother')}}</strong>(Dam)<br/>
                No: {{$generation_5_f = \App\Dog::getParentId($generation_3_a,'mother','registration_number')}}
            </div>
        </td>
        <td><i class="identifier">11a </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($generation_5_f,'father')}}</strong>(Sire)<br/>
                No: {{ \App\Dog::getParentId($generation_5_f,'father','registration_number')}}
            </div>
        </td>

    </tr>


    <tr>
        <td><i class="identifier">new 13 </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($generation_5_f,'mother')}}</strong>(Dam)<br/>
                No: {{ \App\Dog::getParentId($generation_5_f,'mother','registration_number')}}
            </div>
        </td>
    </tr>

    <tr>
        <td rowspan="8"><i class="identifier">new 3 hello </i>
            <div class="caption box">
                Name:<strong>{{\App\Dog::getParent($dog->mother,'mother')}}</strong>(Dam)<br/>
                No: {{$generation_two_b = \App\Dog::getParentId($dog->mother,'mother','registration_number')}}

            </div>


        </td>
        <td rowspan="4"><i class="identifier">new 3 a huy </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($generation_two_b,'father')}}</strong>(Sire)<br/>
                No: {{$generation_3_c = \App\Dog::getParentId($generation_two_b,'father','registration_number')}}
            </div>
        </td>
        <td rowspan="2"><i class="identifier">new 3 b w </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($generation_3_c,'father')}}</strong>(Sire)<br/>
                No: {{$generation_5_h = \App\Dog::getParentId($generation_3_c,'father','registration_number')}}
            </div>
        </td>
        <td><i class="identifier">new 3 c </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($generation_5_h,'father')}}</strong>(Sire)<br/>
                No: {{ \App\Dog::getParentId($generation_5_h,'father','registration_number')}}
            </div>
        </td>


    </tr>

    <tr>
        <td><i class="identifier">new 4 </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($generation_5_h,'mother')}}</strong>(Dam)<br/>
                No: {{ \App\Dog::getParentId($generation_5_h,'mother','registration_number')}}
            </div>
        </td>
    </tr>

    <tr>
        <td rowspan="2"><i class="identifier">new 5 r </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($generation_3_c,'mother')}}</strong>(Dam)<br/>
                No: {{$generation_5_i= \App\Dog::getParentId($generation_3_c,'mother','registration_number')}}
            </div>
        </td>
        <td><i class="identifier">5a </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($generation_5_i,'father')}}</strong>(Sire)<br/>
                No: {{ \App\Dog::getParentId($generation_5_i,'father','registration_number')}}
            </div>
        </td>
    </tr>
    <tr>
        <td><i class="identifier">new 6 </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($generation_5_i,'mother')}}</strong>(Sire)<br/>
                No: {{ \App\Dog::getParentId($generation_5_i,'mother','registration_number')}}
            </div>

        </td>
    </tr>

    <tr>
        <td rowspan="4"><i class="identifier">new 7 </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($generation_two_b,'mother')}}</strong>(Dam)<br/>
                No: {{$generation_4_b = \App\Dog::getParentId($generation_two_b,'mother','registration_number')}}
            </div>
        </td>
        <td rowspan="2"><i class="identifier">7a w </i>
            <div class="caption box">
                Name : <strong>{{\App\Dog::getParent($generation_4_b,'father')}}</strong>(Sire)<br/>
                No: {{$generation_5_j =\App\Dog::getParentId($generation_4_b,'father','registration_number')}}
            </div>

        </td>
        <td><i class="identifier">7b</i>
            Name : <strong>{{\App\Dog::getParent($generation_5_j,'father')}}</strong>(Sire)
            No: {{\App\Dog::getParentId($generation_5_j,'father','registration_number')}}


        </td>

    </tr>


    <tr>
        <td><i class="identifier">8a </i>

            Name : <strong>{{\App\Dog::getParent($generation_5_j,'mother')}}</strong>(Sire) <br/>
            No: {{\App\Dog::getParentId($generation_5_j,'mother','registration_number')}}

        </td>
    </tr>

    <tr>
        <td rowspan="2"><i class="identifier">12a w </i>
            Name : <strong>{{\App\Dog::getParent($generation_4_b,'mother')}}</strong>(Dam) <br/>
            No: {{$generation_5_k = \App\Dog::getParentId($generation_4_b,'mother','registration_number')}}
        </td>
        <td><i class="identifier">12b </i>
            Name : <strong>{{\App\Dog::getParent($generation_5_k,'father')}}</strong>(Sire) <br/>
            No: {{ \App\Dog::getParentId($generation_5_k,'father','registration_number')}}
        </td>
    </tr>

    <tr>
        <td><i class="identifier">14a </i>
            Name : <strong>{{\App\Dog::getParent($generation_5_k,'mother')}}</strong>(Dam) <br/>
            No: {{ \App\Dog::getParentId($generation_5_k,'mother','registration_number')}}
        </td>


    </tr>

</table>