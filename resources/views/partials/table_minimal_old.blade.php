
<html>
<head>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="{{asset('../../bower_components/jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('../../bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery.validate.js')}}"></script>

    <style>
        .hide-column{
            display: none;
        }
        .water-mark{
            z-index: 20000;
            position: absolute;
            top: 450px;
            left: 320px;
            font-weight: bold;
            font-size: 60px;
            opacity: 0;
            -ms-transform: rotate(45deg); /* IE 9 */
            -webkit-transform: rotate(45deg); /* Chrome, Safari, Opera */
            transform: rotate(45deg);
        }

        .first{
            border: 1px solid #000000;
            height: 50px;
            width: 90%;
            margin: auto;
            background-color: rgb(172, 209, 165);
        }

        .second{
            border: 1px solid #000000;
            height: 50px;
            width: 100%;
            margin: auto;
            background-color: rgb(172, 209, 165);
        }
        .third{
            border: 1px solid #000000;
            height: 50px;
            width: 100%;
            margin: auto;
            background-color: #ffffff;
            border-top: 0;
            border-bottom: 0;
        }

        .fourth{
            border: 1px solid #000000;
            height: 50px;
            width: 95%;
            margin: auto;
            background-color: rgb(172, 209, 165);
        }

        .table-container{
            border: 25px solid transparent;
            padding: 15px;
            /*border-image-source: url(/img/main-2.png);*/
            /*border-image-repeat: round;*/
            /*border-image-slice: 85;*/
        }

        /*hr {*/
        /*padding: 0;*/
        /*border: none;*/
        /*border-top: medium double #333;*/
        /*color: #333;*/
        /*text-align: center;*/
        /*width: 95%;*/
        /*margin-left: 0px;*/
        /*}*/
        /*hr:after {*/
        /*/!*content: "�";*!/*/
        /*display: inline-block;*/
        /*position: relative;*/
        /*top: -0.7em;*/
        /*font-size: 1.5em;*/
        /*padding: 0 0.25em;*/
        /*background: white;*/
        /*}*/
        .male {
            /*background-color: rgba(191,133,10,1);*/
            /*background-color: #FFC86E;*/
            /*background-color: rgba(255, 200, 110,1);*/
            background-color:rgb(172, 209, 165);
            color: #000000;
            font-family: Sans Serif;
            font-size: 8pt;
            text-align: center;
            vertical-align: middle;
            border-collapse: collapse;
            border: 1px solid #000000;
        }

        .female {
            background-color: #FFFFFF;
            color: #000000;
            font-family: Sans Serif;
            font-size: 8pt;
            text-align: center;
            vertical-align: middle;
            border-collapse: collapse;
            border: 1px solid #000000;
        }

        table,.dog-info{
            margin:auto;
        }

        .dog-info{
            width: 94%;
        }

        hr{
            width: 94%;
            margin:auto;
        }


    </style>

    <script>
//        $('#pedigree-table tr ').each(function (index, element) {
//            if($(element +' td').attr('rowspan') === '16'){
//                alert('found 16');
//            }
//        })
        
    </script>
    <title></title>

</head>


<div class="content">

    <div class="container-fluid">

        <div class="portlet portlet-default">



            <div class="portlet-body main-table">

                <!-- Begin Pedigree Table -->

                <div class="table-container">

                    <?php
                    $dob = Carbon\Carbon::createFromFormat('Y-m-d',$dog->dob);
                    $difference = $dob->diffInMonths(Carbon\Carbon::now());
                    $certificate = $difference > 12 ? 'PEDIGREE ' : 'BIRTH ';
                    ?>

                    <div class="header">
                        {{--<h3 class="text-center" style="margin-top: 6px; margin-bottom: 10px;">KENNEL UNION OF GHANA  CERTIFIED {{$certificate}} CERTIFICATE</h3><br>--}}
                        {{--<h3 class="text-center">CERTIFIED PEDIGREE </h3>--}}
                    </div>


                    <div>

                        <hr>
                        <br>

                        <?php $parents = \App\HelperClasses\Dog::generateAncestorsFromRegistrationNumber($dog->registration_number) ?>

                        <?php $parent = (\App\DogGeneration::whereDogId($dog->id)->first()) ?>

                        <table border="0" cellpadding="2" width='95%'>
                            <tr>
                                <td>
                                    <table id="pedigree-table" style="border: 1px solid #000000; border-collapse: collapse;" border="1" cellpadding="2" cellspacing="2" width="100%"  CELLSPACING="2">
                                        <tr>
                                            <th style="text-align: center;">I</th>
                                            <th style="text-align: center;">II</th>
                                            <th style="text-align: center;">III</th>
                                            <th style="text-align: center;">IV</th>
                                            {{--<th style="text-align: center;">V</th>--}}

                                        </tr>

                                        <tr>
                                            <td rowspan='16' width='17%' class='male'>
                                                <div class="h5">
                                                    1
                                                    {{--{{$parents['first']['sire']}} <br>--}}

                                                    {{--{"sire":{"name":"White Knight Majestic Zytrix","registration_number":"201603029","titles":""},"dam":{"name":"White Knight Sun Seeker","registration_number":"2016030210","titles":""}}--}}


                                                    {{$parent->first_generation['sire']['name']}}<br>
                                                    {{$parent->first_generation['sire']['registration_number']}}<br>

                                                    <?php $firstGeneration = \App\HelperClasses\Dog::getFirstGenerationParents($dog->father); ?>
                                                    {{$firstGeneration['name']}}<br>
                                                    {{$firstGeneration['number']}}<br>

                                                </div>
                                            </td>
                                            <td rowspan='8' width='17%' class='male'>
                                                <div class="h5">
                                                    3
                                                    {{--{"sire":{"sire":{"name":"Jalaneque Azean-Angel For White Knight ","registration_number":"2016030219","titles":""},--}}
                                                    {{--"dam":{"name":"Cart White Knight KeepSake Tezrah ","registration_number":"2016030229","titles":""}},--}}
                                                    {{--"Dam":{"sire":{"name":"White Knight Lethal Weapon","registration_number":"2016030220","titles":""},--}}
                                                    {{--"dam":{"name":"White Knight X Laskan Lights ","registration_number":"2016030221","titles":""}}}--}}

                                                    {{$parent->second_generation['sire']['sire']['name']}}<br>
                                                    {{$parent->second_generation['sire']['sire']['registration_number']}}<br>
                                                    {{$parent->second_generation['sire']['sire']['titles']}}<br>

	                                                <?php $secondGenerationFather = \App\HelperClasses\Dog::getSecondGenerationNumbers($dog->father); ?>
                                                    {{$secondGenerationFather['father_name']}}<br>
                                                    {{$secondGenerationFather['father_number']}}<br>

                                                </div>
                                            </td>
                                            <td rowspan='4' width='17%' class='male'>
                                                <div class="h5">
                                                    7

                                                    {{$parent->third_generation['Sire']['sire']['name']}}<br>
                                                    {{$parent->third_generation['Sire']['sire']['registration_number']}}<br>
                                                    {{$parent->third_generation['Sire']['sire']['titles']}}<br>

                                                    <?php $thirdGeneration = \App\HelperClasses\Dog::getThirdGenerationNumbers($secondGenerationFather['father_number']) ?>

                                                    {{$thirdGeneration['father_number']}}<br>
                                                    {{$thirdGeneration['father_name']}}<br>

                                                </div>
                                            </td>
                                            <td rowspan='2' width='17%' class='male'>
                                                <div class="h5">
                                                    15

	                                                <?php $fourthGenerationOne = \App\HelperClasses\Dog::getFourthGenerationNumbers($thirdGeneration['father_number']) ?>

                                                    {{$fourthGenerationOne['father_number']}}<br>
                                                    {{$fourthGenerationOne['father_name']}}<br>

                                                </div>
                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp;</td>
                                        </tr>

                                        <tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp;</td>
                                        </tr>

                                        <tr>
                                            <td rowspan='2' width='17%' class='female'>
                                                <div class="h5">
                                                    16

	                                                <?php $fourthGenerationTwo = \App\HelperClasses\Dog::getFourthGenerationNumbers($thirdGeneration['father_number']) ?>

                                                    {{$fourthGenerationTwo['mother_number']}}<br>
                                                    {{$fourthGenerationTwo['mother_name']}}<br>

                                                </div>
                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td rowspan='4' width='17%' class='female'>
                                                <div class="h5">
                                                    8

                                                    {{$parent->third_generation['Sire']['dam']['name']}}<br>
                                                    {{$parent->third_generation['Sire']['dam']['registration_number']}}<br>
                                                    {{$parent->third_generation['Sire']['dam']['titles']}}<br>



	                                                <?php $thirdGeneration = \App\HelperClasses\Dog::getThirdGenerationNumbers($secondGenerationFather['father_number']) ?>

                                                    {{$thirdGeneration['mother_number']}}<br>
                                                    {{$thirdGeneration['mother_name']}}<br>

                                                </div>

                                            </td>
                                            <td rowspan='2' width='17%' class='male'>
                                                <div class="h5">
                                                    17

	                                                <?php $fourthGenerationThree = \App\HelperClasses\Dog::getFourthGenerationNumbers('',$thirdGeneration['mother_number']) ?>

                                                    {{$fourthGenerationThree['father_number']}}<br>
                                                    {{$fourthGenerationThree['father_name']}}<br>

                                                </div>

                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td rowspan='2' width='17%' class='female'>
                                                <div class="h5">
                                                    18

	                                                <?php $fourthGenerationFour = \App\HelperClasses\Dog::getFourthGenerationNumbers('',$thirdGeneration['mother_number']) ?>

                                                    {{$fourthGenerationFour['mother_number']}}<br>
                                                    {{$fourthGenerationFour['mother_name']}}<br>
                                                </div>

                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td rowspan='8' width='17%' class='female'>
                                                <div class="h5">
                                                    4

                                                    {{$parent->second_generation['sire']['dam']['name']}}<br>
                                                    {{$parent->second_generation['sire']['dam']['registration_number']}}<br>
                                                    {{$parent->second_generation['sire']['dam']['titles']}}<br>

	                                                <?php $secondGenerationMother = \App\HelperClasses\Dog::getSecondGenerationNumbers($dog->father); ?>
                                                    {{$secondGenerationFather['mother_name']}}<br>
                                                    {{$secondGenerationFather['mother_number']}}<br>

                                                </div>
                                            </td>
                                            <td rowspan='4' width='17%' class='male'>
                                                <div class="h5">

                                                    9

                                                    {{$parent->third_generation['Dam']['sire']['name']}}<br>
                                                    {{$parent->third_generation['Dam']['sire']['registration_number']}}<br>
                                                    {{$parent->third_generation['Dam']['sire']['titles']}}<br>


                                                <?php $thirdGeneration = \App\HelperClasses\Dog::getThirdGenerationNumbers('',$secondGenerationMother['mother_number']) ?>

                                                    {{$thirdGeneration['father_number']}}<br>
                                                    {{$thirdGeneration['father_name']}}<br>

                                                </div>
                                            </td>
                                            <td rowspan='2' width='17%' class='male'>


                                                <div class="h5">

	                                                <?php $fourthGenerationFive = \App\HelperClasses\Dog::getFourthGenerationNumbers($thirdGeneration['father_number']) ?>

                                                    {{$fourthGenerationFive['father_number']}}<br>
                                                    {{$fourthGenerationFive['father_name']}}<br>

                                                </div>

                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td rowspan='2' width='17%' class='female'>
                                                <div class="h5">

	                                                <?php $fourthGenerationSix = \App\HelperClasses\Dog::getFourthGenerationNumbers($thirdGeneration['father_number']) ?>

                                                    {{$fourthGenerationSix['mother_number']}}<br>
                                                    {{$fourthGenerationSix['mother_name']}}<br>

                                                </div>


                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td rowspan='4' width='17%' class='female'>
                                                10
                                                <div class="h5">

                                                    {{$parent->third_generation['Dam']['dam']['name']}}<br>
                                                    {{$parent->third_generation['Dam']['dam']['registration_number']}}<br>
                                                    {{$parent->third_generation['Dam']['dam']['titles']}}<br>

	                                            <?php $thirdGeneration = \App\HelperClasses\Dog::getThirdGenerationNumbers('',$secondGenerationMother['mother_number']) ?>

                                                {{$thirdGeneration['mother_number']}}<br>
                                                {{$thirdGeneration['mother_name']}}<br>

                                                </div>

                                            </td>
                                            <td rowspan='2' width='17%' class='male'>
                                                <div class="h5">

	                                                <?php $fourthGenerationSeven = \App\HelperClasses\Dog::getFourthGenerationNumbers('',$thirdGeneration['mother_number']) ?>

                                                    {{$fourthGenerationSeven['father_number']}}<br>
                                                    {{$fourthGenerationSeven['father_name']}}<br>

                                                </div>

                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td rowspan='2' width='17%' class='female'>
                                                <div class="h5">

	                                                <?php $fourthGenerationEight = \App\HelperClasses\Dog::getFourthGenerationNumbers('',$thirdGeneration['mother_number']) ?>

                                                    {{$fourthGenerationEight['mother_number']}}<br>
                                                    {{$fourthGenerationEight['mother_name']}}<br>

                                                </div>
                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp;</td>
                                        </tr><tr>
                                            <td rowspan='16' width='17%' class='female' >

                                                <div class="h5">
                                                    2

                                                    {{$parent->first_generation['dam']['name']}}<br>
                                                    {{$parent->first_generation['dam']['registration_number']}}<br>

                                                <?php $firstGeneration = \App\HelperClasses\Dog::getFirstGenerationParents($dog->mother); ?>
                                                    {{$firstGeneration['name']}}<br>
                                                    {{$firstGeneration['number']}}<br>

                                                </div>
                                            </td>
                                            <td rowspan='8' width='17%' class='male'>
                                                <div class="h5">
                                                    5

                                                    {{$parent->second_generation['Dam']['sire']['name']}}<br>
                                                    {{$parent->second_generation['Dam']['sire']['registration_number']}}<br>
                                                    {{$parent->second_generation['Dam']['sire']['titles']}}<br>


                                                <?php $secondGeneration = \App\HelperClasses\Dog::getSecondGenerationNumbers('',$dog->mother); ?>
                                                    {{$secondGeneration['father_name']}}<br>
                                                    {{$secondGeneration['father_number']}}<br>

                                                </div>
                                            </td>
                                            <td rowspan='4' width='17%' class='male'>
                                                <div class="h5">
                                                    11

                                                    {{$parent->third_generation['SecondSire']['sire']['name']}}<br>
                                                    {{$parent->third_generation['SecondSire']['sire']['registration_number']}}<br>
                                                    {{$parent->third_generation['SecondSire']['sire']['titles']}}<br>

	                                                <?php $thirdGeneration = \App\HelperClasses\Dog::getThirdGenerationNumbers($secondGeneration['father_number']) ?>

                                                    {{$thirdGeneration['father_number']}}<br>
                                                    {{$thirdGeneration['father_name']}}<br>

                                                </div>

                                            </td>
                                            <td rowspan='2' width='17%' class='male'>
                                                <div class="h5">

	                                                <?php $fourthGenerationNine = \App\HelperClasses\Dog::getFourthGenerationNumbers($thirdGeneration['father_number']) ?>

                                                    {{$fourthGenerationNine['father_number']}}<br>
                                                    {{$fourthGenerationNine['father_name']}}<br>

                                                </div>

                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='2' width='17%' class='female'>
                                                <div class="h5">

	                                                <?php $fourthGenerationTen = \App\HelperClasses\Dog::getFourthGenerationNumbers($thirdGeneration['father_number']) ?>

                                                    {{$fourthGenerationTen['mother_number']}}<br>
                                                    {{$fourthGenerationTen['mother_name']}}<br>

                                                </div>

                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='4' width='17%' class='female'>
                                                12
                                                <div class="h5">

                                                    {{$parent->third_generation['SecondSire']['dam']['name']}}<br>
                                                    {{$parent->third_generation['SecondSire']['dam']['registration_number']}}<br>
                                                    {{$parent->third_generation['SecondSire']['dam']['titles']}}<br>

	                                                <?php $thirdGeneration = \App\HelperClasses\Dog::getThirdGenerationNumbers($secondGeneration['father_number']) ?>

                                                    {{$thirdGeneration['mother_number']}}<br>
                                                    {{$thirdGeneration['mother_name']}}<br>

                                                </div>
                                            </td>
                                            <td rowspan='2' width='17%' class='male'>

                                                <div class="h5">

	                                                <?php $fourthGenerationEleven = \App\HelperClasses\Dog::getFourthGenerationNumbers('',$thirdGeneration['mother_number']) ?>

                                                    {{$fourthGenerationEleven['father_number']}}<br>
                                                    {{$fourthGenerationEleven['father_name']}}<br>

                                                </div>

                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='2' width='17%' class='female'>
                                                <div class="h5">


	                                                <?php $fourthGenerationTwelve = \App\HelperClasses\Dog::getFourthGenerationNumbers('',$thirdGeneration['mother_number']) ?>

                                                    {{$fourthGenerationTwelve['mother_number']}}<br>
                                                    {{$fourthGenerationTwelve['mother_name']}}<br>

                                                </div>
                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='8' width='17%' class='female'>
                                                <div class="h5">
                                                    6

                                                    {{$parent->second_generation['Dam']['dam']['name']}}<br>
                                                    {{$parent->second_generation['Dam']['dam']['registration_number']}}<br>
                                                    {{$parent->second_generation['Dam']['dam']['titles']}}<br>

	                                                <?php $secondGeneration = \App\HelperClasses\Dog::getSecondGenerationNumbers('',$dog->mother); ?>
                                                    {{$secondGeneration['mother_name']}}<br>
                                                    {{$secondGeneration['mother_number']}}<br>

                                                </div>
                                            </td>
                                            <td rowspan='4' width='17%' class='male'>
                                                <div class="h5">
                                                    13

                                                    {{$parent->third_generation['SecondDam']['sire']['name']}}<br>
                                                    {{$parent->third_generation['SecondDam']['sire']['registration_number']}}<br>
                                                    {{$parent->third_generation['SecondDam']['sire']['titles']}}<br>

	                                                <?php $thirdGeneration = \App\HelperClasses\Dog::getThirdGenerationNumbers('',$secondGeneration['mother_number']) ?>

                                                    {{$thirdGeneration['father_number']}}<br>
                                                    {{$thirdGeneration['father_name']}}<br>


                                                </div>

                                            </td>
                                            <td rowspan='2' width='17%' class='male'>

                                                <div class="h5">

	                                                <?php $fourthGenerationThirteen = \App\HelperClasses\Dog::getFourthGenerationNumbers($thirdGeneration['father_number']) ?>

                                                    {{$fourthGenerationThirteen['father_number']}}<br>
                                                    {{$fourthGenerationThirteen['father_name']}}<br>

                                                </div>
                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='2' width='17%' class='female'>
                                                <div class="h5">

	                                                <?php $fourthGenerationFourteen = \App\HelperClasses\Dog::getFourthGenerationNumbers($thirdGeneration['father_number']) ?>

                                                    {{$fourthGenerationFourteen['mother_number']}}<br>
                                                    {{$fourthGenerationFourteen['mother_name']}}<br>

                                                </div>
                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='4' width='17%' class='female'>
                                                <div class="h5">
                                                    14

                                                    {{$parent->third_generation['SecondDam']['dam']['name']}}<br>
                                                    {{$parent->third_generation['SecondDam']['dam']['registration_number']}}<br>
                                                    {{$parent->third_generation['SecondDam']['dam']['titles']}}<br>

	                                                <?php $thirdGeneration = \App\HelperClasses\Dog::getThirdGenerationNumbers('',$secondGeneration['mother_number']) ?>

                                                    {{$thirdGeneration['mother_number']}}<br>
                                                    {{$thirdGeneration['mother_name']}}<br>
                                                </div>
                                            </td>
                                            <td rowspan='2' width='17%' class='male'>
                                                <div class="h5">

	                                                <?php $fourthGenerationFifteen = \App\HelperClasses\Dog::getFourthGenerationNumbers($thirdGeneration['mother_number']) ?>

                                                    {{$fourthGenerationFifteen['father_number']}}<br>
                                                    {{$fourthGenerationFifteen['father_name']}}<br>

                                                </div>

                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp;</td>
                                        </tr><tr>
                                            <td rowspan='2' width='17%' class='female hide-column'>
                                                <div class="h5">

	                                                <?php $fourthGenerationFifteen = \App\HelperClasses\Dog::getFourthGenerationNumbers($thirdGeneration['mother_number']) ?>

                                                    {{$fourthGenerationFifteen['mother_number']}}<br>
                                                    {{$fourthGenerationFifteen['mother_name']}}<br>

                                                </div>
                                            </td>
                                            <td rowspan='1' class="hide-column"  width='17%' class='male'>&nbsp; </td>
                                        </tr>
                                        {{--<tr>--}}
                                        {{--<td  style="display: none;"rowspan='1' width='17%' class='female'>&nbsp; 62</td>--}}
                                        {{--</tr>--}}
                                        <tr>
                                    </table>
                                </td>
                        </table>

                    </div>
                </div>
                <!-- End Pedigree Table -->

                </table>
                @if($dog->father || $dog->mother)
                    <?php
                    $father_id = \App\Dog::getDogId($dog->father);
                    $mother_id = \App\Dog::getDogId($dog->mother);
                    ?>
                @endif

            </div>



        </div>

    </div> <!-- /.table-responsive -->

</div> <!-- /.portlet-body -->
</html>

