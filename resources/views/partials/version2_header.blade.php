<!-- header -->
<header class="header">
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
                    {{--<div class="header-location"><i class="fa fa-home"></i> <a href="#">455 Martinson, Los Angeles</a></div>--}}
                    <div class="header-email"><i class="fa fa-envelope-o"></i> <a href="mailto:info@kennelunionghana.com">info@kennelunionghana.com</a></div>
                    {{--<div class="header-phone"><i class="fa fa-phone"></i> <a href="#">8 (043) 567 - 89 - 30</a></div>--}}
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <div class="header-social pull-right">
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-google-plus"></i></a>
                        <a href="#"><i class="fa fa-dribbble"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-bottom">
        <nav class="navbar navbar-universal navbar-custom">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="logo"><a href="http://pedigree.kennelunionghana.com" class="navbar-brand page-scroll">
                                {{--<img src="{{asset('kug_version2/frontend/assets/images/logo/logo.png')}}" alt="logo"/>--}}
                                {{--<h2><strong>Kennel Union of Ghana</strong></h2>--}}
                                <h3><strong>Kennel Union of Ghana</strong></h3>

                            </a></div>
                    </div>
                    <div class="col-lg-9">
                        <div class="navbar-header">
                            <button type="button" data-toggle="collapse" data-target=".navbar-main-collapse" class="navbar-toggle"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                        </div>
                        <div class="collapse navbar-collapse navbar-main-collapse">
                            @if(Auth::check() && Auth::user()->administrator == 1)

                                {!! $HomeNav2->asUl(array('class' => 'nav navbar-nav navbar-right')) !!}

                            @elseif(Auth::check() && Auth::user()->administrator == 0)

                                {!! $HomeNav3->asUl(array('class' => 'nav navbar-nav navbar-right')) !!}


                            @else

                            {!! $HomeNav1->asUl(array('class' => 'nav navbar-nav navbar-right')) !!}

                            @endif
                            {{--<ul class="nav navbar-nav navbar-right">--}}
                                {{--<li ><a href="{{url('/')}}">Home</a></li>--}}
                                {{--<li><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Pages <span class="caret"></span></a>--}}
                                {{--<ul class="dropdown-menu">--}}
                                {{--<li><a href="about-us.html">About Us</a></li>--}}
                                {{--<li><a href="contacts.html">Contacts</a></li>--}}
                                {{--<li><a href="faq.html">F.A.Q</a></li>--}}
                                {{--<li><a href="gallery.html">Gallery</a></li>--}}
                                {{--<li><a href="404.html">404 Page Not Found</a></li>--}}
                                {{--</ul>--}}
                                {{--</li>--}}
                                {{--@if(Auth::check() && Auth::user()->administrator == 1)--}}
                                    {{--<li><a href="{{url('/version2')}}">Dashboard</a></li>--}}
                                {{--@elseif(Auth::check() && Auth::user()->administrator == 0)--}}
                                    {{--<li><a href="{{url('/member')}}">Dashboard</a></li>--}}
                                {{--@endif--}}

                                {{--<li>--}}
                                    {{--@if(Auth::guest())--}}
                                        {{--<a href="{{url('auth/login')}}">--}}
                                            {{--Log in--}}
                                        {{--</a>--}}
                                    {{--@else--}}

                                        {{--<a href="{{url('auth/logout')}}">--}}
                                            {{--Log out--}}
                                        {{--</a>--}}
                                    {{--@endif--}}

                                {{--</li>--}}

                            {{--</ul>--}}
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</header>
<!-- /header -->