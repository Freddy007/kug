<div class="portlet-body main-table">

    <!-- Begin Pedigree Table -->

    <style>

        .dog-name{
            font-weight: bolder;
        }

        .column{
            display: none;
        }

        .edit-column{
            display: none;
        }

        .hide-column{
            display: none;
        }
        .water-mark{
            z-index: 20000;
            position: absolute;
            top: 450px;
            left: 320px;
            font-weight: bold;
            font-size: 60px;
            opacity: 0;
            -ms-transform: rotate(45deg); /* IE 9 */
            -webkit-transform: rotate(45deg); /* Chrome, Safari, Opera */
            transform: rotate(45deg);
        }

        .first{
            border: 1px solid #000000;
            height: 50px;
            width: 90%;
            margin: auto;
            background-color: rgb(172, 209, 165);
        }

        .second{
            border: 1px solid #000000;
            height: 50px;
            width: 100%;
            margin: auto;
            background-color: rgb(172, 209, 165);
        }
        .third{
            border: 1px solid #000000;
            height: 50px;
            width: 100%;
            margin: auto;
            background-color: #ffffff;
            border-top: 0;
            border-bottom: 0;
        }

        .fourth{
            border: 1px solid #000000;
            height: 50px;
            width: 95%;
            margin: auto;
            background-color: rgb(172, 209, 165);
        }

        .table-container{
            border: 25px solid transparent;
            padding: 15px;
            border-image-source: url(/img/main-2.png);
            border-image-repeat: round;
            border-image-slice: 85;
        }

        /*hr {*/
        /*padding: 0;*/
        /*border: none;*/
        /*border-top: medium double #333;*/
        /*color: #333;*/
        /*text-align: center;*/
        /*width: 95%;*/
        /*margin-left: 0px;*/
        /*}*/
        /*hr:after {*/
        /*/!*content: "�";*!/*/
        /*display: inline-block;*/
        /*position: relative;*/
        /*top: -0.7em;*/
        /*font-size: 1.5em;*/
        /*padding: 0 0.25em;*/
        /*background: white;*/
        /*}*/
        .male {
            /*background-color: rgba(191,133,10,1);*/
            /*background-color: #FFC86E;*/
            /*background-color: rgba(255, 200, 110,1);*/
            background-color:rgb(172, 209, 165);
            color: #000000;
            font-family: Sans Serif;
            font-size: 8pt;
            text-align: center;
            vertical-align: middle;
            border-collapse: collapse;
            border: 1px solid #000000;
        }

        .female {
            background-color: #FFFFFF;
            color: #000000;
            font-family: Sans Serif;
            font-size: 8pt;
            text-align: center;
            vertical-align: middle;
            border-collapse: collapse;
            border: 1px solid #000000;
        }

        table,.dog-info{
            margin:auto;
        }

        .dog-info{
            width: 94%;
        }

        hr{
            width: 94%;
            margin:auto;
        }

        @media print {
            .first{
                border: 1px solid #000000;
                height: 50px;
                width: 90%;
                margin: auto;
                background-color: rgb(172, 209, 165) !important;
            }

            .second{
                border: 1px solid #000000;
                height: 50px;
                width: 100%;
                margin: auto;
                background-color: rgb(172, 209, 165) !important;
            }
            .third{
                border: 1px solid #000000;
                height: 50px;
                width: 100%;
                margin: auto;
                background-color: #ffffff;
                border-top: 0;
                border-bottom: 0;
            }

            .fourth{
                border: 1px solid #000000;
                height: 50px;
                width: 95%;
                margin: auto;
                /*margin-top: -150px;*/
                background-color: rgb(172, 209, 165) !important;
            }
            .table-container{
                border: 25px solid transparent;
                padding: 15px;
                border-image-source: url(/img/main-2.png);
                border-image-repeat: round;
                border-image-slice: 75;
            }

            /*.image_one{*/
            /*position: absolute;*/
            /*top: 60px;*/
            /*left: 40px;*/
            /*}*/

            /*.image_two{*/
            /*position: absolute;*/
            /*right: 40px;*/
            /*top: 60px;*/
            /*}*/

            .left-border{
                position: absolute;
                top: 5px;
                left: -14px;

            }

            .right-border{
                position: absolute;
                top: 5px;
                right: -14px;
            }

            .full-border{
                width: 100%;
                margin-top: 25px;

            }

            .full-border-flag{
                margin-left: -15px;;
                /*margin-right: -30px;;*/
                width: 103%;
                height:8px;
                /*margin-bottom: 2px;*/
            }

            .flag{
                width: 320px;
                height:8px;
                /*margin-bottom: 2px;*/
            }

            .male{
                background-color: rgb(172, 209, 165) !important;
                /*background-color: rgba(191,133,10,1)!important;*/
                -webkit-print-color-adjust: exact;
            }
            .image_one{
                position: absolute;
                left: 230px;
                top: 40px;
            }

            .image_two{
                position: absolute;
                right: 230px;
                top: 40px;
            }


            .flag{
                width: 320px;
                height:8px;
                /*margin-bottom: 2px;*/
            }
            header {
                display: none;
            }
            .mainnav{
                display: none;
            }
            footer,title,.print{
                display:none;
            }

            .flag_one{
                background-color: red !important;
            }
            .flag_two{
                background-color: yellow !important;
            }
            .flag_three{
                background-color: green !important;
            }
            /*hr{*/
            /*margin-bottom: -60px;;*/
            /*}*/

            #print{
                display: none;
            }

            @page { size: auto;  margin: 0mm; }

            table{
                height: 72%;
                margin-top: -60px;
                /*background-color: yellowgreen !important;*/
                -webkit-print-color-adjust: exact;
            }

        }

        .image_one{
            position: absolute;
            left: 230px;
            top: 40px;
        }

        .image_two{
            position: absolute;
            right: 230px;
            top: 40px;
        }


    </style>

    <div class="table-container">

        <?php
        $dob = Carbon\Carbon::createFromFormat('Y-m-d',$dog->dob);
        $difference = $dob->diffInMonths(Carbon\Carbon::now());
        $certificate = $difference > 12 ? 'PEDIGREE ' : 'BIRTH ';
        ?>

        <table class="table" style="border-top: none;">
            <tr>
                <td>
                    @if($dog->breed == 'German Shepherd Dog' )
                    <img width="85" height="85" style="border-radius: 50%" src="{{url('/img/shepherd_logo.jpg')}}">
                        @elseif($dog->breed == 'Rottweiler')
                        <img width="85" height="85" style="border-radius: 50%" src="{{url('/img/rottweiler_logo.jpg')}}">
                        @else
                        <img width="85" height="85" style="border-radius: 50%; position: relative; left: 45px" src="{{url('/img/kug_logo.jpg')}}">
                    @endif
                </td>
                <td> <h3 class="text-center" style="margin-top: 6px; margin-bottom: 10px;">KENNEL UNION OF GHANA {{$certificate}} CERTIFICATE</h3></td>
                <td>
                    <img width="85" height="85" style="border-radius: 50%; position: relative; left: 45px" src="{{url('/img/kug_logo.jpg')}}">
                </td>
            </tr>

        </table>

        <div class="print">
            <?php
            $name = Request::query('name')? "name=". Request::query('name')."&":'';
            $phone = Request::query('phone')?"phone=".Request::query('phone')."&":'';
            $address = Request::query('address')?"address=".Request::query('address'):'';
            ?>
            <div class="print">
{{--                'http://3.123.126.62/certification/public?name=$dog->name&id=$dog->id'--}}
{{--                <a href="{{url('/print-full-cert',$dog->id)."?$name$phone$address"}}" class="btn btn-success pull-left" style="margin-top: -17px;" >GENERATE CERTIFICATE</a></div><br>--}}
                <a href="{{('http://3.123.126.62/certification/public/'.$dog->id)}}" class="btn btn-success pull-left" style="margin-top: -17px;" >GENERATE CERTIFICATE</a></div><br>

        <div class="water-mark">
            KENNEL UNION OF GHANA
        </div>

        <div class="row dog-info" style="color:#283846; text-transform: uppercase">
            <div class="row first" style="margin-bottom: 10px;" >

                <table class="table table-bordered">
                    <tr>
                        <td>Dog Name: {{$dog->name}}</td>
                        <td> REG ID: {{$dog->registration_number}}

                            @if($dog->other_registration_number)
                                | {{$dog->other_registration_number}}
                            @endif
                        </td>
                    </tr>

                </table>

            </div>

            <div class="clearfix"></div>

            <div class="row second">
                   <?php  $kennel_name = $dog->kennel_name != null?"(".$dog->kennel_name.")": ""; ?>

                    <table class="table table-bordered">
                    <tr>
                        <td>BREEDER : {{ strtoupper($dog->first_name.' '. $dog->last_name).$kennel_name}}</td>
                        <td>COAT : {{$dog->coat}}</td>
                        <td>DOB : {{$dog->dob}}</td>
                    </tr>

                </table>

            </div>

            <div class="row third">
                <table class="table table-bordered">
                    <tr>
                        <td>BREED : {{$dog->breed}} </td>
                        <td>COLOUR : {{$dog->colour}}</td>
                        <td>SEX : {{$dog->sex}} </td>
                    </tr>

                </table>

            </div>

            <div class="row second">
                <div class="col-md-12  col-xs-hidden" style="padding-top: 13px;">

                    @if(Request::query('name'))
                        <div class="col-md-4 col-sm-4 col-xs-4">OWNER: {{Request::query('name')}} </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">PHONE: {{Request::query('phone')}} </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">ADDRESS: {{Request::query('address')}} </div>
                    @endif
                </div>
            </div>

        </div>


        <div>

            <hr>
            <br>

	        <?php $parent = (\App\DogGeneration::whereDogId($dog->id)->first()) ?>


            <table border="0" cellpadding="2" width='95%'>
                <tr>
                    <td>
                        <table id="pedigree-table" style="border: 1px solid #000000; text-transform: uppercase;
                                    border-collapse: collapse;" border="1" cellpadding="2" cellspacing="2" width="100%"  CELLSPACING="2">
                            <tr>
                                <th style="text-align: center;">I</th>
                                <th style="text-align: center;">II</th>
                                <th style="text-align: center;">III</th>
                                <th style="text-align: center;">IV</th>
                                {{--<th style="text-align: center;">V</th>--}}

                            </tr>

                            <tr>
                                <td rowspan='16' width='17%' class='male'>
                                    <div class="h5">
                                        <span class="column">1</span>

                                        <div class="dog-name">

                                            {{$parent->first_generation['sire']['name']}}<br>

                                        </div>

                                        {{$parent->first_generation['sire']['registration_number']}}<br>
                                        {{$parent->first_generation['sire']['titles']}}<br>

                                    </div>
                                    {{--$parent->second_generation['sire']['sire']['name']--}}

                                    @if($parent->first_generation['sire']['name'] == '')

                                        <button class="btn btn-sm btn-success edit-column"
                                                data-sire-parent="second,sire,sire"
                                                data-dam-parent="second,sire,dam"
                                                data-generation="first"
                                                data-position="sire" >edit
                                        </button>

                                    @endif
                                </td>
                                <td rowspan='8' width='17%' class='male'>
                                    <div class="h5">

                                        <span class="column">3</span>


                                        <div class="dog-name">

                                            {{$parent->second_generation['sire']['sire']['name']}}<br>
                                        </div>

                                        {{$parent->second_generation['sire']['sire']['registration_number']}}<br>
                                        {{$parent->second_generation['sire']['sire']['titles']}}<br>


                                        @if($parent->second_generation['sire']['sire']['name'] == '')

                                            <button class="btn btn-sm btn-success edit-column"
                                                    data-sire-parent="third,Sire,sire"
                                                    data-dam-parent="third,Sire,dam"

                                                    data-generation="second"
                                                    data-position="sire,sire" >edit</button>

                                        @endif

                                    </div>
                                </td>
                                <td rowspan='4' width='17%' class='male'>
                                    <div class="h5">

                                        <span class="column">7</span>
                                        <div class="dog-name">

                                            <div class="dog-name">

                                                {{$parent->third_generation['Sire']['sire']['name']}}<br>
                                            </div>

                                            {{$parent->third_generation['Sire']['sire']['registration_number']}}<br>
                                            {{$parent->third_generation['Sire']['sire']['titles']}}<br>

                                            {{--$parent->fourth_generation['Sire']['sire']['name']--}}

                                            @if($parent->third_generation['Sire']['sire']['name'] == '')

                                                <button class="btn btn-sm btn-success edit-column"

                                                        data-sire-parent="fourth,Sire,sire"
                                                        data-dam-parent="fourth,Sire,dam"

                                                        data-generation="third"
                                                        data-position="Sire,sire" >
                                                    edit</button>

                                            @endif
                                        </div>
                                </td>
                                <td rowspan='2' width='17%' class='male'>
                                    <div class="h5">

                                        <span class="column">15</span>


                                        <div class="dog-name">

                                            {{$parent->fourth_generation['Sire']['sire']['name']}}<br>

                                        </div>

                                        {{$parent->fourth_generation['Sire']['sire']['registration_number']}}<br>
                                        {{$parent->fourth_generation['Sire']['sire']['titles']}}<br>

                                        @if($parent->fourth_generation['Sire']['sire']['name'] == '')

                                            <button class="btn btn-sm btn-success edit-column" data-generation="fourth" data-position="Sire,sire" >edit</button>

                                        @endif


                                    </div>
                                </td>
                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp;</td>
                            </tr>

                            <tr>
                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp;</td>
                            </tr>

                            <tr>
                                <td rowspan='2' width='17%' class='female'>
                                    <div class="h5">

                                        <span class="column">16</span>

                                        <div class="dog-name">

                                            {{$parent->fourth_generation['Sire']['dam']['name']}}<br>
                                        </div>

                                        {{$parent->fourth_generation['Sire']['dam']['registration_number']}}<br>
                                        {{$parent->fourth_generation['Sire']['dam']['titles']}}<br>

                                        @if($parent->fourth_generation['Sire']['dam']['name'] == '')

                                            <button class="btn btn-sm btn-success edit-column" data-generation="fourth" data-position="Sire,dam" >edit</button>

                                        @endif

                                    </div>
                                </td>
                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                            </tr>
                            <tr>
                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                            </tr>
                            <tr>
                                <td rowspan='4' width='17%' class='female'>
                                    <div class="h5">

                                        <span class="column">8</span>


                                        <div class="dog-name">
                                            {{$parent->third_generation['Sire']['dam']['name']}}<br>
                                        </div>

                                        {{$parent->third_generation['Sire']['dam']['registration_number']}}<br>
                                        {{$parent->third_generation['Sire']['dam']['titles']}}<br>

                                        {{--$parent->fourth_generation['Dam']['sire']['name']--}}

                                        @if($parent->third_generation['Sire']['dam']['name'] =='')

                                            <button class="btn btn-sm btn-success edit-column"

                                                    data-sire-parent="fourth,Dam,sire"
                                                    data-dam-parent="fourth,Dam,dam"

                                                    data-generation="third"
                                                    data-position="Sire,dam" >
                                                edit</button>

                                        @endif
                                    </div>

                                </td>
                                <td rowspan='2' width='17%' class='male'>
                                    <div class="h5">

                                        <span class="column">17</span>

                                        <div class="dog-name">

                                            {{$parent->fourth_generation['Dam']['sire']['name']}}<br>

                                        </div>

                                        {{$parent->fourth_generation['Dam']['sire']['registration_number']}}<br>
                                        {{$parent->fourth_generation['Dam']['sire']['titles']}}<br>

                                        @if($parent->fourth_generation['Dam']['sire']['name'] == '')

                                            <button class="btn btn-sm btn-success edit-column" data-generation="fourth" data-position="Dam,sire" >edit</button>

                                        @endif

                                    </div>

                                </td>
                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                            </tr>
                            <tr>
                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                            </tr>
                            <tr>
                                <td rowspan='2' width='17%' class='female'>
                                    <div class="h5">

                                        <span class="column">18</span>

                                        <div class="dog-name">

                                            {{$parent->fourth_generation['Dam']['dam']['name']}}<br>
                                        </div>
                                        {{$parent->fourth_generation['Dam']['dam']['registration_number']}}<br>
                                        {{$parent->fourth_generation['Dam']['dam']['titles']}}<br>

                                        @if($parent->fourth_generation['Dam']['dam']['name'] == '')

                                            <button class="btn btn-sm btn-success edit-column" data-generation="fourth" data-position="Dam,dam" >edit</button>

                                        @endif

                                    </div>

                                </td>
                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                            </tr>
                            <tr>
                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                            </tr>
                            <tr>
                                <td rowspan='8' width='17%' class='female'>
                                    <div class="h5">

                                        <span class="column">4</span>

                                        <div class="dog-name">

                                            {{$parent->second_generation['sire']['dam']['name']}}<br>
                                        </div>

                                        {{$parent->second_generation['sire']['dam']['registration_number']}}<br>
                                        {{$parent->second_generation['sire']['dam']['titles']}}<br>


                                        @if($parent->second_generation['sire']['dam']['name'] == '')

                                            <button class="btn btn-sm btn-success edit-column"
                                                    data-sire-parent="third,Dam,sire"
                                                    data-dam-parent="third,Dam,dam"

                                                    data-generation="second"
                                                    data-position="sire,dam" >edit</button>
                                        @endif

                                    </div>
                                </td>
                                <td rowspan='4' width='17%' class='male'>
                                    <div class="h5">


                                        <span class="column">9</span>

                                        <div class="dog-name">

                                            {{$parent->third_generation['Dam']['sire']['name']}}<br>
                                        </div>

                                        {{$parent->third_generation['Dam']['sire']['registration_number']}}<br>
                                        {{$parent->third_generation['Dam']['sire']['titles']}}<br>

                                        {{--$parent->fourth_generation['SecondSire']['sire']['name']--}}

                                        @if($parent->third_generation['Dam']['sire']['name'] == '')

                                            <button class="btn btn-sm btn-success edit-column"

                                                    data-sire-parent="fourth,SecondSire,sire"
                                                    data-dam-parent="fourth,SecondSire,dam"

                                                    data-generation="third"
                                                    data-position="Dam,sire" >
                                                edit</button>
                                        @endif

                                    </div>
                                </td>
                                <td rowspan='2' width='17%' class='male'>

                                    <span class="column">19</span>

                                    <div class="h5">

                                        <div class="dog-name">

                                            {{$parent->fourth_generation['SecondSire']['sire']['name']}}<br>
                                        </div>
                                        {{$parent->fourth_generation['SecondSire']['sire']['registration_number']}}<br>
                                        {{$parent->fourth_generation['SecondSire']['sire']['titles']}}<br>

                                        @if($parent->fourth_generation['SecondSire']['sire']['name'] == '')

                                            <button class="btn btn-sm btn-success edit-column" data-generation="fourth" data-position="SecondSire,sire" >edit</button>

                                        @endif


                                    </div>

                                </td>
                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                            </tr>
                            <tr>
                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                            </tr>
                            <tr>
                                <td rowspan='2' width='17%' class='female'>

                                    <span class="column">20</span>

                                    <div class="h5">

                                        <div class="dog-name">
                                            {{$parent->fourth_generation['SecondSire']['dam']['name']}}<br>

                                        </div>
                                        {{$parent->fourth_generation['SecondSire']['dam']['registration_number']}}<br>
                                        {{$parent->fourth_generation['SecondSire']['dam']['titles']}}<br>

                                        @if($parent->fourth_generation['SecondSire']['dam']['name'] == '')

                                            <button class="btn btn-sm btn-success edit-column" data-generation="fourth" data-position="SecondSire,dam" >edit</button>

                                        @endif

                                    </div>


                                </td>
                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp;</td>
                            </tr>
                            <tr>
                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                            </tr>
                            <tr>
                                <td rowspan='4' width='17%' class='female'>

                                    <span class="column">10</span>

                                    <div class="h5">

                                        <div class="dog-name">
                                            {{$parent->third_generation['Dam']['dam']['name']}}<br>
                                        </div>

                                        <div class="dog-name">

                                            {{$parent->third_generation['Dam']['dam']['registration_number']}}<br>
                                        </div>

                                        {{$parent->third_generation['Dam']['dam']['titles']}}<br>

                                        @if($parent->third_generation['Dam']['dam']['name'] == '')

                                            <button class="btn btn-sm btn-success edit-column"

                                                    data-sire-parent="fourth,SecondDam,sire"
                                                    data-dam-parent="fourth,SecondSire,dam"

                                                    data-generation="third"
                                                    data-position="Dam,dam" >
                                                edit</button>

                                        @endif
                                    </div>

                                </td>
                                <td rowspan='2' width='17%' class='male'>

                                    <span class="column">21</span>

                                    <div class="h5">

                                        <div class="dog-name">
                                            {{$parent->fourth_generation['SecondDam']['sire']['name']}}<br>

                                        </div>
                                        {{$parent->fourth_generation['SecondDam']['sire']['registration_number']}}<br>
                                        {{$parent->fourth_generation['SecondDam']['sire']['titles']}}<br>

                                        @if($parent->fourth_generation['SecondDam']['sire']['name'] == '')

                                            <button class="btn btn-sm btn-success edit-column" data-generation="fourth" data-position="SecondDam,sire" >edit</button>

                                        @endif


                                    </div>

                                </td>
                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                            </tr>
                            <tr>
                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                            </tr>
                            <tr>
                                <td rowspan='2' width='17%' class='female'>


                                    <span class="column">22</span>

                                    <div class="h5">
                                        <div class="dog-name">
                                            {{$parent->fourth_generation['SecondDam']['dam']['name']}}<br>
                                        </div>
                                        {{$parent->fourth_generation['SecondDam']['dam']['registration_number']}}<br>
                                        {{$parent->fourth_generation['SecondDam']['dam']['titles']}}<br>

                                        @if($parent->fourth_generation['SecondDam']['dam']['name'] == '')

                                            <button class="btn btn-sm btn-success edit-column" data-generation="fourth" data-position="SecondDam,dam" >edit</button>

                                        @endif




                                    </div>
                                </td>
                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                            </tr><tr>
                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='16' width='17%' class='female' >

                                    <span class="column">2</span>

                                    <div class="h5">

                                        <div class="dog-name">
                                            {{$parent->first_generation['dam']['name']}}<br>
                                        </div>
                                        {{$parent->first_generation['dam']['registration_number']}}<br>
                                        {{$parent->first_generation['dam']['titles']}}<br>

                                    </div>

                                    @if($parent->first_generation['dam']['name'] == '')

                                        <button class="btn btn-sm btn-success edit-column"
                                                data-sire-parent="second,sire,sire"
                                                data-dam-parent="second,sire,dam"
                                                data-generation="first"
                                                data-position="dam" >edit
                                        </button>

                                    @endif
                                </td>
                                <td rowspan='8' width='17%' class='male'>
                                    <span class="column">5</span>

                                    <div class="h5">

                                        <div class="dog-name">

                                            {{$parent->second_generation['Dam']['sire']['name']}}<br>
                                        </div>

                                        {{$parent->second_generation['Dam']['sire']['registration_number']}}<br>
                                        {{$parent->second_generation['Dam']['sire']['titles']}}<br>

                                        @if($parent->second_generation['Dam']['sire']['name'] == '')

                                            <button class="btn btn-sm btn-success edit-column"

                                                    data-sire-parent="Third,SecondSire,sire"
                                                    data-dam-parent="Third,SecondSire,dam"

                                                    data-generation="second"
                                                    data-position="Dam,sire" >edit</button>

                                        @endif


                                    </div>
                                </td>
                                <td rowspan='4' width='17%' class='male'>
                                    <span class="column">11</span>

                                    <div class="h5">
                                        <div class="dog-name">
                                            {{$parent->third_generation['SecondSire']['sire']['name']}}<br>
                                        </div>
                                        {{$parent->third_generation['SecondSire']['sire']['registration_number']}}<br>
                                        {{$parent->third_generation['SecondSire']['sire']['titles']}}<br>

                                        @if($parent->third_generation['SecondSire']['sire']['name'] == '')

                                            <button class="btn btn-sm btn-success edit-column"

                                                    data-sire-parent="fourth,ThirdSire,sire"
                                                    data-dam-parent="fourth,ThirdSire,dam"

                                                    data-generation="third"
                                                    data-position="SecondSire,sire" >edit</button>
                                        @endif
                                    </div>

                                </td>
                                <td rowspan='2' width='17%' class='male'>


                                    <span class="column">23</span>


                                    <div class="h5">

                                        <div class="dog-name">

                                            {{$parent->fourth_generation['ThirdSire']['sire']['name']}}<br>
                                        </div>

                                        {{$parent->fourth_generation['ThirdSire']['sire']['registration_number']}}<br>
                                        {{$parent->fourth_generation['ThirdSire']['sire']['titles']}}<br>

                                        @if($parent->fourth_generation['ThirdSire']['sire']['name'] == '')

                                            <button class="btn btn-sm btn-success edit-column" data-generation="fourth" data-position="ThirdSire,sire" >edit</button>

                                        @endif



                                    </div>

                                </td>
                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                            </tr><tr>
                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                            </tr><tr>
                                <td rowspan='2' width='17%' class='female'>

                                    <span class="column">24</span>


                                    <div class="h5">
                                        <div class="dog-name">
                                            {{$parent->fourth_generation['ThirdSire']['dam']['name']}}<br>
                                        </div>
                                        {{$parent->fourth_generation['ThirdSire']['dam']['registration_number']}}<br>
                                        {{$parent->fourth_generation['ThirdSire']['dam']['titles']}}<br>

                                        @if($parent->fourth_generation['ThirdSire']['dam']['name'] == '')

                                            <button class="btn btn-sm btn-success edit-column" data-generation="fourth" data-position="ThirdSire,dam" >edit</button>

                                        @endif



                                    </div>

                                </td>
                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                            </tr><tr>
                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                            </tr><tr>
                                <td rowspan='4' width='17%' class='female'>

                                    <span class="column">12</span>

                                    <div class="h5">
                                        <div class="dog-name">
                                            {{$parent->third_generation['SecondSire']['dam']['name']}}<br>
                                        </div>
                                        {{$parent->third_generation['SecondSire']['dam']['registration_number']}}<br>
                                        {{$parent->third_generation['SecondSire']['dam']['titles']}}<br>

                                        @if($parent->third_generation['SecondSire']['dam']['name'] == '')

                                            <button class="btn btn-sm btn-success edit-column"

                                                    data-sire-parent="fourth,ThirdDam,sire"
                                                    data-dam-parent="fourth,ThirdDam,dam"

                                                    data-generation="third"
                                                    data-position="SecondSire,dam" >edit</button>
                                        @endif
                                    </div>
                                </td>
                                <td rowspan='2' width='17%' class='male'>

                                    <span class="column">25</span>


                                    <div class="h5">
                                        <div class="dog-name">
                                            {{$parent->fourth_generation['ThirdDam']['sire']['name']}}<br>
                                        </div>
                                        {{$parent->fourth_generation['ThirdDam']['sire']['registration_number']}}<br>
                                        {{$parent->fourth_generation['ThirdDam']['sire']['titles']}}<br>

                                        @if($parent->fourth_generation['ThirdDam']['sire']['name'] == '')

                                            <button class="btn btn-sm btn-success edit-column" data-generation="fourth" data-position="ThirdSire,sire" >edit</button>

                                        @endif



                                    </div>

                                </td>
                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                            </tr><tr>
                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                            </tr><tr>
                                <td rowspan='2' width='17%' class='female'>
                                    <span class="column">26</span>


                                    <div class="h5">
                                        <div class="dog-name">
                                            {{$parent->fourth_generation['ThirdDam']['dam']['name']}}<br>
                                        </div>
                                        {{$parent->fourth_generation['ThirdDam']['dam']['registration_number']}}<br>
                                        {{$parent->fourth_generation['ThirdDam']['dam']['titles']}}<br>

                                        @if($parent->fourth_generation['ThirdDam']['dam']['name'] == '')

                                            <button class="btn btn-sm btn-success edit-column" data-generation="fourth" data-position="ThirdSire,dam" >edit</button>

                                        @endif



                                    </div>
                                </td>
                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                            </tr><tr>
                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                            </tr><tr>
                                <td rowspan='8' width='17%' class='female'>
                                    <div class="h5">

                                        <span class="column">6</span>

                                        <div class="dog-name">

                                            {{$parent->second_generation['Dam']['dam']['name']}}<br>
                                        </div>
                                        {{$parent->second_generation['Dam']['dam']['registration_number']}}<br>
                                        {{$parent->second_generation['Dam']['dam']['titles']}}<br>

                                        @if($parent->second_generation['Dam']['dam']['name'] == '')

                                            <button class="btn btn-sm btn-success edit-column" data-generation="second" data-position="Dam,dam" >edit</button>

                                        @endif

                                    </div>
                                </td>
                                <td rowspan='4' width='17%' class='male'>
                                    <span class="column">13</span>

                                    <div class="h5">

                                        <div class="dog-name">
                                            {{$parent->third_generation['SecondDam']['sire']['name']}}<br>
                                        </div>
                                        {{$parent->third_generation['SecondDam']['sire']['registration_number']}}<br>
                                        {{$parent->third_generation['SecondDam']['sire']['titles']}}<br>

                                        @if($parent->third_generation['SecondDam']['sire']['name'] == '')

                                            <button class="btn btn-sm btn-success edit-column"
                                                    data-sire-parent="fourth,FourthSire,sire"
                                                    data-dam-parent="fourth,FourthSire,dam"

                                                    data-generation="third"
                                                    data-position="SecondDam,sire" >
                                                edit</button>
                                        @endif

                                    </div>

                                </td>
                                <td rowspan='2' width='17%' class='male'>


                                    <span class="column">27</span>


                                    <div class="h5">
                                        <div class="dog-name">

                                            {{$parent->fourth_generation['FourthSire']['sire']['name']}}<br>
                                        </div>
                                        {{$parent->fourth_generation['FourthSire']['sire']['registration_number']}}<br>
                                        {{$parent->fourth_generation['FourthSire']['sire']['titles']}}<br>

                                        @if($parent->fourth_generation['FourthSire']['sire']['name'] == '')

                                            <button class="btn btn-sm btn-success edit-column" data-generation="fourth" data-position="FourthSire,sire" >edit</button>

                                        @endif


                                    </div>
                                </td>
                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                            </tr><tr>
                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                            </tr><tr>
                                <td rowspan='2' width='17%' class='female'>

                                    <span class="column">28</span>


                                    <div class="h5">

                                        {{$parent->fourth_generation['FourthSire']['dam']['name']}}<br>
                                        {{$parent->fourth_generation['FourthSire']['dam']['registration_number']}}<br>
                                        {{$parent->fourth_generation['FourthSire']['dam']['titles']}}<br>

                                        @if($parent->fourth_generation['FourthSire']['dam']['name'] == '')

                                            <button class="btn btn-sm btn-success edit-column" data-generation="fourth" data-position="FourthSire,dam" >edit</button>

                                        @endif
                                    </div>
                                </td>
                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                            </tr><tr>
                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                            </tr><tr>
                                <td rowspan='4' width='17%' class='female'>
                                    <span class="column">14</span>

                                    <div class="h5">
                                        <div class="dog-name">

                                            {{$parent->third_generation['SecondDam']['dam']['name']}}<br>
                                        </div>
                                        {{$parent->third_generation['SecondDam']['dam']['registration_number']}}<br>
                                        {{$parent->third_generation['SecondDam']['dam']['titles']}}<br>

                                        @if($parent->third_generation['SecondDam']['dam']['name'] == '')

                                            <button class="btn btn-sm btn-success edit-column"

                                                    data-sire-parent="fourth,FourthDam,sire"
                                                    data-dam-parent="fourth,FourthDam,dam"

                                                    data-generation="third"
                                                    data-position="SecondDam,dam" >
                                                edit</button>
                                        @endif
                                    </div>
                                </td>
                                <td rowspan='2' width='17%' class='male'>
                                    <span class="column">29</span>


                                    <div class="h5">
                                        <div class="dog-name">

                                            {{$parent->fourth_generation['FourthDam']['sire']['name']}}<br>
                                        </div>
                                        {{$parent->fourth_generation['FourthDam']['sire']['registration_number']}}<br>
                                        {{$parent->fourth_generation['FourthDam']['sire']['titles']}}<br>

                                        @if($parent->fourth_generation['FourthDam']['sire']['name'] == '')

                                            <button class="btn btn-sm btn-success edit-column" data-generation="fourth" data-position="FourthDam,sire" >edit</button>

                                        @endif
                                    </div>

                                </td>
                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                            </tr><tr>
                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp;</td>
                            </tr>

                            <tr>
                                <td rowspan='2' width='17%' class='female'>


                                    <span class="column">30</span>

                                    <div class="h5">

                                        <div class="dog-name">
                                            {{$parent->fourth_generation['FourthDam']['dam']['name']}}<br>
                                        </div>
                                        {{$parent->fourth_generation['FourthDam']['dam']['registration_number']}}<br>
                                        {{$parent->fourth_generation['FourthDam']['dam']['titles']}}<br>

                                        @if($parent->fourth_generation['FourthDam']['dam']['name'] == '')

                                            <button class="btn btn-sm btn-success edit-column" data-generation="fourth" data-position="FourthDam,dam" >edit</button>

                                        @endif
                                    </div>
                                </td>
                                <td rowspan='1' class="hide-column"  width='17%' class='male'>&nbsp; </td>
                            </tr>
                            {{--<tr>--}}
                            {{--<td  style="display: none;"rowspan='1' width='17%' class='female'>&nbsp; 62</td>--}}
                            {{--</tr>--}}
                            <tr>
                        </table>
                    </td>
            </table>




            <div class="row fourth" >

                <table class="table table-bordered">
                    <tr>
                        <td>MICROCHIP/TATTOO: {{$dog->microchip_number}}</td>
                        <td>ISSUE DATE:<em> <?php echo date('d-m-y'); ?> </em></td>
                        <td>CERTIFIED :</td>
                    </tr>

                </table>

            </div>

            <div class="row" style="padding-top: 30px;">
                <div class="col-md-3">

                </div>

            </div>
        </div>
    </div>


</div>
</div>