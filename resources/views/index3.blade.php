@extends("layouts.index3_layout")

@section("title")
    Home
    @endsection

@section("content")

    <!-- Main -->
    <div class="main" role="main">

        <!-- Slider -->
        <section class="slider-holder">
            <div class="container">
                <div class="flexslider carousel">
                    <ul class="slides">
                        <li>
                            <img src="images/slide1.jpg" alt="">
                        </li>

                        {{--                        <li>--}}
                        {{--                            <img src="images/slide2.jpg" alt="">--}}
                        {{--                        </li>--}}

                        <li>
                            <img src="images/slide3.jpg" alt="">
                        </li>
                    </ul>

                    <div class="search-box">
                        <h2>Search from the Pedigree Database </h2>
                        <form action="/search-results" method="GET" role="form">
                            <div class="form-group">
                                <input type="text" name="name" class="form-control" placeholder="name, registration or microchip number">
                            </div>

                            <div class="form-group">
                                <div class="select-style">
                                    <select class="form-control">
                                        <option>All sex</option>
                                        <option value="sire">Sire</option>
                                        <option value="dam">Dam</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="select-style">
                                    <select class="form-control">
                                        <option value="all">All breeds</option>

                                        @foreach($breeds as $breed)
                                            <option value="{{$breed->id}}">{{$breed->name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-success">Search</button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- Slider / End -->

        <!-- Page Content -->
        <section class="page-content">
            <div class="container">

                <!-- Light Section -->
                <section class="section-light section-nomargin">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="with-subtitle" data-animation="fadeInUp" data-animation-delay="0">Latest Uploads
{{--                                <small data-animation="fadeInUp" data-animation-delay="100">Subtitle goes here</small>--}}
                            </h2>
                            <div class="row">

                                @foreach($dogs as $dog)

                                    <div class="col-xs-6 col-sm-3 col-md-3" data-animation="fadeInLeft" data-animation-delay="0">
                                        <div class="job-listing-box featured">
                                            <figure class="job-listing-img">
                                                <a href="{{url('/show-dog',$dog->id)}}">
                                                    <img  src={{$dog->image_name == null ? url('https://placehold.it/300x200/cccccc/000000?text=no+image+uploaded'):"/images/catalog/$dog->image_name"}}></a>
                                            </figure>
                                            <div class="job-listing-body">
                                                <h4 class="name"><a href="{{url('/show-dog',$dog->id)}}">{{ $dog->name }}</a></h4>
                                                {{--                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>--}}
                                            </div>
                                            <footer class="job-listing-footer">
                                                <ul class="meta">
                                                    <li class="category">{{$dog->breeder_name}}</li>
                                                    {{--                                                <li class="location"><a href="#">Orlando, FL</a></li>--}}
                                                    <li class="date">Posted {{getDogRegisteredDay($dog->created_at)}}</li>
                                                </ul>
                                            </footer>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>

{{--                        <div class="col-md-4" data-animation="fadeInRight" data-animation-delay="0">--}}
{{--                            <div class="spacer-lg visible-sm visible-xs"></div>--}}
{{--                            <h2>Featured Listings</h2>--}}
{{--                            <!-- Featured Listings -->--}}
{{--                            <div class="owl-carousel owl-theme owl-featured-listings">--}}
{{--                                <div class="job-listing-box featured">--}}
{{--                                    <figure class="job-listing-img">--}}
{{--                                        <a href="job-profile.html"><img src="images/profile-lg-1.jpg" alt=""></a>--}}
{{--                                    </figure>--}}
{{--                                    <div class="job-listing-holder">--}}
{{--                                        <div class="job-listing-body">--}}
{{--                                            <h4 class="name"><a href="job-profile.html">John Doe</a></h4>--}}
{{--                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>--}}
{{--                                        </div>--}}
{{--                                        <footer class="job-listing-footer">--}}
{{--                                            <ul class="meta">--}}
{{--                                                <li class="category">Cat Sitter</li>--}}
{{--                                                <li class="location"><a href="#">Orlando, FL</a></li>--}}
{{--                                                <li class="date">Posted 5 days ago</li>--}}
{{--                                            </ul>--}}
{{--                                        </footer>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="job-listing-box featured">--}}
{{--                                    <figure class="job-listing-img">--}}
{{--                                        <a href="job-profile.html"><img src="images/profile-lg-2.jpg" alt=""></a>--}}
{{--                                    </figure>--}}
{{--                                    <div class="job-listing-holder">--}}
{{--                                        <div class="job-listing-body">--}}
{{--                                            <h4 class="name"><a href="job-profile.html">John Doe</a></h4>--}}
{{--                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>--}}
{{--                                        </div>--}}
{{--                                        <footer class="job-listing-footer">--}}
{{--                                            <ul class="meta">--}}
{{--                                                <li class="category">Cat Sitter</li>--}}
{{--                                                <li class="location"><a href="#">Orlando, FL</a></li>--}}
{{--                                                <li class="date">Posted 5 days ago</li>--}}
{{--                                            </ul>--}}
{{--                                        </footer>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="job-listing-box featured">--}}
{{--                                    <figure class="job-listing-img">--}}
{{--                                        <a href="job-profile.html"><img src="images/samples/profile-lg-3.jpg" alt=""></a>--}}
{{--                                    </figure>--}}
{{--                                    <div class="job-listing-holder">--}}
{{--                                        <div class="job-listing-body">--}}
{{--                                            <h4 class="name"><a href="job-profile.html">John Doe</a></h4>--}}
{{--                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>--}}
{{--                                        </div>--}}
{{--                                        <footer class="job-listing-footer">--}}
{{--                                            <ul class="meta">--}}
{{--                                                <li class="category">Cat Sitter</li>--}}
{{--                                                <li class="location"><a href="#">Orlando, FL</a></li>--}}
{{--                                                <li class="date">Posted 5 days ago</li>--}}
{{--                                            </ul>--}}
{{--                                        </footer>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <!-- Featured Listings / End -->--}}

{{--                        </div>--}}
                    </div>
                </section>
                <!-- Light Section / End -->
            </div>
        </section>
        <!-- Page Content / End -->

        <!-- Footer -->
        <footer class="footer" id="footer">

            <div class="footer-copyright">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-md-4">
                            Copyright &copy; 2020 <a href="/">Kennel Union of Ghana</a> &nbsp;| &nbsp;All Rights Reserved
                        </div>
                        <div class="col-sm-6 col-md-8">
                            <div class="social-links-wrapper">
                                <span class="social-links-txt">Keep in Touch</span>
                                <ul class="social-links social-links__light">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fa fa-rss"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer / End -->

    </div>
    <!-- Main / End -->

@endsection