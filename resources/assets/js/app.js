window.axios = require('axios');

import VueTypeahead from 'vue-typeahead'


/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */

// Vue.component('example', require('./components/Example.vue'));
// Vue.component('user-typeahead', require('./components/UserTypeahead.vue').default);

// var MyComponent = require('./components/VueTypeahead.vue').default;
// Vue.component('vueTypeahead', MyComponent);

let autoCompleteComponent = require('./components/VueAutocomplete.vue').default;

Vue.component('vue-autocomplete',autoCompleteComponent);


let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;

    // Vue.http.interceptors.push(function (request, next) {
    //     request.headers['X-CSRF-TOKEN'] = Laravel.csrfToken;
    //     next();
    // });

} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

const app = new Vue({
    el: '#app',

    // data: function() {
    //     return{
    //         label:'',
    //         value: '',
    //         value1: '',
    //         myTemplate: '<div><h3>{{team}}</h3><h4>Custom Template</h4></div>',
    //         localValues: ['Dhaka', 'Rangpur', 'Rajshahi', 'Sylhet', 'Khulna']
    //     }
    // },
    // methods: {
    //     done: function(data) {
    //         console.log(data);
    //     },
    //     classes: function(){
    //         // console.log(data);
    //     }
    // }
});

