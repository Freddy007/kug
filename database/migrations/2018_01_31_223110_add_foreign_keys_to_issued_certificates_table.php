<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToIssuedCertificatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('issued_certificates', function(Blueprint $table)
		{
			$table->foreign('dog_id')->references('id')->on('dogs')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('issued_certificates', function(Blueprint $table)
		{
			$table->dropForeign('issued_certificates_dog_id_foreign');
			$table->dropForeign('issued_certificates_user_id_foreign');
		});
	}

}
