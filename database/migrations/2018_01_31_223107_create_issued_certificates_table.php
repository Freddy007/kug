<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIssuedCertificatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('issued_certificates', function(Blueprint $table)
		{
			$table->char('id', 36)->primary();
			$table->char('dog_id', 36)->index('issued_certificates_dog_id_foreign');
			$table->string('serial_number');
			$table->char('user_id', 36)->index('issued_certificates_user_id_foreign');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('issued_certificates');
	}

}
