<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDogTransfersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('dog_transfers', function(Blueprint $table)
		{
			$table->foreign('dog_id')->references('id')->on('dogs')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('owner_id')->references('id')->on('owners')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('dog_transfers', function(Blueprint $table)
		{
			$table->dropForeign('dog_transfers_dog_id_foreign');
			$table->dropForeign('dog_transfers_owner_id_foreign');
		});
	}

}
