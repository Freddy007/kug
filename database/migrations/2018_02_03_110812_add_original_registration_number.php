<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOriginalRegistrationNumber extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('dogs', function (Blueprint $table) {
			$table->string('other_registration_number')->nullable()->before('registration_number');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('dogs', function (Blueprint $table) {
			$table->dropColumn('other_registration_number');
		});
	}
}
