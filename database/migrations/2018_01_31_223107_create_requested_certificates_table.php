<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRequestedCertificatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('requested_certificates', function(Blueprint $table)
		{
			$table->char('id', 36)->primary();
			$table->char('dog_id', 36)->index('requested_certificates_dog_id_foreign');
			$table->char('user_id', 36)->index('requested_certificates_user_id_foreign');
			$table->boolean('honoured');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('requested_certificates');
	}

}
