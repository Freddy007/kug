<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDogRelationshipsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('dog_relationships', function(Blueprint $table)
		{
			$table->foreign('dog_id')->references('id')->on('dogs')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('dog_relationships', function(Blueprint $table)
		{
			$table->dropForeign('dog_relationships_dog_id_foreign');
		});
	}

}
