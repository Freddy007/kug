<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRecordersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recorders', function(Blueprint $table)
		{
			$table->char('id', 36)->primary();
			$table->char('staff_id', 36)->index('recorder_staff_id_foreign');
			$table->char('member_id', 36)->nullable()->index('recorder_member_id_foreign');
			$table->char('dog_id', 36)->nullable()->index('recorder_dog_id_foreign');
			$table->string('action');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('recorders');
	}

}
