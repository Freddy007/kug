<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dogs', function(Blueprint $table)
		{
			$table->char('id', 36)->primary();
			$table->char('user_id', 36)->nullable()->index('dogs_user_id_foreign');
			$table->char('breeder_id', 36)->index('breeder_id');
			$table->string('name');
			$table->date('dob');
			$table->enum('sex', array('male','female'));
			$table->string('registration_number');
			$table->text('titles', 65535)->nullable();
			$table->text('performance_titles', 65535)->nullable();
			$table->decimal('appraisal_score', 10, 0)->nullable();
			$table->string('image_name', 60)->nullable();
			$table->string('colour', 30)->nullable();
			$table->decimal('height', 10, 0)->nullable();
			$table->string('coat', 10)->nullable();
			$table->string('elbow_ed_results')->nullable();
			$table->string('hip_hd_results')->nullable();
			$table->string('tattoo_number', 50)->nullable();
			$table->string('microchip_number', 50)->nullable();
			$table->text('DNA', 65535)->nullable();
			$table->text('other_health_checks', 65535)->nullable();
			$table->boolean('confirmed');
			$table->boolean('transferred');
			$table->boolean('delete_request');
			$table->boolean('dead');
			$table->integer('viewed');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dogs');
	}

}
