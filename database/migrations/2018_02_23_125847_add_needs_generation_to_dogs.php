<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNeedsGenerationToDogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::table('dogs', function (Blueprint $table) {
			$table->boolean('needs_generation')->default(false)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('dogs', function (Blueprint $table) {
			$table->dropColumn('needs_generation');
		});
	}

}
