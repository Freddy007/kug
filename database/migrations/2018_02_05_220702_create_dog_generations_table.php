<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDogGenerationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dog_generations', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('dog_id');
            $table->foreign('dog_id')->references('id')->on('dogs')->onDelete('cascade');
            $table->json('first_generation')->nullable();
            $table->json('second_generation')->nullable();
            $table->json('third_generation')->nullable();
            $table->json('fourth_generation')->nullable();
            $table->json('fifth_generation')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dog_generations');
    }
}
