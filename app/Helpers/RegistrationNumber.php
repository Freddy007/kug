<?php
namespace app\Helpers\RegistrationNumber;

use App\Dog;

class RegistrationNumber{

    public static function generateRegistrationNumber(){
	    $year = date('Y');
	    $month = date('m');
	    $day = date('d');
	    $count = Dog::withTrashed()->get()->count() + 1;
	    $registration_number =   $year . $month . $day . $count;
//		$dog = Dog::whereRegistrationNumber($registration_number)->first();

//		if($dog){
//			$registration_number +=2;
//
//			return $registration_number;
//		}else{
	    return $registration_number;
//		}
    }

}