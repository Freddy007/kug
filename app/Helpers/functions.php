<?php
use App\Http\Requests\Request;


function getDogRegisteredDay( $date)
{
    $dt = Carbon\Carbon::parse($date);
    $current = Carbon\Carbon::now();
    $day = $dt->diffInDays($current);
    return \Carbon\Carbon::now()->subDays($day)->diffForHumans();

}


function getMemberRegisteredDay( $date)
{
    $dt = Carbon\Carbon::parse($date);
    $current = Carbon\Carbon::now();
    $day = $dt->diffInDays($current);
    return \Carbon\Carbon::now()->subDays($day)->diffForHumans();

}

function sendEmail($to,$message,$subject){
        \Mail::raw($message, function ($m) use ($to, $subject) {
            $m->to($to)->subject($subject);
        });
}


function registrationNumber(){
    $year = date('Y');
    $month = date('m');
    $day = date('d');
   // $day = date('U');
    $count = \App\Dog::count('registration_number') + 1;
     $registration_number = $year . $month . $day . $count;

    $number = \App\Dog::where('registration_number',$registration_number)->first()
              ? \App\Dog::where('registration_number',$registration_number)->first()->registration_number++ : $registration_number;
    return $number;
}

function getActive($active){
    $item  = isset($active) ? $active : "";
    return $item;
}

function printPdfInline($content){
    $pdf = new  \mikehaertl\wkhtmlto\Pdf(array(
        'binary' => \Configure::read('wkhtmlto.pdf'),
        'commandOptions' => array(
            'escapeCommand' => false,
            'useExec' => true
        ),
        'print-media-type'
    ));
    $pdf->addPage($content);
    if (!$pdf->send()) {
        throw new Exception('Could not create PDF: '.$pdf->getError());
    }
}

function linkButton(){
//    return "<a class='btn btn-success btn-xs add-owner-detail' href='#' data-id=$dog->id> certificate</a>";
}
