<?php

namespace App\Console\Commands;

use App\Dog;
use App\DogGeneration;
use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

class GenerateDog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:dog';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'generate dog generations';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//    	DogGeneration::truncate();
    	try{
//		    $dogs = Dog::orderBy('created_at','asc')->get();
		    $dog = Dog::find("5a87ac10-fa32-11e6-94f0-0bce4d609461");

		    $i = 0;

//		    foreach($dogs as $dog){
			    \App\HelperClasses\Dog::generateAncestorsFromRegistrationNumber($dog->registration_number);
			    $this->comment( "creating generations for $dog->name => ".$i++ . PHP_EOL);
//		    }


	    }catch (\Exception $exception){
		    $this->comment( $exception->getMessage().PHP_EOL
			    .$exception->getTraceAsString()

		    );
	    }

//        $this->comment(PHP_EOL.Inspiring::quote().PHP_EOL);
    }
}
