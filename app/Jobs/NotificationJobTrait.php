<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jimmy
 * Date: 9/10/2016
 * Time: 12:12 AM
 */

namespace App\Jobs;


use Illuminate\Mail\Message;

trait NotificationJobTrait
{
    public  function pushSlackNotification($text, $channel)
    {
//        $url = env('SLACK_WEBHOOK_URL');
        $url = 'https://hooks.slack.com/services/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';
//        $data = ['username' => env('SLACK_BOTNAME'), 'text' => $text];
        $data = ['username' => 'KENNEL-UNION-BOT', 'text' => $text];
        if($channel){
            $data['channel'] = $channel;
        }

        return $this->useCurl($url,$data);
    }

    public function pushSmsNotification($message,$recipient){

        try{
            $ch = curl_init('http://kodesms.com/api/sms/send');
            curl_setopt($ch, CURLOPT_POST, 1);

            $data = ['message'=> $message,'recipient'=>$recipient,'sender_alias'=>env('SMS_SENDER_ALIAS')];
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

            curl_setopt($ch, CURLOPT_HTTPHEADER,
                array("Content-Type: application/json ",
                    "ApiKey: ".env('KODESMS_API_KEY'),
                    "ApiSecret: ".env('KODESMS_API_SECRET')
                )
            );
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            $result = curl_exec($ch);
            curl_close($ch);
            return $result;
        }catch(\Exception $e){
            \Log::critical(sprintf(
                'Curl failed with error #%d: %s',
                $e->getCode(), $e->getMessage()));
            return false;
        }

    }

    public function pushEmailNotification($email, $subject, $view_name, $vars=[]){
        /** @var $this Job */
        try{
            \Mail::send($view_name, $vars,function (Message $m) use($email, $subject) {
                $m->from(env('MAIL_FROM'), env('MAIL_FROM_ALIAS'));
                $m->to($email)->subject($subject);
            });
        }catch(\Exception $e){
            \Log::critical($e->getTraceAsString());
        }
    }

    public function useCurl($url, $data){
        try {
            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_POST, 1);

            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            $result = curl_exec($ch);
            curl_close($ch);
            return $result;

        } catch (\Exception $e) {
            \Log::critical(sprintf(
                'Curl failed with error #%d: %s',
                $e->getCode(), $e->getMessage()));
            return false;
        }
    }

    function doWebRequest($url, $post_arg = false, $options = array()) {
        /* initializing curl */
        $curl_handle = curl_init($url);
        /* set this option the curl_exec function return the response */
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        /* follow the redirection */
        curl_setopt($curl_handle, CURLOPT_FOLLOWLOCATION, 1);
        //ssl fix
        curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, 0);
        if($options){
            if($post_arg && isset($options[CURLOPT_HTTPHEADER])){
                $headers = $options[CURLOPT_HTTPHEADER];
                unset($options[CURLOPT_HTTPHEADER]);
            }
            curl_setopt_array($curl_handle, $options);
        }

        if ($post_arg) {
            curl_setopt($curl_handle, CURLOPT_POST, 1);

            if(is_array($post_arg) || is_object($post_arg)){
                $post_arg = http_build_query($post_arg);
            }
            curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $post_arg);
            if(isset($headers)){
                $headers = array_merge(array('Content-Type: application/x-www-form-urlencoded'), $headers);
            }else{
                $headers = array('Content-Type: application/x-www-form-urlencoded');
            }
            curl_setopt($curl_handle, CURLOPT_HTTPHEADER, $headers);
        }

        /* invoke the request */
        $response = curl_exec($curl_handle);
        //var_dump($response);

        /* cleanup curl stuff */
        curl_close($curl_handle);

        return $response;
    }
}