<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

class AdministratorPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
  public function administer($user)
  {
      return $user->administrator == true;
  }

}
