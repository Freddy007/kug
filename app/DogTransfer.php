<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DogTransfer extends Model
{
    protected $fillable = ['id','dog_id','owner_id','transfer_serial_number','confirmed'];
    //
}
