<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Breed extends Model
{
    //
    protected $table = 'breeders';

    public function dog(){
        return $this->belongsTo('App\Dog');
    }
}
