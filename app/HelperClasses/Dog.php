<?php
/**
 * Created by IntelliJ IDEA.
 * User: Freddy
 * Date: 1/17/2018
 * Time: 11:45 AM
 */

namespace App\HelperClasses;

use App\DogGeneration;
use App\DogRelationship;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

/**
 * Class Dog
 * @package App\HelperClasses
 */
class Dog {


	/**
	 * @var Request
	 */
	private $request;
	private $mother;
	private $father;
	private $dogModel;
	/**
	 * @var DogRelationship
	 */
	private $dogRelationshipModel;
	public $dogId;
	private $userId;
	public $registrationNumber;
	public $custom_registrationNumber;

	/**
	 * @var $request Request
	 */

	public function __construct($request) {
		$this->dogModel = new \App\Dog();
		$this->dogRelationshipModel = new DogRelationship();
		$this->request = $request;
		$this->father = $this->dogModel->getDog($this->request->father)? $this->dogModel->getDog($this->request->father)->registration_number : '' ;
		$this->mother = $this->dogModel->getDog($this->request->mother)? $this->dogModel->getDog($this->request->mother)->registration_number : '' ;
		$this->dogId = Uuid::generate();
		$this->registrationNumber = $this->generateRegistrationNumber();
		$this->userId = \Auth::id();
		$this->custom_registrationNumber = $this->request->custom_registration_number;

	}

	/**
	 * @param $fourthGenerationParentsSiresireBuilder
	 * @param $fourthGenerationParentsSiredamBuilder
	 * @param $fourthGenerationParentsDamsireBuilder
	 * @param $fourthGenerationParentsDamdamBuilder
	 * @param $fourthGenerationParentsSecondSiresireBuilder
	 * @param $fourthGenerationParentsSecondSiredamBuilder
	 * @param $fourthGenerationParentsSecondDamsireBuilder
	 * @param $fourthGenerationParentsSecondDamdamBuilder
	 *
	 * @return array
	 */
	public static function getFourthGenerationResults( $fourthGenerationParentsSiresireBuilder, $fourthGenerationParentsSiredamBuilder, $fourthGenerationParentsDamsireBuilder, $fourthGenerationParentsDamdamBuilder, $fourthGenerationParentsSecondSiresireBuilder, $fourthGenerationParentsSecondSiredamBuilder, $fourthGenerationParentsSecondDamsireBuilder, $fourthGenerationParentsSecondDamdamBuilder ) {
		$fourthGenerationParents = [
			'Sire' => [
				'sire' => [
					'name'                => $fourthGenerationParentsSiresireBuilder && \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSiresireBuilder->father )->first() ? \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSiresireBuilder->father )->first()->name : '',
					'registration_number' => $fourthGenerationParentsSiresireBuilder && $fourthGenerationParentsSiresireBuilder->father ? $fourthGenerationParentsSiresireBuilder->father : '',
					'titles'              => $fourthGenerationParentsSiresireBuilder && \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSiresireBuilder->father )->first() ? \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSiresireBuilder->father )->first()->titles : ''
				],

				'dam' => [
					'name'                => $fourthGenerationParentsSiresireBuilder && \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSiresireBuilder->mother )->first() ? \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSiresireBuilder->mother )->first()->name : "",
					'registration_number' => $fourthGenerationParentsSiresireBuilder && $fourthGenerationParentsSiresireBuilder->mother ? $fourthGenerationParentsSiresireBuilder->mother : '',
					'titles'              => $fourthGenerationParentsSiresireBuilder && \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSiresireBuilder->mother )->first() ? \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSiresireBuilder->mother )->first()->titles : ''
				],
			],

			'Dam' => [
//				'sire' => $thirdGenerationParentSireDamBuilder->father,
//				'dam' => $thirdGenerationParentSireDamBuilder->mother
				'sire' => [
					'name'                => $fourthGenerationParentsSiredamBuilder && \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSiredamBuilder->father )->first() ? \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSiredamBuilder->father )->first()->name : '',
					'registration_number' => $fourthGenerationParentsSiredamBuilder ? $fourthGenerationParentsSiredamBuilder->father : '',
					'titles'              => $fourthGenerationParentsSiredamBuilder && \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSiredamBuilder->father )->first() ? \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSiredamBuilder->father )->first()->titles : ''
				],

				'dam' => [
					'name'                => $fourthGenerationParentsSiredamBuilder && \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSiredamBuilder->mother )->first() ? \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSiredamBuilder->mother )->first()->name : '',
					'registration_number' => $fourthGenerationParentsSiredamBuilder ? $fourthGenerationParentsSiredamBuilder->mother : '',
					'titles'              => $fourthGenerationParentsSiredamBuilder && \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSiredamBuilder->mother )->first() ? \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSiredamBuilder->mother )->first()->titles : ''
				],
			],

			'SecondSire' => [
//				'sire' => $thirdGenerationParentsDamSireBuilder->father,
//				'dam' => $thirdGenerationParentsDamSireBuilder->mother

				'sire' => [
					'name'                => $fourthGenerationParentsDamsireBuilder && \App\Dog::whereRegistrationNumber( $fourthGenerationParentsDamsireBuilder->father )->first() ? \App\Dog::whereRegistrationNumber( $fourthGenerationParentsDamsireBuilder->father )->first()->name : '',
					'registration_number' => $fourthGenerationParentsDamsireBuilder && $fourthGenerationParentsDamsireBuilder->father ? $fourthGenerationParentsDamsireBuilder->father : '',
					'titles'              => $fourthGenerationParentsDamsireBuilder && \App\Dog::whereRegistrationNumber( $fourthGenerationParentsDamsireBuilder->father )->first() ? \App\Dog::whereRegistrationNumber( $fourthGenerationParentsDamsireBuilder->father )->first()->titles : ''
				],

				'dam' => [
					'name'                => $fourthGenerationParentsDamsireBuilder && \App\Dog::whereRegistrationNumber( $fourthGenerationParentsDamsireBuilder->mother )->first() ? \App\Dog::whereRegistrationNumber( $fourthGenerationParentsDamsireBuilder->mother )->first()->name : '',
					'registration_number' => $fourthGenerationParentsDamsireBuilder && $fourthGenerationParentsDamsireBuilder->mother ? $fourthGenerationParentsDamsireBuilder->mother : '',
					'titles'              => $fourthGenerationParentsDamsireBuilder && \App\Dog::whereRegistrationNumber( $fourthGenerationParentsDamsireBuilder->mother )->first() ? \App\Dog::whereRegistrationNumber( $fourthGenerationParentsDamsireBuilder->mother )->first()->titles : ''
				],
			],

			'SecondDam' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
				'sire' => [
					'name'                => $fourthGenerationParentsDamdamBuilder && \App\Dog::whereRegistrationNumber( $fourthGenerationParentsDamdamBuilder->father )->first() ? \App\Dog::whereRegistrationNumber( $fourthGenerationParentsDamdamBuilder->father )->first()->name : '',
					'registration_number' => $fourthGenerationParentsDamdamBuilder && $fourthGenerationParentsDamdamBuilder->father ? $fourthGenerationParentsDamdamBuilder->father : '',
					'titles'              => $fourthGenerationParentsDamdamBuilder && \App\Dog::whereRegistrationNumber( $fourthGenerationParentsDamdamBuilder->father )->first() ? \App\Dog::whereRegistrationNumber( $fourthGenerationParentsDamdamBuilder->father )->first()->titles : ''
				],

				'dam' => [
					'name'                => $fourthGenerationParentsDamdamBuilder && \App\Dog::whereRegistrationNumber( $fourthGenerationParentsDamdamBuilder->mother )->first() ? \App\Dog::whereRegistrationNumber( $fourthGenerationParentsDamdamBuilder->mother )->first()->name : '',
					'registration_number' => $fourthGenerationParentsDamdamBuilder && $fourthGenerationParentsDamdamBuilder->mother ? $fourthGenerationParentsDamdamBuilder->mother : '',
					'titles'              => $fourthGenerationParentsDamdamBuilder && \App\Dog::whereRegistrationNumber( $fourthGenerationParentsDamdamBuilder->mother )->first() ? \App\Dog::whereRegistrationNumber( $fourthGenerationParentsDamdamBuilder->mother )->first()->titles : ''
				],
			],

			'ThirdSire' => [
//				'sire' => $thirdGenerationParentsDamSireBuilder->father,
//				'dam' => $thirdGenerationParentsDamSireBuilder->mother

				'sire' => [
					'name'                => $fourthGenerationParentsSecondSiresireBuilder && \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSecondSiresireBuilder->father )->first() ? \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSecondSiresireBuilder->father )->first()->name : '',
					'registration_number' => $fourthGenerationParentsSecondSiresireBuilder && $fourthGenerationParentsSecondSiresireBuilder->father ? $fourthGenerationParentsSecondSiresireBuilder->father : '',
					'titles'              => $fourthGenerationParentsSecondSiresireBuilder && \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSecondSiresireBuilder->father )->first() ? \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSecondSiresireBuilder->father )->first()->titles : ''
				],

				'dam' => [
					'name'                => $fourthGenerationParentsSecondSiresireBuilder && \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSecondSiresireBuilder->mother )->first() ? \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSecondSiresireBuilder->mother )->first()->name : '',
					'registration_number' => $fourthGenerationParentsSecondSiresireBuilder && $fourthGenerationParentsSecondSiresireBuilder->mother ? $fourthGenerationParentsSecondSiresireBuilder->mother : '',
					'titles'              => $fourthGenerationParentsSecondSiresireBuilder && \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSecondSiresireBuilder->mother )->first() ? \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSecondSiresireBuilder->mother )->first()->titles : ''
				],
			],

			'ThirdDam' => [
//				'sire' => $thirdGenerationParentsDamSireBuilder->father,
//				'dam' => $thirdGenerationParentsDamSireBuilder->mother

				'sire' => [
					'name'                => $fourthGenerationParentsSecondSiredamBuilder && \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSecondSiredamBuilder->father )->first() ? \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSecondSiredamBuilder->father )->first()->name : '',
					'registration_number' => $fourthGenerationParentsSecondSiredamBuilder && $fourthGenerationParentsSecondSiredamBuilder->father ? $fourthGenerationParentsSecondSiredamBuilder->father : '',
					'titles'              => $fourthGenerationParentsSecondSiredamBuilder && \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSecondSiredamBuilder->father )->first() ? \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSecondSiredamBuilder->father )->first()->titles : ''
				],

				'dam' => [
					'name'                => $fourthGenerationParentsSecondSiredamBuilder && \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSecondSiredamBuilder->mother )->first() ? \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSecondSiredamBuilder->mother )->first()->name : '',
					'registration_number' => $fourthGenerationParentsSecondSiredamBuilder && $fourthGenerationParentsSecondSiredamBuilder->mother ? $fourthGenerationParentsSecondSiredamBuilder->mother : '',
					'titles'              => $fourthGenerationParentsSecondSiredamBuilder && \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSecondSiredamBuilder->mother )->first() ? \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSecondSiredamBuilder->mother )->first()->titles : ''
				],
			],

			'FourthSire' => [
//				'sire' => $thirdGenerationParentsDamSireBuilder->father,
//				'dam' => $thirdGenerationParentsDamSireBuilder->mother

				'sire' => [
					'name'                => $fourthGenerationParentsSecondDamsireBuilder && \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSecondDamsireBuilder->father )->first() ? \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSecondDamsireBuilder->father )->first()->name : '',
					'registration_number' => $fourthGenerationParentsSecondDamsireBuilder && $fourthGenerationParentsSecondDamsireBuilder->father ? $fourthGenerationParentsSecondDamsireBuilder->father : '',
					'titles'              => $fourthGenerationParentsSecondDamsireBuilder && \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSecondDamsireBuilder->father )->first() ? \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSecondDamsireBuilder->father )->first()->titles : ''
				],

				'dam' => [
					'name'                => $fourthGenerationParentsSecondDamsireBuilder && \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSecondDamsireBuilder->mother )->first() ? \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSecondDamsireBuilder->mother )->first()->name : '',
					'registration_number' => $fourthGenerationParentsSecondDamsireBuilder && $fourthGenerationParentsSecondDamsireBuilder->mother ? $fourthGenerationParentsSecondDamsireBuilder->mother : '',
					'titles'              => $fourthGenerationParentsSecondDamsireBuilder && \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSecondDamsireBuilder->mother )->first() ? \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSecondDamsireBuilder->mother )->first()->titles : ''
				],
			],

			'FourthDam' => [
//				'sire' => $thirdGenerationParentsDamSireBuilder->father,
//				'dam' => $thirdGenerationParentsDamSireBuilder->mother

				'sire' => [
					'name'                => $fourthGenerationParentsSecondDamdamBuilder && \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSecondDamdamBuilder->father )->first() ? \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSecondDamdamBuilder->father )->first()->name : '',
					'registration_number' => $fourthGenerationParentsSecondDamdamBuilder && $fourthGenerationParentsSecondDamdamBuilder->father ? $fourthGenerationParentsSecondDamdamBuilder->father : '',
					'titles'              => $fourthGenerationParentsSecondDamdamBuilder && \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSecondDamdamBuilder->father )->first() ? \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSecondDamdamBuilder->father )->first()->titles : ''
				],

				'dam' => [
					'name'                => $fourthGenerationParentsSecondDamdamBuilder && \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSecondDamdamBuilder->mother )->first() ? \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSecondDamdamBuilder->mother )->first()->name : '',
					'registration_number' => $fourthGenerationParentsSecondDamdamBuilder && $fourthGenerationParentsSecondDamdamBuilder->mother ? $fourthGenerationParentsSecondDamdamBuilder->mother : '',
					'titles'              => $fourthGenerationParentsSecondDamdamBuilder && \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSecondDamdamBuilder->mother )->first() ? \App\Dog::whereRegistrationNumber( $fourthGenerationParentsSecondDamdamBuilder->mother )->first()->titles : ''
				],
			],
		];

		return $fourthGenerationParents;
	}

	/**
	 * @param $thirdGenerationParentsSireSireBuilder
	 * @param $thirdGenerationParentSireDamBuilder
	 * @param $thirdGenerationParentsDamSireBuilder
	 * @param $thirdGenerationParentsDamDamBuilder
	 *
	 * @return array
	 */
	public static function getThirdGnerationResults( $thirdGenerationParentsSireSireBuilder, $thirdGenerationParentSireDamBuilder, $thirdGenerationParentsDamSireBuilder, $thirdGenerationParentsDamDamBuilder ) {
		$thirdGenerationParents = [
			'Sire' => [
//				'sire' => $thirdGenerationParentsSireSireBuilder->father,
//				'dam' => $thirdGenerationParentsSireSireBuilder->mother,
				'sire' => [
					'name'                => $thirdGenerationParentsSireSireBuilder && \App\Dog::whereRegistrationNumber( $thirdGenerationParentsSireSireBuilder->father )->first() ? \App\Dog::whereRegistrationNumber( $thirdGenerationParentsSireSireBuilder->father )->first()->name : '',
					'registration_number' => $thirdGenerationParentsSireSireBuilder && $thirdGenerationParentsSireSireBuilder->father ? $thirdGenerationParentsSireSireBuilder->father : '',
					'titles'              => $thirdGenerationParentsSireSireBuilder && \App\Dog::whereRegistrationNumber( $thirdGenerationParentsSireSireBuilder->father )->first() ? \App\Dog::whereRegistrationNumber( $thirdGenerationParentsSireSireBuilder->father )->first()->titles : ''
				],

				'dam' => [
					'name'                => $thirdGenerationParentsSireSireBuilder && \App\Dog::whereRegistrationNumber( $thirdGenerationParentsSireSireBuilder->mother )->first() ? \App\Dog::whereRegistrationNumber( $thirdGenerationParentsSireSireBuilder->mother )->first()->name : "",
					'registration_number' => $thirdGenerationParentsSireSireBuilder && $thirdGenerationParentsSireSireBuilder->mother ? $thirdGenerationParentsSireSireBuilder->mother : '',
					'titles'              => $thirdGenerationParentsSireSireBuilder && \App\Dog::whereRegistrationNumber( $thirdGenerationParentsSireSireBuilder->mother )->first() ? \App\Dog::whereRegistrationNumber( $thirdGenerationParentsSireSireBuilder->mother )->first()->titles : ''
				],
			],

			'Dam' => [
//				'sire' => $thirdGenerationParentSireDamBuilder->father,
//				'dam' => $thirdGenerationParentSireDamBuilder->mother
				'sire' => [
					'name'                => $thirdGenerationParentSireDamBuilder && \App\Dog::whereRegistrationNumber( $thirdGenerationParentSireDamBuilder->father )->first() ? \App\Dog::whereRegistrationNumber( $thirdGenerationParentSireDamBuilder->father )->first()->name : '',
					'registration_number' => $thirdGenerationParentSireDamBuilder ? $thirdGenerationParentSireDamBuilder->father : '',
					'titles'              => $thirdGenerationParentSireDamBuilder && \App\Dog::whereRegistrationNumber( $thirdGenerationParentSireDamBuilder->father )->first() ? \App\Dog::whereRegistrationNumber( $thirdGenerationParentSireDamBuilder->father )->first()->titles : ''
				],

				'dam' => [
					'name'                => $thirdGenerationParentSireDamBuilder && \App\Dog::whereRegistrationNumber( $thirdGenerationParentSireDamBuilder->mother )->first() ? \App\Dog::whereRegistrationNumber( $thirdGenerationParentSireDamBuilder->mother )->first()->name : '',
					'registration_number' => $thirdGenerationParentSireDamBuilder ? $thirdGenerationParentSireDamBuilder->mother : '',
					'titles'              => $thirdGenerationParentSireDamBuilder && \App\Dog::whereRegistrationNumber( $thirdGenerationParentSireDamBuilder->mother )->first() ? \App\Dog::whereRegistrationNumber( $thirdGenerationParentSireDamBuilder->mother )->first()->titles : ''
				],
			],

			'SecondSire' => [
//				'sire' => $thirdGenerationParentsDamSireBuilder->father,
//				'dam' => $thirdGenerationParentsDamSireBuilder->mother

				'sire' => [
					'name'                => $thirdGenerationParentsDamSireBuilder && \App\Dog::whereRegistrationNumber( $thirdGenerationParentsDamSireBuilder->father )->first() ? \App\Dog::whereRegistrationNumber( $thirdGenerationParentsDamSireBuilder->father )->first()->name : '',
					'registration_number' => $thirdGenerationParentsDamSireBuilder && $thirdGenerationParentsDamSireBuilder->father ? $thirdGenerationParentsDamSireBuilder->father : '',
					'titles'              => $thirdGenerationParentsDamSireBuilder && \App\Dog::whereRegistrationNumber( $thirdGenerationParentsDamSireBuilder->father )->first() ? \App\Dog::whereRegistrationNumber( $thirdGenerationParentsDamSireBuilder->father )->first()->titles : ''
				],

				'dam' => [
					'name'                => $thirdGenerationParentsDamSireBuilder && \App\Dog::whereRegistrationNumber( $thirdGenerationParentsDamSireBuilder->mother )->first() ? \App\Dog::whereRegistrationNumber( $thirdGenerationParentsDamSireBuilder->mother )->first()->name : '',
					'registration_number' => $thirdGenerationParentsDamSireBuilder && $thirdGenerationParentsDamSireBuilder->mother ? $thirdGenerationParentsDamSireBuilder->mother : '',
					'titles'              => $thirdGenerationParentsDamSireBuilder && \App\Dog::whereRegistrationNumber( $thirdGenerationParentsDamSireBuilder->mother )->first() ? \App\Dog::whereRegistrationNumber( $thirdGenerationParentsDamSireBuilder->mother )->first()->titles : ''
				],
			],

			'SecondDam' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
				'sire' => [
					'name'                => $thirdGenerationParentsDamDamBuilder && \App\Dog::whereRegistrationNumber( $thirdGenerationParentsDamDamBuilder->father )->first() ? \App\Dog::whereRegistrationNumber( $thirdGenerationParentsDamDamBuilder->father )->first()->name : '',
					'registration_number' => $thirdGenerationParentsDamDamBuilder && $thirdGenerationParentsDamDamBuilder->father ? $thirdGenerationParentsDamDamBuilder->father : '',
					'titles'              => $thirdGenerationParentsDamDamBuilder && \App\Dog::whereRegistrationNumber( $thirdGenerationParentsDamDamBuilder->father )->first() ? \App\Dog::whereRegistrationNumber( $thirdGenerationParentsDamDamBuilder->father )->first()->titles : ''
				],

				'dam' => [
					'name'                => $thirdGenerationParentsDamDamBuilder && \App\Dog::whereRegistrationNumber( $thirdGenerationParentsDamDamBuilder->mother )->first() ? \App\Dog::whereRegistrationNumber( $thirdGenerationParentsDamDamBuilder->mother )->first()->name : '',
					'registration_number' => $thirdGenerationParentsDamDamBuilder && $thirdGenerationParentsDamDamBuilder->mother ? $thirdGenerationParentsDamDamBuilder->mother : '',
					'titles'              => $thirdGenerationParentsDamDamBuilder && \App\Dog::whereRegistrationNumber( $thirdGenerationParentsDamDamBuilder->mother )->first() ? \App\Dog::whereRegistrationNumber( $thirdGenerationParentsDamDamBuilder->mother )->first()->titles : ''
				],
			]

		];

		return $thirdGenerationParents;
	}

	/**
	 * @param $secondSireParentBuilder
	 * @param $secondDamParentBuilder
	 *
	 * @return array
	 */
	public static function getSecondGenerationResults( $secondSireParentBuilder, $secondDamParentBuilder ) {
		$secondGenerationParents = [
			'sire' => [
//				'sire' => $secondSireParentBuilder->father,
				'sire' => [
					'name'                => $secondSireParentBuilder && \App\Dog::whereRegistrationNumber( $secondSireParentBuilder->father )->first() ? \App\Dog::whereRegistrationNumber( $secondSireParentBuilder->father )->first()->name : '',
					'registration_number' => $secondSireParentBuilder && $secondSireParentBuilder->father ? $secondSireParentBuilder->father : '',
					'titles'              => $secondSireParentBuilder && \App\Dog::whereRegistrationNumber( $secondSireParentBuilder->father )->first() ? \App\Dog::whereRegistrationNumber( $secondSireParentBuilder->father )->first()->titles : ''
				],
				'dam'  => [
					'name'                => $secondSireParentBuilder && \App\Dog::whereRegistrationNumber( $secondSireParentBuilder->mother )->first() ? \App\Dog::whereRegistrationNumber( $secondSireParentBuilder->mother )->first()->name : '',
					'registration_number' => $secondSireParentBuilder && $secondSireParentBuilder->mother ? $secondSireParentBuilder->mother : '',
					'titles'              => $secondSireParentBuilder && \App\Dog::whereRegistrationNumber( $secondSireParentBuilder->mother )->first() ? \App\Dog::whereRegistrationNumber( $secondSireParentBuilder->mother )->first()->titles : ''
				]
			],

			'Dam' => [
//				'sire' => $secondDamParentBuilder->father,
//				'dam' => $secondDamParentBuilder->mother,
				'sire' => [
					'name'                => $secondDamParentBuilder && \App\Dog::whereRegistrationNumber( $secondDamParentBuilder->father )->first() ?
						\App\Dog::whereRegistrationNumber( $secondDamParentBuilder->father )->first()->name : '',
					'registration_number' => $secondDamParentBuilder ? $secondDamParentBuilder->father : '',
					'titles'              => $secondDamParentBuilder && \App\Dog::whereRegistrationNumber( $secondDamParentBuilder->father )->first() ? \App\Dog::whereRegistrationNumber( $secondDamParentBuilder->father )->first()->titles : ''
				],
				'dam'  => [
					'name'                => $secondDamParentBuilder && \App\Dog::whereRegistrationNumber( $secondDamParentBuilder->mother )->first() ? \App\Dog::whereRegistrationNumber( $secondDamParentBuilder->mother )->first()->name : '',
					'registration_number' => $secondDamParentBuilder && $secondDamParentBuilder->mother ? $secondDamParentBuilder->mother : '',
					'titles'              => $secondDamParentBuilder && \App\Dog::whereRegistrationNumber( $secondDamParentBuilder->mother )->first() ? \App\Dog::whereRegistrationNumber( $secondDamParentBuilder->mother )->first()->titles : ''
				]
			]
		];

		return $secondGenerationParents;
	}

	/**
	 * @param $parentBuilder
	 *
	 * @return array
	 */
	public static function getFirstGenerationResults( $parentBuilder ) {
		$firstGenerationParents = [
			'sire' => [
				'name'                => $parentBuilder && self::getByRegistrationNumber( $parentBuilder->father ) ? self::getByRegistrationNumber( $parentBuilder->father )->name : '',
				'registration_number' => $parentBuilder && $parentBuilder->father ? $parentBuilder->father : '',
				'titles'              => $parentBuilder && self::getByRegistrationNumber( $parentBuilder->father ) ? self::getByRegistrationNumber( $parentBuilder->father )->titles : ''
			],
			'dam'  => [

				'name'                => $parentBuilder && self::getByRegistrationNumber( $parentBuilder->mother ) ? self::getByRegistrationNumber( $parentBuilder->mother )->name : '',
				'registration_number' => $parentBuilder && $parentBuilder->mother ? $parentBuilder->mother : '',
				'titles'              => $parentBuilder && \App\Dog::whereRegistrationNumber( $parentBuilder->mother )->first() ? \App\Dog::whereRegistrationNumber( $parentBuilder->mother )->first()->titles : ''
			]
		];

		return $firstGenerationParents;
	}

	public function beginRegistrationProcess(){
		\DB::transaction(function(){
			if($this->dogHasCustomRegistrationNumber()){
				$this->registerForeignDog();
				self::generateAncestorsFromRegistrationNumber($this->custom_registrationNumber);
				$this->registerParent(null,$this->custom_registrationNumber);
			}else{
				$this->registerDog();
				self::generateAncestorsFromRegistrationNumber($this->registrationNumber);
				$this->registerParent();
			}

			if($this->hasImage()){
				$this->saveImage();
			}

		});

		return (string)$this->dogId;
	}

	/**
	 *
	 */
	public function registerDog(){

//		$this->dogModel->create(
			\App\Dog::create(

			[
				'id' => $this->dogId,
				'user_id' => $this->userId,
				'name' => $this->request->name,
				'dob' => $this->request->dob,
				'coat' => $this->request->dob,
				'breeder_id' => $this->request->breeder_id,
				'sex' => $this->request->sex,
				'registration_number' => $this->registrationNumber,
//				'registration_number' => $this->request->registration_number,
				'titles' => $this->request->titles,
				'performance_titles' => $this->request->performance_titles,
				'colour' => $this->request->colour,
				'height' => $this->request->height,
				'elbow_ed_results' => $this->request->elbow_ed_results,
				'appraisal_score' => $this->request->appraisal_score,
				'hip_hd_results' => $this->request->hip_hd_results,
				'tattoo_number' => $this->request->tattoo_number,
				'microchip_number' => $this->request->microchip_number,
				'DNA'  => $this->request->DNA,
				'other_health_checks' => $this->request->other_health_checks
			]
		);
	}

	public function registerForeignDog(){
		\App\Dog::create(
			[
				'id' => $this->dogId,
				'user_id' => $this->userId,
				'name' => $this->request->name,
				'dob' => $this->request->dob,
				'coat' => $this->request->dob,
				'breeder_id' => $this->request->breeder_id,
				'sex' => $this->request->sex,
				'registration_number' => $this->request->custom_registration_number,
//				'registration_number' => $this->generateRegistrationNumber(),
				'other_registration_number' => $this->request->custom_registration_number,
				'titles' => $this->request->titles,
				'performance_titles' => $this->request->performance_titles,
				'colour' => $this->request->colour,
				'height' => $this->request->height,
				'elbow_ed_results' => $this->request->elbow_ed_results,
				'appraisal_score' => $this->request->appraisal_score,
				'hip_hd_results' => $this->request->hip_hd_results,
				'tattoo_number' => $this->request->tattoo_number,
				'microchip_number' => $this->request->microchip_number,
				'DNA'  => $this->request->DNA,
				'other_health_checks' => $this->request->other_health_checks
			]
		);
	}

	public function registerLitter(){

		\DB::transaction(function(){
			$names   =      $this->request->get('name');
			$genders =      $this->request->get('sex');
			$colours =      $this->request->get('colour');
			$heights =      $this->request->get('height');
			$coats   =      $this->request->get('coat');
			$microchip_numbers = $this->request->get('microchip_number');

			foreach($names as $key=> $name){

				$sex = $genders[$key];
				$colour = $colours[$key];
				$height = $heights[$key];
				$coat = $coats[$key];
				$dog_id = Uuid::generate();
				$microchip_number = $microchip_numbers[$key];
				$registration_number = $this->generateRegistrationNumber();

//				if(!$name == "" || $sex) {

				$this->dogModel->create([
					'id' => $dog_id,
					'name' => $name,
					'breeder_id' => $this->request->get('breeder_id'),
					'registration_number'  => $registration_number,
					'user_id' => \Auth::id(),
					'dob' => $this->request->dob,
					'sex' => $sex,
					'colour' => $colour,
					'height' => $height,
					'coat' => $coat,
					'microchip_number' => $microchip_number,
					'owner' => $this->request->get('owner')
				]);
					$this->registerParent($dog_id);

					self::generateAncestorsFromRegistrationNumber($registration_number);
			}

//			return response()->json([json_encode($dogIdsArray)]);
		});
	}

	public function generateRegistrationNumber(){
		$year = date('Y');
		$month = date('m');
		$day = date('d');
		$count = $this->dogModel->withTrashed()->get()->count() + 1;
		$registration_number =   $year . $month . $day . $count;
			return $registration_number;
	}

	private function dogRegistrationNumberExists(){
		if($this->dogModel->whereRegistrationNumber($this->registrationNumber)->first()){
			return true;
		}else{
			return false;
		}
	}

	private function updateDogRegistrationNumber(){
		$this->dogModel->find($this->dogId)->update([
			'registration_number' => $this->registrationNumber
		]);
	}

	private function updateCustomDogRegistrationNumber(){
		$this->dogModel->find($this->dogId)->update([
			'registration_number' => $this->request->custom_registration_number
		]);
	}

	private function saveImage(){
		$id = $this->dogId;
		$path = '/public/images/catalog';
		$imageName = $this->request->hasFile('pic') ? 'image_' . $id . '.' . $this->request->file('pic')->getClientOriginalExtension(): '';

		if($this->request->hasFile('pic')) {
			$this->request->file('pic')->move(base_path() . $path, $imageName);
		}
	}

	private function hasImage(){
		if($this->request->hasFile('pic')){
			return true;
		}else{
			return false;
		}
	}

	private function dogHasCustomRegistrationNumber(){
		if($this->request->has('custom_registration_number')){
			return true;
		}else{
			return false;
		}
	}

	private function generateUuid(){
		return Uuid::generate();
	}

	private function registerParent($dog_id=null, $registrationNumber = null){
		if( $registrationNumber != null ){
			$dog = \App\Dog::whereRegistrationNumber($registrationNumber)->first();
		}else {
		$dog = \App\Dog::whereRegistrationNumber($this->registrationNumber)->first();
		}

		$this->dogRelationshipModel->create(
			[
				'id'=> Uuid::generate(),
			    'dog_id' => $dog_id !== null ? $dog_id: $dog->id,
				'father' => $this->father,
				'mother' => $this->mother
			]);
	}


	public static function getFirstGenerationParents($father_number=null, $mother_number=null){
		if ($father_number){
			return ['number' => $father_number, 'name' => \App\Dog::getRelationship($father_number)];

		}elseif($mother_number){
			return ['number' => $mother_number, 'name' => \App\Dog::getRelationship($mother_number)];
		}
	}

	public static function getSecondGenerationNumbers($father_number=null, $mother_number = null){
		if($father_number){
			return self::DogGenerationArrayResults($father_number);

		}elseif($mother_number){
			return self::DogGenerationArrayResults('',$mother_number);
		}
	}

	public static function getThirdGenerationNumbers($father_number=null, $mother_number = null){
		if($father_number){
			return self::DogGenerationArrayResults($father_number);

		}elseif($mother_number){
			return self::DogGenerationArrayResults('',$mother_number);
		}
	}

	public static function getFourthGenerationNumbers($father_number=null, $mother_number = null){
		if($father_number){
			return self::DogGenerationArrayResults($father_number);

		}elseif($mother_number){
			return self::DogGenerationArrayResults('',$mother_number);
		}
	}

	private static function DogGenerationArrayResults($father_number = null, $mother_number=null){

		if($father_number){
			return [
				'father_name' => strtoupper(\App\Dog::getParent($father_number,'father')),
				'mother_name' => strtoupper(\App\Dog::getParent($father_number,'mother')),
				'mother_number' =>strtoupper(\App\Dog::getParentId($father_number,'mother','registration_number')),
				'father_number' =>strtoupper(\App\Dog::getParentId($father_number,'father','registration_number')),
			];

		}elseif($mother_number){
			return [
				'father_name' => strtoupper( \App\Dog::getParent($mother_number,'father')),
				'mother_name' => strtoupper(\App\Dog::getParent($mother_number,'mother')),
				'mother_number' => strtoupper(\App\Dog::getParentId($mother_number,'mother','registration_number')),
				'father_number' =>strtoupper(\App\Dog::getParentId($mother_number,'father','registration_number')),
			];
		}
	}

	private static function getByRegistrationNumber($registration_number){
		return \App\Dog::whereRegistrationNumber($registration_number)->first();
	}

	public static function generateAncestorsFromRegistrationNumber($registration_number, $update=false){

		    $results = [];
			$id = \App\Dog::getDogId($registration_number);
			$parentBuilder = DogRelationship::where('dog_id', $id)->first();

		$firstGenerationParents = self::getFirstGenerationResults( $parentBuilder );

		$results['first-generation'] = $firstGenerationParents;

			// Generate second generation
		$firstGenerationParentSireId = \App\Dog::getDogId($firstGenerationParents['sire']['registration_number']);
		$firstGenerationParentDamId = \App\Dog::getDogId($firstGenerationParents['dam']['registration_number']);
		$secondSireParentBuilder = DogRelationship::where('dog_id', $firstGenerationParentSireId)->first();
		$secondDamParentBuilder = DogRelationship::where('dog_id', $firstGenerationParentDamId)->first();

		if( \App\Dog::getDogId($firstGenerationParents['sire']['registration_number']) ||
		    \App\Dog::getDogId($firstGenerationParents['dam']['registration_number']) ) {

			$secondGenerationParents = self::getSecondGenerationResults( $secondSireParentBuilder, $secondDamParentBuilder );

			$results['second-generation'] = $secondGenerationParents;
		}else {
			$secondGenerationParents = [
				'sire'=>[
//				'sire' => $secondSireParentBuilder->father,
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					]
				],

				'Dam'=>[
//				'sire' => $secondDamParentBuilder->father,
//				'dam' => $secondDamParentBuilder->mother,
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => '',
					],
					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					]
				]
			];

			$results['second-generation'] = $secondGenerationParents;
		}

		if(\App\Dog::getDogId($secondGenerationParents['sire']['sire']['registration_number']) || \App\Dog::getDogId($secondGenerationParents['sire']['dam']['registration_number'])){
			// Generate third generation
			$secondGenerationParentSireSireId = \App\Dog::getDogId($secondGenerationParents['sire']['sire']['registration_number']);
			$secondGenerationParentSireDamId = \App\Dog::getDogId($secondGenerationParents['sire']['dam']['registration_number']);

			$secondGenerationParentDamSireId = \App\Dog::getDogId($secondGenerationParents['Dam']['sire']['registration_number']);
			$secondGenerationParentDamDamId = \App\Dog::getDogId($secondGenerationParents['Dam']['dam']['registration_number']);

			$thirdGenerationParentsSireSireBuilder = DogRelationship::where('dog_id', $secondGenerationParentSireSireId)->first();
			$thirdGenerationParentSireDamBuilder = DogRelationship::where('dog_id', $secondGenerationParentSireDamId)->first();


			$thirdGenerationParentsDamSireBuilder = DogRelationship::where('dog_id', $secondGenerationParentDamSireId)->first();
			$thirdGenerationParentsDamDamBuilder = DogRelationship::where('dog_id', $secondGenerationParentDamDamId)->first();

			$thirdGenerationParents = self::getThirdGnerationResults( $thirdGenerationParentsSireSireBuilder, $thirdGenerationParentSireDamBuilder, $thirdGenerationParentsDamSireBuilder, $thirdGenerationParentsDamDamBuilder );

			$results['third-generation'] = $thirdGenerationParents;

		}else{
			$thirdGenerationParents = [
				'Sire'=>[
//				'sire' => $thirdGenerationParentsSireSireBuilder->father,
//				'dam' => $thirdGenerationParentsSireSireBuilder->mother,
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'Dam'=>[
//				'sire' => $thirdGenerationParentSireDamBuilder->father,
//				'dam' => $thirdGenerationParentSireDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => '',
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => '',
					],
				],

				'SecondSire'=>[
//				'sire' => $thirdGenerationParentsDamSireBuilder->father,
//				'dam' => $thirdGenerationParentsDamSireBuilder->mother

					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'SecondDam' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				]

			];

			$results['third-generation'] = $thirdGenerationParents;
		}


		if( \App\Dog::getDogId($thirdGenerationParents['Sire']['sire']['registration_number']) || \App\Dog::getDogId($thirdGenerationParents['Sire']['dam']['registration_number'])
		    || \App\Dog::getDogId($thirdGenerationParents['Dam']['sire']['registration_number'])){
			$thirdGenerationParentsSiresireId = \App\Dog::getDogId($thirdGenerationParents['Sire']['sire']['registration_number']);
			$thirdGenerationParentsSiredamId = \App\Dog::getDogId($thirdGenerationParents['Sire']['dam']['registration_number']);

			//First Builder
			$fourthGenerationParentsSiresireBuilder = DogRelationship::where('dog_id', $thirdGenerationParentsSiresireId)->first();
			$fourthGenerationParentsSiredamBuilder = DogRelationship::where('dog_id', $thirdGenerationParentsSiredamId)->first();


			$thirdGenerationParentsDamsireId = \App\Dog::getDogId($thirdGenerationParents['Dam']['sire']['registration_number']);
			$thirdGenerationParentsDamdamId = \App\Dog::getDogId($thirdGenerationParents['Dam']['dam']['registration_number']);

			//Second Builder
			$fourthGenerationParentsDamsireBuilder = DogRelationship::where('dog_id', $thirdGenerationParentsDamsireId)->first();
			$fourthGenerationParentsDamdamBuilder = DogRelationship::where('dog_id', $thirdGenerationParentsDamdamId)->first();


			$thirdGenerationParentsSecondSiresireId = \App\Dog::getDogId($thirdGenerationParents['SecondSire']['sire']['registration_number']);
			$thirdGenerationParentsSecondSiredamId = \App\Dog::getDogId($thirdGenerationParents['SecondSire']['dam']['registration_number']);

			//Third Builder
			$fourthGenerationParentsSecondSiresireBuilder = DogRelationship::where('dog_id', $thirdGenerationParentsSecondSiresireId)->first();
			$fourthGenerationParentsSecondSiredamBuilder = DogRelationship::where('dog_id', $thirdGenerationParentsSecondSiredamId)->first();


			$thirdGenerationParentsSecondDamsireId = \App\Dog::getDogId($thirdGenerationParents['SecondDam']['sire']['registration_number']);
			$thirdGenerationParentsSecondDamdamId = \App\Dog::getDogId($thirdGenerationParents['SecondDam']['dam']['registration_number']);

			//Fourth Builder
			$fourthGenerationParentsSecondDamsireBuilder = DogRelationship::where('dog_id', $thirdGenerationParentsSecondDamsireId)->first();
			$fourthGenerationParentsSecondDamdamBuilder = DogRelationship::where('dog_id', $thirdGenerationParentsSecondDamdamId)->first();


			$fourthGenerationParents = self::getFourthGenerationResults( $fourthGenerationParentsSiresireBuilder, $fourthGenerationParentsSiredamBuilder, $fourthGenerationParentsDamsireBuilder, $fourthGenerationParentsDamdamBuilder, $fourthGenerationParentsSecondSiresireBuilder, $fourthGenerationParentsSecondSiredamBuilder, $fourthGenerationParentsSecondDamsireBuilder, $fourthGenerationParentsSecondDamdamBuilder );

			$results['fourth-generation'] = $fourthGenerationParents;

		}else{
			$fourthGenerationParents = [
				'Sire'=>[
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'Dam'=>[

					'sire' => [
						'name' => '',
						'registration_number' =>  '',
						'titles' => ''
					],

					'dam' => [
						'name' =>'',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'SecondSire'=>[

					'sire' => [
						'name' => '' ,
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' =>'',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'SecondDam' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' =>'',
						'titles' => ''
					],
				],

				'ThirdSire'=>[
//				'sire' => $thirdGenerationParentsDamSireBuilder->father,
//				'dam' => $thirdGenerationParentsDamSireBuilder->mother

					'sire' => [
						'name' => '' ,
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'ThirdDam'=>[
//				'sire' => $thirdGenerationParentsDamSireBuilder->father,
//				'dam' => $thirdGenerationParentsDamSireBuilder->mother

					'sire' => [
						'name' =>'' ,
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'FourthSire'=>[
//				'sire' => $thirdGenerationParentsDamSireBuilder->father,
//				'dam' => $thirdGenerationParentsDamSireBuilder->mother

					'sire' => [
						'name' => '' ,
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
				]
			],

			    'FourthDam'=>[
//				'sire' => $thirdGenerationParentsDamSireBuilder->father,
//				'dam' => $thirdGenerationParentsDamSireBuilder->mother

					'sire' => [
						'name' => '' ,
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],
				];
			$results['fourth-generation'] = $fourthGenerationParents;
		}

		if($update !== false){
			DogGeneration::whereDogId($id)->first()->update([
				'first_generation' => ($results['first-generation']),
				'second_generation' => ($results['second-generation']),
				'third_generation' => ($results['third-generation']),
				'fourth_generation' => ($results['fourth-generation']),
			]);
		}else{
			DogGeneration::create([
				'id'        => Uuid::generate(),
				'dog_id'    => $id,
				'first_generation' => ($results['first-generation']),
				'second_generation' => ($results['second-generation']),
				'third_generation' => ($results['third-generation']),
				'fourth_generation' => ($results['fourth-generation']),
			]);
		}

//
		return $results;
	}

	public static function setUpGenerationJsonStructure(){
		$results = [];

		$firstGenerationParents = [
			'sire' => [
				'name' => '',
				'registration_number' => '',
				'titles' => ''
			],
			'dam' => [
//					'name' => $parentBuilder,
				'name' => '',
				'registration_number' => '',
				'titles' =>  ''
			]
		];

		$results['first-generation'] = $firstGenerationParents;

		$secondGenerationParents = [
			'sire'=>[
				'sire' => [
					'name' => '',
					'registration_number' => '',
					'titles' => ''
				],
				'dam' => [
					'name' => '',
					'registration_number' => '',
					'titles' => ''
				]
			],

			'Dam'=>[

				'sire' => [
					'name' => '',
					'registration_number' => '',
					'titles' => '',
				],
				'dam' => [
					'name' => '',
					'registration_number' => '',
					'titles' => ''
				]
			]
		];

		$results['second-generation'] = $secondGenerationParents;

		$thirdGenerationParents = [
			'Sire'=>[
//				'sire' => $thirdGenerationParentsSireSireBuilder->father,
//				'dam' => $thirdGenerationParentsSireSireBuilder->mother,
				'sire' => [
					'name' => '',
					'registration_number' => '',
					'titles' => ''
				],

				'dam' => [
					'name' => '',
					'registration_number' => '',
					'titles' => ''
				],
			],

			'Dam'=>[
//				'sire' => $thirdGenerationParentSireDamBuilder->father,
//				'dam' => $thirdGenerationParentSireDamBuilder->mother
				'sire' => [
					'name' => '',
					'registration_number' => '',
					'titles' => '',
				],

				'dam' => [
					'name' => '',
					'registration_number' => '',
					'titles' => '',
				],
			],

			'SecondSire'=>[

				'sire' => [
					'name' => '',
					'registration_number' => '',
					'titles' => ''
				],

				'dam' => [
					'name' => '',
					'registration_number' => '',
					'titles' => ''
				],
			],

			'SecondDam' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
				'sire' => [
					'name' => '',
					'registration_number' => '',
					'titles' => ''
				],

				'dam' => [
					'name' => '',
					'registration_number' => '',
					'titles' => ''
				],
			]

		];

		$results['third-generation'] = $thirdGenerationParents;

		$fourthGenerationParents = [
			'Sire'=>[
				'sire' => [
					'name' => '',
					'registration_number' => '',
					'titles' => ''
				],

				'dam' => [
					'name' => '',
					'registration_number' => '',
					'titles' => ''
				],
			],

			'Dam'=>[

				'sire' => [
					'name' => '',
					'registration_number' =>  '',
					'titles' => ''
				],

				'dam' => [
					'name' =>'',
					'registration_number' => '',
					'titles' => ''
				],
			],

			'SecondSire'=>[

				'sire' => [
					'name' => '' ,
					'registration_number' => '',
					'titles' => ''
				],

				'dam' => [
					'name' =>'',
					'registration_number' => '',
					'titles' => ''
				],
			],

			'SecondDam' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
				'sire' => [
					'name' => '',
					'registration_number' => '',
					'titles' => ''
				],

				'dam' => [
					'name' => '',
					'registration_number' =>'',
					'titles' => ''
				],
			],

			'ThirdSire'=>[
//				'sire' => $thirdGenerationParentsDamSireBuilder->father,
//				'dam' => $thirdGenerationParentsDamSireBuilder->mother

				'sire' => [
					'name' => '' ,
					'registration_number' => '',
					'titles' => ''
				],

				'dam' => [
					'name' => '',
					'registration_number' => '',
					'titles' => ''
				],
			],

			'ThirdDam'=>[
//				'sire' => $thirdGenerationParentsDamSireBuilder->father,
//				'dam' => $thirdGenerationParentsDamSireBuilder->mother

				'sire' => [
					'name' =>'' ,
					'registration_number' => '',
					'titles' => ''
				],

				'dam' => [
					'name' => '',
					'registration_number' => '',
					'titles' => ''
				],
			],

			'FourthSire'=>[
//				'sire' => $thirdGenerationParentsDamSireBuilder->father,
//				'dam' => $thirdGenerationParentsDamSireBuilder->mother

				'sire' => [
					'name' => '' ,
					'registration_number' => '',
					'titles' => ''
				],

				'dam' => [
					'name' => '',
					'registration_number' => '',
					'titles' => ''
				],
			],

			'FourthDam'=>[

			'sire' => [
				'name' => '' ,
				'registration_number' => '',
				'titles' => ''
			],

			'dam' => [
				'name' => '',
				'registration_number' => '',
				'titles' => ''
			],
		]
		];
		$results['fourth-generation'] = $fourthGenerationParents;

		return ($results);
	}
}