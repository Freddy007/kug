<?php
/**
 * Created by IntelliJ IDEA.
 * User: Freddy
 * Date: 2/12/2018
 * Time: 4:01 PM
 */

namespace App\HelperClasses;


use App\DogGeneration;
use App\DogRelationship;
use App\Http\Requests\Request;
use Webpatser\Uuid\Uuid;

class Pedigree {

	/**
	 * @var $request Request
	 */
	private $request;
	private $generation;
	private $position;
	private $name;
	private $registration_number;
	private $titles;
	private $dog_id;
	private $dog_been_inserted_id;
	private $father;
	private $mother;
	private $parent_generation;
	private $builder;

	public function __construct($request=null) {
		$this->request = $request;

		$this->generation           = $this->request->generation."_generation";
		$this->position             = explode(',',$this->request->position);
		$this->name                 = $this->request->name;
		$this->registration_number  = $this->request->registration_number;
		$this->titles               = $this->request->titles;
		$this->dog_id               = $this->request->dog_id;
		$this->dog_been_inserted_id = $this->request->id;
		$this->father               = explode(',',$this->request->get('father'));
		$this->mother               = explode(',',$this->request->get('mother'));
		$this->parent_generation    = $this->mother[0].'_generation';
		$this->builder              = DogGeneration::whereDogId($this->dog_id)->first();
	}


	public function addToPedigreeTable(){
		$builder = DogGeneration::whereDogId($this->dog_id)->first();

		$generation_results = json_decode( json_encode( $this->builder->$this->generation ), true );

		$parent_generation_results = json_decode( json_encode( $this->builder->$this->parent_generation ), true );

		$dog_been_inserted_builder = DogGeneration::whereDogId($this->dog_been_inserted_id)->first();

		$dog_been_inserted_first_generation_results = json_decode(json_encode($dog_been_inserted_builder->first_generation),true);

		if($this->generation == 'first_generation'){
			unset($generation_results[$this->position[0]]);

			$generation_results[$this->position[0]] = [
				'name'                => $this->name,
				'registration_number' => $this->registration_number,
				'titles'              => $this->titles
			];

		}else {
			unset( $generation_results[ $this->position[0] ][ $this->position[1] ] );
			$generation_results[ $this->position[0] ][ $this->position[1] ] = [
				'name'                => $this->name,
				'registration_number' => $this->registration_number,
				'titles'              => $this->titles
			];
		}

		if( $this->request->mother !== 'undefined' && $this->request->father !== 'undefined'){
			unset($parent_generation_results[$this->father[1]][$this->father[2]]);

			$parent_generation_results[$this->father[1]][$this->father[2]] = [
				'name'                => $dog_been_inserted_first_generation_results['sire']['name'],
				'registration_number' => $dog_been_inserted_first_generation_results['sire']['registration_number'],
				'titles'              => $dog_been_inserted_first_generation_results['sire']['titles']
			];

			unset($parent_generation_results[$this->mother[1]][$this->mother[2]]);
			$parent_generation_results[$this->mother[1]][$this->mother[2]] = [
				'name'                => $dog_been_inserted_first_generation_results['dam']['name'],
				'registration_number' => $dog_been_inserted_first_generation_results['dam']['registration_number'],
				'titles'              => $dog_been_inserted_first_generation_results['dam']['titles']
			];

			$this->builder->update([
				$this->generation         =>  $generation_results,
				$this->parent_generation  =>  $parent_generation_results
			]);

		}else{
			$this->builder->update([
				$this->generation        =>  $generation_results
			]);
		}
	}

	public static function generateAncestorsFromRegistrationNumber($registration_number){

		$results = [];
		$id = \App\Dog::getDogId($registration_number);
		$parentBuilder = DogRelationship::where('dog_id', $id)->first();


		$firstGenerationParents = [
			'sire' => [
				'name' => $parentBuilder && self::getByRegistrationNumber($parentBuilder->father) ? self::getByRegistrationNumber($parentBuilder->father)->name :'',
				'registration_number' => $parentBuilder && $parentBuilder->father ? $parentBuilder->father :'',
				'titles' => $parentBuilder && self::getByRegistrationNumber($parentBuilder->father) ? self::getByRegistrationNumber($parentBuilder->father)->titles:''
			],
			'dam' => [
//					'name' => $parentBuilder,
				'name' => $parentBuilder && self::getByRegistrationNumber($parentBuilder->mother) ? self::getByRegistrationNumber($parentBuilder->mother)->name :'',
				'registration_number' => $parentBuilder && $parentBuilder->mother ? $parentBuilder->mother :'',
				'titles' =>  $parentBuilder &&  \App\Dog::whereRegistrationNumber($parentBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($parentBuilder->mother)->first()->titles :''
			]
		];

		$results['first-generation'] = $firstGenerationParents;


		// Generate second generation
		$firstGenerationParentSireId = \App\Dog::getDogId($firstGenerationParents['sire']['registration_number']);
		$firstGenerationParentDamId = \App\Dog::getDogId($firstGenerationParents['dam']['registration_number']);
		$secondSireParentBuilder = DogRelationship::where('dog_id', $firstGenerationParentSireId)->first();
		$secondDamParentBuilder = DogRelationship::where('dog_id', $firstGenerationParentDamId)->first();

		if( \App\Dog::getDogId($firstGenerationParents['sire']['registration_number']) ||
		    \App\Dog::getDogId($firstGenerationParents['dam']['registration_number']) )
		{
			$secondGenerationParents = [
				'sire'=>[
//				'sire' => $secondSireParentBuilder->father,
					'sire' => [
						'name' => $secondSireParentBuilder && \App\Dog::whereRegistrationNumber($secondSireParentBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($secondSireParentBuilder->father)->first()->name :'',
						'registration_number' => $secondSireParentBuilder && $secondSireParentBuilder->father ? $secondSireParentBuilder->father : '',
						'titles' =>  $secondSireParentBuilder && \App\Dog::whereRegistrationNumber($secondSireParentBuilder->father)->first()  ?  \App\Dog::whereRegistrationNumber($secondSireParentBuilder->father)->first()->titles :''
					],
					'dam' => [
						'name' => $secondSireParentBuilder && \App\Dog::whereRegistrationNumber($secondSireParentBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($secondSireParentBuilder->mother)->first()->name :'',
						'registration_number' => $secondSireParentBuilder && $secondSireParentBuilder->mother? $secondSireParentBuilder->mother:'',
						'titles' => $secondSireParentBuilder && \App\Dog::whereRegistrationNumber($secondSireParentBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($secondSireParentBuilder->mother)->first()->titles :''
					]
				],

				'Dam'=>[
//				'sire' => $secondDamParentBuilder->father,
//				'dam' => $secondDamParentBuilder->mother,
					'sire' => [
						'name' => $secondDamParentBuilder && \App\Dog::whereRegistrationNumber($secondDamParentBuilder->father)->first() ?
							\App\Dog::whereRegistrationNumber($secondDamParentBuilder->father)->first()->name :'',
						'registration_number' => $secondDamParentBuilder ? $secondDamParentBuilder->father : '',
						'titles' => $secondDamParentBuilder && \App\Dog::whereRegistrationNumber($secondDamParentBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($secondDamParentBuilder->father)->first()->titles : ''
					],
					'dam' => [
						'name' => $secondDamParentBuilder && \App\Dog::whereRegistrationNumber($secondDamParentBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($secondDamParentBuilder->mother)->first()->name :'',
						'registration_number' => $secondDamParentBuilder && $secondDamParentBuilder->mother ? $secondDamParentBuilder->mother : '',
						'titles' => $secondDamParentBuilder && \App\Dog::whereRegistrationNumber($secondDamParentBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($secondDamParentBuilder->mother)->first()->titles :''
					]
				]
			];

			$results['second-generation'] = $secondGenerationParents;
		}else {
			$secondGenerationParents = [
				'sire'=>[
//				'sire' => $secondSireParentBuilder->father,
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					]
				],

				'Dam'=>[
//				'sire' => $secondDamParentBuilder->father,
//				'dam' => $secondDamParentBuilder->mother,
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => '',
					],
					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					]
				]
			];

			$results['second-generation'] = $secondGenerationParents;
		}

		if(\App\Dog::getDogId($secondGenerationParents['sire']['sire']['registration_number']) || \App\Dog::getDogId($secondGenerationParents['sire']['dam']['registration_number'])){
			// Generate third generation
			$secondGenerationParentSireSireId = \App\Dog::getDogId($secondGenerationParents['sire']['sire']['registration_number']);
			$secondGenerationParentSireDamId = \App\Dog::getDogId($secondGenerationParents['sire']['dam']['registration_number']);

			$secondGenerationParentDamSireId = \App\Dog::getDogId($secondGenerationParents['Dam']['sire']['registration_number']);
			$secondGenerationParentDamDamId = \App\Dog::getDogId($secondGenerationParents['Dam']['dam']['registration_number']);

			$thirdGenerationParentsSireSireBuilder = DogRelationship::where('dog_id', $secondGenerationParentSireSireId)->first();
			$thirdGenerationParentSireDamBuilder = DogRelationship::where('dog_id', $secondGenerationParentSireDamId)->first();


			$thirdGenerationParentsDamSireBuilder = DogRelationship::where('dog_id', $secondGenerationParentDamSireId)->first();
			$thirdGenerationParentsDamDamBuilder = DogRelationship::where('dog_id', $secondGenerationParentDamDamId)->first();

			$thirdGenerationParents = [
				'Sire'=>[
//				'sire' => $thirdGenerationParentsSireSireBuilder->father,
//				'dam' => $thirdGenerationParentsSireSireBuilder->mother,
					'sire' => [
						'name' => \App\Dog::whereRegistrationNumber($thirdGenerationParentsSireSireBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsSireSireBuilder->father)->first()->name :'',
						'registration_number' => $thirdGenerationParentsSireSireBuilder->father,
						'titles' => \App\Dog::whereRegistrationNumber($thirdGenerationParentsSireSireBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsSireSireBuilder->father)->first()->titles :''
					],

					'dam' => [
						'name' => \App\Dog::whereRegistrationNumber($thirdGenerationParentsSireSireBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsSireSireBuilder->mother)->first()->name : "",
						'registration_number' => $thirdGenerationParentsSireSireBuilder->mother,
						'titles' => \App\Dog::whereRegistrationNumber($thirdGenerationParentsSireSireBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsSireSireBuilder->mother)->first()->titles: ''
					],
				],

				'Dam'=>[
//				'sire' => $thirdGenerationParentSireDamBuilder->father,
//				'dam' => $thirdGenerationParentSireDamBuilder->mother
					'sire' => [
						'name' => $thirdGenerationParentSireDamBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentSireDamBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentSireDamBuilder->father)->first()->name : '',
						'registration_number' => $thirdGenerationParentSireDamBuilder ? $thirdGenerationParentSireDamBuilder->father : '',
						'titles' => $thirdGenerationParentSireDamBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentSireDamBuilder->father)->first()  ? \App\Dog::whereRegistrationNumber($thirdGenerationParentSireDamBuilder->father)->first()->titles :''
					],

					'dam' => [
						'name' => $thirdGenerationParentSireDamBuilder &&  \App\Dog::whereRegistrationNumber($thirdGenerationParentSireDamBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentSireDamBuilder->mother)->first()->name :'',
						'registration_number' => $thirdGenerationParentSireDamBuilder ? $thirdGenerationParentSireDamBuilder->mother : '',
						'titles' => $thirdGenerationParentSireDamBuilder &&  \App\Dog::whereRegistrationNumber($thirdGenerationParentSireDamBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentSireDamBuilder->mother)->first()->titles :''
					],
				],

				'SecondSire'=>[
//				'sire' => $thirdGenerationParentsDamSireBuilder->father,
//				'dam' => $thirdGenerationParentsDamSireBuilder->mother

					'sire' => [
						'name' => $thirdGenerationParentsDamSireBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamSireBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamSireBuilder->father)->first()->name :'' ,
						'registration_number' => $thirdGenerationParentsDamSireBuilder && $thirdGenerationParentsDamSireBuilder->father ?$thirdGenerationParentsDamSireBuilder->father :'',
						'titles' => $thirdGenerationParentsDamSireBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamSireBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamSireBuilder->father)->first()->titles :''
					],

					'dam' => [
						'name' => $thirdGenerationParentsDamSireBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamSireBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamSireBuilder->mother)->first()->name :'',
						'registration_number' => $thirdGenerationParentsDamSireBuilder && $thirdGenerationParentsDamSireBuilder->mother ? $thirdGenerationParentsDamSireBuilder->mother :'',
						'titles' => $thirdGenerationParentsDamSireBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamSireBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamSireBuilder->mother)->first()->titles :''
					],
				],

				'SecondDam' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => $thirdGenerationParentsDamDamBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamDamBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamDamBuilder->father)->first()->name :'',
						'registration_number' => $thirdGenerationParentsDamDamBuilder && $thirdGenerationParentsDamDamBuilder->father ? $thirdGenerationParentsDamDamBuilder->father :'',
						'titles' => $thirdGenerationParentsDamDamBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamDamBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamDamBuilder->father)->first()->titles :''
					],

					'dam' => [
						'name' =>$thirdGenerationParentsDamDamBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamDamBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamDamBuilder->mother)->first()->name : '',
						'registration_number' => $thirdGenerationParentsDamDamBuilder && $thirdGenerationParentsDamDamBuilder->mother ? $thirdGenerationParentsDamDamBuilder->mother :'',
						'titles' => $thirdGenerationParentsDamDamBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamDamBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamDamBuilder->mother)->first()->titles :''
					],
				]

			];

			$results['third-generation'] = $thirdGenerationParents;

		}else{
			$thirdGenerationParents = [
				'Sire'=>[
//				'sire' => $thirdGenerationParentsSireSireBuilder->father,
//				'dam' => $thirdGenerationParentsSireSireBuilder->mother,
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'Dam'=>[
//				'sire' => $thirdGenerationParentSireDamBuilder->father,
//				'dam' => $thirdGenerationParentSireDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => '',
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => '',
					],
				],

				'SecondSire'=>[
//				'sire' => $thirdGenerationParentsDamSireBuilder->father,
//				'dam' => $thirdGenerationParentsDamSireBuilder->mother

					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'SecondDam' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				]

			];

			$results['third-generation'] = $thirdGenerationParents;
		}


		if( \App\Dog::getDogId($thirdGenerationParents['Sire']['sire']['registration_number']) || \App\Dog::getDogId($thirdGenerationParents['Sire']['dam']['registration_number'])
		    || \App\Dog::getDogId($thirdGenerationParents['Dam']['sire']['registration_number'])){
			$thirdGenerationParentsSiresireId = \App\Dog::getDogId($thirdGenerationParents['Sire']['sire']['registration_number']);
			$thirdGenerationParentsSiredamId = \App\Dog::getDogId($thirdGenerationParents['Sire']['dam']['registration_number']);

			//First Builder
			$fourthGenerationParentsSiresireBuilder = DogRelationship::where('dog_id', $thirdGenerationParentsSiresireId)->first();
			$fourthGenerationParentsSiredamBuilder = DogRelationship::where('dog_id', $thirdGenerationParentsSiredamId)->first();


			$thirdGenerationParentsDamsireId = \App\Dog::getDogId($thirdGenerationParents['Dam']['sire']['registration_number']);
			$thirdGenerationParentsDamdamId = \App\Dog::getDogId($thirdGenerationParents['Dam']['dam']['registration_number']);

			//Second Builder
			$fourthGenerationParentsDamsireBuilder = DogRelationship::where('dog_id', $thirdGenerationParentsDamsireId)->first();
			$fourthGenerationParentsDamdamBuilder = DogRelationship::where('dog_id', $thirdGenerationParentsDamdamId)->first();


			$thirdGenerationParentsSecondSiresireId = \App\Dog::getDogId($thirdGenerationParents['SecondSire']['sire']['registration_number']);
			$thirdGenerationParentsSecondSiredamId = \App\Dog::getDogId($thirdGenerationParents['SecondSire']['dam']['registration_number']);

			//Third Builder
			$fourthGenerationParentsSecondSiresireBuilder = DogRelationship::where('dog_id', $thirdGenerationParentsSecondSiresireId)->first();
			$fourthGenerationParentsSecondSiredamBuilder = DogRelationship::where('dog_id', $thirdGenerationParentsSecondSiredamId)->first();


			$thirdGenerationParentsSecondDamsireId = \App\Dog::getDogId($thirdGenerationParents['SecondDam']['sire']['registration_number']);
			$thirdGenerationParentsSecondDamdamId = \App\Dog::getDogId($thirdGenerationParents['SecondDam']['dam']['registration_number']);

			//Fourth Builder
			$fourthGenerationParentsSecondDamsireBuilder = DogRelationship::where('dog_id', $thirdGenerationParentsSecondDamsireId)->first();
			$fourthGenerationParentsSecondDamdamBuilder = DogRelationship::where('dog_id', $thirdGenerationParentsSecondDamdamId)->first();


			$fourthGenerationParents = [
				'Sire'=>[
					'sire' => [
						'name' => $fourthGenerationParentsSiresireBuilder && \App\Dog::whereRegistrationNumber($fourthGenerationParentsSiresireBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($fourthGenerationParentsSiresireBuilder->father)->first()->name :'',
						'registration_number' =>$fourthGenerationParentsSiresireBuilder && $fourthGenerationParentsSiresireBuilder->father ? $fourthGenerationParentsSiresireBuilder->father :'',
						'titles' => $fourthGenerationParentsSiresireBuilder && \App\Dog::whereRegistrationNumber($fourthGenerationParentsSiresireBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($fourthGenerationParentsSiresireBuilder->father)->first()->titles :''
					],

					'dam' => [
						'name' => $fourthGenerationParentsSiresireBuilder && \App\Dog::whereRegistrationNumber($fourthGenerationParentsSiresireBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($fourthGenerationParentsSiresireBuilder->mother)->first()->name : "",
						'registration_number' => $fourthGenerationParentsSiresireBuilder && $fourthGenerationParentsSiresireBuilder->mother ? $fourthGenerationParentsSiresireBuilder->mother :'',
						'titles' => $fourthGenerationParentsSiresireBuilder && \App\Dog::whereRegistrationNumber($fourthGenerationParentsSiresireBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($fourthGenerationParentsSiresireBuilder->mother)->first()->titles: ''
					],
				],

				'Dam'=>[
//				'sire' => $thirdGenerationParentSireDamBuilder->father,
//				'dam' => $thirdGenerationParentSireDamBuilder->mother
					'sire' => [
						'name' => $fourthGenerationParentsSiredamBuilder && \App\Dog::whereRegistrationNumber($fourthGenerationParentsSiredamBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($fourthGenerationParentsSiredamBuilder->father)->first()->name : '',
						'registration_number' => $fourthGenerationParentsSiredamBuilder ? $fourthGenerationParentsSiredamBuilder->father : '',
						'titles' => $fourthGenerationParentsSiredamBuilder && \App\Dog::whereRegistrationNumber($fourthGenerationParentsSiredamBuilder->father)->first()  ? \App\Dog::whereRegistrationNumber($fourthGenerationParentsSiredamBuilder->father)->first()->titles :''
					],

					'dam' => [
						'name' => $fourthGenerationParentsSiredamBuilder &&  \App\Dog::whereRegistrationNumber($fourthGenerationParentsSiredamBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($fourthGenerationParentsSiredamBuilder->mother)->first()->name :'',
						'registration_number' => $fourthGenerationParentsSiredamBuilder ? $fourthGenerationParentsSiredamBuilder->mother : '',
						'titles' => $fourthGenerationParentsSiredamBuilder &&  \App\Dog::whereRegistrationNumber($fourthGenerationParentsSiredamBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($fourthGenerationParentsSiredamBuilder->mother)->first()->titles :''
					],
				],

				'SecondSire'=>[
//				'sire' => $thirdGenerationParentsDamSireBuilder->father,
//				'dam' => $thirdGenerationParentsDamSireBuilder->mother

					'sire' => [
						'name' => $fourthGenerationParentsDamsireBuilder && \App\Dog::whereRegistrationNumber($fourthGenerationParentsDamsireBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($fourthGenerationParentsDamsireBuilder->father)->first()->name :'' ,
						'registration_number' => $fourthGenerationParentsDamsireBuilder && $fourthGenerationParentsDamsireBuilder->father ?$fourthGenerationParentsDamsireBuilder->father :'',
						'titles' => $fourthGenerationParentsDamsireBuilder && \App\Dog::whereRegistrationNumber($fourthGenerationParentsDamsireBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($fourthGenerationParentsDamsireBuilder->father)->first()->titles :''
					],

					'dam' => [
						'name' => $fourthGenerationParentsDamsireBuilder && \App\Dog::whereRegistrationNumber($fourthGenerationParentsDamsireBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($fourthGenerationParentsDamsireBuilder->mother)->first()->name :'',
						'registration_number' => $fourthGenerationParentsDamsireBuilder && $fourthGenerationParentsDamsireBuilder->mother ? $fourthGenerationParentsDamsireBuilder->mother :'',
						'titles' => $fourthGenerationParentsDamsireBuilder && \App\Dog::whereRegistrationNumber($fourthGenerationParentsDamsireBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($fourthGenerationParentsDamsireBuilder->mother)->first()->titles :''
					],
				],

				'SecondDam' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => $fourthGenerationParentsDamdamBuilder && \App\Dog::whereRegistrationNumber($fourthGenerationParentsDamdamBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($fourthGenerationParentsDamdamBuilder->father)->first()->name :'',
						'registration_number' => $fourthGenerationParentsDamdamBuilder && $fourthGenerationParentsDamdamBuilder->father ? $fourthGenerationParentsDamdamBuilder->father :'',
						'titles' => $fourthGenerationParentsDamdamBuilder && \App\Dog::whereRegistrationNumber($fourthGenerationParentsDamdamBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($fourthGenerationParentsDamdamBuilder->father)->first()->titles :''
					],

					'dam' => [
						'name' =>$fourthGenerationParentsDamdamBuilder && \App\Dog::whereRegistrationNumber($fourthGenerationParentsDamdamBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($fourthGenerationParentsDamdamBuilder->mother)->first()->name : '',
						'registration_number' => $fourthGenerationParentsDamdamBuilder && $fourthGenerationParentsDamdamBuilder->mother ? $fourthGenerationParentsDamdamBuilder->mother :'',
						'titles' => $fourthGenerationParentsDamdamBuilder && \App\Dog::whereRegistrationNumber($fourthGenerationParentsDamdamBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($fourthGenerationParentsDamdamBuilder->mother)->first()->titles :''
					],
				],

				'ThirdSire'=>[
//				'sire' => $thirdGenerationParentsDamSireBuilder->father,
//				'dam' => $thirdGenerationParentsDamSireBuilder->mother

					'sire' => [
						'name' => $fourthGenerationParentsSecondSiresireBuilder && \App\Dog::whereRegistrationNumber($fourthGenerationParentsSecondSiresireBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($fourthGenerationParentsSecondSiresireBuilder->father)->first()->name :'' ,
						'registration_number' => $fourthGenerationParentsSecondSiresireBuilder && $fourthGenerationParentsSecondSiresireBuilder->father ?$fourthGenerationParentsSecondSiresireBuilder->father :'',
						'titles' => $fourthGenerationParentsSecondSiresireBuilder && \App\Dog::whereRegistrationNumber($fourthGenerationParentsSecondSiresireBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($fourthGenerationParentsSecondSiresireBuilder->father)->first()->titles :''
					],

					'dam' => [
						'name' => $fourthGenerationParentsSecondSiresireBuilder && \App\Dog::whereRegistrationNumber($fourthGenerationParentsSecondSiresireBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($fourthGenerationParentsSecondSiresireBuilder->mother)->first()->name :'',
						'registration_number' => $fourthGenerationParentsSecondSiresireBuilder && $fourthGenerationParentsSecondSiresireBuilder->mother ? $fourthGenerationParentsSecondSiresireBuilder->mother :'',
						'titles' => $fourthGenerationParentsSecondSiresireBuilder && \App\Dog::whereRegistrationNumber($fourthGenerationParentsSecondSiresireBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($fourthGenerationParentsSecondSiresireBuilder->mother)->first()->titles :''
					],
				],

				'ThirdDam'=>[
//				'sire' => $thirdGenerationParentsDamSireBuilder->father,
//				'dam' => $thirdGenerationParentsDamSireBuilder->mother

					'sire' => [
						'name' => $fourthGenerationParentsSecondSiredamBuilder && \App\Dog::whereRegistrationNumber($fourthGenerationParentsSecondSiredamBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($fourthGenerationParentsSecondSiredamBuilder->father)->first()->name :'' ,
						'registration_number' => $fourthGenerationParentsSecondSiredamBuilder && $fourthGenerationParentsSecondSiredamBuilder->father ?$fourthGenerationParentsSecondSiredamBuilder->father :'',
						'titles' => $fourthGenerationParentsSecondSiredamBuilder && \App\Dog::whereRegistrationNumber($fourthGenerationParentsSecondSiredamBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($fourthGenerationParentsSecondSiredamBuilder->father)->first()->titles :''
					],

					'dam' => [
						'name' => $fourthGenerationParentsSecondSiredamBuilder && \App\Dog::whereRegistrationNumber($fourthGenerationParentsSecondSiredamBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($fourthGenerationParentsSecondSiredamBuilder->mother)->first()->name :'',
						'registration_number' => $fourthGenerationParentsSecondSiredamBuilder && $fourthGenerationParentsSecondSiredamBuilder->mother ? $fourthGenerationParentsSecondSiredamBuilder->mother :'',
						'titles' => $fourthGenerationParentsSecondSiredamBuilder && \App\Dog::whereRegistrationNumber($fourthGenerationParentsSecondSiredamBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($fourthGenerationParentsSecondSiredamBuilder->mother)->first()->titles :''
					],
				],

				'FourthSire'=>[
//				'sire' => $thirdGenerationParentsDamSireBuilder->father,
//				'dam' => $thirdGenerationParentsDamSireBuilder->mother

					'sire' => [
						'name' => $fourthGenerationParentsSecondDamsireBuilder && \App\Dog::whereRegistrationNumber($fourthGenerationParentsSecondDamsireBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($fourthGenerationParentsSecondDamsireBuilder->father)->first()->name :'' ,
						'registration_number' => $fourthGenerationParentsSecondDamsireBuilder && $fourthGenerationParentsSecondDamsireBuilder->father ?$fourthGenerationParentsSecondDamsireBuilder->father :'',
						'titles' => $fourthGenerationParentsSecondDamsireBuilder && \App\Dog::whereRegistrationNumber($fourthGenerationParentsSecondDamsireBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($fourthGenerationParentsSecondDamsireBuilder->father)->first()->titles :''
					],

					'dam' => [
						'name' => $fourthGenerationParentsSecondDamsireBuilder && \App\Dog::whereRegistrationNumber($fourthGenerationParentsSecondDamsireBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($fourthGenerationParentsSecondDamsireBuilder->mother)->first()->name :'',
						'registration_number' => $fourthGenerationParentsSecondDamsireBuilder && $fourthGenerationParentsSecondDamsireBuilder->mother ? $fourthGenerationParentsSecondDamsireBuilder->mother :'',
						'titles' => $fourthGenerationParentsSecondDamsireBuilder && \App\Dog::whereRegistrationNumber($fourthGenerationParentsSecondDamsireBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($fourthGenerationParentsSecondDamsireBuilder->mother)->first()->titles :''
					],
				],

				'FourthDam'=>[
//				'sire' => $thirdGenerationParentsDamSireBuilder->father,
//				'dam' => $thirdGenerationParentsDamSireBuilder->mother

					'sire' => [
						'name' => $fourthGenerationParentsSecondDamdamBuilder && \App\Dog::whereRegistrationNumber($fourthGenerationParentsSecondDamdamBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($fourthGenerationParentsSecondDamdamBuilder->father)->first()->name :'' ,
						'registration_number' => $fourthGenerationParentsSecondDamdamBuilder && $fourthGenerationParentsSecondDamdamBuilder->father ?$fourthGenerationParentsSecondDamdamBuilder->father :'',
						'titles' => $fourthGenerationParentsSecondDamdamBuilder && \App\Dog::whereRegistrationNumber($fourthGenerationParentsSecondDamdamBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($fourthGenerationParentsSecondDamdamBuilder->father)->first()->titles :''
					],

					'dam' => [
						'name' => $fourthGenerationParentsSecondDamdamBuilder && \App\Dog::whereRegistrationNumber($fourthGenerationParentsSecondDamdamBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($fourthGenerationParentsSecondDamdamBuilder->mother)->first()->name :'',
						'registration_number' => $fourthGenerationParentsSecondDamdamBuilder && $fourthGenerationParentsSecondDamdamBuilder->mother ? $fourthGenerationParentsSecondDamdamBuilder->mother :'',
						'titles' => $fourthGenerationParentsSecondDamdamBuilder && \App\Dog::whereRegistrationNumber($fourthGenerationParentsSecondDamdamBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($fourthGenerationParentsSecondDamdamBuilder->mother)->first()->titles :''
					],
				],
			];

			$results['fourth-generation'] = $fourthGenerationParents;

		}else{
			$fourthGenerationParents = [
				'Sire'=>[
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'Dam'=>[

					'sire' => [
						'name' => '',
						'registration_number' =>  '',
						'titles' => ''
					],

					'dam' => [
						'name' =>'',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'SecondSire'=>[

					'sire' => [
						'name' => '' ,
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' =>'',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'SecondDam' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' =>'',
						'titles' => ''
					],
				],

				'ThirdSire'=>[
//				'sire' => $thirdGenerationParentsDamSireBuilder->father,
//				'dam' => $thirdGenerationParentsDamSireBuilder->mother

					'sire' => [
						'name' => '' ,
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'ThirdDam'=>[
//				'sire' => $thirdGenerationParentsDamSireBuilder->father,
//				'dam' => $thirdGenerationParentsDamSireBuilder->mother

					'sire' => [
						'name' =>'' ,
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'FourthSire'=>[
//				'sire' => $thirdGenerationParentsDamSireBuilder->father,
//				'dam' => $thirdGenerationParentsDamSireBuilder->mother

					'sire' => [
						'name' => '' ,
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					]
				],

				'FourthDam'=>[
//				'sire' => $thirdGenerationParentsDamSireBuilder->father,
//				'dam' => $thirdGenerationParentsDamSireBuilder->mother

					'sire' => [
						'name' => '' ,
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],
			];
			$results['fourth-generation'] = $fourthGenerationParents;
		}

		DogGeneration::create([
			'id'        => Uuid::generate(),
			'dog_id'    => $id,
			'first_generation' => ($results['first-generation']),
			'second_generation' => ($results['second-generation']),
			'third_generation' => ($results['third-generation']),
			'fourth_generation' => ($results['fourth-generation']),
		]);
//
		return $results;
	}

	public static function generateFirstGenerationSirePartialAncestorsFromRegistrationNumber($registration_number){
		$results = [];
		$id = \App\Dog::getDogId($registration_number);
		$parentBuilder = DogRelationship::where('dog_id', $id)->first();


		$firstGenerationParents = [
		'sire' =>[
			'sire' => [
				'name' => $parentBuilder && self::getByRegistrationNumber($parentBuilder->father) ? self::getByRegistrationNumber($parentBuilder->father)->name :'',
				'registration_number' => $parentBuilder && $parentBuilder->father ? $parentBuilder->father :'',
				'titles' => $parentBuilder && self::getByRegistrationNumber($parentBuilder->father) ? self::getByRegistrationNumber($parentBuilder->father)->titles:''
			],
			'dam' => [
//					'name' => $parentBuilder,
				'name' => $parentBuilder && self::getByRegistrationNumber($parentBuilder->mother) ? self::getByRegistrationNumber($parentBuilder->mother)->name :'',
				'registration_number' => $parentBuilder && $parentBuilder->mother ? $parentBuilder->mother :'',
				'titles' =>  $parentBuilder &&  \App\Dog::whereRegistrationNumber($parentBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($parentBuilder->mother)->first()->titles :''
			]
		],

		'Dam' => [
			'sire' => [
				'name' =>'',
				'registration_number' => '',
				'titles' => ''
			],
			'dam' => [
				'name' => '',
				'registration_number' => '',
				'titles' => ''
			]
		]
		];

		$results['first-generation'] = $firstGenerationParents;


		// Generate second generation
		$firstGenerationParentSireId = \App\Dog::getDogId($firstGenerationParents['sire']['sire']['registration_number']);
		$firstGenerationParentDamId = \App\Dog::getDogId($firstGenerationParents['sire']['dam']['registration_number']);
		$secondSireParentBuilder = DogRelationship::where('dog_id', $firstGenerationParentSireId)->first();
		$secondDamParentBuilder = DogRelationship::where('dog_id', $firstGenerationParentDamId)->first();

		if( \App\Dog::getDogId($firstGenerationParents['sire']['sire']['registration_number']) ||
		    \App\Dog::getDogId($firstGenerationParents['sire']['dam']['registration_number']) )
		{
			$secondGenerationParents = [
				'sire'=>[
//				'sire' => $secondSireParentBuilder->father,
					'sire' => [
						'name' => $secondSireParentBuilder && \App\Dog::whereRegistrationNumber($secondSireParentBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($secondSireParentBuilder->father)->first()->name :'',
						'registration_number' => $secondSireParentBuilder && $secondSireParentBuilder->father ? $secondSireParentBuilder->father : '',
						'titles' =>  $secondSireParentBuilder && \App\Dog::whereRegistrationNumber($secondSireParentBuilder->father)->first()  ?  \App\Dog::whereRegistrationNumber($secondSireParentBuilder->father)->first()->titles :''
					],
					'dam' => [
						'name' => $secondSireParentBuilder && \App\Dog::whereRegistrationNumber($secondSireParentBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($secondSireParentBuilder->mother)->first()->name :'',
						'registration_number' => $secondSireParentBuilder && $secondSireParentBuilder->mother? $secondSireParentBuilder->mother:'',
						'titles' => $secondSireParentBuilder && \App\Dog::whereRegistrationNumber($secondSireParentBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($secondSireParentBuilder->mother)->first()->titles :''
					]
				],

				'Dam'=>[
//				'sire' => $secondDamParentBuilder->father,
//				'dam' => $secondDamParentBuilder->mother,
					'sire' => [
						'name' => $secondDamParentBuilder && \App\Dog::whereRegistrationNumber($secondDamParentBuilder->father)->first() ?
							\App\Dog::whereRegistrationNumber($secondDamParentBuilder->father)->first()->name :'',
						'registration_number' => $secondDamParentBuilder ? $secondDamParentBuilder->father : '',
						'titles' => $secondDamParentBuilder && \App\Dog::whereRegistrationNumber($secondDamParentBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($secondDamParentBuilder->father)->first()->titles : ''
					],
					'dam' => [
						'name' => $secondDamParentBuilder && \App\Dog::whereRegistrationNumber($secondDamParentBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($secondDamParentBuilder->mother)->first()->name :'',
						'registration_number' => $secondDamParentBuilder && $secondDamParentBuilder->mother ? $secondDamParentBuilder->mother : '',
						'titles' => $secondDamParentBuilder && \App\Dog::whereRegistrationNumber($secondDamParentBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($secondDamParentBuilder->mother)->first()->titles :''
					]
				],

				'SecondSire' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'SecondDam' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				]
			];

			$results['second-generation'] = $secondGenerationParents;
		}else {
			$secondGenerationParents = [
				'sire'=>[
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					]
				],

				'Dam'=>[
//				'sire' => $secondDamParentBuilder->father,
//				'dam' => $secondDamParentBuilder->mother,
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => '',
					],
					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					]
				],

				'SecondSire' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'SecondDam' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				]
			];

			$results['second-generation'] = $secondGenerationParents;
		}

		if(\App\Dog::getDogId($secondGenerationParents['sire']['sire']['registration_number']) || \App\Dog::getDogId($secondGenerationParents['sire']['dam']['registration_number'])){
			// Generate third generation
			$secondGenerationParentSireSireId = \App\Dog::getDogId($secondGenerationParents['sire']['sire']['registration_number']);
			$secondGenerationParentSireDamId = \App\Dog::getDogId($secondGenerationParents['sire']['dam']['registration_number']);

			$secondGenerationParentDamSireId = \App\Dog::getDogId($secondGenerationParents['Dam']['sire']['registration_number']);
			$secondGenerationParentDamDamId = \App\Dog::getDogId($secondGenerationParents['Dam']['dam']['registration_number']);

			$thirdGenerationParentsSireSireBuilder = DogRelationship::where('dog_id', $secondGenerationParentSireSireId)->first();
			$thirdGenerationParentSireDamBuilder = DogRelationship::where('dog_id', $secondGenerationParentSireDamId)->first();


			$thirdGenerationParentsDamSireBuilder = DogRelationship::where('dog_id', $secondGenerationParentDamSireId)->first();
			$thirdGenerationParentsDamDamBuilder = DogRelationship::where('dog_id', $secondGenerationParentDamDamId)->first();

			$thirdGenerationParents = [
				'Sire'=>[
//				'sire' => $thirdGenerationParentsSireSireBuilder->father,
//				'dam' => $thirdGenerationParentsSireSireBuilder->mother,
					'sire' => [
						'name' => \App\Dog::whereRegistrationNumber($thirdGenerationParentsSireSireBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsSireSireBuilder->father)->first()->name :'',
						'registration_number' => $thirdGenerationParentsSireSireBuilder->father,
						'titles' => \App\Dog::whereRegistrationNumber($thirdGenerationParentsSireSireBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsSireSireBuilder->father)->first()->titles :''
					],

					'dam' => [
						'name' => \App\Dog::whereRegistrationNumber($thirdGenerationParentsSireSireBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsSireSireBuilder->mother)->first()->name : "",
						'registration_number' => $thirdGenerationParentsSireSireBuilder->mother,
						'titles' => \App\Dog::whereRegistrationNumber($thirdGenerationParentsSireSireBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsSireSireBuilder->mother)->first()->titles: ''
					],
				],

				'Dam'=>[
//				'sire' => $thirdGenerationParentSireDamBuilder->father,
//				'dam' => $thirdGenerationParentSireDamBuilder->mother
					'sire' => [
						'name' => $thirdGenerationParentSireDamBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentSireDamBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentSireDamBuilder->father)->first()->name : '',
						'registration_number' => $thirdGenerationParentSireDamBuilder ? $thirdGenerationParentSireDamBuilder->father : '',
						'titles' => $thirdGenerationParentSireDamBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentSireDamBuilder->father)->first()  ? \App\Dog::whereRegistrationNumber($thirdGenerationParentSireDamBuilder->father)->first()->titles :''
					],

					'dam' => [
						'name' => $thirdGenerationParentSireDamBuilder &&  \App\Dog::whereRegistrationNumber($thirdGenerationParentSireDamBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentSireDamBuilder->mother)->first()->name :'',
						'registration_number' => $thirdGenerationParentSireDamBuilder ? $thirdGenerationParentSireDamBuilder->mother : '',
						'titles' => $thirdGenerationParentSireDamBuilder &&  \App\Dog::whereRegistrationNumber($thirdGenerationParentSireDamBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentSireDamBuilder->mother)->first()->titles :''
					],
				],

				'SecondSire'=>[
//				'sire' => $thirdGenerationParentsDamSireBuilder->father,
//				'dam' => $thirdGenerationParentsDamSireBuilder->mother

					'sire' => [
						'name' => $thirdGenerationParentsDamSireBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamSireBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamSireBuilder->father)->first()->name :'' ,
						'registration_number' => $thirdGenerationParentsDamSireBuilder && $thirdGenerationParentsDamSireBuilder->father ?$thirdGenerationParentsDamSireBuilder->father :'',
						'titles' => $thirdGenerationParentsDamSireBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamSireBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamSireBuilder->father)->first()->titles :''
					],

					'dam' => [
						'name' => $thirdGenerationParentsDamSireBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamSireBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamSireBuilder->mother)->first()->name :'',
						'registration_number' => $thirdGenerationParentsDamSireBuilder && $thirdGenerationParentsDamSireBuilder->mother ? $thirdGenerationParentsDamSireBuilder->mother :'',
						'titles' => $thirdGenerationParentsDamSireBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamSireBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamSireBuilder->mother)->first()->titles :''
					],
				],

				'SecondDam' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => $thirdGenerationParentsDamDamBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamDamBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamDamBuilder->father)->first()->name :'',
						'registration_number' => $thirdGenerationParentsDamDamBuilder && $thirdGenerationParentsDamDamBuilder->father ? $thirdGenerationParentsDamDamBuilder->father :'',
						'titles' => $thirdGenerationParentsDamDamBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamDamBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamDamBuilder->father)->first()->titles :''
					],

					'dam' => [
						'name' =>$thirdGenerationParentsDamDamBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamDamBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamDamBuilder->mother)->first()->name : '',
						'registration_number' => $thirdGenerationParentsDamDamBuilder && $thirdGenerationParentsDamDamBuilder->mother ? $thirdGenerationParentsDamDamBuilder->mother :'',
						'titles' => $thirdGenerationParentsDamDamBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamDamBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamDamBuilder->mother)->first()->titles :''
					],
				],

				'ThirdSire' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'ThirdDam' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'FourthSire' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'FourthDam' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				]
			];

			$results['third-generation'] = $thirdGenerationParents;

		}else{
			$thirdGenerationParents = [
				'Sire'=>[
//				'sire' => $thirdGenerationParentsSireSireBuilder->father,
//				'dam' => $thirdGenerationParentsSireSireBuilder->mother,
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'Dam'=>[
//				'sire' => $thirdGenerationParentSireDamBuilder->father,
//				'dam' => $thirdGenerationParentSireDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => '',
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => '',
					],
				],

				'SecondSire'=>[
//				'sire' => $thirdGenerationParentsDamSireBuilder->father,
//				'dam' => $thirdGenerationParentsDamSireBuilder->mother

					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'SecondDam' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

			   'ThirdSire' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
				'sire' => [
					'name' => '',
					'registration_number' => '',
					'titles' => ''
				],

				'dam' => [
					'name' => '',
					'registration_number' => '',
					'titles' => ''
				],
			],
			   'ThirdDam' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
				'sire' => [
					'name' => '',
					'registration_number' => '',
					'titles' => ''
				],

				'dam' => [
					'name' => '',
					'registration_number' => '',
					'titles' => ''
				],
			],
			   'FourthSire' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
				'sire' => [
					'name' => '',
					'registration_number' => '',
					'titles' => ''
				],

				'dam' => [
					'name' => '',
					'registration_number' => '',
					'titles' => ''
				],
			],
			   'FourthDam' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
				'sire' => [
					'name' => '',
					'registration_number' => '',
					'titles' => ''
				],

				'dam' => [
					'name' => '',
					'registration_number' => '',
					'titles' => ''
				],
			],


			];

			$results['third-generation'] = $thirdGenerationParents;
		}


//		DogGeneration::create([
//			'id'        => Uuid::generate(),
//			'dog_id'    => $id,
//			'second_generation' => ($results['first-generation']),
//			'third_generation' => ($results['second-generation']),
//			'fourth_generation' => ($results['third-generation']),
//		]);
//
		return $results;
	}

	public static function generateSecondGenerationSirePartialAncestorsFromRegistrationNumber($registration_number){
		$results = [];
		$id = \App\Dog::getDogId($registration_number);
		$parentBuilder = DogRelationship::where('dog_id', $id)->first();


		$firstGenerationParents = [
			'sire' =>[
				'sire' => [
					'name' => $parentBuilder && self::getByRegistrationNumber($parentBuilder->father) ? self::getByRegistrationNumber($parentBuilder->father)->name :'',
					'registration_number' => $parentBuilder && $parentBuilder->father ? $parentBuilder->father :'',
					'titles' => $parentBuilder && self::getByRegistrationNumber($parentBuilder->father) ? self::getByRegistrationNumber($parentBuilder->father)->titles:''
				],
				'dam' => [
//					'name' => $parentBuilder,
					'name' => $parentBuilder && self::getByRegistrationNumber($parentBuilder->mother) ? self::getByRegistrationNumber($parentBuilder->mother)->name :'',
					'registration_number' => $parentBuilder && $parentBuilder->mother ? $parentBuilder->mother :'',
					'titles' =>  $parentBuilder &&  \App\Dog::whereRegistrationNumber($parentBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($parentBuilder->mother)->first()->titles :''
				]
			],

			'Dam' => [
				'sire' => [
					'name' =>'',
					'registration_number' => '',
					'titles' => ''
				],
				'dam' => [
					'name' => '',
					'registration_number' => '',
					'titles' => ''
				]
			]
		];

		$results['first-generation'] = $firstGenerationParents;


		// Generate second generation
		$firstGenerationParentSireId = \App\Dog::getDogId($firstGenerationParents['sire']['sire']['registration_number']);
		$firstGenerationParentDamId = \App\Dog::getDogId($firstGenerationParents['sire']['dam']['registration_number']);
		$secondSireParentBuilder = DogRelationship::where('dog_id', $firstGenerationParentSireId)->first();
		$secondDamParentBuilder = DogRelationship::where('dog_id', $firstGenerationParentDamId)->first();

		if( \App\Dog::getDogId($firstGenerationParents['sire']['sire']['registration_number']) ||
		    \App\Dog::getDogId($firstGenerationParents['sire']['dam']['registration_number']) )
		{
			$secondGenerationParents = [
				'sire'=>[
//				'sire' => $secondSireParentBuilder->father,
					'sire' => [
						'name' => $secondSireParentBuilder && \App\Dog::whereRegistrationNumber($secondSireParentBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($secondSireParentBuilder->father)->first()->name :'',
						'registration_number' => $secondSireParentBuilder && $secondSireParentBuilder->father ? $secondSireParentBuilder->father : '',
						'titles' =>  $secondSireParentBuilder && \App\Dog::whereRegistrationNumber($secondSireParentBuilder->father)->first()  ?  \App\Dog::whereRegistrationNumber($secondSireParentBuilder->father)->first()->titles :''
					],

					'dam' => [
						'name' => $secondSireParentBuilder && \App\Dog::whereRegistrationNumber($secondSireParentBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($secondSireParentBuilder->mother)->first()->name :'',
						'registration_number' => $secondSireParentBuilder && $secondSireParentBuilder->mother? $secondSireParentBuilder->mother:'',
						'titles' => $secondSireParentBuilder && \App\Dog::whereRegistrationNumber($secondSireParentBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($secondSireParentBuilder->mother)->first()->titles :''
					]
				],

				'Dam'=>[
//				'sire' => $secondDamParentBuilder->father,
//				'dam' => $secondDamParentBuilder->mother,
					'sire' => [
						'name' => $secondDamParentBuilder && \App\Dog::whereRegistrationNumber($secondDamParentBuilder->father)->first() ?
							\App\Dog::whereRegistrationNumber($secondDamParentBuilder->father)->first()->name :'',
						'registration_number' => $secondDamParentBuilder ? $secondDamParentBuilder->father : '',
						'titles' => $secondDamParentBuilder && \App\Dog::whereRegistrationNumber($secondDamParentBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($secondDamParentBuilder->father)->first()->titles : ''
					],

					'dam' => [
						'name' => $secondDamParentBuilder && \App\Dog::whereRegistrationNumber($secondDamParentBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($secondDamParentBuilder->mother)->first()->name :'',
						'registration_number' => $secondDamParentBuilder && $secondDamParentBuilder->mother ? $secondDamParentBuilder->mother : '',
						'titles' => $secondDamParentBuilder && \App\Dog::whereRegistrationNumber($secondDamParentBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($secondDamParentBuilder->mother)->first()->titles :''
					]
				],

				'SecondSire' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'SecondDam' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				]
			];

			$results['second-generation'] = $secondGenerationParents;
		}else {
			$secondGenerationParents = [
				'sire'=>[
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					]
				],

				'Dam'=>[
//				'sire' => $secondDamParentBuilder->father,
//				'dam' => $secondDamParentBuilder->mother,
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => '',
					],
					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					]
				],

				'SecondSire' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'SecondDam' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				]
			];

			$results['second-generation'] = $secondGenerationParents;
		}

		if(\App\Dog::getDogId($secondGenerationParents['Sire']['sire']['registration_number']) || \App\Dog::getDogId($secondGenerationParents['Sire']['dam']['registration_number'])){
			// Generate third generation
			$secondGenerationParentSireSireId = \App\Dog::getDogId($secondGenerationParents['Sire']['sire']['registration_number']);
			$secondGenerationParentSireDamId = \App\Dog::getDogId($secondGenerationParents['Sire']['dam']['registration_number']);

			$secondGenerationParentDamSireId = \App\Dog::getDogId($secondGenerationParents['Dam']['sire']['registration_number']);
			$secondGenerationParentDamDamId = \App\Dog::getDogId($secondGenerationParents['Dam']['dam']['registration_number']);

			$thirdGenerationParentsSireSireBuilder = DogRelationship::where('dog_id', $secondGenerationParentSireSireId)->first();
			$thirdGenerationParentSireDamBuilder = DogRelationship::where('dog_id', $secondGenerationParentSireDamId)->first();


			$thirdGenerationParentsDamSireBuilder = DogRelationship::where('dog_id', $secondGenerationParentDamSireId)->first();
			$thirdGenerationParentsDamDamBuilder = DogRelationship::where('dog_id', $secondGenerationParentDamDamId)->first();

			$thirdGenerationParents = [

				'Sire'=>[
//				'sire' => $thirdGenerationParentsSireSireBuilder->father,
//				'dam' => $thirdGenerationParentsSireSireBuilder->mother,
					'sire' => [
						'name' => \App\Dog::whereRegistrationNumber($thirdGenerationParentsSireSireBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsSireSireBuilder->father)->first()->name :'',
						'registration_number' => $thirdGenerationParentsSireSireBuilder->father,
						'titles' => \App\Dog::whereRegistrationNumber($thirdGenerationParentsSireSireBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsSireSireBuilder->father)->first()->titles :''
					],

					'dam' => [
						'name' => \App\Dog::whereRegistrationNumber($thirdGenerationParentsSireSireBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsSireSireBuilder->mother)->first()->name : "",
						'registration_number' => $thirdGenerationParentsSireSireBuilder->mother,
						'titles' => \App\Dog::whereRegistrationNumber($thirdGenerationParentsSireSireBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsSireSireBuilder->mother)->first()->titles: ''
					],
				],

				'Dam'=>[
//				'sire' => $thirdGenerationParentSireDamBuilder->father,
//				'dam' => $thirdGenerationParentSireDamBuilder->mother
					'sire' => [
						'name' => $thirdGenerationParentSireDamBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentSireDamBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentSireDamBuilder->father)->first()->name : '',
						'registration_number' => $thirdGenerationParentSireDamBuilder ? $thirdGenerationParentSireDamBuilder->father : '',
						'titles' => $thirdGenerationParentSireDamBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentSireDamBuilder->father)->first()  ? \App\Dog::whereRegistrationNumber($thirdGenerationParentSireDamBuilder->father)->first()->titles :''
					],

					'dam' => [
						'name' => $thirdGenerationParentSireDamBuilder &&  \App\Dog::whereRegistrationNumber($thirdGenerationParentSireDamBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentSireDamBuilder->mother)->first()->name :'',
						'registration_number' => $thirdGenerationParentSireDamBuilder ? $thirdGenerationParentSireDamBuilder->mother : '',
						'titles' => $thirdGenerationParentSireDamBuilder &&  \App\Dog::whereRegistrationNumber($thirdGenerationParentSireDamBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentSireDamBuilder->mother)->first()->titles :''
					],
				],

				'SecondSire'=>[
//				'sire' => $thirdGenerationParentsDamSireBuilder->father,
//				'dam' => $thirdGenerationParentsDamSireBuilder->mother

					'sire' => [
						'name' => $thirdGenerationParentsDamSireBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamSireBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamSireBuilder->father)->first()->name :'' ,
						'registration_number' => $thirdGenerationParentsDamSireBuilder && $thirdGenerationParentsDamSireBuilder->father ?$thirdGenerationParentsDamSireBuilder->father :'',
						'titles' => $thirdGenerationParentsDamSireBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamSireBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamSireBuilder->father)->first()->titles :''
					],

					'dam' => [
						'name' => $thirdGenerationParentsDamSireBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamSireBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamSireBuilder->mother)->first()->name :'',
						'registration_number' => $thirdGenerationParentsDamSireBuilder && $thirdGenerationParentsDamSireBuilder->mother ? $thirdGenerationParentsDamSireBuilder->mother :'',
						'titles' => $thirdGenerationParentsDamSireBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamSireBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamSireBuilder->mother)->first()->titles :''
					],
				],

				'SecondDam' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => $thirdGenerationParentsDamDamBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamDamBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamDamBuilder->father)->first()->name :'',
						'registration_number' => $thirdGenerationParentsDamDamBuilder && $thirdGenerationParentsDamDamBuilder->father ? $thirdGenerationParentsDamDamBuilder->father :'',
						'titles' => $thirdGenerationParentsDamDamBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamDamBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamDamBuilder->father)->first()->titles :''
					],

					'dam' => [
						'name' =>$thirdGenerationParentsDamDamBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamDamBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamDamBuilder->mother)->first()->name : '',
						'registration_number' => $thirdGenerationParentsDamDamBuilder && $thirdGenerationParentsDamDamBuilder->mother ? $thirdGenerationParentsDamDamBuilder->mother :'',
						'titles' => $thirdGenerationParentsDamDamBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamDamBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamDamBuilder->mother)->first()->titles :''
					],
				],

				'ThirdSire' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'ThirdDam' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'FourthSire' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'FourthDam' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				]
			];

			$results['third-generation'] = $thirdGenerationParents;

		}else{
			$thirdGenerationParents = [
				'Sire'=>[
//				'sire' => $thirdGenerationParentsSireSireBuilder->father,
//				'dam' => $thirdGenerationParentsSireSireBuilder->mother,
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'Dam'=>[
//				'sire' => $thirdGenerationParentSireDamBuilder->father,
//				'dam' => $thirdGenerationParentSireDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => '',
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => '',
					],
				],

				'SecondSire'=>[
//				'sire' => $thirdGenerationParentsDamSireBuilder->father,
//				'dam' => $thirdGenerationParentsDamSireBuilder->mother

					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'SecondDam' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'ThirdSire' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'ThirdDam' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'FourthSire' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'FourthDam' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],
			];

			$results['third-generation'] = $thirdGenerationParents;
		}


//		DogGeneration::create([
//			'id'        => Uuid::generate(),
//			'dog_id'    => $id,
//			'second_generation' => ($results['first-generation']),
//			'third_generation' => ($results['second-generation']),
//			'fourth_generation' => ($results['third-generation']),
//		]);
//
		return $results;
	}

	public static function SaveFirstGenerationSirePartialAncestors($registration_number, $generation,$generation_results,$builder){
		$pedigree               = Pedigree::generateFirstGenerationSirePartialAncestorsFromRegistrationNumber( $registration_number );
		$sire_second_generation = json_decode( json_encode( $builder->second_generation ), true );
		$sire_third_generation  = json_decode( json_encode( $builder->third_generation ), true );
		$sire_fourth_generation = json_decode( json_encode( $builder->fourth_generation ), true );

		unset( $sire_second_generation['sire']['sire'] );
		unset( $sire_second_generation['sire']['dam'] );

		$sire_second_generation['sire']['sire'] = [
			'name'                => $pedigree['first-generation']['sire']['sire']['name'],
			'registration_number' => $pedigree['first-generation']['sire']['sire']['registration_number'],
			'titles'              => $pedigree['first-generation']['sire']['sire']['titles']
		];

		$sire_second_generation['sire']['dam'] = [
			'name'                => $pedigree['first-generation']['sire']['dam']['name'],
			'registration_number' => $pedigree['first-generation']['sire']['dam']['registration_number'],
			'titles'              => $pedigree['first-generation']['sire']['dam']['titles']
		];

		unset( $sire_third_generation['Sire']['sire'] );
		unset( $sire_third_generation['Sire']['dam'] );

		$sire_third_generation['Sire']['sire'] = [
			'name'                => $pedigree['second-generation']['sire']['sire']['name'],
			'registration_number' => $pedigree['second-generation']['sire']['sire']['registration_number'],
			'titles'              => $pedigree['second-generation']['sire']['sire']['titles']
		];

		$sire_third_generation['Sire']['dam'] = [
			'name'                => $pedigree['second-generation']['sire']['dam']['name'],
			'registration_number' => $pedigree['second-generation']['sire']['dam']['registration_number'],
			'titles'              => $pedigree['second-generation']['sire']['dam']['titles']
		];

		unset( $sire_third_generation['Dam']['sire'] );
		unset( $sire_third_generation['Dam']['dam'] );

		$sire_third_generation['Dam']['sire'] = [
			'name'                => $pedigree['second-generation']['Dam']['sire']['name'],
			'registration_number' => $pedigree['second-generation']['Dam']['sire']['registration_number'],
			'titles'              => $pedigree['second-generation']['Dam']['sire']['titles']
		];

		$sire_third_generation['Dam']['dam'] = [
			'name'                => $pedigree['second-generation']['Dam']['dam']['name'],
			'registration_number' => $pedigree['second-generation']['Dam']['dam']['registration_number'],
			'titles'              => $pedigree['second-generation']['Dam']['dam']['titles']
		];

		// Fourth Generation

		unset( $sire_fourth_generation['SecondSire']['sire'] );
		unset( $sire_fourth_generation['SecondSire']['dam'] );

		$sire_fourth_generation['SecondSire']['sire'] = [
			'name'                => $pedigree['third-generation']['SecondSire']['sire']['name'],
			'registration_number' => $pedigree['third-generation']['SecondSire']['sire']['registration_number'],
			'titles'              => $pedigree['third-generation']['SecondSire']['sire']['titles']
		];

		$sire_fourth_generation['SecondSire']['dam'] = [
			'name'                => $pedigree['third-generation']['SecondSire']['dam']['name'],
			'registration_number' => $pedigree['third-generation']['SecondSire']['dam']['registration_number'],
			'titles'              => $pedigree['third-generation']['SecondSire']['dam']['titles']
		];

		unset( $sire_fourth_generation['SecondDam']['sire'] );
		unset( $sire_fourth_generation['SecondDam']['dam'] );

		$sire_fourth_generation['SecondDam']['sire'] = [
			'name'                => $pedigree['third-generation']['SecondDam']['sire']['name'],
			'registration_number' => $pedigree['third-generation']['SecondDam']['sire']['registration_number'],
			'titles'              => $pedigree['third-generation']['SecondDam']['sire']['titles']
		];

		$sire_fourth_generation['SecondDam']['dam'] = [
			'name'                => $pedigree['third-generation']['SecondDam']['dam']['name'],
			'registration_number' => $pedigree['third-generation']['SecondDam']['dam']['registration_number'],
			'titles'              => $pedigree['third-generation']['SecondDam']['dam']['titles']
		];

		$builder->update( [
			$generation         => $generation_results,
			'second_generation' => $sire_second_generation,
			'third_generation'  => $sire_third_generation,
			'fourth_generation' => $sire_fourth_generation,
		]);
	}

	public static function generateFirstGenerationDamPartialAncestorsFromRegistrationNumber($registration_number){
		$results = [];
		$id = \App\Dog::getDogId($registration_number);
		$parentBuilder = DogRelationship::where('dog_id', $id)->first();

		$firstGenerationParents = [
			'sire' =>[
				'sire' => [
					'name' => '',
					'registration_number' => '',
					'titles' => ''
				],
				'dam' => [
					'name' => '',
					'registration_number' => '',
					'titles' =>  ''
				]
			],

			'Dam' =>[
				'sire' => [
					'name' => $parentBuilder && self::getByRegistrationNumber($parentBuilder->father) ? self::getByRegistrationNumber($parentBuilder->father)->name :'',
					'registration_number' => $parentBuilder && $parentBuilder->father ? $parentBuilder->father :'',
					'titles' => $parentBuilder && self::getByRegistrationNumber($parentBuilder->father) ? self::getByRegistrationNumber($parentBuilder->father)->titles:''
				],
				'dam' => [
					'name' => $parentBuilder && self::getByRegistrationNumber($parentBuilder->mother) ? self::getByRegistrationNumber($parentBuilder->mother)->name :'',
					'registration_number' => $parentBuilder && $parentBuilder->mother ? $parentBuilder->mother :'',
					'titles' =>  $parentBuilder &&  \App\Dog::whereRegistrationNumber($parentBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($parentBuilder->mother)->first()->titles :''
				]
			]
		];

		$results['first-generation'] = $firstGenerationParents;

		// Generate second generation
		$firstGenerationParentSireId = \App\Dog::getDogId($firstGenerationParents['Dam']['sire']['registration_number']);
		$firstGenerationParentDamId =  \App\Dog::getDogId($firstGenerationParents['Dam']['dam']['registration_number']);
		$secondSireParentBuilder = DogRelationship::where('dog_id', $firstGenerationParentSireId)->first();
		$secondDamParentBuilder = DogRelationship::where('dog_id', $firstGenerationParentDamId)->first();

		if( \App\Dog::getDogId($firstGenerationParents['Dam']['sire']['registration_number']) ||
		    \App\Dog::getDogId($firstGenerationParents['Dam']['dam']['registration_number']) )
		{
			$secondGenerationParents = [
				'Sire'=>[
					'sire' => [
						'name' => '',
						'registration_number' =>  '',
						'titles' =>  ''
					],
					'dam' => [
						'name' =>'',
						'registration_number' => '',
						'titles' => ''
					]
				],

				'Dam'=>[
					'sire' => [
						'name' =>'',
						'registration_number' => '',
						'titles' =>  ''
					],
					'dam' => [
						'name' => '',
						'registration_number' =>'',
						'titles' => ''
					]
				],

				'SecondSire'=>[
					'sire' => [
						'name' => $secondSireParentBuilder && \App\Dog::whereRegistrationNumber($secondSireParentBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($secondSireParentBuilder->father)->first()->name :'',
						'registration_number' => $secondSireParentBuilder && $secondSireParentBuilder->father ? $secondSireParentBuilder->father : '',
						'titles' =>  $secondSireParentBuilder && \App\Dog::whereRegistrationNumber($secondSireParentBuilder->father)->first()  ?  \App\Dog::whereRegistrationNumber($secondSireParentBuilder->father)->first()->titles :''
					],
					'dam' => [
						'name' => $secondSireParentBuilder && \App\Dog::whereRegistrationNumber($secondSireParentBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($secondSireParentBuilder->mother)->first()->name :'',
						'registration_number' => $secondSireParentBuilder && $secondSireParentBuilder->mother? $secondSireParentBuilder->mother:'',
						'titles' => $secondSireParentBuilder && \App\Dog::whereRegistrationNumber($secondSireParentBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($secondSireParentBuilder->mother)->first()->titles :''
					]
				],

				'SecondDam'=>[
					'sire' => [
						'name' => $secondDamParentBuilder && \App\Dog::whereRegistrationNumber($secondDamParentBuilder->father)->first() ?
							\App\Dog::whereRegistrationNumber($secondDamParentBuilder->father)->first()->name :'',
						'registration_number' => $secondDamParentBuilder ? $secondDamParentBuilder->father : '',
						'titles' => $secondDamParentBuilder && \App\Dog::whereRegistrationNumber($secondDamParentBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($secondDamParentBuilder->father)->first()->titles : ''
					],
					'dam' => [
						'name' => $secondDamParentBuilder && \App\Dog::whereRegistrationNumber($secondDamParentBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($secondDamParentBuilder->mother)->first()->name :'',
						'registration_number' => $secondDamParentBuilder && $secondDamParentBuilder->mother ? $secondDamParentBuilder->mother : '',
						'titles' => $secondDamParentBuilder && \App\Dog::whereRegistrationNumber($secondDamParentBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($secondDamParentBuilder->mother)->first()->titles :''
					]
				]
			];

			$results['second-generation'] = $secondGenerationParents;
		}else {
			$secondGenerationParents = [
				'Sire'=>[
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					]
				],

				'Dam'=>[
//				'sire' => $secondDamParentBuilder->father,
//				'dam' => $secondDamParentBuilder->mother,
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => '',
					],
					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					]
				],

				'SecondSire' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'SecondDam' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				]
			];

			$results['second-generation'] = $secondGenerationParents;
		}

		if(\App\Dog::getDogId($secondGenerationParents['SecondSire']['sire']['registration_number']) ||
		   \App\Dog::getDogId($secondGenerationParents['SecondSire']['dam']['registration_number'])){
			// Generate third generation
			$secondGenerationParentSireSireId = \App\Dog::getDogId($secondGenerationParents['SecondSire']['sire']['registration_number']);
			$secondGenerationParentSireDamId = \App\Dog::getDogId($secondGenerationParents['SecondSire']['dam']['registration_number']);

			$secondGenerationParentDamSireId = \App\Dog::getDogId($secondGenerationParents['SecondDam']['sire']['registration_number']);
			$secondGenerationParentDamDamId = \App\Dog::getDogId($secondGenerationParents['SecondDam']['dam']['registration_number']);

			$thirdGenerationParentsSireSireBuilder = DogRelationship::where('dog_id', $secondGenerationParentSireSireId)->first();
			$thirdGenerationParentSireDamBuilder = DogRelationship::where('dog_id', $secondGenerationParentSireDamId)->first();


			$thirdGenerationParentsDamSireBuilder = DogRelationship::where('dog_id', $secondGenerationParentDamSireId)->first();
			$thirdGenerationParentsDamDamBuilder = DogRelationship::where('dog_id', $secondGenerationParentDamDamId)->first();

			$thirdGenerationParents = [

				'Sire' => [
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'Dam' => [
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'SecondSire' => [
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'SecondDam' => [
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'ThirdSire'=>[
					'sire' => [
						'name' => $thirdGenerationParentsSireSireBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsSireSireBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsSireSireBuilder->father)->first()->name :'',
						'registration_number' => $thirdGenerationParentsSireSireBuilder && $thirdGenerationParentsSireSireBuilder->father ?  $thirdGenerationParentsSireSireBuilder->father :'',
						'titles' => $thirdGenerationParentsSireSireBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsSireSireBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsSireSireBuilder->father)->first()->titles :''
					],

					'dam' => [
						'name' => $thirdGenerationParentsSireSireBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsSireSireBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsSireSireBuilder->mother)->first()->name : "",
						'registration_number' =>$thirdGenerationParentsSireSireBuilder && $thirdGenerationParentsSireSireBuilder->mother ? $thirdGenerationParentsSireSireBuilder->mother :'',
						'titles' => $thirdGenerationParentsSireSireBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsSireSireBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsSireSireBuilder->mother)->first()->titles: ''
					],
				],

				'ThirdDam'=>[
					'sire' => [
						'name' => $thirdGenerationParentSireDamBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentSireDamBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentSireDamBuilder->father)->first()->name : '',
						'registration_number' => $thirdGenerationParentSireDamBuilder ? $thirdGenerationParentSireDamBuilder->father : '',
						'titles' => $thirdGenerationParentSireDamBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentSireDamBuilder->father)->first()  ? \App\Dog::whereRegistrationNumber($thirdGenerationParentSireDamBuilder->father)->first()->titles :''
					],

					'dam' => [
						'name' => $thirdGenerationParentSireDamBuilder &&  \App\Dog::whereRegistrationNumber($thirdGenerationParentSireDamBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentSireDamBuilder->mother)->first()->name :'',
						'registration_number' => $thirdGenerationParentSireDamBuilder ? $thirdGenerationParentSireDamBuilder->mother : '',
						'titles' => $thirdGenerationParentSireDamBuilder &&  \App\Dog::whereRegistrationNumber($thirdGenerationParentSireDamBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentSireDamBuilder->mother)->first()->titles :''
					],
				],

				'FourthSire'=>[
					'sire' => [
						'name' => $thirdGenerationParentsDamSireBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamSireBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamSireBuilder->father)->first()->name :'' ,
						'registration_number' => $thirdGenerationParentsDamSireBuilder && $thirdGenerationParentsDamSireBuilder->father ?$thirdGenerationParentsDamSireBuilder->father :'',
						'titles' => $thirdGenerationParentsDamSireBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamSireBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamSireBuilder->father)->first()->titles :''
					],

					'dam' => [
						'name' => $thirdGenerationParentsDamSireBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamSireBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamSireBuilder->mother)->first()->name :'',
						'registration_number' => $thirdGenerationParentsDamSireBuilder && $thirdGenerationParentsDamSireBuilder->mother ? $thirdGenerationParentsDamSireBuilder->mother :'',
						'titles' => $thirdGenerationParentsDamSireBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamSireBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamSireBuilder->mother)->first()->titles :''
					],
				],

				'FourthDam' => [
					'sire' => [
						'name' => $thirdGenerationParentsDamDamBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamDamBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamDamBuilder->father)->first()->name :'',
						'registration_number' => $thirdGenerationParentsDamDamBuilder && $thirdGenerationParentsDamDamBuilder->father ? $thirdGenerationParentsDamDamBuilder->father :'',
						'titles' => $thirdGenerationParentsDamDamBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamDamBuilder->father)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamDamBuilder->father)->first()->titles :''
					],

					'dam' => [
						'name' =>$thirdGenerationParentsDamDamBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamDamBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamDamBuilder->mother)->first()->name : '',
						'registration_number' => $thirdGenerationParentsDamDamBuilder && $thirdGenerationParentsDamDamBuilder->mother ? $thirdGenerationParentsDamDamBuilder->mother :'',
						'titles' => $thirdGenerationParentsDamDamBuilder && \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamDamBuilder->mother)->first() ? \App\Dog::whereRegistrationNumber($thirdGenerationParentsDamDamBuilder->mother)->first()->titles :''
					],
				],
			];

			$results['third-generation'] = $thirdGenerationParents;

		}else{
			$thirdGenerationParents = [
				'Sire'=>[
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'Dam'=>[
//				'sire' => $thirdGenerationParentSireDamBuilder->father,
//				'dam' => $thirdGenerationParentSireDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => '',
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => '',
					],
				],

				'SecondSire'=>[
//				'sire' => $thirdGenerationParentsDamSireBuilder->father,
//				'dam' => $thirdGenerationParentsDamSireBuilder->mother

					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'SecondDam' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],

				'ThirdSire' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],
				'ThirdDam' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],
				'FourthSire' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],
				'FourthDam' => [
//				'sire' => $thirdGenerationParentsDamDamBuilder->father,
//				'dam' => $thirdGenerationParentsDamDamBuilder->mother
					'sire' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],

					'dam' => [
						'name' => '',
						'registration_number' => '',
						'titles' => ''
					],
				],


			];

			$results['third-generation'] = $thirdGenerationParents;
		}

//		DogGeneration::create([
//			'id'        => Uuid::generate(),
//			'dog_id'    => $id,
//			'second_generation' => ($results['first-generation']),
//			'third_generation' => ($results['second-generation']),
//			'fourth_generation' => ($results['third-generation']),
//		]);
//
		return $results;
	}

	public static function SaveFirstGenerationDamPartialAncestors($registration_number, $generation,$generation_results,$builder){

		$pedigree              = Pedigree::generateFirstGenerationDamPartialAncestorsFromRegistrationNumber( $registration_number );
		$dam_second_generation = json_decode( json_encode( $builder->second_generation ), true );
		$dam_third_generation  = json_decode( json_encode( $builder->third_generation ), true );
		$dam_fourth_generation = json_decode( json_encode( $builder->fourth_generation ), true );

		unset( $dam_second_generation['Dam']['sire']);
		unset( $dam_second_generation['Dam']['dam']);

		$dam_second_generation['Dam']['sire'] = [
			'name'                => $pedigree['first-generation']['Dam']['sire']['name'],
			'registration_number' => $pedigree['first-generation']['Dam']['sire']['registration_number'],
			'titles'              => $pedigree['first-generation']['Dam']['sire']['titles']
		];

		$dam_second_generation['Dam']['dam'] = [
			'name'                => $pedigree['first-generation']['Dam']['dam']['name'],
			'registration_number' => $pedigree['first-generation']['Dam']['dam']['registration_number'],
			'titles'              => $pedigree['first-generation']['Dam']['dam']['titles']
		];

		unset( $dam_third_generation['SecondSire']['sire'] );
		unset( $dam_third_generation['SecondSire']['dam'] );

		$dam_third_generation['SecondSire']['sire'] = [
			'name'                => $pedigree['second-generation']['SecondSire']['sire']['name'],
			'registration_number' => $pedigree['second-generation']['SecondSire']['sire']['registration_number'],
			'titles'              => $pedigree['second-generation']['SecondSire']['sire']['titles']
		];

		$dam_third_generation['SecondSire']['dam'] = [
			'name'                => $pedigree['second-generation']['SecondSire']['dam']['name'],
			'registration_number' => $pedigree['second-generation']['SecondSire']['dam']['registration_number'],
			'titles'              => $pedigree['second-generation']['SecondSire']['dam']['titles']
		];

		unset( $dam_third_generation['SecondDam']['sire'] );
		unset( $dam_third_generation['SecondDam']['dam'] );

		$dam_third_generation['SecondDam']['sire'] = [
			'name'                => $pedigree['second-generation']['SecondDam']['sire']['name'],
			'registration_number' => $pedigree['second-generation']['SecondDam']['sire']['registration_number'],
			'titles'              => $pedigree['second-generation']['SecondDam']['sire']['titles']
		];

		$dam_third_generation['SecondDam']['dam'] = [
			'name'                => $pedigree['second-generation']['SecondDam']['dam']['name'],
			'registration_number' => $pedigree['second-generation']['SecondDam']['dam']['registration_number'],
			'titles'              => $pedigree['second-generation']['SecondDam']['dam']['titles']
		];

		// Fourth Generation
		unset( $dam_fourth_generation['ThirdSire']['sire'] );
		unset( $dam_fourth_generation['ThirdSire']['dam'] );

		$dam_fourth_generation['ThirdSire']['sire'] = [
			'name'                => $pedigree['third-generation']['ThirdSire']['sire']['name'],
			'registration_number' => $pedigree['third-generation']['ThirdSire']['sire']['registration_number'],
			'titles'              => $pedigree['third-generation']['ThirdSire']['sire']['titles']
		];

		$dam_fourth_generation['ThirdSire']['dam'] = [
			'name'                => $pedigree['third-generation']['ThirdSire']['dam']['name'],
			'registration_number' => $pedigree['third-generation']['ThirdSire']['dam']['registration_number'],
			'titles'              => $pedigree['third-generation']['ThirdSire']['dam']['titles']
		];

		unset( $dam_fourth_generation['ThirdDam']['sire'] );
		unset( $dam_fourth_generation['ThirdDam']['dam'] );

		$dam_fourth_generation['ThirdDam']['sire'] = [
			'name'                => $pedigree['third-generation']['ThirdSire']['sire']['name'],
			'registration_number' => $pedigree['third-generation']['ThirdSire']['sire']['registration_number'],
			'titles'              => $pedigree['third-generation']['ThirdSire']['sire']['titles']
		];

		$dam_fourth_generation['ThirdDam']['dam'] = [
			'name'                => $pedigree['third-generation']['ThirdDam']['dam']['name'],
			'registration_number' => $pedigree['third-generation']['ThirdDam']['dam']['registration_number'],
			'titles'              => $pedigree['third-generation']['ThirdDam']['dam']['titles']
		];

		unset( $dam_fourth_generation['FourthSire']['sire'] );
		unset( $dam_fourth_generation['FourthSire']['dam'] );

		$dam_fourth_generation['FourthSire']['sire'] = [
			'name'                => $pedigree['third-generation']['FourthSire']['sire']['name'],
			'registration_number' => $pedigree['third-generation']['FourthSire']['sire']['registration_number'],
			'titles'              => $pedigree['third-generation']['FourthSire']['sire']['titles']
		];

		$dam_fourth_generation['FourthSire']['dam'] = [
			'name'                => $pedigree['third-generation']['FourthSire']['dam']['name'],
			'registration_number' => $pedigree['third-generation']['FourthSire']['dam']['registration_number'],
			'titles'              => $pedigree['third-generation']['FourthSire']['dam']['titles']
		];

		unset( $dam_fourth_generation['FourthDam']['sire'] );
		unset( $dam_fourth_generation['FourthDam']['dam'] );

		$dam_fourth_generation['FourthDam']['sire'] = [
			'name'                => $pedigree['third-generation']['FourthDam']['sire']['name'],
			'registration_number' => $pedigree['third-generation']['FourthDam']['sire']['registration_number'],
			'titles'              => $pedigree['third-generation']['FourthDam']['sire']['titles']
		];

		$dam_fourth_generation['FourthDam']['dam'] = [
			'name'                => $pedigree['third-generation']['FourthDam']['dam']['name'],
			'registration_number' => $pedigree['third-generation']['FourthDam']['dam']['registration_number'],
			'titles'              => $pedigree['third-generation']['FourthDam']['dam']['titles']
		];

		$builder->update( [
			$generation         => $generation_results,
			'second_generation' => $dam_second_generation,
			'third_generation'  => $dam_third_generation,
			'fourth_generation' => $dam_fourth_generation,
		]);
//		return $dam_second_generation;
	}

	public static function generateOffSpringFirstGenerations($request,$builder,$reg_number,$sex){
		$offspring = explode(',',$request->get('offspring'));
		$offspring_generation = $offspring[0].'_generation';

		 $offspring_generation_results = json_decode(json_encode($builder->$offspring_generation),true);

		 if(!isset($offspring[2])){
			 $offspring_name = $offspring_generation_results[$offspring[1]]['name'];
			 $offspring_registration_number = $offspring_generation_results[$offspring[1]]['registration_number'];
			 $offspring_titles = $offspring_generation_results[$offspring[1]]['titles'];

			 $dog = \App\Dog::whereRegistrationNumber($offspring_registration_number)->first();

			 if($dog){
				 $dog_relationship = DogRelationship::whereDogId($dog->id)->first();

				 if($sex == 'male'){
					 if($dog_relationship){
						 $dog_relationship->update([
							 'father' => $reg_number
						 ]);
					 }else{
						 DogRelationship::create([
							 'id' => Uuid::generate(),
							 'dog_id' => $dog->id,
							 'father' => $reg_number
						 ]);
					 }

				 }else{
					 if($dog_relationship){
						 $dog_relationship->update([
							 'mother' => $reg_number
						 ]);
					 }else{
						 DogRelationship::create([
							 'id' => Uuid::generate(),
							 'dog_id' => $dog->id,
							 'father' => $reg_number
						 ]);
					 }
				 }
			 }

		 }else{
			 $offspring_name = $offspring_generation_results[$offspring[1]][$offspring[2]]['name'];
			 $offspring_registration_number = $offspring_generation_results[$offspring[1]][$offspring[2]]['registration_number'];
			 $offspring_titles = $offspring_generation_results[$offspring[1]][$offspring[2]]['titles'];

			 $dog = \App\Dog::whereRegistrationNumber($offspring_registration_number)->first();

			 if($dog){
				 $dog_relationship = DogRelationship::whereDogId($dog->id)->first();

				 if($sex == 'male'){
					 if($dog_relationship){
						 $dog_relationship->update([
							 'father' => $reg_number
						 ]);
					 }else{
						 DogRelationship::create([
							 'id' => Uuid::generate(),
							 'dog_id' => $dog->id,
							 'father' => $reg_number
						 ]);
					 }

				 }else{
					 if($dog_relationship){
						 $dog_relationship->update([
							 'mother' => $reg_number
						 ]);
					 }else{
						 DogRelationship::create([
							 'id' => Uuid::generate(),
							 'dog_id' => $dog->id,
							 'father' => $reg_number
						 ]);
					 }
				 }
			 }
		 }

		static::generateAncestorsFromRegistrationNumber($offspring_registration_number);
	}

	private static function getByRegistrationNumber($registration_number){
		return \App\Dog::whereRegistrationNumber($registration_number)->first();
	}

}