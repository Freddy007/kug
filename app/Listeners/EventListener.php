<?php

namespace App\Listeners;

use App\Events\SomeEvent;
use App\HelperClasses\Dog;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SomeEvent  $event
     * @return void
     */
    public function handle(SomeEvent $event)
    {
        $event->registrationNumber;
//	    User::create([
//		    'id' => \Uuid::generate(),
//		    'name' => 'Event 2',
//		    'email' => 'event1@gmail.com',
//		    'password' => bcrypt('password')
//	    ]);

	    Dog::generateAncestorsFromRegistrationNumber( $event->registrationNumber, true );

    }
}
