<?php

namespace App\Events;

use App\Events\Event;
use App\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SomeEvent extends Event
{
    use SerializesModels;

//    public $user;

	public $registrationNumber;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($registrationNumber)
    {
        //
	    $this->registrationNumber = $registrationNumber;

    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
