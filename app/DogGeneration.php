<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class DogGeneration extends Model
{
    //

	protected $fillable = [
		'id',
		'dog_id',
		'first_generation',
		'second_generation',
		'third_generation',
		'fourth_generation',
		'fifth_generation'
	];

	protected $casts = [
		'first_generation' => 'json',
		'second_generation' => 'json',
		'third_generation' => 'json',
		'fourth_generation' => 'json',
		'fifth_generation' => 'json'
	];



	public static function createGeneration($dog_id){
		static::create([
			'id' => Uuid::generate(),
			'dog_id' => $dog_id,
			'first_generation' => \App\HelperClasses\Dog::setUpGenerationJsonStructure()['first-generation'],
			'second_generation' => \App\HelperClasses\Dog::setUpGenerationJsonStructure()['second-generation'],
			'third_generation' => \App\HelperClasses\Dog::setUpGenerationJsonStructure()['third-generation'],
			'fourth_generation' => \App\HelperClasses\Dog::setUpGenerationJsonStructure()['fourth-generation']
		]);
	}

	public static function determineGenerationToUpdate($current_generation){
		if($current_generation == 'second'){
			return 'first_generation';
		}elseif ($current_generation == 'third'){
			return 'second_generation';
		}elseif ($current_generation == 'fourth'){
			return 'third_generation';
		}
	}

	public static function updateGeneration(){
//		if($request->offspring !== 'undefined' && $generation !== 'first_generation'){
//			$offspring            = $request->offspring;
//			$offspring_generation = $offspring[0];
//
//			$offspring_generation_results = json_decode(json_encode($builder->$offspring_generation),true);
//			$offspring_registration_number = $offspring_generation_results[$offspring[1]][$offspring[2]]['registration_number'];
//			$offspring_dog = Dog::whereRegistrationNumber($offspring_registration_number)->first();
//
//			if($offspring_dog){
//				$offspring_generation_builder = json_decode(json_encode(DogGeneration::whereDogId($offspring_dog->id)->first()->first_generation),true);
//
//				if(isset($offspring[2]) && $offspring[2] == 'sire'){
//					unset($offspring_generation_builder['sire']);
//					$offspring_generation_builder['sire'] =[
//						'name'                => $dog_been_inserted_first_generation_results['sire']['name'],
//						'registration_number' => $dog_been_inserted_first_generation_results['sire']['registration_number'],
//						'titles'              => $dog_been_inserted_first_generation_results['sire']['titles']
//					];
//					DogGeneration::whereDogId($offspring_dog->id)->first()->update([
//						'first_generation' => ''
//					]);
//				}elseif (isset($offspring[2]) && $offspring[2] == 'dam'){
//					unset($offspring_generation_builder['dam']);
//
//					$offspring_generation_builder['dam'] =[
//						'name'                => $dog_been_inserted_first_generation_results['dam']['name'],
//						'registration_number' => $dog_been_inserted_first_generation_results['dam']['registration_number'],
//						'titles'              => $dog_been_inserted_first_generation_results['dam']['titles']
//					];
//				}elseif(!isset($offspring[2]) && $offspring[1] == 'sire'){
//					unset($offspring_generation_builder['sire']);
//
//					$offspring_generation_builder['sire'] =[
//						'name'                => $dog_been_inserted_first_generation_results['sire']['name'],
//						'registration_number' => $dog_been_inserted_first_generation_results['sire']['registration_number'],
//						'titles'              => $dog_been_inserted_first_generation_results['sire']['titles']
//					];
//				}elseif (!isset($offspring[2] ) && $offspring[1] == 'dam'){
//					unset($offspring_generation_builder['dam']);
//
//					$offspring_generation_builder['dam'] =[
//						'name'                => $dog_been_inserted_first_generation_results['dam']['name'],
//						'registration_number' => $dog_been_inserted_first_generation_results['dam']['registration_number'],
//						'titles'              => $dog_been_inserted_first_generation_results['dam']['titles']
//					];
//				}
//
//
////						    unset($offspring_generation_builder['dam']);
//
//
////						    $offspring_generation_results[$offspring[1]][$offspring[2]] = [
////
////						    ];
//
//			}
//
//
//		}

	}
}
