<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Dog;
use DB;

class DogRelationship extends Model
{
    protected $fillable = ['id','dog_id','father','mother'];


}
