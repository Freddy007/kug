<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
        ModelNotFoundException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
	    if (app()->bound('sentry') && $this->shouldReport($e)) {
		    app('sentry')->captureException($e);
	    }

	    parent::report($e);
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
//    public function render($request, Exception $e)
//    {
//        if ($e instanceof ModelNotFoundException) {
//            $e = new NotFoundHttpException($e->getMessage(), $e);
//        }
//
//        return parent::render($request, $e);
//    }

    public function render($request, Exception $e) {
//        if ($e instanceof NotFoundHttpException)
//            return response(view('errors.404'), 404);
//
//	    if ($e instanceof Exception)
//		    return response(view('error.server-error'), 500);

//        if ($e instanceof \ErrorException)
//            return response(view('errors.404'), 404);

        return parent::render($request, $e);
    }
}
