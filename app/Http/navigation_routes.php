<?php
/**
 * Created by IntelliJ IDEA.
 * User: Freddy
 * Date: 7/13/2017
 * Time: 11:49 AM
 */

Menu::make('AdminNav',function($menu){
    $navigation_menu = array('Dashboard','All Members','Dogs','Breeds','Certificates','Trash');
    $navigation_links = array('Dashboard' => 'version2',
        'All Members'=>'version2/all-members',
        'Dogs' => 'version2/all-dogs',
        'Trash' => 'admin/trash',
        'Breeds' => 'version2/all-breeds',
        'Certificates' => 'version2/certificate-requests',
//                              'Logger'=>'version2/logger'
    );

    foreach($navigation_menu as $nav_menu){
        $about = $menu->add("$nav_menu",    array('url'  => $navigation_links[$nav_menu], 'class' => 'menu-dropdown classic-menu-dropdown'));
        $about->link->attr(array('class' => 'nav-link'));
    }
});


Menu::make('GuestMainPageNav',function($menu){
    $navigation_menu = array('Home','Contact Us','Login', "Register");
    $navigation_links = array('Home' => '',
        'Contact Us'=>'contact-us',
        'Login' => 'auth/login',
        'Register' => 'auth/register',
    );

    foreach($navigation_menu as $nav_menu){
        $about = $menu->add("$nav_menu",    array('url'  => $navigation_links[$nav_menu]));
        $about->link->attr(array('class' => 'nav-link'));
    }
});

Menu::make('MainPageNav',function($menu){
    $navigation_menu = array('Home','Dashboard','Contact Us');
    $navigation_links = array('Home' => '',
        'Dashboard' => 'version2/all-dogs',
        'Contact Us'=>'contact-us'
    );

    foreach($navigation_menu as $nav_menu){
        $about = $menu->add("$nav_menu",    array('url'  => $navigation_links[$nav_menu]));
        $about->link->attr(array('class' => 'nav-link'));
    }
});

Menu::make('MemberNav',function($menu){
    $navigation_menu = array('Home','Member page',"Contact Us");
    $navigation_links = array('Home' => '','Member page' => 'member', 'Contact Us'=>'contact-us');

    foreach($navigation_menu as $nav_menu){
        $about = $menu->add("$nav_menu",    array('url'  => $navigation_links[$nav_menu], 'class' => 'menu-dropdown classic-menu-dropdown'));
        $about->link->attr(array('class' => 'nav-link'));
    }
});

Menu::make('HomeNav1',function($menu){
    $navigation_menu = array('HOME','LOG IN');
    $navigation_links = array('HOME' => '','LOG IN' => 'auth/login');

    foreach($navigation_menu as $nav_menu){
        $menu->add("$nav_menu",    array('url'  => $navigation_links[$nav_menu]));
    }
});

Menu::make('HomeNav2',function($menu){
    $navigation_menu = array('HOME','DASHBOARD','LOGOUT');
    $navigation_links = array('HOME' => '','DASHBOARD' => 'version2','LOGOUT'=>'auth/logout');

    foreach($navigation_menu as $nav_menu){
        $menu->add("$nav_menu",    array('url'  => $navigation_links[$nav_menu]));
    }
});

Menu::make('HomeNav3',function($menu){
    $navigation_menu = array('HOME','MEMBER PAGE','LOGOUT');
    $navigation_links = array('HOME' => '','MEMBER PAGE' => 'member','LOGOUT' => 'auth/logout');

    foreach($navigation_menu as $nav_menu){
        $menu->add("$nav_menu",    array('url'  => $navigation_links[$nav_menu]));
    }
});