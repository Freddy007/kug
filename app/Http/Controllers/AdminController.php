<?php

namespace App\Http\Controllers;

use App\Breed;
use App\Breeder;
use App\Dog;
use App\DogGeneration;
use App\DogRelationship;
use App\Events\SomeEvent;
use App\HelperClasses\Pedigree;
use App\Helpers\NumberGeneration;
use app\Helpers\RegistrationNumber\RegistrationNumber;
use App\Recorder;
use App\RequestedCertificate;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Webpatser\Uuid\Uuid;
use Yajra\Datatables\Datatables;

/**
 * Class AdminController
 * @package App\Http\Controllers
 */
class AdminController extends Controller
{

	public function __construct() {

	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        \Cache::remember('dashboard_members',360,function(){
            $total =  User::count();
            $confirmed_count = \App\User::whereConfirmed(true)->count();
            $confirmed_percent = intval($confirmed_count  / \App\User::count() * 100);
            return ['total' => $total,'confirmed_percent'=> $confirmed_percent];
        });

        \Cache::remember('dashboard_dogs',360,function(){
            $total =  Dog::count();
            $confirmed_count = \App\Dog::whereConfirmed(true)->count();
            $confirmed_percent = intval($confirmed_count  / \App\Dog::count() * 100);
            return ['total' => $total,'confirmed_percent'=> $confirmed_percent];
        });

        \Cache::remember('dashboard_breeds',360,function(){
            $total =  Breed::count();
//            $confirmed_count = \App\Breed::whereConfirmed(true)->count();
//            $confirmed_percent = intval($confirmed_count  / \App\Breed::count() * 100);
            return ['total' => $total,'confirmed_percent'=> ''];
        });

         \Cache::remember('dashboard_certificates',360,function(){
            $total =  RequestedCertificate::count();
            $confirmed_count = RequestedCertificate::whereHonoured(true)->count();
            $confirmed_percent = intval($confirmed_count  / RequestedCertificate::count() * 100);
            return ['total' => $total,'confirmed_percent'=> $confirmed_percent];
        });


        return view('version2.admin.dashboard',compact('members'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDemo()
    {
        //
        return view('version2.admin.demo');
    }

    /**
     * Show the form for creating a registering a new Dog.
     *
     * @return \Illuminate\Http\Response
     */

    public function getRegisterDog()
    {
        return view('version2.admin.register_new_dog');
    }

    public function getAllDogsData(Request $request){

        $builder = Dog::leftJoin('users','users.id','=','dogs.user_id')
            ->leftJoin('breeders','breeders.id','=','dogs.breeder_id')
            ->select(DB::raw('dogs.*,CONCAT(users.first_name," ",users.last_name) as owner,breeders.name as breed'));

        $dogsData = Dog::queryDogsByField($builder,$request);


        $dogs = new Collection();

        foreach($dogsData as $dog){
            $dob = Carbon::createFromFormat('Y-m-d',$dog->dob);
            $difference = $dob->diffInMonths(Carbon::now());
            $dogs->push([
                'id' => $dog->id,
                'name'=> $dog->name,
                'dob'=> $dog->dob,
                'breed'=> $dog->breed,
                'sex'=> $dog->sex,
                'registration_number'=> $dog->registration_number,
                'breeder'=> $dog->owner == null ? '' : $dog->owner,
                'status' => $dog->confirmed == true ? "confirmed " : $this->getStatusButtonHtml($dog) ,
                'certificate' => $this->getCertificateButtonHtml($dog),
                'action' => $this->getActionButtonHtml($dog)
	            ]);
        }


        return Datatables::of($dogs)->make(true);

    }

    public function getDeletedDogs(Request $request){

	    switch($request->query('dogs')) {
		    case "unconfirmed":
			    $query = "?dogs=unconfirmed";
			    break;
		    case "confirmed":
			    $query = "?dogs=confirmed";
			    break;
		    case "male":
			    $query = "?dogs=male";
			    break;
		    case "female":
			    $query = "?dogs=female";
			    break;

		    default:
			    $query = "";
	    }

	    return view('version2.admin.all_dogs',compact('query'));
    }


	public function getDeletedDogsData(Request $request){
		$dogs = Dog::onlyTrashed()->get();


	}

	/**
     * @param Request $request
     * @return mixed
     */
    public function getMembersData(Request $request){

        return User::membersDataForTable($request);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function getAllMembers(Request $request){

        if(\Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }

        switch($request->query('members')) {
            case "unconfirmed":
                $query = "?members=unconfirmed";
                break;
            case "confirmed":
                $query = "?members=confirmed";
                break;
            case "administrator":
                $query = "?members=administrator";
                break;
            case "member":
                $query = "?members=member";
                break;

            default:
                $query = "";
        }

        return view('version2.admin.all_members',compact('users','index','query'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAllDogs(Request $request){

        switch($request->query('dogs')) {
            case "unconfirmed":
                $query = "?dogs=unconfirmed";
                break;
            case "confirmed":
                $query = "?dogs=confirmed";
                break;
            case "male":
                $query = "?dogs=male";
                break;
            case "female":
                $query = "?dogs=female";
                break;

            default:
                $query = "";
        }

        return view('version2.admin.all_dogs',compact('query'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function getAllBreeds(Request $request){
        if(\Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }
        $breeders = Breed::paginate();
        if ($request->term) {
            $query = $request->term;
            $breeders = Breed::where('name', 'LIKE', '%' . $query . '%')->get();
        }
        $paginator = new LengthAwarePaginator(range(1,count($breeders)),count($breeders),10,
            \Illuminate\Pagination\Paginator::resolveCurrentPage(),
            ['path' => \Illuminate\Pagination\Paginator::resolveCurrentPath()]
        );

        $breeders_menu = 'active';

        return view('version2.admin.all_breeds',compact('breeders','breeders_menu','paginator'));
    }


    /**
     * @param $user_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|string
     */
    public function getUserDogs($user_id){

        if(\Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }

        try {
            $dogs = \DB::table('dogs')
                ->leftJoin('users','users.id','=','dogs.user_id')
                ->leftJoin('breeders','breeders.id','=','dogs.breeder_id')
                ->where('user_id',$user_id)
                ->select(\DB::raw('dogs.*,CONCAT(users.first_name," ",users.last_name)
                 as user_name,breeders.name as breeder_name'))
                ->paginate();
            $user = User::findOrFail($user_id);
            $first_name = $user->first_name;
            $last_name = $user->last_name;
            $user = \Auth::getUser()->first_name;

        }catch(\Exception $e){
            flash('There is an error!');
//            return 'there is an error';
            return redirect()->back();
        }

        $members = 'active';

        // $dogs =  Dog::where('user_id',$user_id)->get();
        return view('version2.admin.member_dogs',compact('dogs','first_name','last_name','user','members'

        ));
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function getCertificateRequests(){
        if(\Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }
        $requests = RequestedCertificate::leftJoin('users','users.id','=','requested_certificates.user_id')
            ->leftJoin('dogs','dogs.id','=','requested_certificates.dog_id')
            ->select(\DB::raw('CONCAT(users.first_name," ", users.last_name) as member, users.kennel_name, users.phone, dogs.name,
                       dogs.registration_number,requested_certificates.honoured,requested_certificates.created_at,requested_certificates.id'))
            ->orderBy('requested_certificates.honoured')
            ->paginate();


        $certificate ='active';
        //var_dump($requests);exit;
        return view('version2.admin.certificate_requests',compact('requests','certificate'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function getRegisterNewDog(){
        if(\Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }

       $breeds =  \Cache::rememberForever('breeds',function(){
            return  Breeder::all();
        });

       $users = \Cache::rememberForever('users',function(){
           return User::all('id','first_name','last_name');
       });

        return view('version2.admin.register_new_dog',compact('breeds','users'));
    }

    /**
     * @param Request $request
     * @return \Exception|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postRegisterNewDog(Request $request){

        \Cache::forget('dogs');

        $validator = Validator::make($request->all(), [
            'name'=>'required',
            'sex'=>'required',
            'dob'=>'required',
            'breeder_id'=> 'required',
            'colour'=>'required'
        ]);

        if ($validator->passes()) {

	        $dog = new \App\HelperClasses\Dog($request);
	        $dog_id = $dog->beginRegistrationProcess();
            return response()->json(['message'=>'Added a new dog','dog_id' =>$dog_id])
                             ->setStatusCode(200);
        }
        return response()->json(['error'=>$validator->errors()->all()]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function getRegisterLitter(){
        if(\Gate::denies('administer')){
            flash()->error('You are not authorized to access that page!');
            return redirect('/');
        }

        $breeds = \Cache::get('breeds');

        $users = User::all('id','first_name','last_name');

//        $dogs_active = 'active';

        return view('version2.admin.register_litter',compact('breeds','users','dogs_active'
        ));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postRegisterLitter(Request $request){
        if(\Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }
        try{
//            $this->validate($request,['name'=>'required','dob'=>'required','father'=> 'required','breeder_id'=> 'required']);

	        $dog = (new \App\HelperClasses\Dog($request));
	         $dog->registerLitter();

	        return response()->json(['data' => ''])->setStatusCode(200);

        }catch(\Exception $e){

            return response()->json(
            	[
            		'error'=>$e->getMessage(),
		            'trace' => $e->getTraceAsString()
	            ])->setStatusCode(500);
        }
    }

    /**
     * @param $dog_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function getPedigreeCertificate($dog_id){
        if(\Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }

        $dog = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
            ->leftJoin('dog_relationships','dog_relationships.dog_id','=','dogs.id')
            ->leftJoin('users','users.id','=','dogs.user_id')
            ->where('dogs.id',$dog_id)
            ->select(\DB::raw('dogs.*,breeders.name as breed,dog_relationships.father,dog_relationships.mother,
                              users.first_name,users.last_name,users.kennel_name'))
            ->first();
        // $view_count = $dog->viewed + 1;
        //Dog::where('id',$dog_id)->update(['viewed'=> $view_count]);
        $type_certificate = "PEDIGREE";

        \Cache::forget('serial_number');
        \Cache::rememberForever('serial_number', function () {
            return NumberGeneration::generateUsingDate();
        });

        return view('version2.admin.pedigree-certificate',compact('dog','type_certificate'));
//        return view('version2.admin.certificates.certificate',compact('dog','type_certificate'));
    }

    public function postAddDogToGenerations(Request $request){


	    try {
		    $reg_number = RegistrationNumber::generateRegistrationNumber();

		    DB::transaction( function () use ( $request,$reg_number ) {

			    $builder            = DogGeneration::where( 'dog_id', $request->dog_id )->first();
			    $generation         = $request->generation . "_generation";
			    $position           =  explode( ",", $request->get( 'position' ) );

			    $generation_results = json_decode( json_encode( $builder->$generation ), true );

			    $dog_id     = Uuid::generate();

			    Dog::create(
				    [
					    'id'                  => $dog_id,
					    'user_id'             => \Auth::id(),
					    'name'                => $request->name,
					    'dob'                 => $request->dob,
					    'coat'                => $request->dob,
					    'breeder_id'          => $request->breeder_id,
//					    'sex'                 => $position[1] == 'sire' ? 'male' : 'female',
					    'sex'                 => $this->getSex($position),
					    'registration_number' => $reg_number,
					    'titles'              => $request->titles,
					    'performance_titles'  => $request->performance_titles,
					    'colour'              => $request->colour,
					    'height'              => $request->height,
					    'elbow_ed_results'    => $request->elbow_ed_results,
					    'appraisal_score'     => $request->appraisal_score,
					    'hip_hd_results'      => $request->hip_hd_results,
					    'tattoo_number'       => $request->tattoo_number,
					    'microchip_number'    => $request->microchip_number,
					    'DNA'                 => $request->DNA,
					    'other_health_checks' => $request->other_health_checks
				    ]
			    );

			    if(!isset($position[1])){
				    unset( $generation_results[ $position[0]]);
				    $generation_results[ $position[0]] = [
					    'name'                => $request->name,
					    'registration_number' => $reg_number,
					    'titles'              => $request->titles
				    ];
			    }else{
				    unset( $generation_results[ $position[0] ][ $position[1]]);
				    $generation_results[ $position[0] ][ $position[1] ] = [
					    'name'                => $request->name,
					    'registration_number' => $reg_number,
					    'titles'              => $request->titles
				    ];
			    }

			    if ( $generation_model = DogGeneration::whereDogId( $request->dog_id)->first() ) {
				    $generation_model->update( [
					    $generation => $generation_results
				    ]);
			    } else {
				    DogGeneration::create( [
					    'id'        => Uuid::generate(),
					    'dog_id'    => $request->dog_id,
					    $generation => $generation_results
				    ]);
			    }

			    $mother_info        = explode(",",$request->get('mother'));
			    $father_info        = explode(",",$request->get('father'));

			    $father_gen         = $father_info[0]."_generation";

			    $dog_relationship = DogRelationship::whereDogId($dog_id)->first();
			    if($dog_relationship){
				    $dog_relationship->update([
					    'father' =>  $request->mother !== 'undefined' && $request->father !== 'undefined'  ?
						    json_decode(json_encode($builder->$father_gen),true)[$father_info[1]][$father_info[2]]['registration_number'] : '',

					    'mother' =>  $request->mother !== 'undefined' && $request->father !== 'undefined'  ?
						    json_decode(json_encode($builder->$father_gen),true)[$mother_info[1]][$mother_info[2]]['registration_number'] : ''
				    ]);
			    }else{
				    $builder->$father_gen;
				    DogRelationship::create([
						    'id' => Uuid::generate(),
						    'dog_id' => $dog_id,
						    'father' => $request->mother !== 'undefined' && $request->father !== 'undefined'  ?
							    json_decode(json_encode($builder->$father_gen),true)[$father_info[1]][$father_info[2]]['registration_number'] : '',
						    'mother' =>  $request->mother !== 'undefined' && $request->father !== 'undefined' ?
							    json_decode(json_encode($builder->$father_gen),true)[$mother_info[1]][$mother_info[2]]['registration_number']:''
					    ]
				    );
			    }

			    if($request->offspring !== 'undefined' ){
				    Pedigree::generateOffSpringFirstGenerations($request,$builder,$reg_number,$this->getSex($position));
			    }
		    });

		    return response()->json(['message' => 'success',
		                             'request' => $request->mother,
		                             'dog_id' => $reg_number
		    ])->setStatusCode(200);

	    }catch(\Exception $e){
		    return response()->json(['message' => 'failed',
		                             'error' => $e->getMessage(),
		                             'trace' => $e->getTraceAsString(),
			                         'request' => $request->mother
		    ])->setStatusCode(500);
	    }
    }


    public function postGenerateAncestors($dog_id){
    	try{
		    \App\HelperClasses\Dog::generateAncestorsFromRegistrationNumber($dog_id);
		    return response()->json(['message' => 'success'])->setStatusCode(200);
	    }catch(\Exception $e){
		    return response()->json(['message' => 'failed'])->setStatusCode(500);
	    }
    }

    public function postAddToPedigreeTable(Request $request){

    	try{
//    		$pedigree = new Pedigree($request);
//    		$pedigree->addToPedigreeTable();
		    $generation          = $request->generation."_generation";

		    DB::transaction(function() use($request,$generation){

			    $position            = explode(',',$request->position);
			    $name                = $request->name;
			    $registration_number = $request->registration_number;
			    $titles              = $request->titles;
			    $dog_id              = $request->dog_id;
			    $dog_been_inserted_id  = $request->id;
			    $father               = explode(',',$request->get('father'));
			    $mother               = explode(',',$request->get('mother'));
			    $parent_generation    = $mother[0].'_generation';

			    $builder = DogGeneration::whereDogId($dog_id)->first();

			    $generation_results = json_decode( json_encode( $builder->$generation ), true );

			    $parent_generation_results = json_decode( json_encode( $builder->$parent_generation ), true );

			    $dog_been_inserted_builder = DogGeneration::whereDogId($dog_been_inserted_id)->first();

			    $dog_been_inserted_first_generation_results = json_decode(json_encode($dog_been_inserted_builder->first_generation),true);
//
			    if($generation == 'first_generation') {
				    unset( $generation_results[ $position[0] ] );

				    $generation_results[ $position[0] ] = [
					    'name'                => $name,
					    'registration_number' => $registration_number,
					    'titles'              => $titles
				    ];

				    if ( $position[0] == 'sire' ) {

					    $current_dog_builder = Dog::find($dog_id)->first();

                        DogRelationship::whereDogId($dog_id)
                        ->first()->update([
						    'father' => $registration_number
					    ]);

					    Pedigree::SaveFirstGenerationSirePartialAncestors($registration_number,$generation,$generation_results,$builder);

				    } elseif ( $position[0] == 'dam' ) {

					    $current_dog_builder = Dog::find( $dog_id )->first();

					    DogRelationship::whereDogId( $dog_id )->first()->update( [
						    'mother' => $registration_number
					    ]);

					    Pedigree::SaveFirstGenerationDamPartialAncestors($registration_number,$generation,$generation_results,$builder);

				    }
			    }else {
					    unset( $generation_results[ $position[0] ][ $position[1] ] );
					    $generation_results[ $position[0] ][ $position[1] ] = [
						    'name'                => $name,
						    'registration_number' => $registration_number,
						    'titles'              => $titles
					    ];
				    }

				    if( $request->mother !== 'undefined' && $request->father !== 'undefined'){
					    unset($parent_generation_results[$father[1]][$father[2]]);

					    $parent_generation_results[$father[1]][$father[2]] = [
						    'name'                => $dog_been_inserted_first_generation_results['sire']['name'],
						    'registration_number' => $dog_been_inserted_first_generation_results['sire']['registration_number'],
						    'titles'              => $dog_been_inserted_first_generation_results['sire']['titles']
					    ];

					    unset($parent_generation_results[$mother[1]][$mother[2]]);
					    $parent_generation_results[$mother[1]][$mother[2]] = [
						    'name'                => $dog_been_inserted_first_generation_results['dam']['name'],
						    'registration_number' => $dog_been_inserted_first_generation_results['dam']['registration_number'],
						    'titles'              => $dog_been_inserted_first_generation_results['dam']['titles']
					    ];

					    $builder->update([
						    $generation         =>  $generation_results,
						    $parent_generation  =>  $parent_generation_results
					    ]);

//					    DogRelationship::whereDogId($dog_been_inserted_id)->first()->update([
//						    'father'  => $dog_been_inserted_first_generation_results['sire']['registration_number'],
//						    'mother'  => $dog_been_inserted_first_generation_results['dam']['registration_number']
//					    ]);

				    }else{
					    $builder->update([
						    $generation         =>  $generation_results
					    ]);
				    }

			    if($request->offspring !== 'undefined' ){
				    Pedigree::generateOffSpringFirstGenerations($request,$builder,$registration_number,$this->getSex($position));
			    }
		    });

		    return response()->json([
			    'message' => 'success',
			    'redirect' => $generation == 'third_generation' ? false: true
		    ])->setStatusCode(200);

	       }catch(\Exception $e){

    	    \Sentry::captureException($e);

			    return response()->json([
				    'message' => 'failed',
				    'error'   => $e->getMessage(),
				    'trace' => $e->getTraceAsString()
			    ])->setStatusCode(500);
	    }
    }

    /**
     * @param $dog_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function getDog(Request $request, $dog_id){
        if(\Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }

        $number = Dog::whereId($dog_id)->first()->registration_number;

        $dog = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
            ->leftJoin('dog_relationships','dog_relationships.dog_id','=','dogs.id')
            ->where('dogs.id',$dog_id)
            ->select(DB::raw('dogs.*,breeders.name as breeder_name,dog_relationships.father,dog_relationships.mother'))
            ->first();

        if($request->ajax()){
	        return view('version2.admin.partials.pedigree_table_partial',compact('dog','dog_relationships'));
        }else{
	        return view('version2.admin.dog',compact('dog','dog_relationships'));

        }
    }

    /**
     * @param $dog_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function getEditDog($dog_id){

        if(\Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }

        $dog = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
            ->leftJoin('dog_relationships','dog_relationships.dog_id','=','dogs.id')
            ->leftJoin('users','users.id','=','dogs.user_id')
            ->where('dogs.id',$dog_id)
            ->select(DB::raw('dogs.*,breeders.name as breeder_name,dog_relationships.father,dog_relationships.mother,
            CONCAT(users.first_name," ",users.last_name) as member, users.id as member_id'))
            ->first();

        $breeders = Breeder::all();

        $users = User::all();

        return view('version2.admin.edit_dog',compact('dog','breeders','users'));
    }

    /**
     * @param Request $request
     * @param $dog_id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postUpdateDog(Request $request, $dog_id){
//        return $request;
        if(\Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }
        try {

            Dog::updateDog($request,$dog_id);
            flash()->success("You have successfully updated a dog's info! ");

        }catch(\Exception $e){
            flash()->error("Error updating a dog! ". $e->getMessage());
        }

        return \Redirect::back();
    }

    public function getLogger(){
       $logger =  Recorder::orderBy('created_at','desc')->paginate();
        return view('version2.admin.logger',compact('logger'));
    }

    public function postConfirmDog($dog_id,Request $request){


        if(Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }

        try{
            DB::transaction(function()use($request,$dog_id){
                Dog::where('id',$dog_id)->update(['confirmed'=> $request->status ]);
                $dog = Dog::leftJoin('users','users.id','=','dogs.user_id')
                    ->where('dogs.id',$dog_id)
                    ->select(DB::raw('users.*,dogs.name as dog_name'))
                    ->first();
                $email = $dog->email;

                $dog_name = $dog->dog_name;
                $message =  'Your Dog '  . $dog_name . ' has been confirmed. log in now at http://www.pedigree.kennelunionofghana.com for more details';
//            if($dog->email){
//                sendEmail($email,$message, 'Confirmation Message');
//            }

                $admin = \Auth::user();
                $admin_member = $admin->first_name ." ".$admin->last_name;

                $message  =  "Admin Member $admin_member just confirmed $dog_name ";
//                Recorder::Log($admin->id,$admin->id,'',$message);
                return \Redirect::back();


            });

            flash()->success('A new dog has been confirmed!');
        }catch(\Exception $e){
            flash()->error('Error confirming dog! '.$e->getMessage());
            return \Redirect::back();

        }
    }

    public function getProfile(){
        return view('version2.admin.profile');
    }

    public function getProfileAccount(){
        $user_builder = \Auth::user();
        return view('version2.admin.profile_account',compact('user_builder'));
    }

    public function postUpdateProfileAccount(Request $request){

        flash()->success('Successfully updated your profile!');
        User::find(\Auth::id())->update($request->all());

        return redirect()->back();
    }

	private function getSex($position){
		if(isset($position[1]) && $position[1] == 'sire'){
			return 'male';
		}elseif (isset($position[1]) && $position[1] == 'dam'){
			return 'female';
		}else{
			if($position[0] == 'sire'){
				return 'male';
			}else{
				return 'female';
			}
		}
	}

    //update profile password action
    public function postUpdatePassword(Request $request){
        $this->validate($request,[
            'password' => 'required',
            'new_password' => 'required',
        ]);
        if(\Hash::check($request->password,\Auth::user()->password)){
            if($request->new_password == $request->confirm_password){
                User::find(\Auth::id())->update([
                    'password' => bcrypt($request->new_password)
                ]);
                flash()->success('Successfully updated the password!');
            }else {
                flash()->error('New password and confirm password do not match!');
            }

            return redirect()->back();

        }else{
//            return 'wrong';
            flash()->warning('Passwords do not match!');
            return redirect()->back();
        }
    }

    public function getCertificateButtonHtml($dog){
	    return "<a class='btn btn-success btn-xs add-owner-detail' href='#' data-id=$dog->id> certificate</a>";
    }

    public function getStatusButtonHtml($dog){
    	return "<button id=\"btnConfirm-$dog->id\" class=\"btn btn-success btn-sm\"
                                                data-toggle=\"tooltip\" data-placement=\"left\" title=\"confirm new dog\"><i class=\"fa fa-thumbs-o-up\"></i></button>";
    }

	/**
	 * @param Dog $dog
	 * @return string
	 */

	public function getActionButtonHtml($dog){
	    return " <a href=\"/version2/dog/$dog->id\" class=\"btn btn-default btn-xs\"><i class='fa fa-eye'></i></a>".
	    "<a href=\"/version2/edit-dog/$dog->id\" class=\"btn btn-warning btn-xs\"><i class=\"fa fa-pencil-square-o\"></i> </a>".
	    "<button class=\"btn btn-danger btn-xs\" id=\"delete-dog-$dog->id\"
                                       data-toggle=\"tooltip\" data-placement=\"left\" title=\"move $dog->name to trash\"><i class=\"fa fa-trash\"></i></button>";

    }
}
