<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\ConfirmUser;
use Webpatser\Uuid\Uuid;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Auth;
use Mail;
use Illuminate\Support\Facades\App;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */
    protected $redirectPath = '/version2/all-dogs';
    protected $redirectTo = '/auth/login';

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;
    use AuthenticableTrait;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['getLogout','getRegister','postRegister']]);
    }

    public function getRegister()
    {
        $members = 'active';

        return view('auth.register',compact('members'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'title' => 'required',
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'phone' => 'required',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'id' => Uuid::generate(),
            'title' => $data['title'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

    }

    public function postRegister(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $id = Uuid::generate();

        $email = \Cache::get('notification_email','frederickankamah988@gmail.com');

        User::create(['id'=>$id,'title'=>$request->title,'first_name'=>$request->first_name,
                      'last_name'=> $request->last_name, 'kennel_name' => $request->kennel_name, 'phone' => $request->phone,
                      'email' => $request->email,'password'=>bcrypt($request->password)

        ]);

        \Cache::forget('users');

        $user = \App\User::find($id);

        try {
            \Mail::send('emails.member_confirmation', ['user' => $user], function ($m) use ($email, $user) {
                $m->to($email, $user->first_name)->subject('New Registration !');
            });
            //return redirect($this->redirectPath());
            return redirect()->intended($this->redirectPath());
        }catch(\Exception $e){
            flash()->error('there was an error in sending mail');
            return redirect()->intended($this->redirectPath());

        }
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, [
            $this->loginUsername() => 'required', 'password' => 'required',
        ]);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }

        if (\Auth::validate(['email' => $request->email, 'password' => $request->password, 'confirmed' => 0])) {
            flash()->error('Your Account has not been confirmed yet !');
            return redirect($this->loginPath())
                ->withInput($request->only('email', 'remember'));
                flash()->error('Your Account has not been activated yet !');
            }
        $credentials = $this->getCredentials($request);

        if ($throttles) {
            $this->incrementLoginAttempts($request);
        }

        if (\Auth::attempt($credentials, $request->has('remember'))) {
            if(\Auth::getUser()->administrator == 1){
                return redirect('/version2/all-dogs');
            }
            return redirect('/');//->intended($this->redirectPath());
           // return redirect()->intended($this->redirectPath());
        }
        return redirect($this->loginPath())
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                $this->loginUsername() => $this->getFailedLoginMessage(),
            ]);
    }
}
