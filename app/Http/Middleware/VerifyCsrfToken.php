<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use App\Http\Middleware\Closure;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [

        '/add-new-column',
        '/member/register-dog',
        '/member/register-litter',
        '/member/check-dog/*',
        '/show-offspring/*',
        '/admin/breeder/*',
        '/admin/issue-certificate/*',
        '/check-dog-name/*',
        '/admin/report-number',
        '/admin/all-dogs-data',
        '/admin/array-data',
        '/admin/members-data',
        '/admin/register-dog',

        //  version 2
        'version2/all-breeds',
        '/version2/register-new-dog',
        'version2/register-litter',
        'version2/members-data',
	    '/version2/generate-ancestors/*',
	    '/version2/add-to-pedigree-table',
	    '/generate-ancestors/*',
	    '/add-to-pedigree-table',
	    '/search-results'

    ];

    public function handle($request, \Closure $next){
        if($request->input('_token')) {
            if ( \Session::getToken() != $request->input('_token')) {
                return redirect()->guest('/')
                    ->with('global', 'Your session has expired. Please try logging in again.');
            }
        }
        return parent::handle($request, $next);
    }
}
