<?php

// Home
Breadcrumbs::register('home', function($breadcrumbs)
{
    $breadcrumbs->push('Pedigree Database', url('/'));
});

Breadcrumbs::register('dashboard', function($breadcrumbs)
{
    $breadcrumbs->push('Dashboard', url('/admin'));
});

Breadcrumbs::register('all-dogs',function($breadcrumbs){
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('All Dogs', url('/admin/all-dogs'));
});

Breadcrumbs::register('register-dog',function($breadcrumbs){
    $breadcrumbs->parent('all-dogs');
    $breadcrumbs->push('Register Dog', url('/admin/register-dog'));
});

Breadcrumbs::register('all-members',function($breadcrumbs){
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('All Members', url('/admin/all-members'));
});

Breadcrumbs::register('all-breeders',function($breadcrumbs){
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('All Breeds', url('/admin/all-breeders'));
});

Breadcrumbs::register('all-transfers',function($breadcrumbs){
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('All Transfers', url('/admin/dog-transfers'));
});

Breadcrumbs::register('transfer-dog',function($breadcrumbs){
    $breadcrumbs->parent('all-transfers');
    $breadcrumbs->push('Transfer Dog', url('/admin/transfer-dog'));
});

Breadcrumbs::register('register-litter',function($breadcrumbs){
    $breadcrumbs->parent('all-dogs');
    $breadcrumbs->push('Register Litter', url('/admin/register-litter'));
});

Breadcrumbs::register('certificate-requests',function($breadcrumbs){
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Certificate Requests', url('/admin/certificate-requests'));
});


Breadcrumbs::register('editCustomer', function($breadcrumbs,$customer)
{
    $breadcrumbs->parent('customers');
    $breadcrumbs->push('Edit Customer', url('/crm/customers/edit',$customer->id));
});

Breadcrumbs::register('progeny', function($breadcrumbs,$dog)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Progeny', url('/show-offspring',$dog->registration_number));
});

Breadcrumbs::register('offspring', function($breadcrumbs,$dog)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Offspring', url('/show-dog',$dog->id));
});