<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

use App\HelperClasses\Dog;

$router->get('/pdf/view', function() {
    $html = view('admin.hello')->render();

    return PDF::load($html)->show();

    return $this->pdf
        ->load($html)
        ->download();

});

include_once 'navigation_routes.php';

Route::get('/regenerate-dog/{number}',function($number){

	$dog = \App\Dog::whereRegistrationNumber($number)->first()->id;

	Dog::generateAncestorsFromRegistrationNumber($number,true);

//	return redirect()->to("/version2/dog/$dog");
	return redirect()->to("/main-table?id=$dog");
});

Route::get('/regenerate-dog-generations/{id}',function($id){

    $dog1 = \App\DogGeneration::whereDogId($id)->first();
    $dog1->delete();

    $dog = \App\Dog::find($id);
    $structure =  Dog::generateAncestorsFromRegistrationNumber($dog->registration_number,false);

    \App\DogGeneration::create([
        'id'               => \Webpatser\Uuid\Uuid::generate(),
        'dog_id'           => $id,
        'first_generation' => $structure['first-generation'],
        'second_generation' => $structure['second-generation'],
        'third_generation' => $structure['third-generation'],
        'fourth_generation' => $structure['fourth-generation']
    ]);

    return json_encode(["message"=> "successfully regenerated generations table! Go back to previous page",
        "structure" => $structure]);
});

Route::get('/empty-json-structure/{dog_id}',function($dog_id){

	$structure = Dog::setUpGenerationJsonStructure();

	if(!\App\DogGeneration::whereDogId($dog_id)->first()){
		\App\DogGeneration::create([
			'id'               => \Webpatser\Uuid\Uuid::generate(),
			'dog_id'           => $dog_id,
			'first_generation' => $structure['first-generation'],
			'second_generation' => $structure['second-generation'],
			'third_generation' => $structure['third-generation'],
			'fourth_generation' => $structure['fourth-generation']
		]);
	}else{
		\App\DogGeneration::whereDogId($dog_id)->first()->update([
			'first_generation' => $structure['first-generation'],
			'second_generation' => $structure['second-generation'],
			'third_generation' => $structure['third-generation'],
			'fourth_generation' => $structure['fourth-generation']
		]);
	}
});

Route::get('/test2/{number}',function($number){

//	\App\HelperClasses\Dog::generateAncestorsFromRegistrationNumber($number,true);
});

Route::get('test3',function(){
//	return \App\HelperClasses\Pedigree::generateAncestorsFromRegistrationNumber(201711021915);
	return \App\HelperClasses\Pedigree::generateFirstGenerationDamPartialAncestorsFromRegistrationNumber(201802102296);
});

// Authentication routes...2
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

Route::get('login2', 'HomeController@getLogin');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::get('/test-uid',function (){
	return \App\Dog::create([
		'name' => 'No Uid Dog',
		'breeder_id' => '4db07470-e077-11e5-8a95-c336206736d5',
		'user_id' => Auth::id(),
		'dob' => '2017-01-01',
		'sex' => 'male',
		'registration_number' =>''
	]);
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
$router->group(['middleware' => 'auth'], function() {
    Route::controller('member','MemberController');
    Route::controller('admin', 'DashboardController');
    Route::controller('version2', 'AdminController');
});


Route::controller('/','HomeController');




