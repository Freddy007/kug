 function useName(){
        alert('dog not found!');
        let dog_name = $('.select2-search__field').val();
        $('.hidden-name-input').removeClass('hide').val(dog_name);
        $('#select-input').addClass('hide');
        $('#save-btn').removeAttr('disabled');
    }

    $( document ).ready(function() {

        let generation = '';
        let position = '';
        let sire_parent = '';
        let dam_parent = '';
        let offspring = '';
        let gender = '';
        let dog_parent = $('.dog_parent');
        let dog_id = $("#dog_id").data("id");

        $(document).off('click').on('click', '.edit-column', function (e) {

            let positionString = ($(this).data('position')).split(",");

            generation = $(this).data('generation');
            position = positionString.length > 1 ? positionString[1] : positionString[0] ;
            sire_parent = $(this).data('sire-parent');
            dam_parent = $(this).data('dam-parent');
            offspring = $(this).data('offspring');

            self = $(this);

            // alert($(this).data('generation'));
            // alert($(this).data('position'));

            $('#edit-column-modal').modal();


    dog_parent.on('select2:select', function (e) {
            let name = $(this).val();
            position_array = position.split(",");
            let gender = typeof position_array[1] !== 'undefined' ? position_array[1] : position_array[0];

            $.ajax({
                url: '/dog-by-name/' + name + '?gender=' + gender,
                type: "GET",
                statusCode: statusCodeResponses,
            }).done(function (data) {
                $('.help-block.name').text(" This dog's name already exists!").css('color', 'red');
                $('#save-btn').attr('disabled', true);

                swal("Do you want to insert this dog?", {
                    buttons: {
                        cancel: "Cancel!",
                        catch: {
                            text: "Yes, insert!",
                            value: "insert",
                        }
                    },
                }).then((value) => {
                        switch (value) {

                            case "insert":
                                appendPedigree(data);
                                break;

                            default:
                                addToPedigreeTableFailed()
                        }
                    });
            });
        });

            $('#register-dog-form').off('submit').on('submit', function (e) {

                e.preventDefault();

                $('#save-btn').attr('disabled', true);

                let formData = new FormData(this);

                formData.append('generation', generation);
                formData.append('position', position);
                formData.append('dog_id', dog_id);
                if (typeof self.data('sire-parent') !== undefined) {
                    formData.append('father', self.data('sire-parent'));
                    formData.append('mother', self.data('dam-parent'));
                }

                formData.append('offspring', offspring);

                let initiate = $.ajax({
                    url: $(this).attr('action'),                   // Url to which the request is send
                    type: "POST",                                 // Type of request to be send, called as method
                    data: formData,                              // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                    contentType: false,                         // The content type used when sending data to the server.
                    cache: false,                              // To unable request pages to be cached
                    processData: false,                        // To send DOMDocument or non processed data file it is set to false
                }).then(function (data) {
                    $.post("/version2/generate-ancestors/" + data.dog_id, function () {

                    }).fail(function (data) {
                        alert('dog generation failed');
                    })
                });

                initiate.done(function (data) {
                    $.get("", function (data) {
                        $('#pedigree-table').html(data);
                    });
                    // Display a success toast
                    // toastr.success('Successfully added a new dog!');
                    $('#edit-column-modal').modal('hide');
                    $('#save-btn').removeAttr('disabled');

                });

                initiate.fail(function () {
                    // toastr.error('failed to add a dog!!');
                    $('#save-btn').removeAttr('disabled');
                })
            });

        });

        $('#name').off('change').on('change', function () {

            let name = $('#name').val();
            let position_array = position.split(",");
            gender = typeof position_array[1] !== 'undefined' ? position_array[1] : position_array[0];

            $.ajax({
                url: '/dog-by-name/' + name + '?gender=' + gender,
                type: "GET",
                statusCode: statusCodeResponses,
            }).done(function (data) {
                $('.help-block.name')
                    .text(" This dog's name already exists!").css('color', 'red');
                $('#save-btn').attr('disabled', true);
                swal("Do you want to insert this dog?", {
                    buttons: {
                        cancel: "Cancel!",
                        catch: {
                            text: "Yes, insert!",
                            value: "insert",
                        }
                    },
                })
                    .then((value) => {
                        switch (value) {

                            case "insert":
                                appendPedigree(data);
                                break;

                            default:
                             addToPedigreeTableFailed()


                        }
                    });
            })
        });

        let statusCodeResponses = {
            403: function () {
                // swal('error', "You can't put this dog here!");
                alert("You can't put this dog here!");
                $('#save-btn').attr('disabled', true);
                $('#name').val('')
            }
        };

        function addToPedigreeTableSuccess() {
            $('#edit-column-modal').modal('hide');

            $.get("", function () {

            }).done(function (data) {
                $('#pedigree-table').html(data);
            });


            // swal("Added!", "This dog will be added to your table.", "success");
        }

        function addToPedigreeTableFailed() {
            $('#save-btn').removeAttr('disabled');

            $('.help-block.name').text(' ');
            location.reload();
        }

        function appendPedigreeDetails(data, generation, position, sire_parent, dam_parent, offspring) {
            let formData = new FormData();
            formData.append('generation', generation);
            formData.append('position', position);
            formData.append('name', data.name);
            formData.append('registration_number', data.registration_number);
            formData.append('titles', data.titles);
            formData.append('id', data.id);
            formData.append('dog_id', dog_id);
            formData.append('father', sire_parent);
            formData.append('mother', dam_parent);
            formData.append('offspring', offspring);

            return formData;
        }

    
        dog_parent.select2({
            language: {
                noResults: function (params) {
                    return "Not Found... <a href='#' class='btn btn-success use-name' onclick='useName()'>Use name</a>";
                }
            },
            ajax: {
                //url: "{{url('member/dogs')}}",
                url: "/member/dogs",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page,
//                    g: $(this).data('gender')
                        g: position === "sire" ? "male" : "female"
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 1,
            templateResult: formatRepo, 
            templateSelection: formatRepoSelection
        });

        let position_array = position.split(",");
        gender = typeof position_array[1] !== 'undefined' ? position_array[1] : position_array[0];

        function formatRepo(dog) {
            if (dog.loading) return dog.text;

            // var markup = "<div class='select2-result-repository clearfix'>" +
            //     "<div class='select2-result-repository__avatar'><img width='70' height='50' src='/images/catalog/" + dog.image_name + "' /></div>" +
            //     "<div class='select2-result-repository__meta'>" +
            //     "<div class='select2-result-repository__title'>" + dog.name + "</div>" +
            //     "<div class='select2-result-repository__title'>" + dog.registration_number + "</div>";

                var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__avatar'><img width='70' height='50' src='https://i.pinimg.com/600x315/1c/8b/8b/1c8b8b31deca8c38226398e29c4700d4.jpg' /></div>" +
                "<div class='select2-result-repository__meta'>" +
                "<div class='select2-result-repository__title'>" + dog.name + "</div>" +
                "<div class='select2-result-repository__title'>" + dog.registration_number + "</div>";

            if (dog.breeder_name) {
                markup += "<div class='select2-result-repository__description'>" + " Breed : " + dog.breeder_name + "</div>";
            }

            markup += "<div class='select2-result-repository__description'>" + " Sex : " + dog.sex + "</div>";
            markup += "</div></div>";

            return markup;
        }

        function formatRepoSelection(dog) {
//        return dog.registration_number;
            return dog.name;
        }

        function appendPedigree(data) {
            let formData = appendPedigreeDetails(data, generation, position, sire_parent, dam_parent, offspring);
            $.ajax({
                url: '/version2/add-to-pedigree-table',
                type: "POST",                                 // Type of request to be send, called as method
                data: formData,                              // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,                         // The content type used when sending data to the server.
                cache: false,                              // To unable request pages to be cached
                processData: false,
                statusCode: {
                    404: function() {
                      alert( "page not found" );
                    },
                    500: function() {
                        alert( "failed!" );
                      }
                  }

            }).done(function (data) {
                addToPedigreeTableSuccess();
                if (data.redirect === true) {
                    setTimeout(function () {
                      
                    }, 3000)
                }
            }).then(function () {
                setTimeout(function () {
                location.reload();
                }, 3000)

            })
        }
    });

