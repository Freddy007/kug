/**
 * Created by diddy on 12/2/2016.
 */

//var myApp = angular.module('myApp', ['simplePagination','ngAnimate']);

var adminApp = angular.module('adminApp',['ngAnimate']);


adminApp.controller('DogRegistrationController', ['$scope', '$http', function($scope, $http) {

    $http.get('/version2/all-breeds').success(function(data){
        $scope.breeds = data;
    })

}]);

