-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 04, 2016 at 01:06 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kennel_union`
--

-- --------------------------------------------------------

--
-- Table structure for table `breeders`
--

CREATE TABLE IF NOT EXISTS `breeders` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `breeders`
--

INSERT INTO `breeders` (`id`, `name`, `created_at`, `updated_at`) VALUES
('19f55580-da44-11e5-b81b-438c9a97bae0', 'adil', '2016-02-23 15:42:53', '2016-02-23 15:42:53'),
('5a77e7a0-cac5-11e5-afe5-dfdf2b9d1f43', 'Acacia Hills', '2016-02-03 22:27:47', '2016-02-03 22:27:47'),
('61506a70-cac5-11e5-b076-3f7f272ab1eb', 'Acacia', '2016-02-03 22:27:59', '2016-02-23 16:40:26'),
('6a3cb400-cac5-11e5-a53d-c33a4c974f45', 'Acker', '2016-02-03 22:28:14', '2016-02-03 22:28:14'),
('71e36080-cac5-11e5-bb70-57a015cf4921', 'Admirals', '2016-02-03 22:28:27', '2016-02-03 22:28:27'),
('78845a40-cac5-11e5-a3c6-198f2043140d', 'Adils', '2016-02-03 22:28:38', '2016-02-23 16:39:01'),
('80bfc630-cac5-11e5-b2b0-ed2708049b83', 'Absolute', '2016-02-03 22:28:52', '2016-02-23 16:47:37');

-- --------------------------------------------------------

--
-- Table structure for table `dogs`
--

CREATE TABLE IF NOT EXISTS `dogs` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `breeder_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dob` date NOT NULL,
  `sex` enum('male','female') COLLATE utf8_unicode_ci NOT NULL,
  `registration_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titles` text COLLATE utf8_unicode_ci,
  `performance_titles` text COLLATE utf8_unicode_ci,
  `image_name` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `colour` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `height` decimal(10,0) DEFAULT NULL,
  `elbow_ed_results` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hip_hd_results` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tattoo_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `microchip_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DNA` text COLLATE utf8_unicode_ci,
  `other_health_checks` text COLLATE utf8_unicode_ci,
  `confirmed` tinyint(1) NOT NULL,
  `dead` tinyint(1) NOT NULL,
  `viewed` int(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dogs`
--

INSERT INTO `dogs` (`id`, `user_id`, `breeder_id`, `name`, `dob`, `sex`, `registration_number`, `titles`, `performance_titles`, `image_name`, `colour`, `height`, `elbow_ed_results`, `hip_hd_results`, `tattoo_number`, `microchip_number`, `DNA`, `other_health_checks`, `confirmed`, `dead`, `viewed`, `created_at`, `updated_at`, `deleted_at`) VALUES
('2f141150-d09e-11e5-9604-49487cfc25ba', '43bbfc40-cac4-11e5-a4d2-f1a376c70f69', '80bfc630-cac5-11e5-b2b0-ed2708049b83', 'menaj', '2016-02-02', 'female', '201602116', 'HBO', NULL, 'image_2f141150-d09e-11e5-9604-49487cfc25ba.jpg', 'white', '2', 'GOOD', 'GOOD', '456989716', '4236594', '4892LKO', 'GOOD', 0, 0, 4, '2016-02-11 09:02:31', '2016-02-26 10:09:06', NULL),
('42085f50-d09d-11e5-9559-45dbeeb807fc', '43bbfc40-cac4-11e5-a4d2-f1a376c70f69', '61506a70-cac5-11e5-b076-3f7f272ab1eb', 'mag', '2016-02-02', 'male', '201602113', 'HBO', NULL, 'image_42085f50-d09d-11e5-9559-45dbeeb807fc.jpe', 'white', '3', 'GOOD', 'GOOD', '45698971', '4236594', '4892LKO', 'GOOD', 0, 0, 3, '2016-02-11 08:55:54', '2016-03-03 12:53:11', NULL),
('459769a0-d09f-11e5-bd1c-67c7342e3663', 'e2092da0-d4a7-11e5-8567-c10f37027f74', '78845a40-cac5-11e5-a3c6-198f2043140d', 'major', '2016-02-01', 'male', '201602117', 'HBO', NULL, 'image_459769a0-d09f-11e5-bd1c-67c7342e3663.jpg', 'white', '3', 'GOOD', 'GOOD', '45698971', '4236594', '4892LKO', 'GOOD', 1, 0, 9, '2016-02-11 09:10:19', '2016-03-02 10:45:11', NULL),
('552c4aa0-d09c-11e5-ba05-611171763c55', 'e2092da0-d4a7-11e5-8567-c10f37027f74', '6a3cb400-cac5-11e5-a53d-c33a4c974f45', 'Mak', '2016-02-02', 'male', '201602111', 'HBO', NULL, 'image_552c4aa0-d09c-11e5-ba05-611171763c55.jpg', 'white', '3', 'GOOD', 'GOOD', '45698971', '4236594', '4892LKO', 'GOOD', 0, 0, 0, '2016-02-11 08:49:16', '2016-02-26 10:09:06', NULL),
('81e23800-e09d-11e5-a66a-45aa533cba75', '52e4a850-d571-11e5-b6e2-3ded6f9b631e', '61506a70-cac5-11e5-b076-3f7f272ab1eb', 'Duke of Bosch', '2016-02-29', 'male', '2016030213', '', NULL, 'image_81e23800-e09d-11e5-a66a-45aa533cba75.jpg', 'white', '18', '', '', '', '', '', '', 1, 0, 0, '2016-03-02 17:37:59', '2016-03-03 08:34:45', NULL),
('8a243720-d09f-11e5-9b62-ddfddbfd26da', '43bbfc40-cac4-11e5-a4d2-f1a376c70f69', '78845a40-cac5-11e5-a3c6-198f2043140d', 'mel', '2016-02-03', 'female', '201602118', 'HBO', NULL, 'image_8a243720-d09f-11e5-9b62-ddfddbfd26da.jpg', 'white', '3', 'GOOD', 'GOOD', '45698971', '4236594', '4892LKO', 'GOOD', 1, 0, 29, '2016-02-11 09:12:14', '2016-03-03 13:05:31', NULL),
('90c36230-d09d-11e5-9cf9-1bbb0d83bfce', '43bbfc40-cac4-11e5-a4d2-f1a376c70f69', '6a3cb400-cac5-11e5-a53d-c33a4c974f45', 'smatty', '2016-02-02', 'female', '201602114', 'HBO', NULL, 'image_90c36230-d09d-11e5-9cf9-1bbb0d83bfce.jpg', 'white', '2', 'GOOD', 'GOOD', '45698971', '4236594', '4892LKO', 'GOOD', 1, 0, 257, '2016-02-11 08:58:06', '2016-02-29 21:49:35', NULL),
('a0721f50-d09c-11e5-93d0-a9cc4f72663c', '43bbfc40-cac4-11e5-a4d2-f1a376c70f69', '6a3cb400-cac5-11e5-a53d-c33a4c974f45', 'Nikki', '2016-02-03', 'female', '201602112', 'HBO', NULL, 'image_a0721f50-d09c-11e5-93d0-a9cc4f72663c.jpg', 'white', '4', 'GOOD', 'GOOD', '45698971', '423659', '4892LKO', 'GOOD', 0, 0, 0, '2016-02-11 08:51:23', '2016-02-26 10:09:06', NULL),
('a53ba350-d4aa-11e5-849b-15dce550f014', '43bbfc40-cac4-11e5-a4d2-f1a376c70f69', '6a3cb400-cac5-11e5-a53d-c33a4c974f45', 'hilly', '2016-02-03', 'female', '201602169', 'hbo', NULL, 'image_a53ba350-d4aa-11e5-849b-15dce550f014.jpg', 'red', '5', 'GOOD', 'GOOD', '45698971', '423659', '4892LKO', 'GOOD', 1, 0, 4, '2016-02-16 12:41:48', '2016-02-29 20:31:22', NULL),
('ae462571-dc6b-11e5-ae65-645106b2d802', '58c19af0-d989-11e5-91a6-95e04abb99f4', '6a3cb400-cac5-11e5-a53d-c33a4c974f45', 'milla', '2016-01-13', 'male', '2016022612', 'HBO', 'hbo', NULL, 'white', '12', 'good', 'good', '4526565565265', '4526565565265', '4526565565265', 'good', 0, 0, 0, '2016-02-02 00:00:00', '2016-02-26 10:09:06', '2016-02-08 00:00:00'),
('be823e80-d09d-11e5-aac2-f3c862031baf', '43bbfc40-cac4-11e5-a4d2-f1a376c70f69', '5a77e7a0-cac5-11e5-afe5-dfdf2b9d1f43', 'joy', '2016-02-02', 'male', '201602115', 'HBO', NULL, 'image_be823e80-d09d-11e5-aac2-f3c862031baf.jpg', 'white', '3', 'GOOD', 'GOOD', '45698971', '4236594', '4892LKO', 'GOOD', 0, 0, 1, '2016-02-11 08:59:22', '2016-03-02 08:23:31', NULL),
('cc65f8e0-e08d-11e5-bb13-bbba69507a52', '4f745d50-d987-11e5-8386-2fba3c43ad79', '6a3cb400-cac5-11e5-a53d-c33a4c974f45', 'melan', '2016-03-10', 'female', '2016030212', '', NULL, 'image_cc65f8e0-e08d-11e5-bb13-bbba69507a52.jpg', 'white', '21', '', '', '', '', '', '', 0, 0, 0, '2016-03-02 15:45:32', '2016-03-03 08:35:02', NULL),
('d4295b80-d4aa-11e5-99a5-e991a3420610', '43bbfc40-cac4-11e5-a4d2-f1a376c70f69', '6a3cb400-cac5-11e5-a53d-c33a4c974f45', 'hillyer', '2016-02-03', 'female', '2016021610', 'hbo', NULL, 'image_d4295b80-d4aa-11e5-99a5-e991a3420610.jpg', 'red', '5', 'GOOD', 'GOOD', '45698971', '423659', '4892LKO', 'GOOD', 1, 0, 17, '2016-02-16 12:43:07', '2016-03-03 07:44:21', NULL),
('e789f380-dd34-11e5-acca-31f13c8bd9e6', 'e2092da0-d4a7-11e5-8567-c10f37027f74', '5a77e7a0-cac5-11e5-afe5-dfdf2b9d1f43', 'mella', '2016-02-08', 'female', '2016022711', 'hbo', NULL, 'image_e789f380-dd34-11e5-acca-31f13c8bd9e6.jpg', 'white', '5', 'GOOD', 'GOOD', '45698971', '4236594', '4892LKO', 'GOOD', 1, 0, 1, '2016-02-27 09:31:39', '2016-02-28 22:14:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `dog_relationships`
--

CREATE TABLE IF NOT EXISTS `dog_relationships` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `dog_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `father` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mother` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dog_relationships`
--

INSERT INTO `dog_relationships` (`id`, `dog_id`, `father`, `mother`, `created_at`, `updated_at`) VALUES
('2f238020-d09e-11e5-b59b-014adb246ff0', '2f141150-d09e-11e5-9604-49487cfc25ba', '', '201602114', '2016-02-11 09:02:31', '2016-02-11 09:02:31'),
('42168db0-d09d-11e5-afd8-afd140c70f57', '42085f50-d09d-11e5-9559-45dbeeb807fc', '201602114', NULL, '2016-02-11 08:55:54', '2016-03-03 12:53:11'),
('45ae84c0-d09f-11e5-8e91-bb941029093c', '459769a0-d09f-11e5-bd1c-67c7342e3663', '201602115', '201602114', '2016-02-11 09:10:19', '2016-02-11 09:10:19'),
('554825c0-d09c-11e5-b455-5dd64bd9534f', '552c4aa0-d09c-11e5-ba05-611171763c55', '201602113', '201602112', '2016-02-11 08:49:16', '2016-02-11 08:49:16'),
('81eedb60-e09d-11e5-8eaf-d58b2a2f1ac1', '81e23800-e09d-11e5-a66a-45aa533cba75', '', '', '2016-03-02 17:37:59', '2016-03-02 17:37:59'),
('8a38c6a0-d09f-11e5-9f82-85448457cdeb', '8a243720-d09f-11e5-9b62-ddfddbfd26da', '201602112', '201602112', '2016-02-11 09:12:14', '2016-03-03 13:05:31'),
('90d1e710-d09d-11e5-a71b-21d9653d3884', '90c36230-d09d-11e5-9cf9-1bbb0d83bfce', '201602117', '201602116', '2016-02-11 08:58:06', '2016-02-11 08:58:06'),
('a087e020-d09c-11e5-8e80-8b947781fd7c', 'a0721f50-d09c-11e5-93d0-a9cc4f72663c', '', '', '2016-02-11 08:51:23', '2016-02-11 08:51:23'),
('a5510be0-d4aa-11e5-a748-fbd15483f093', 'a53ba350-d4aa-11e5-849b-15dce550f014', '', '', '2016-02-16 12:41:48', '2016-02-16 12:41:48'),
('be904d20-d09d-11e5-8e85-6392a06c2527', 'be823e80-d09d-11e5-aac2-f3c862031baf', '201602117', '201602116', '2016-02-11 08:59:23', '2016-02-11 08:59:23'),
('cc91c030-e08d-11e5-a7e6-179c9db030d8', 'cc65f8e0-e08d-11e5-bb13-bbba69507a52', '201602111', '201602114', '2016-03-02 15:45:33', '2016-03-02 15:45:33'),
('d43a1f10-d4aa-11e5-b6ca-cb2e0b3d215e', 'd4295b80-d4aa-11e5-99a5-e991a3420610', '', '', '2016-02-16 12:43:07', '2016-02-16 12:43:07'),
('e79bfe20-dd34-11e5-a9cd-0358a03da278', 'e789f380-dd34-11e5-acca-31f13c8bd9e6', '', '', '2016-02-27 09:31:39', '2016-02-27 09:31:39');

-- --------------------------------------------------------

--
-- Table structure for table `dog_transfers`
--

CREATE TABLE IF NOT EXISTS `dog_transfers` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `dog_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `owner_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `transfer_serial_number` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dog_transfers`
--

INSERT INTO `dog_transfers` (`id`, `dog_id`, `owner_id`, `transfer_serial_number`, `created_at`, `updated_at`) VALUES
('d8afd820-e156-11e5-aee0-836e2fd90a96', '90c36230-d09d-11e5-9cf9-1bbb0d83bfce', 'd8afecc0-e156-11e5-b134-bb380642230c', NULL, '2016-03-03 15:44:42', '2016-03-03 15:44:42');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_100000_create_password_resets_table', 1),
('2016_03_03_134348_create_owners_table', 2),
('2016_03_03_135457_create_dog_transfers_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `owners`
--

CREATE TABLE IF NOT EXISTS `owners` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sec_phone_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `owners`
--

INSERT INTO `owners` (`id`, `first_name`, `last_name`, `phone_number`, `sec_phone_number`, `email`, `notes`, `created_at`, `updated_at`) VALUES
('d8afecc0-e156-11e5-b134-bb380642230c', 'Frederick', 'Ankamah', '0240120250', NULL, '', '', '2016-03-03 15:44:42', '2016-03-03 15:44:42');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed` tinyint(1) NOT NULL,
  `administrator` tinyint(1) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `title`, `first_name`, `last_name`, `email`, `phone`, `password`, `confirmed`, `administrator`, `remember_token`, `created_at`, `updated_at`) VALUES
('43bbfc40-cac4-11e5-a4d2-f1a376c70f69', 'Mr', 'Frederick', 'Ankamah', 'frederickankamah988@gmail.com', '0240120250', '$2y$10$wFj0EQGIVazmZo0TT31A.eLTqgnSjlWvUS32.AuocQJDeQlBFg3Vy', 1, 1, '3WG3JTgqQDL99VkNkNhlFq8pqZpyRmy1JLk0oz44zN8UUViHzXi9xBO0NOW3', '2016-02-03 22:20:00', '2016-03-02 15:43:09'),
('4f745d50-d987-11e5-8386-2fba3c43ad79', 'kaysie', 'kaysie', 'Owusu', 'kaysie@gmail.com', '0240120250', '$2y$10$Gxdz4Ewnc02yol9SmSh4f.8JVnrgVBFWWyXtIllnTFZ4zXMBwY7Mq', 0, 1, NULL, '2016-02-22 17:11:28', '2016-03-03 08:49:07'),
('52e4a850-d571-11e5-b6e2-3ded6f9b631e', 'Mr', 'Humble', 'Kafui', 'humble@gmail.com', '0244972233', '$2y$10$k5DBI0aDyd5e931gqISPs.cZWcTHXZ9anM485OIHgy0S2jG1P7Cai', 0, 1, NULL, '2016-02-17 12:24:00', '2016-02-22 12:21:47'),
('58c19af0-d989-11e5-91a6-95e04abb99f4', 'Kyna', 'Kyna', 'june', 'jume3@gmail.com', '0240120250', '$2y$10$50Ecm2JD3Bv609kbUOBQYuopzCJzetvSQkWRePnfzUqYUxwcyrKqq', 0, 1, NULL, '2016-02-22 17:26:03', '2016-03-03 08:48:58'),
('64d3f780-d669-11e5-93ed-6b8a3db69652', 'Mr', 'Gyandoh', 'Kukuaa', 'frederickankamah@gmail.com', '0240120250', '$2y$10$nxpflOabnhdyFaI1T3xltOMmMW6wdkH/RhLU3lrZkMrYD2.cFEUAO', 1, 0, NULL, '2016-02-18 17:59:45', '2016-03-01 16:01:11'),
('7b124220-d966-11e5-a04a-fdfbcafdab70', 'Mr', 'Sarah ', 'Gyimah', 'gyimah@gmail.com', '0240120250', '$2y$10$XRDiGWGyK27wcZyZwC2cjO50zV1HDtpWYQSc48.mPock0GQOAzeDG', 0, 0, NULL, '2016-02-22 13:16:28', '2016-02-22 13:16:28'),
('8d3c7180-d987-11e5-9a9b-43b23996db54', 'Kyna', 'Kyna', 'june', 'jume@gmail.com', '0240120250', '$2y$10$HUIOCYNYX3RvSqDma9kjx.wPMsSQTEY1OqDf/NE6bIy9TGa/uNRdm', 0, 0, NULL, '2016-02-22 17:13:12', '2016-02-22 17:13:12'),
('bae3d650-d96a-11e5-a009-172419e2a996', 'Mr', 'Felicia', 'Okyere', 'felicia@email.com', '0240120250', '$2y$10$1BJw79aKu/q1R0ON4OguUOrNM2IyT6c2.qVhysapA/fLulWuo7J4K', 1, 0, NULL, '2016-02-22 13:46:53', '2016-03-01 15:57:00'),
('bccdaf30-d231-11e5-bcca-3de614abf762', 'Mr', 'Evans', 'Ankamah', 'user@gmail.com', '0240120250', '$2y$10$p5RWjTyS8v0yHoVMs5168./PtvV7SBnjOdYcjCoiExHzTMz7ISSXG', 1, 1, NULL, '2016-02-13 09:11:17', '2016-02-19 22:31:34'),
('c4262d20-d669-11e5-8504-3fa9e10f99d8', 'Mr', 'Abigail', 'Gyimah', 'abigail@gmail.com', '0240120250', '$2y$10$PZRiMmdOdpgADGTzeFHGnOsKCM10ryfx5tz52ASggV7Xhhb9O2ydq', 0, 0, NULL, '2016-02-18 18:02:25', '2016-02-22 12:21:13'),
('c48ed0f0-d965-11e5-a3b3-73538ebd594e', 'Mr', 'Desire', 'Ameleke', 'freddy100dollar@gmail.com', '0240120250', '$2y$10$OXUcEpr9PgI/X/b7dSu32OP.Qykjc3a9boWLujX9N9iZVnymy.b3.', 0, 0, NULL, '2016-02-22 13:11:22', '2016-02-22 13:11:22'),
('d8e85880-cb11-11e5-87b0-417b0cc41224', 'Mrs', 'Sarah ', 'Gyandoh', 'sarah@gmail.com', '0240120250', '$2y$10$xiKxcW44.eVBcN6HkLb/t.xk5OoZhVFAGRo8ixjqkn4XPtjc2rGQe', 1, 0, 'McHaAEZdrmxGoVMObVOkvl7bzLOFzsZBzCOYVScmbepmC0w3VRfjSPUrqeDg', '2016-02-04 07:35:22', '2016-03-03 08:16:55'),
('e2092da0-d4a7-11e5-8567-c10f37027f74', 'Mrs', 'Sandra', 'Ankamah', 'sandra@gmail.com', '0240120250', '$2y$10$.Lawqahob9OoJgcpu7XcsucILN4L.an0mbYtZZkUXckOJfd.K4wA2', 1, 0, 'PAUORD8iJYbertnMmsCtY5jNM27UrASC1SBThQqxjoxUn4LhZwTCAZ4rnrqK', '2016-02-16 12:22:02', '2016-03-03 08:13:06'),
('eafdc4b0-d986-11e5-8f5d-93df6e088fd9', 'Sandra', 'Sandra', 'Agyei', 'san@gmail.com', '0240120250', '$2y$10$mjDY2IeO3ZCkWnm1sEq.ZuswNg0pq4fvphxUlQ/egEBbWRacAPJn6', 0, 1, NULL, '2016-02-22 17:08:39', '2016-03-01 15:48:51'),
('ff3da0b0-d988-11e5-ae75-8fe9d4168fe9', 'Kyna', 'Kyna', 'june', 'jume2@gmail.com', '0240120250', '$2y$10$da9vI3NelIJO0A1XpxfEo..Bz/B.5jDcMFfNMXfPr71eEb.9wpy.K', 1, 0, NULL, '2016-02-22 17:23:32', '2016-03-03 08:48:32');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `breeders`
--
ALTER TABLE `breeders`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dogs`
--
ALTER TABLE `dogs`
 ADD PRIMARY KEY (`id`), ADD KEY `dogs_user_id_foreign` (`user_id`), ADD KEY `breeder_id` (`breeder_id`);

--
-- Indexes for table `dog_relationships`
--
ALTER TABLE `dog_relationships`
 ADD PRIMARY KEY (`id`), ADD KEY `dog_relationships_dog_id_foreign` (`dog_id`);

--
-- Indexes for table `dog_transfers`
--
ALTER TABLE `dog_transfers`
 ADD PRIMARY KEY (`id`), ADD KEY `dog_transfers_dog_id_foreign` (`dog_id`), ADD KEY `dog_transfers_owner_id_foreign` (`owner_id`);

--
-- Indexes for table `owners`
--
ALTER TABLE `owners`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
 ADD KEY `password_resets_email_index` (`email`), ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `dogs`
--
ALTER TABLE `dogs`
ADD CONSTRAINT `dogs_ibfk_1` FOREIGN KEY (`breeder_id`) REFERENCES `breeders` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `dogs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `dog_relationships`
--
ALTER TABLE `dog_relationships`
ADD CONSTRAINT `dog_relationships_dog_id_foreign` FOREIGN KEY (`dog_id`) REFERENCES `dogs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `dog_transfers`
--
ALTER TABLE `dog_transfers`
ADD CONSTRAINT `dog_transfers_dog_id_foreign` FOREIGN KEY (`dog_id`) REFERENCES `dogs` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `dog_transfers_owner_id_foreign` FOREIGN KEY (`owner_id`) REFERENCES `owners` (`id`) ON DELETE CASCADE;
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
