module.exports = {
    module: {
        loaders: [
            // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
            //{ test: /\.vue$/, loader: 'vue-loader' },
            { test: /\.tsx?$/, loader: 'vue-ts-loader' },
            { test: /\.js$/, loader: 'buble' },
            // { test: /\.js$/, loader: 'vue-loader' },
            {
                test: /\.json$/,
                loader: 'json'
            }
        ]
    },

    vue: {
        loaders: {
            ts: 'vue-ts-loader',
            js: 'vue-loader'
            // js: 'buble'
        },
        // important for cooperating with vue-loader
        esModule: true
    },
    resolve: {
        extensions: ['*', '.js', '.jsx', '.vue', '.ts', '.tsx'],
    }
}